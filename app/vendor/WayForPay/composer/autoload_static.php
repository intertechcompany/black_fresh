<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitcb3aa117f0f42be860634e7d883ca5bf
{
    public static $prefixLengthsPsr4 = array (
        'a' => 
        array (
            'anlutro\\cURL\\' => 13,
        ),
        'W' => 
        array (
            'WayForPay\\SDK\\' => 14,
        ),
        'E' => 
        array (
            'Easy\\Collections\\' => 17,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'anlutro\\cURL\\' => 
        array (
            0 => __DIR__ . '/..' . '/anlutro/curl/src',
        ),
        'WayForPay\\SDK\\' => 
        array (
            0 => __DIR__ . '/..' . '/wayforpay/php-sdk/src',
        ),
        'Easy\\Collections\\' => 
        array (
            0 => __DIR__ . '/..' . '/easyframework/collections/src',
        ),
    );

    public static $fallbackDirsPsr4 = array (
        0 => __DIR__ . '/..' . '/easyframework/generics/src',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitcb3aa117f0f42be860634e7d883ca5bf::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitcb3aa117f0f42be860634e7d883ca5bf::$prefixDirsPsr4;
            $loader->fallbackDirsPsr4 = ComposerStaticInitcb3aa117f0f42be860634e7d883ca5bf::$fallbackDirsPsr4;

        }, null, ClassLoader::class);
    }
}
