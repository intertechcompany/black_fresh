<?php
// uncomment the following to define a path alias
Yii::setPathOfAlias('app', dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR);


// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

$protectedPath = realpath(Yii::getPathOfAlias('system') . DS . '..' . DS . 'app');
$runtimePath   = realpath(Yii::getPathOfAlias('system') . DS . '..' . DS . 'runtime');
$sessionPath   = realpath(Yii::getPathOfAlias('system') . DS . '..' . DS . 'tmp');
$assetsPath    = realpath(Yii::getPathOfAlias('system') . DS . '..' . DS . 'www' . DS . 'assets');

return array(
	'basePath'=>$protectedPath,
	'id'=>'black',
	'name'=>'Black',
	'charset'=>'utf-8',
	'defaultController'=>'site',
	
	'language' => 'uk',
	'sourceLanguage'=>'en',
	'timeZone'=>'Europe/Kiev',
	
	'runtimePath' => $runtimePath,

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.models.Forms.*',
		'application.components.*',
//		'application.components.googlemerchantcenter.*',
	),
	
	// application components
	'components'=>array(
		'assetManager'=>array(
			'basePath' => $assetsPath,
			'baseUrl' => 'https://black.loc/assets',
		),
		'format'=>array(
			'class'=>'CLocalizedFormatter',
		),
		'user'=>array(
			'class'=>'application.components.WebUser',
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'autoRenewCookie'=>true,
			'identityCookie'=>array(
				'path' => '/',
				'domain' => '.black.loc',
				'secure' => true,
				'httpOnly' => true,
			),
			'loginUrl'=>array('site/login'),
		),
		'urlManager'=>array(
			'class'=>'application.components.UrlManager',
			'actAddressedDoubleUrl'=>'404',
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'caseSensitive'=>true,
			'useStrictParsing'=>true,
			'routeVar'=>'_r',
			'rules'=>array(
                ''=>'site/index',
                'ajax/<_a:\w+>'=>'ajax/<_a>',
                'api/<call:(updateNpData)>'=>'api/call',
                'auth/<_a:(facebook|google|login|loginFb|fbLink|googleLink|confirmGoogle|confirmFb)>'=>'auth/<_a>',

                '<_a:(logout|account|accountedit|cart|checkout|school|news|login|registration|accountaddress|accountAddressCreate|accountCardCreate|accountSubscription|reset)>'=>'site/<_a>',

                'fondy'=>'site/fondy',
                'festivals'=>'site/festivals',
                'parents'=>'site/parents',
                'accountAddressRemove/<alias:[a-z0-9\-]+>'=>'site/accountAddressRemove',
                'accountAddressUpdate/<alias:[a-z0-9\-]+>'=>'site/accountAddressUpdate',
                'accountCardRemove/<alias:[a-z0-9\-]+>'=>'site/accountCardRemove',
                'accountCardMain/<alias:[a-z0-9\-]+>'=>'site/accountCardMain',
                'catalog/<alias:[a-z0-9\-]+>'=>'site/category',
                'product/<alias:[a-z0-9\-]+>'=>'site/product',
                'school/<alias:[a-z0-9\-]+>'=>'site/course',
                'quiz'=>'site/quiz',
                'quiz/user/answer'=>'site/userQuizAnswer',
                'quiz/edit'=>'site/quizEditPage',
                'language/current'=>'site/language',
                'questions'=>'site/questions',
                'answers'=>'site/answers',
                'feedback'=>'site/feedback',
                'festivals-feedback'=>'site/festivalsFeedback',
                'user-promo'=>'site/accountPromo',
                'product-properties'=>'site/productProperties',
                'user-feedback'=>'site/userFeedbackPage',
                'user/feedback'=>'site/userFeedback',
                'answers/new'=>'site/SaveAnswers',
                'news/<alias:[a-z0-9\-]+>'=>'site/newscategory',
                'news/<category_alias:[a-z0-9\-]+>/<alias:[a-z0-9\-]+>'=>'site/newsarticle',
                'knowledge-base'=>'site/base',
                'knowledge-base/<alias:[a-z0-9\-]+>'=>'site/basecategory',
                'knowledge-base/<category_alias:[a-z0-9\-]+>/<alias:[a-z0-9\-]+>'=>'site/basearticle',
                'thank-you'=>'site/thankyou',
                'payment/api'=>'payment/api',
                'payment/result'=>'payment/result',
		        'payment/sub/callback'=>'payment/callbackSub',
                'sub'=>'site/sub',
                'sub-cart'=>'site/subCart',
                'next-payment' => 'site/nextPayment',
                'sub/mark' => 'site/saveUserMark',
                'sub/questions' => 'site/subQuestions',
                'sub/answers' => 'site/subAnswers',
                'thank-you-sub'=>'site/thankYouSub',
                'confirm-sub'=>'site/confirmSub',
                'payment/result/sub'=>'payment/resultSub',
                'subscription/pause' => 'site/pauseSub',
                'subscription/remove' => 'site/removeSub',
                'subscription/change' => 'site/changeSub',
                'subscription/change/qty' => 'site/changeSubQty',
                'subscription' => 'site/accountSubscription',
                'sitemap'=>'site/sitemap',
                'sitemap.xml'=>'site/sitemapXml',
                'google_feed_ua.xml'=>'site/googleFeed',
                'google_feed_ru.xml'=>'site/googleFeed',
                'google_feed_en.xml'=>'site/googleFeed',
                'fb_feed_ua.xml'=>'site/fbFeed',
                'fb_feed_ru.xml'=>'site/fbFeed',
                'fb_feed_en.xml'=>'site/fbFeed',
                '<alias:[a-z0-9\-]+>'=>'site/page',
            ),
        ),
		'db'=>array(
            'connectionString'=>'mysql:host=mysql_dev;dbname=fresh;port=3307',
            'emulatePrepare'=>true,
            'username'=>'fresh',
            'password'=>'fresh',
			'charset'=>'utf8',
			'autoConnect'=>false,
		),
		'session'=>array(
			//'class'=>'CCacheHttpSession',
			'class'=>'CHttpSession',
			'autoStart'=>true,
			'cookieMode'=>'only',
			'sessionName'=>'sessID',
			'savePath'=>$sessionPath,
			'gCProbability'=>1,
			'timeout'=>7200,
			'cookieParams' => array(
				'path' => '/',
				'domain' => '.black.loc',
				'secure' => true,
				'httpOnly' => true,
			),
		),
		'widgetFactory'=>array(
			'widgets'=>array(
				'LinkPager'=>array(
					'cssFile'                    => false,
					'header'                     => '',
					'firstPageCssClass'          => 'page-first',
					'lastPageCssClass'           => 'page-last',
					'previousPageCssClass'       => 'cp-arrow',
					'nextPageCssClass'           => 'cp-arrow',
					'internalPageCssClass'       => false,
					'internalHiddenPageCssClass' => 'page-num',
					'prevPageLabel'              => '<',
					'nextPageLabel'              => '>',
					// 'firstPageLabel'          => '<<',
					// 'lastPageLabel'           => '>>',
				),
			),
		),
		'errorHandler'=>array(
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning, info',
					'except'=>'exception.CHttpException.*',
				),
				/*
				// uncomment the following to show log messages on web pages
				array(
					'class'=>'CWebLogRoute',
					//'categories' => 'application',
					'levels'=>'error, warning, trace, profile, info',
				),
				array(
					// направляем результаты профайлинга в ProfileLogRoute (отображается внизу страницы)
					'class'=>'CProfileLogRoute',
					'levels'=>'profile',
				),
				*/
			),
		),
		'cache'=>array(
			'class'=>'CDummyCache',
		),
	),
	
	'onBeginRequest' => array('BeginRequest', 'onStartSite'),
	//'onEndRequest' => array('BeginRequest', 'onStopSite'),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		'dev' => false,
		'secret' => 'bg7,{Fk',
		'currencies' => [
			'usd' => [
				'code' => 'usd',
				'name' => 'USD',
				'suffix' => '',
				'prefix' => '$',
			],
			'eur' => [
				'code' => 'eur',
				'name' => 'EUR',
				'suffix' => '€',
				'prefix' => '',
			],
			'uah' => [
				'code' => 'uah',
				'name' => 'Грн.',
				'suffix' => ' грн.',
				'prefix' => '',
			],
		],
		'site' => 'http://black.loc/',
		'langs' => array(
            'uk' => [
				'name' => 'Укр',
				'url' => '',
			],
//            'ru' => [
//				'name' => 'Рус',
//				'url' => 'ru',
//			],
            'en' => [
				'name' => 'Eng',
				'url' => 'en',
			],
        ),
		'currency' => 'uah',
		'translations' => array(),
		'has_sale' => false,
		'mailer' => array(
			'from_email' => 'support@fresh.black',
			'from_name' => 'Black',
			'sendgrid' => array(
				'api_key' => '*****',
			),
			'mailgun' => array(
				'domain' => 'mailer.fresh.black',
				'api_key' => 'key-***',
			),
			'esputnik' => array(
				'login' => 'dashkevich.m@nuare.coffee',
				'password' => 'ESputnik1999Black',
			),
		),
		'wfp' => array(
			'account' => 'new_fresh_black',
			'secret' => 'e8574c1a9029be4106c0862da2b3494dcca60c1b',
			'password' => '5e9aaba2a7fadfbd27264f86990ddc48',
		),
		'np' => array(
			'api_key' => 'f892ab0bd4f124685cd2a7a535fcad5f',
		),
        'fondy' => array(
            'merchantId' => '1523302',
            'secretKey' => 'I7nPfW7U8Okdu3XvG5AV9LQYIKopVb9t',
        ),
        'brands' => []
	),
);
