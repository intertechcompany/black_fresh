CREATE TABLE `delivery_frequency`(
	`id` BigInt( 255 ) AUTO_INCREMENT NOT NULL,
	`frequency` Int( 255 ) NOT NULL,
	`title_en` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`title_uk` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`title_ru` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	CONSTRAINT `unique_id` UNIQUE( `id` )
)