ALTER TABLE page ADD feedback_form BOOL DEFAULT FALSE AFTER page_content;
ALTER TABLE page ADD feedback_file varchar(100) NULL AFTER feedback_form;

CREATE TABLE feedback (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    page_id int NULL,
    first_name VARCHAR(30) NULL,
    last_name VARCHAR(30) NULL,
    email VARCHAR(30) NULL,
    phone VARCHAR(30) NULL,
    comment VARCHAR(30) NULL,
    is_send BOOL DEFAULT FALSE,
    created datetime DEFAULT now()
)