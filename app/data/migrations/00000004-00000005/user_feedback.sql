create table user_feedback (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    user_id int,
    reason_id int,
    message VARCHAR(255) NULL,
    created datetime DEFAULT now()
);

create table user_feedback_reason (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    reason_uk VARCHAR(100),
    reason_ru VARCHAR(100),
    reason_en VARCHAR(100)
);

create table user_feedback_file (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    feedback_id int,
    file VARCHAR (255)
);