create TABLE sub_remove_reason (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    user_id int NULL,
    reason TEXT NULL,
    created datetime DEFAULT now()
);