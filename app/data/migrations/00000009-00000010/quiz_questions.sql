alter TABLE quiz_questions add title_under_en VARCHAR(255) NULL AFTER type;
alter TABLE quiz_questions add title_under_ru VARCHAR(255) NULL AFTER title_under_en;
alter TABLE quiz_questions add title_under_uk VARCHAR(255) NULL AFTER title_under_ru;

alter TABLE quiz_questions add text_under_en VARCHAR(255) NULL AFTER title_under_uk;
alter TABLE quiz_questions add text_under_ru VARCHAR(255) NULL AFTER text_under_en;
alter TABLE quiz_questions add text_under_uk VARCHAR(255) NULL AFTER text_under_ru;