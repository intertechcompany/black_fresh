CREATE table sub_next_payments (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    order_reference VARCHAR (255),
    next_date DATE,
    first_pay BOOLEAN DEFAULT FALSE,
    status varchar (50) DEFAULT NULL,
    delivery_status varchar (50) DEFAULT 'new',
    payment text DEFAULT NULL,
    created datetime DEFAULT now(),
    updated datetime DEFAULT now()
);

create table sub_product (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    sub_id int,
    product_id int,
    variant_id int,
    option_id int,
    quantity int,
    price DOUBLE,
    variant_title VARCHAR(50),
    option_title VARCHAR(50),
    title VARCHAR(50)
);

create table sub_order (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    order_reference VARCHAR (255),
    user_id int NULL,
    frequency_id int,
    price DOUBLE,
    status VARCHAR (50),
    payment_status VARCHAR (50) DEFAULT NULL,
    delivery int DEFAULT null,
    delivery_address VARCHAR (255) DEFAULT null,
    np_city VARCHAR (50) DEFAULT null,
    np_department VARCHAR (255) DEFAULT null,
    payment text DEFAULT NULL,
    created DATETIME DEFAULT now(),
    updated datetime DEFAULT now()
);