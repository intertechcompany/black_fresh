CREATE TABLE quiz_history (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    user_id INT NOT NULL,
    product_id INT NOT NULL,
    quest_1 INT NOT NULL,
    quest_2 INT NOT NULL,
    quest_3 INT NOT NULL,
    quest_4 INT NOT NULL,
    quest_5 INT NOT NULL,
    quest_6 INT NOT NULL
);

CREATE TABLE quiz_questions (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    title_en VARCHAR(100) NULL,
    title_ru VARCHAR(100) NULL,
    title_uk VARCHAR(100) NULL,
    type VARCHAR(10) NULL
);

CREATE TABLE quiz_answers (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    question_id INT NOT NULL,
    title_en VARCHAR(100) NULL,
    title_ru VARCHAR(100) NULL,
    title_uk VARCHAR(100) NULL,
    icon varchar(100) NULL
);

CREATE TABLE quiz_product (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    product_id INT NOT NULL,
    answ_1 VARCHAR(40) NOT NULL,
    answ_2 VARCHAR(40) NOT NULL,
    answ_3 VARCHAR(40) NOT NULL,
    answ_4 VARCHAR(40) NOT NULL,
    answ_5 VARCHAR(40) NOT NULL,
    answ_6 VARCHAR(40) NOT NULL
);