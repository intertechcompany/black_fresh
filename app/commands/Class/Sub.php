<?php


class Sub
{
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function getNewSub()
    {
        $sql = "SELECT * FROM sub_order WHERE status = 'new' AND payment_status = 'Approved'";

        return $this->db->query($sql);
    }

    public function getNextRegularPaymentSub()
    {
        $nextDay = date('Y-m-d', strtotime("+2 day"));

        $sql = "SELECT so.*, snp.* FROM sub_order as so left join sub_next_payments as snp on so.order_reference = snp.order_reference  WHERE snp.next_date = '$nextDay' and snp.first_pay != 1 AND snp.status != 'ok'";

        return $this->db->query($sql);
    }

    public function getFrequency($id)
    {
        $sql = "select * from delivery_frequency where id = $id";

        return $this->db->query($sql);
    }

    public function getPaymentToday()
    {
        $currentDate = date("Y-m-d");

        $sql = "select * from sub_next_payments where next_date = '$currentDate' and first_pay != 1";

        return $this->db->query($sql);
    }

    public function getUser($userId)
    {
        $sql = "select u.user_email from user as u where user_id = $userId";

        return $this->db->query($sql);
    }

    public function checkPaymentStatus($subOrder)
    {
        foreach ($subOrder as $sub) {
            $checkData = [
                "requestType" => "STATUS",
                "merchantAccount" => "new_fresh_black",
                "merchantPassword" => "5e9aaba2a7fadfbd27264f86990ddc48",
                "orderReference" => $sub['order_reference'],
            ];

            $result = $this->sendRequest($checkData);

            $this->updatePaymentNext($sub, $result->status, $result, true);
        }
    }

    public function setRegularPayments($subOrder)
    {
        foreach ($subOrder as $sub) {

            $user = $this->getUser($sub['user_id']);

            $updatedSub = $this->checkNextProduct($sub['order_reference']);

            if ($updatedSub) {
                $sub['price'] = $updatedSub['price'];
            }

            $timeStamp = strtotime($sub['next_date']);
            $date = date('Y-m-d', $timeStamp - ('1 day'));

            $paymentData = [
                "requestType" => "CREATE",
                "merchantAccount" => "new_fresh_black",
                "merchantPassword" => "5e9aaba2a7fadfbd27264f86990ddc48",
                "regularMode" => "once",
                "amount" => $sub['price'],
                "currency" => "UAH",
                "dateBegin" => $date,
                "dateEnd" => $date,
                "orderReference" => $sub['order_reference'],
                "email" => $user[0]['user_email']
            ];

            $result = $this->sendRequest($paymentData);

            if ($result->reason == 'Ok' && $result->reasonCode == 4100) {
                $this->updatePaymentNext($sub, 'ok', $result);
            } else {
                $this->updatePaymentNext($sub, 'error', $result, true);
            }
        }
    }

    public function checkNextProduct($orderRef)
    {
        $sql = "select * from sub_order where order_reference = '$orderRef'";
        $sub = $this->db->query($sql);
        $sub = $sub[0];

        $subId = $sub['id'];
        $sql = "select * from sub_product where sub_id = $subId";
        $subProduct = $this->db->query($sql);
        $subProduct = $subProduct[0];


        $subMainProduct = $sub['main_product'];
        $sql = "select * from product where product_id = $subMainProduct";
        $product = $this->db->query($sql);
        $product = $product[0];

        if ($product['is_replacement']) {
            $productId = $product['product_id'];
            $sql = "select * from product_replacements where product_id = $productId";
            $productReplacements = $this->db->query($sql);

            $sql = "select * from sub_replacements_history where sub_id = $subId";
            $subReplacementHistory = $this->db->query($sql);

            if ($productReplacements) {
                if ($subReplacementHistory) {
                    $newProducts = [];

                    foreach ($productReplacements as $replacement) {
                        foreach ($subReplacementHistory as $item) {
                            if ($replacement['product_rep_id'] != $item['product_rep_id']) {
                                $newProducts[] = $replacement['product_rep_id'];
                            }
                        }
                    }

                    if (!$newProducts) {
                        foreach ($productReplacements as $replacement) {
                            $newProducts[] = $replacement['product_rep_id'];
                        }
                    }

                    $productNew = [];

                    foreach ($newProducts as $newProduct) {
                        $sql = "select * from product where product_id = $newProduct";
                        $productNew = $this->db->query($sql);
                        $productNew = $productNew[0];

                        if ($productNew['product_instock'] == 'in_stock') {
                            break;
                        }
                    }

                    return $this->updateSub($sub, $productNew, $subProduct);
                } else {
                    $newProducts = [];

                    foreach ($productReplacements as $replacement) {
                        $newProducts[] = $replacement['product_rep_id'];
                    }

                    $productNew = [];

                    foreach ($newProducts as $newProduct) {
                        $sql = "select * from product where product_id = $newProduct";
                        $productNew = $this->db->query($sql);
                        $productNew = $productNew[0];

                        if ($productNew['product_instock'] == 'in_stock') {
                            break;
                        }
                    }

                    return $this->updateSub($sub, $productNew, $subProduct);
                }
            }
            return;
        } else {
            return;
        }
    }

    public function updateSub($sub, $product, $subProduct)
    {
        $subId = $sub['id'];

        $price = $product['product_price'];
        $productId = $product['product_id'];
        $currentDate = date("Y-m-d H:i:s");

        $sql = "select product_title from product_lang where product_id = $productId and language_code = 'uk'";
        $productLang = $this->db->query($sql);
        $productLang = $productLang[0];

        $productTitle = $productLang['product_title'];

        $price = $price * $subProduct['quantity'];

        $sql = "update sub_order set price = $price, updated = '$currentDate' where id = $subId";
        $this->db->query($sql);
        $sub['price'] = $price;

        $sql = "update sub_product set product_id = $productId, title = '$productTitle', price = $price where sub_id = $subId";
        $this->db->query($sql);

        $sql = "insert into sub_replacements_history (sub_id, product_rep_id) values ($subId, $productId)";
        $this->db->query($sql);

        return $sub;
    }

    public function nextPaymentDate($newSubs)
    {
        foreach ($newSubs as $sub) {
            $currentDate = date("Y-m-d H:i:s");

            $frequency = $this->getFrequency($sub['frequency_id']);

            $nextDay = date('Y-m-d', strtotime("+". ((7 * $frequency[0]['frequency']) + 1) . "day"));
            $tomorrow = date('Y-m-d', strtotime("+1 day"));

            $orderRef = $sub['order_reference'];

            $sql = "insert into sub_next_payments (order_reference, next_date, status, first_pay) values ('$orderRef', '$tomorrow', 'new', 1)";
            $this->db->query($sql);

            $sql = "insert into sub_next_payments (order_reference, next_date, status) values ('$orderRef', '$nextDay', 'new')";
            $this->db->query($sql);

            $sql = "update sub_order set status = 'checked', `updated` = '$currentDate' where id =" . $sub['id'];
            $this->db->query($sql);
        }
    }

    public function updatePaymentNext($sub, $status, $result, $update = false)
    {
        $json = json_encode($result);
        $sql = "update sub_next_payments set status = '$status', payment = '$json', updated = now() where id =" . $sub['id'];
        $this->db->query($sql);

        if (!$update) {
            $this->createNextPaymentDay($sub);
        }
    }

    public function createNextPaymentDay($sub)
    {
        $frequency = $this->getFrequency($sub['frequency_id']);

        $nextDay = date('Y-m-d', strtotime("+". ((7 * $frequency[0]['frequency']) + 1) . "day"));

        $orderRef = $sub['order_reference'];

        $sql = "insert into sub_next_payments (order_reference, next_date, status) values ('$orderRef', '$nextDay', 'new')";
        $this->db->query($sql);
    }

    public function sendRequest($data)
    {
        $url = 'https://api.wayforpay.com/regularApi';

        $options = [
            'http' => [
                'method'  => 'POST',
                'content' => json_encode($data),
                'header'=>  "Content-Type: application/json\r\n" . "Accept: application/json\r\n"
            ]
        ];

        $context  = stream_context_create( $options );
        $result = file_get_contents( $url, false, $context );

        return json_decode( $result );
    }
}