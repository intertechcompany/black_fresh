<?php

include ('Class/Db.php');
include ('Class/Sub.php');

$db = new DB();
$subClass = new Sub($db);

$newSubs = $subClass->getNewSub();
$subClass->nextPaymentDate($newSubs);

$nextRegularSub = $subClass->getNextRegularPaymentSub();
$subClass->setRegularPayments($nextRegularSub);

$paymentToday = $subClass->getPaymentToday();
$subClass->checkPaymentStatus($paymentToday);
