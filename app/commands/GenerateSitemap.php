<?php
require dirname(__FILE__, 3) . '/vendor/autoload.php';

use Commands\Sitemap\Sitemap;

$dotenv = Dotenv\Dotenv::createImmutable(dirname(__FILE__, 3));
$dotenv->load();

$sitemap = new Sitemap();
$sitemap->generate();