<?php

namespace Commands\Sitemap;

use Exception;
use mysqli;

class SitemapDbConnector
{
    protected $connection;

    /**
     * @param string $host
     * @param string $user
     * @param string $password
     * @param string $dbName
     * @param string $port
     * @throws Exception
     */
    public function __construct(
        string $host = '127.0.01',
        string $user = 'root',
        string $password = 'root',
        string $dbName = 'black',
        string $port = '3306'
    )
    {
        $this->connection = new mysqli($host, $user, $password, $dbName, $port);
        $this->query("SET NAMES UTF8");

        $this->checkConnection();
    }

    /**
     * @param string $sql
     * @return array|bool
     * @throws Exception
     */
    public function query(string $sql)
    {
        $data = [];
        $this->checkConnection();
        $result = $this->connection->query($sql);

        if (mysqli_error($this->connection)) {
            throw new Exception(mysqli_error($this->connection));
        }

        if (is_bool($result)) {
            return $result;
        }

        while ($row = mysqli_fetch_assoc($result)) {
            $data[] = $row;
        }

        mysqli_free_result($result);

        return $data;
    }

    /**
     * @throws Exception
     */
    private function checkConnection()
    {
        if (!$this->connection) {
            throw new Exception('Could not connect to DB ');
        }
    }
}