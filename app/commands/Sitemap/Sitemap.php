<?php

namespace Commands\Sitemap;

use DOMDocument;

class Sitemap
{
    private $dir;

    private $dom;

    private $dbConnector;

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        $this->dbConnector = new SitemapDbConnector(
            $_ENV['SITEMAP_MYSQL_HOST'],
            $_ENV['SITEMAP_MYSQL_USER'],
            $_ENV['SITEMAP_MYSQL_PASSWORD'],
            $_ENV['SITEMAP_MYSQL_DB'],
            $_ENV['SITEMAP_MYSQL_PORT']
        );
        $this->dir = (dirname(__FILE__, 4)) . '/www';
        $this->dom = new DOMDocument('1.0', 'utf-8');
    }

    /**
     * @throws \Exception
     */
    public function generate()
    {
        $products = $this->getProducts();

        if (!empty($products)) {
            $this->prepareProducts($products);
        }

        $this->dom->save($this->dir . '/sitemap.xml');
    }

    /**
     * @throws \Exception
     */
    private function getProducts()
    {
        return $this->dbConnector
            ->query(
                "SELECT * FROM product AS p
                        JOIN product_lang as pl 
                        ON p.product_id = pl.product_id AND pl.language_code = 'uk' AND pl.product_visible = 1 
                    "
            );
    }

    private function prepareProducts(array $products)
    {
        $urlSet = $this->dom->createElement('urlset');
        $urlSet->setAttribute('xmlns','http://www.sitemaps.org/schemas/sitemap/0.9');

        foreach ($products as $product) {
            if ((bool)$product['active'] === false) {
                continue;
            }

            $date = max([
                $this->prepareDate($product['created']),
                $this->prepareDate($product['saved'])
            ]);

            $url = $this->dom->createElement('url');

            $loc = $this->dom->createElement('loc');
            $text = $this->dom->createTextNode(
                htmlentities($this->prepareProductLink($product), ENT_QUOTES)
            );
            $loc->appendChild($text);
            $url->appendChild($loc);

            $lastMod = $this->dom->createElement('lastmod');
            $text = $this->dom->createTextNode(date('Y-m-d', $date));
            $lastMod->appendChild($text);
            $url->appendChild($lastMod);

            $priority = $this->dom->createElement('priority');
            $text = $this->dom->createTextNode((($date + 604800) > time()) ? '1' : '0.5');
            $priority->appendChild($text);
            $url->appendChild($priority);

            $urlSet->appendChild($url);
        }

        $this->dom->appendChild($urlSet);
    }

    /**
     * @param $product
     * @return string
     */
    private function prepareProductLink($product): string
    {
        return $_ENV['SITEMAP_PRODUCT_URL'] . '/' . $product['product_alias'];
    }

    /**
     * @param $date
     * @return false|int
     */
    private function prepareDate($date)
    {
        if ($date === '' || $date === null || $date === '0000-00-00 00:00:00') {
            return strtotime('now');
        }

        return strtotime($date);
    }
}