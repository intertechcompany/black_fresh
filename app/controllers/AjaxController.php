<?php

class AjaxController extends Controller
{
	public $json = array();

	protected function beforeAction($action)
	{
		// отключаем web-логи для ajax запросов
		foreach (Yii::app()->log->routes as $route) {
			if ($route instanceof CWebLogRoute || $route instanceof CProfileLogRoute) {
				$route->enabled = false;
			}
		}

		return parent::beforeAction($action);
	}

	public function init() {
		parent::init();
		Yii::app()->errorHandler->errorAction = 'ajax/error';
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// block direct access to ajax.php
		throw new CHttpException(403, 'Access denied.');
	}

	public function actionDiscount()
	{
		$discount = Yii::app()->request->getPost('discount');
		$type = Yii::app()->request->getPost('type');
		$products = Yii::app()->request->getPost('products');

		if ($products !== '') {
		    $products = json_decode($products, true);
        }

		if (!empty($discount)) {
            $cartPrice = Cart::model()->getPrice();
            $actionPrice = Cart::model()->getActionPrice();
            if ((float)$actionPrice !== $cartPrice) {
                return $this->json = [
                    'error' => true,
                    'errorCode' => '', // $error_message,
                    'errorFields' => [
                        'code' =>[Lang::t('checkout.tip.discount.error.promotion')]
                    ]
                ];
            }

			$model = new DiscountForm();
			$model->attributes = $discount;

			if ($model->validate()) {
			    $error = false;
			    $errorMessage = [];

			    if ($type == 'buy' && $model->discount['is_buy'] != 1) {
			        $error = true;

                    $errorMessage['fields'] = [
                        'code' => [Lang::t('checkout.tip.discount.error.service')]
                    ];
                } elseif ($type == 'sub' && $model->discount['is_sub'] != 1) {
                    $error = true;

                    $errorMessage['fields'] = [
                        'code' => [Lang::t('checkout.tip.discount.error.service')]
                    ];
                } elseif ($type == 'school' && $model->discount['is_school'] != 1) {
                    $error = true;

                    $errorMessage['fields'] = [
                        'code' => [Lang::t('checkout.tip.discount.error.service')]
                    ];
                }

			    if ($error) {
                    return $this->json = [
                        'error' => true,
                        'errorCode' => '', // $error_message,
                        'errorFields' => $errorMessage['fields'],
                    ];
                }

			    $checkValid = [];
                $data = [];
                $today = date('Y-m-d');

                $errorMessage['fields']['code'] = [];

			    if ($model->discount['is_auto']) {
			        $checkValid['auto'] = Discount::model()->checkAuto($model->discount);
                }

                if ($model->discount['is_auto_season']) {
                    $checkValid['season'] = Discount::model()->checkSeason($model->discount);
                }

                if ($model->discount['is_personal']) {
                    $checkValid['personal'] = Discount::model()->checkPersonal($model->discount);
                }

                if (!empty($checkValid) && ((isset($checkValid['auto']) && $checkValid['auto']) || (isset($checkValid['season']) && $checkValid['season']) || (isset($checkValid['personal']) && $checkValid['personal']))) {
                    if ($type == 'buy') {
                        $data = Discount::model()->setBuyDiscount($model->discount, $products);
                    } elseif ($type == 'sub') {
                        $data = Discount::model()->setBuyDiscount($model->discount, $products, 'sub');
                    }

                    if ($data['newPrice'] == 0) {
                        if ($type == 'buy') {
                            $data['newPrice'] = Cart::model()->getPrice();
                        } elseif ($type == 'sub') {
                            $data['newPrice'] = Cart::model()->getSubPrice();
                        }
                    }

                    Discount::model()->apply($model->discount);

                    $this->json = [
                        'message' => Lang::t('checkout.tip.discount'),
                        'code' => $model->discount['discount_code'],
                        'value' => $data['value'],
                        'delete_btn' => Lang::t('checkout.tip.discount.btn'),
                        'price' => number_format($data['newPrice'], 0, '.', ' '),
                    ];
                } else {
                    if (!empty($checkValid)) {
                        if (isset($checkValid['auto']) && !$checkValid['auto']) {
                            if (!Yii::app()->user->id) {
                                $errorMessage['fields']['code'][] = Lang::t('checkout.tip.discount.error.user.login');
                            } else {
                                $errorMessage['fields']['code'][] = Lang::t('checkout.tip.discount.error.user.used');
                            }
                        }

                        if (isset($checkValid['season']) && !$checkValid['season']) {
                            if ($model->discount['season_date_start'] > $today) {
                                $errorMessage['fields']['code'][] = Lang::t('checkout.tip.discount.error.season.start');
                            } elseif ($model->discount['season_date_end'] < $today) {
                                $errorMessage['fields']['code'][] = Lang::t('checkout.tip.discount.error.season.end');
                            }
                        }

                        if (isset($checkValid['personal']) && !$checkValid['personal']) {
                            $errorMessage['fields']['code'][] = Lang::t('checkout.tip.discount.error.user.personal');
                        }

                        return $this->json = [
                            'error' => true,
                            'errorCode' => '', // $error_message,
                            'errorFields' => $errorMessage['fields'],
                        ];
                    } else {
                        if ($type == 'buy') {
                            $data = Discount::model()->setBuyDiscount($model->discount, $products);
                        } elseif ($type == 'sub') {
                            $data = Discount::model()->setBuyDiscount($model->discount, $products, 'sub');
                        }

                        if ($data['newPrice'] == 0) {
                            if ($type == 'buy') {
                                $data['newPrice'] = Cart::model()->getPrice();
                            } elseif ($type == 'sub') {
                                $data['newPrice'] = Cart::model()->getSubPrice();
                            }
                        }

                        Discount::model()->apply($model->discount);

                        $this->json = [
                            'message' => Lang::t('checkout.tip.discount'),
                            'code' => $model->discount['discount_code'],
                            'value' => $data['value'],
                            'delete_btn' => Lang::t('checkout.tip.discount.btn'),
                            'price' => number_format($data['newPrice'], 0, '.', ' '),
                        ];
                    }
                }
			} else {
				Discount::model()->clear();

				$errors = $model->jsonErrors();
				$error_message = implode("<br>\n", $errors['messages']);

				$this->json = [
					'error' => true,
					'errorCode' => '', // $error_message,
					'errorFields' => $errors['fields'],
				];
			}
		} else {
			throw new CHttpException(400, 'Bad request!');
		}
	}

	public function actionRemoveDiscount()
    {
        $type = Yii::app()->request->getQuery('type');

        Discount::model()->clear();

        if ($type && $type == 'sub') {
            $price = Cart::model()->getSubPrice();

            $user = Yii::app()->user->id;
            $sub = Subscription::model()->getSubByUser($user);

            if (!$sub) {
                $price = round($price - ($price * 0.3));
            }
        } else {
            $price = Cart::model()->getPrice();
        }

        return $this->json = [
            'price' => $price
        ];
    }

	/**
     * Registration form.
     */
    public function actionRegistration()
    {
        $registration = Yii::app()->request->getPost('registration');

        if (!empty($registration)) {
            $model = new RequestForm();
            $model->attributes = $registration;

            if ($model->validate()) {
                $request_id = Request::model()->add($model);

                if ($request_id) {
					$subject = Lang::t('email.subject.registration', ['{request_id}' => $request_id]);
					$body = Yii::app()->getController()->renderPartial('//email/registration', [
						'model' => $model,
						'request_id' => $request_id,
					], true);

					// send email to admins
					$mailer = new EsputnikMailer();
					$emails = explode(', ', Yii::app()->params->settings['notify_mail']);
					$mailer->send($emails, $subject, $body);

					// send email to client
					$mailer->send([$model->email], $subject, $body);

					$this->json = [
                        'msg' => $this->renderPartial('success', [], true), // Lang::t('form.tip.success'),
                    ];
                } else {
                    $this->json = [
                        'error' => true,
                        'errorCode' => Lang::t('form.error.requestNotSent'),
                        'errorFields' => [],
                    ];
                }
            } else {
                $errors = $model->jsonErrors();
                $this->json = [
                    'error' => true,
                    'errorCode' => '', // implode("<br>\n", $errors['messages']),
                    'errorFields' => $errors['fields'],
                ];
            }
        }
	}

	/**
     * Subscribe form.
     */
    public function actionSubscribe()
    {
        $subscribe = Yii::app()->request->getPost('subscribe');

        if (!empty($subscribe)) {
            $model = new SubscriberForm();
            $model->attributes = $subscribe;

            if ($model->validate()) {
                $subscriber_id = Subscriber::model()->add($model);

                if ($subscriber_id) {
                    $this->json = [
                        'message' => Lang::t('subscribe.tip.success'),
                    ];
                } else {
                    $this->json = [
                        'error' => true,
                        'errorCode' => Lang::t('form.error.requestNotSent'),
                        'errorFields' => [],
                    ];
                }
            } else {
                $errors = $model->jsonErrors();
                $this->json = [
                    'error' => true,
                    'errorCode' => '', // implode("<br>\n", $errors['messages']),
                    'errorFields' => $errors['fields'],
                ];
            }
        }
    }

	public function actionNpDepartments()
	{
		header('Access-Control-Allow-Origin: *');

		$city = Yii::app()->request->getQuery('city', '');
		$city = trim($city);

		$this->json = [
			'options' => [],
		];

		if (!empty($city)) {
			$np_model = new NovaPoshta();
			$np_city = $np_model->getCity($city);

			if (!empty($np_city)) {
				$np_departments = $np_model->getDepartments($np_city['city_ref']);

				if (!empty($np_departments)) {
					$options = [];
					$field = (Yii::app()->language === 'ru') ? 'department_name_ru' : 'department_name';

					foreach ($np_departments as $np_department) {
						$this->json['options'][] = [
							'value' => $np_department[$field],
							'text' => $np_department[$field],
						];
					}

					$cost = $np_model->getDeliveryPrice($np_city['city_ref']);

					$this->json['cost'] = isset($cost[0]['Cost']) ? 'орієнтовна ціна ' . $cost[0]['Cost'] . ' грн' : 0;
				}
			}
		}
	}

    public function actionNpStreets()
    {
        header('Access-Control-Allow-Origin: *');

        $city = Yii::app()->request->getQuery('city', '');
        $city = trim($city);

        $this->json = [
            'options' => [],
        ];

        if (!empty($city)) {
            $np_model = new NovaPoshta();
            $np_city = $np_model->getCity($city);

            if (!empty($np_city)) {
                $streets = $this->getNpStreets($np_city['city_ref'], 1);
                $page = 1;
                $total = $streets->info->totalCount;
                $countPage = round($total / 500);

                foreach ($streets->data as $street) {
                    $this->json['options'][] = [
                        'value' => $street->StreetsType . ' ' . $street->Description,
                        'text' => $street->StreetsType . ' ' . $street->Description,
                    ];
                }

                if ($page < $countPage) {
                    for ($page = 2; $page <= $countPage; $page++) {
                        $streets = $this->getNpStreets($np_city['city_ref'], $page);

                        foreach ($streets->data as $street) {
                            $this->json['options'][] = [
                                'value' => $street->StreetsType . ' ' . $street->Description,
                                'text' => $street->StreetsType . ' ' . $street->Description,
                            ];
                        }
                    }
                }

                $cost = $np_model->getDeliveryPrice($np_city['city_ref']);

                $this->json['cost'] = isset($cost[0]['Cost']) ? 'орієнтовна ціна ' . $cost[0]['Cost'] . ' грн' : 0;
            }
        }
    }

    public function getNpStreets($cityRef, $page = 1)
    {
        $params = [
            'modelName' => 'Address',
            'calledMethod' => 'getStreet',
            'methodProperties' => [
                'CityRef' => $cityRef,
                'Page' => $page,
            ],
            'apiKey' => Yii::app()->params['np']['api_key']
        ];

        $data_string = json_encode($params);

        $ch = curl_init('https://api.novaposhta.ua/v2.0/json/');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);

        return json_decode($result);
    }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			//echo $error['message'];
			$this->json = array('error' => 'Y', 'errorCode' => $error['message'], 'errorFields' => array());
		}
	}

	protected function afterAction($action)
	{
		header('Vary: Accept');

		if (strpos(Yii::app()->request->getAcceptTypes(), 'application/json') !== false) {
			header('Content-type: application/json; charset=utf-8');
		}

		echo json_encode($this->json);
		Yii::app()->end();
	}
}
