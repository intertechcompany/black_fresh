<?php
class PaymentController extends Controller
{
	/**
	 * Overrided method from CController.
	 * Set global indexing site setting.
	 * 
	 * @param CAction $action the action to be executed.
	 * @return boolean whether the action should be executed.
	 */
	protected function beforeAction($action)
	{
		return parent::beforeAction($action);
	}

    /**
     * Payment callback.
     *
     * @throws CException
     * @throws PaymentException
     */
    public function actionApi()
    {
        $payment = new Payment();
        $data = $payment->getData(true);
        $payment->callback($data);

        header('Content-type: application/json; charset=utf-8');
        echo $payment->response($data);
//        echo json_encode(['status' => 'ok']);
    }

    /**
     * Payment callback (sub)
     *
     * @throws CException
     * @throws PaymentException
     */
    public function actionCallbackSub()
    {
        $payment = new Payment();
        $data = $payment->getData(true);
        $payment->actionResultSub($data);
        header('Content-type: application/json; charset=utf-8');
        echo $payment->response($data);
    }

    /**
     * User return after payment (order)
     *
     * @throws CException
     * @throws PaymentException
     */
    public function actionResult()
    {
        $payment = new Payment();
        $data = $payment->getData();
        $payment->result($data);
    }

    /**
     * User return after payment (sub)
     *
     * @throws CException
     * @throws PaymentException
     */
    public function actionResultSub()
    {
        $payment = new Payment();
        $data = $payment->getData();
        $payment->actionResultSub($data);
    }
}
