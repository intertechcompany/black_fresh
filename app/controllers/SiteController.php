<?php

class SiteController extends Controller
{
    private $json = array();
    private $per_page = 99;

    /**
     * Overrided method from CController.
     * Set global indexing site setting.
     *
     * @param CAction $action the action to be executed.
     * @return boolean whether the action should be executed.
     */
    protected function beforeAction($action)
    {
        if (!Yii::app()->user->isGuest && in_array($this->route, array('site/login', 'site/reset'))) {
            $this->redirect(array('site/account'));
        } elseif (Yii::app()->user->isGuest && in_array($this->route, array(
                'site/account',
                'site/orders',
                'site/order',
            ))) {
            if (Yii::app()->request->isAjaxRequest) {
                echo json_encode(array(
                    'redirect' => $this->createUrl('site/login'),
                ));
            } else {
                $this->redirect(array('site/login'));
            }
        }

        $category_list = Category::model()->getCategoriesList();

        if (Yii::app()->params->settings['no_index']) {
            $this->noIndex = true;
        }

        if (!empty(@$_GET['offline'])) {
            setcookie("offline", true, time()+3600, '/');
        }

        if(stripos($_SERVER['QUERY_STRING'], 'utm_campaign') !== false) {
            setcookie("utm_params", $_SERVER['QUERY_STRING'], time()+(60*60*24*30*12), '/');
            $_SESSION['utm_params'] = $_SERVER['QUERY_STRING'];
        }

//        if(!Yii::app()->user->isGuest && @Yii::app()->user->id == 2097) {
//            echo json_encode($_SESSION);
//            exit;
//        }

        return parent::beforeAction($action);
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        $this->pageTitle = Lang::t('seo.meta.titleHome');
        $this->pageDescription = Lang::t('seo.meta.descriptionHome');
        $this->canonicalUrl = $this->createAbsoluteUrl('index');

        $categories = Yii::app()->params->categories_tree;
        $banners = Banner::model()->getBanners();
        $products = Product::model()->getBestSellersProducts();
        $base_categories = BaseCategory::model()->getBaseCategoriesList();

        $this->render('index', array(
            'categories' => $categories,
            'banners' => $banners,
            'products' => $products,
            'base_categories' => $base_categories,
        ));
    }

    public function actionSub()
    {
        unset($_SESSION['sub']);

        $qtySub = Yii::app()->request->getPost('qty-sub');
        $qtySubCustom = Yii::app()->request->getPost('qty-sub-custom');

        $cart_model = Cart::model();
        $action = Yii::app()->request->getPost('action');
        $cart = Yii::app()->request->getPost('cart');
        $is_popup = Yii::app()->request->getPost('is_popup');

        if ($qtySub && !$qtySubCustom) {
            $cart['qty'] = $qtySub;
        } else {
            $cart['qty'] = $qtySubCustom;
        }

        if (!empty($cart) && in_array($action, array('add', 'add_custom_product'))) {
            $model = new CartForm($action);
            $model->attributes = $cart;

            if ($model->validate()) {
                if ($action == 'add_custom_product') {
                    $cart_model->addCustomProduct($model);
                } else {
                    $cart_model->addSub($model, $cart['frequency']);
                }
            }
        }

        $response = new stdClass();
        $response->message = 'sub';
        return $this->renderJSON($response);
    }

    public function actionChangeSub()
    {
        $user = Yii::app()->user->id;
        if (!$user) {
            $response = new stdClass();
            $response->message = 'no such user';
            return $this->renderJSON($response);
        }

        $data = json_decode(Yii::app()->request->getRawBody());

        $subNextPayments = Subscription::model()->getNexSubPayments($user);

        foreach ($subNextPayments as $nextPayment) {
            if ($nextPayment['status'] == 'ok') {
                $this->removeSubPayment($nextPayment);
            }
        }

        $sub = Subscription::model()->getSubByUser($user);
        Subscription::model()->deleteSubPayments($sub, true);

        Subscription::model()->createSubNextPayment($sub, $data);

        $response = new stdClass();
        $response->message = 'ok';
        return $this->renderJSON($response);
    }

    public function actionPauseSub()
    {
        $user = Yii::app()->user->id;
        if (!$user) {
            $response = new stdClass();
            $response->message = 'no such user';
            return $this->renderJSON($response);
        }

        $data = json_decode(Yii::app()->request->getRawBody());
        $pause = $data->pause;
        $subs = Subscription::model()->getNexSubPayments($user);

        foreach ($subs as $sub) {
            if ($sub['status'] == 'ok') {
                $this->removeSubPayment($sub);
            }
            $date = new DateTime($sub['next_date']);
            $nextDate = date('Y-m-d', strtotime("+". 7 * $pause . "day", $date->getTimestamp()));
            Subscription::model()->pauseSub($sub['id'], $nextDate);
        }

        $response = new stdClass();
        $response->message = 'ok';
        return $this->renderJSON($response);
    }

    public function actionRemoveSub()
    {
        $user = Yii::app()->user->id;

        if (!$user) {
            $response = new stdClass();
            $response->message = 'no such user';
            return $this->renderJSON($response);
        }

        $data = json_decode(Yii::app()->request->getRawBody());

        $sub = Subscription::model()->getSubByUser($user);

        $result = $this->removeSubPayment($sub);

        Subscription::model()->deleteSub($sub);
        Subscription::model()->saveReasonSub($user, $data->text, $sub, $result);

        $response = new stdClass();
        $response->message = 'ok';
        return $this->renderJSON($response);
    }

    public function actionConfirmSub()
    {
        $user = Yii::app()->user->id;
        if (!$user) {
            $_SESSION['HTTP_REFERER'] = $_SERVER['HTTP_REFERER'];
            return $this->redirect(array('site/login'));
        }

        $subHistory = Subscription::model()->checkIfHaveSubHistory($user);

        $cart_model = Cart::model();
        $cart = $cart_model->getSubProducts();
        $has_unavailable = $cart_model->hasUnavailableProducts($cart);

        $order_model = Order::model();
        $order = Yii::app()->request->getPost('order');

        $discountSession = Discount::model()->getDiscount();

        if ($discountSession) {
            $discount = Discount::model()->getDiscountByCode($discountSession['discount_code']);
        } else {
            $discount = false;
        }

        if (!empty($order)) {
            if ($has_unavailable) {
                $this->json = [
                    'error' => true,
                    'errorCode' => Lang::t('checkout.tip.notAllowedProduct'),
                    'errorFields' => [],
                ];
                return;
            }

            $model = new OrderForm();
            $model->attributes = $order;

            if ($model->validate()) {
                $products = [];

                foreach ($cart as $item) {
                    $products[] = [
                        'product' => $item['product_id'],
                        'qty' => $item['qty'],
                        'variant' => $item['variant']['variant_price'] ?? $item['price']
                    ];
                }

                $cart_price = $cart_model->getSubPrice();
                $discount_value = Discount::model()->checkDiscount($discount, 'sub', $products);

                $userCount = Subscription::model()->checkIfHaveSub($user);

                if ($userCount['total'] >= 1) {
                    $sub = Subscription::model()->getSubByUser($user);
                    $this->removeSubPayment($sub);
                    $orderId = Subscription::model()->updateSub($cart, $order, $sub);
                } else {
                    if ($user) {
                        $sub = Subscription::model()->getSubByUser($user);
                        if (!$sub && $subHistory['total'] <= 0) {
                            $cart_price = round($cart_price - ($cart_price * 0.2));
                        } else {
                            $subCart = Subscription::model()->getFirstCartValue($cart);
                            $percentage = 5;
                            if ($subCart['qty'] >= 2) {
                                $percentageValue = ($percentage * (int) $subCart['qty']) / 100;

                                if ($percentageValue >= 1) {
                                    $percentageValue = 0.9;
                                }

                                $cart_price = round($cart_price - ($cart_price * $percentageValue));
                            }
                        }
                    }
                    $orderId = Subscription::model()->add($cart, $order);
                }

                if ($orderId) {
                    if ($subHistory['total'] <= 0) {
                        Subscription::model()->setUserSubDiscount($user);
                    }
                    Discount::model()->clear();

                    $thank_you = [
                        'order_id' => $orderId,
                    ];

                    $subOrder = Subscription::model()->getSubById($orderId);

                    if ($discount && isset($discount_value['newPrice'])) {
                        $cart_price = $discount_value['newPrice'];
                    }

                    $order_products = Subscription::model()->getSubProduct($orderId);
                    $_SESSION['thankYou'] = $cart_price;

                    // online payment
                    if ($model->payment == 2 && !empty($order) && $cart_price < Yii::app()->params->settings['max_order_amount']) {
                        $payment = new Payment;
                        $this->json['paymentForm'] = $payment->getPaymentFormHtml($subOrder, $cart_price, $order, [$order_products], true);
                        return;
                    }
                } else {
                    $this->json = [
                        'error' => true,
                        'errorCode' => Lang::t('checkout.error.orderFailed'), // Lang::t('checkout.error.couldNotCreateOrder'),
                        'errorFields' => [],
                    ];
                }
            }
            return;
        }

        $this->pageTitle = Lang::t('seo.meta.titleCheckout');
        $this->noIndex = true;

        $this->layout = '//layouts/checkout';

        $customer = $order_model->getUser();

        $np = new NovaPoshta();
        $np_cities = $np->getCities();
        $np_departments = array();

        if (!empty($customer['np_city'])) {
            $np_city = $np->getCity($customer['np_city']);

            if (!empty($np_city)) {
                $np_departments = $np->getDepartments($np_city['city_ref']);
            }
        }

        $this->render('checkoutSub', array(
            'cart' => $cart,
            'has_unavailable' => $has_unavailable,
            'discount' => $discount,
            'total' => $cart_model->getTotal(),
            'price' => $cart_model->getSubPrice(),
            'payment' => $order_model->getPayment(),
            'delivery' => $order_model->getDelivery(),
            'customer' => $customer,
            'np_cities' => $np_cities,
            'np_departments' => $np_departments,
        ));
    }

    public function removeSubPayment($sub)
    {
        $account = Yii::app()->params->wfp['account'];
        $password = Yii::app()->params->wfp['password'];

        $url = 'https://api.wayforpay.com/regularApi';

        $data = [
            "requestType" => "REMOVE",
            "merchantAccount" => $account,
            "merchantPassword" => $password,
            "orderReference" => $sub['order_reference']
        ];

        $options = [
            'http' => [
                'method'  => 'POST',
                'content' => json_encode($data),
                'header'=>  "Content-Type: application/json\r\n" . "Accept: application/json\r\n"
            ]
        ];

        $context  = stream_context_create( $options );
        return file_get_contents( $url, false, $context );
    }

    public function actionSaveUserMark()
    {
        $user = Yii::app()->user->id;

        if (!$user) {
            $response = new stdClass();
            $response->message = 'user';
            $this->renderJSON($response);
        }

        $data = json_decode(Yii::app()->request->getRawBody());

        if ($data->mark == 'good') {
            $sub = Subscription::model()->getSubByUser($user);
            $subProduct = Subscription::model()->getSubProduct($sub['id']);

            $userMark = Subscription::model()->getUserMark($user, $subProduct['product_id']);

            if (!$userMark) {
                Subscription::model()->saveUserMark($user, $subProduct, 1);
            }
        } elseif ($data->mark == 'bad') {
            $sub = Subscription::model()->getSubByUser($user);
            $subProduct = Subscription::model()->getSubProduct($sub['id']);

            $userMark = Subscription::model()->getUserMark($user, $subProduct['product_id']);

            if (!$userMark) {
                Subscription::model()->saveUserMark($user, $subProduct, 0, $data->data);
            }
        }
    }

    public function actionAccountSubscription()
    {
        $user = Yii::app()->user->id;

        if (!$user) {
            return $this->redirect(['site/login']);
        }

        $sub = Subscription::model()->getSubByUser($user);

        $newProducts = [];

        if ($sub) {
            $subProduct = Subscription::model()->getSubProduct($sub['id']);
            $productOriginal = Product::model()->getProductById($subProduct['product_id']);
            $deliveryFreq = DeliveryFrequency::model()->getDeliveryByFrequency($sub['frequency_id']);
            $nextPayment = Subscription::model()->getNexSubPayments($user);

            $userMark = Subscription::model()->getUserMark($user, $subProduct['product_id']);

            if (!$nextPayment) {
                $nextPayment = false;
            } else {
                $nextPayment = $nextPayment[0];
            }

            if (!$userMark) {
                $userMark = false;
            }

            for ($i = 1; $i < 4; $i++) {
                $subNewProducts = Subscription::model()->getNewSubProductsBySub($i, $subProduct['product_id']);
                $rndNumb = rand(0, count($subNewProducts) - 1);

                if (count($subNewProducts) > 0) {
                    $product = $subNewProducts[$rndNumb];

                    $variants = Product::model()->getProductVariants($product['product_id']);
                    $options = Product::model()->getProductOptions($product['product_id']);
                    $newProducts[] = ['product' => $product, 'product_variants' => $variants, 'options' => $options];
                }
            }

            if (empty($newProducts)) {
                for ($i = 1; $i < 4; $i++) {
                    $subNewProducts = Subscription::model()->getNewSubProducts($i);

                    if (!empty($subNewProducts)) {
                        if (count($subNewProducts) === 1) {
                            $rndNumb = 0;
                        } else {
                            $rndNumb = rand(0, count($subNewProducts) - 1);
                        }

                        $product = $subNewProducts[$rndNumb] ?? null;

                        if ($product) {
                            $variants = Product::model()->getProductVariants($product['product_id']);
                            $options = Product::model()->getProductOptions($product['product_id']);
                            $newProducts[] = [
                                'product' => $product,
                                'product_variants' => $variants,
                                'options' => $options
                            ];
                        }
                    }
                }
            }

            return $this->render(
                'accountSubscription',
                [
                    'sub' => $sub,
                    'product' => $productOriginal,
                    'subProduct' => $subProduct,
                    'deliveryFreq' => $deliveryFreq,
                    'next' => $nextPayment,
                    'rndProducts' => $newProducts,
                    'userMark' => $userMark,
                    'user' => User::model()->getUserById($user)
                ]
            );
        }

        if (empty($newProducts)) {
            for ($i = 1; $i < 4; $i++) {
                $subNewProducts = Subscription::model()->getNewSubProducts($i);

                if (!empty($subNewProducts)) {
                    if (count($subNewProducts) === 1) {
                        $rndNumb = 0;
                    } else {
                        $rndNumb = rand(0, count($subNewProducts) - 1);
                    }

                    $product = $subNewProducts[$rndNumb] ?? null;

                    if ($product) {
                        $variants = Product::model()->getProductVariants($product['product_id']);
                        $options = Product::model()->getProductOptions($product['product_id']);
                        $newProducts[] = [
                            'product' => $product,
                            'product_variants' => $variants,
                            'options' => $options
                        ];
                    }
                }
            }
        }

        return $this->render(
            'accountSubscription',
            [
                'product' => Product::model()->getProductById(3),
                'sub' => null,
                'rndProducts' => $newProducts,
                'user' => User::model()->getUserById($user)
            ]
        );
    }

    public function actionThankYouSub()
    {
        $this->render('thankYouSub');
    }

    protected function renderJSON($data)
    {
        header('Content-type: application/json');
        echo CJSON::encode($data);

        foreach (Yii::app()->log->routes as $route) {
            if($route instanceof CWebLogRoute) {
                $route->enabled = false;
            }
        }
        Yii::app()->end();
    }

    public function actionNextPayment()
    {
        $user = Yii::app()->user->id;

        $sub = Subscription::model()->getSubByUser($user);

        if ($sub) {
            $nextPayment = Subscription::model()->getNexSubPayments($user);

            if (!$nextPayment) {
                $nextPayment = false;
            } else {
                $nextPayment = $nextPayment[0];
            }

            $response = new stdClass();
            $response->message = 'ok';
            $response->next = $nextPayment['next_date'];

            $this->renderJSON($response);
        }

        if (!$user) {
            $response = new stdClass();
            $response->message = 'no such user';
            $this->renderJSON($response);
        }
    }

    public function actionQuestions()
    {
        $this->canonicalUrl = $this->createAbsoluteUrl('questions');

        $questions = Quiz::model()->getQuestions();
        $this->renderJSON($questions);
    }

    public function actionSubQuestions()
    {
        $questions = Subscription::model()->getSubQuestions();
        $this->renderJSON($questions);
    }

    public function actionSubAnswers()
    {
        $answers = Subscription::model()->getAnswers();
        $this->renderJSON($answers);
    }

    public function actionAnswers()
    {
        $this->canonicalUrl = $this->createAbsoluteUrl('answers');

        $answers = Quiz::model()->getAnswers();
        $this->renderJSON($answers);
    }

    public function actionUserQuizAnswer()
    {
        $this->canonicalUrl = $this->createAbsoluteUrl('quiz/user/answer');

        $user = Yii::app()->user->id;

        if (!$user) {
            $response = new stdClass();
            $response->message = 'no such user';
            $this->renderJSON($response);
        }

        $userQuiz = Quiz::model()->getUserQuiz($user);

        $this->renderJSON($userQuiz);
    }

    public function actionSaveAnswers()
    {
        $user = Yii::app()->user->id;

//        if (!$user) {
//            $response = new stdClass();
//            $response->message = 'no such user';
//            $this->renderJSON($response);
//        }

        $data = Yii::app()->request->getRawBody();

        if (!$data) {
            $response = new stdClass();
            $response->message = 'error';
            $this->renderJSON($response);
        }

        $data = json_decode($data);

        if ($user) {
            $userQuiz = Quiz::model()->getUserQuiz($user);
        } else {
            $userQuiz = false;
        }

        if ($userQuiz && $data[0] == 'edit') {
            for ($i = 1; $i < 7; $i++) {
                if (!isset($data[$i]) || $data[$i] == null) {
                    $data[$i] = $userQuiz['quest_'.$i];
                }
            }
            array_splice($data,6,0,array_shift($data));

            $quizProducts = Quiz::model()->getProduct($data[0], $data[1], $data[2], $data[3], $data[4], $data[5]);

            $rndNumb = rand(1, count($quizProducts));

            Quiz::model()->saveAnswers(Yii::app()->user->id, $quizProducts[$rndNumb - 1]['product_id'], $data[0],
                $data[1], $data[2], $data[3], $data[4], $data[5]
            );

            if ($quizProducts) {
                $rndNumb = rand(1, count($quizProducts));

                Quiz::model()->saveAllAnswers($quizProducts[$rndNumb - 1]['product_id'], $data[0],
                    $data[1], $data[2], $data[3], $data[4], $data[5]);
            } else {
                Quiz::model()->saveAllAnswers(0, $data[0]->result,
                    $data[1], $data[2], $data[3], $data[4], $data[5]);
            }

            if ($quizProducts) {
                $product = Product::model()->getProductById($quizProducts[$rndNumb - 1]['product_id']);
                $variants = Product::model()->getProductVariants($product['product_id']);
            } else {
                $product = false;
            }

            if ($product) {
                $assetsUrl = Yii::app()->assetManager->getBaseUrl();
                $product['assetUrl'] = $assetsUrl;
                $product['message'] = 'success';
                $product['variants'] = $variants;
                $product['text'] = $quizProducts[$rndNumb - 1];

                $this->renderJSON($product);
            }
        } else {
            $quizProducts = Quiz::model()->getProduct($data[0]->result, $data[1]->result, $data[2]->result, $data[3]->result, $data[4]->result, $data[5]->result);

            if ($quizProducts) {
                $rndNumb = rand(1, count($quizProducts));

                if ($user) {
                    Quiz::model()->saveAnswers(Yii::app()->user->id, $quizProducts[$rndNumb - 1]['product_id'], $data[0]->result,
                        $data[1]->result, $data[2]->result, $data[3]->result, $data[4]->result, $data[5]->result
                    );
                }
            }
        }

        $quizProducts = Quiz::model()->getProduct($data[0]->result, $data[1]->result, $data[2]->result, $data[3]->result, $data[4]->result, $data[5]->result);

        if ($quizProducts) {
            $rndNumb = rand(1, count($quizProducts));

            Quiz::model()->saveAllAnswers($quizProducts[$rndNumb - 1]['product_id'], $data[0]->result,
                $data[1]->result, $data[2]->result, $data[3]->result, $data[4]->result, $data[5]->result);
        } else {
            Quiz::model()->saveAllAnswers(0, $data[0]->result,
                $data[1]->result, $data[2]->result, $data[3]->result, $data[4]->result, $data[5]->result);
        }

        if ($quizProducts) {
            $product = Product::model()->getProductById($quizProducts[$rndNumb - 1]['product_id']);
            $variants = Product::model()->getProductVariants($product['product_id']);
        } else {
            $product = false;
        }

        if ($product) {
            $assetsUrl = Yii::app()->assetManager->getBaseUrl();
            $product['assetUrl'] = $assetsUrl;
            $product['message'] = 'success';
            $product['variants'] = $variants;
            $product['text'] = $quizProducts[$rndNumb - 1];

            $this->renderJSON($product);
        }

        $response = new stdClass();
        $response->message = 'empty';
        $this->renderJSON($response);
    }

    public function actionLanguage()
    {
        $lang = Yii::app()->language;

        $response = new stdClass();
        $response->lang = $lang;

        $this->renderJSON($response);
    }

    public function actionQuizEditPage()
    {
        $user = Yii::app()->user->id;

        if (!$user) {
            return $this->redirect(['site/index']);
        }

        return  $this->render(
            'accountQuizEdit',
            [

            ]
        );
    }

    public function actionQuiz()
    {
        $user = Yii::app()->user->id;

        $this->pageTitle = Lang::t('seo.meta.titleHome');
        $this->pageDescription = Lang::t('seo.meta.descriptionHome');
        $this->canonicalUrl = $this->createAbsoluteUrl('quiz');

        $categories = Yii::app()->params->categories_tree;
        $banners = Banner::model()->getBanners();
        $products = Product::model()->getBestSellersProducts();
        $base_categories = BaseCategory::model()->getBaseCategoriesList();

        $this->render('quiz', array(
            'categories' => $categories,
            'banners' => $banners,
            'products' => $products,
            'base_categories' => $base_categories,
        ));
    }

	public function isValidGoogleCaptcha($secret, $googleCaptchaCode)
    {
        try {
            $url = 'https://www.google.com/recaptcha/api/siteverify';
            $data = [
                'secret' => $secret,
                'response' => $googleCaptchaCode
            ];

            $options = [
                'http' => [
                    'header' => 'Content-type: application/x-www-form-urlencoded',
                    'method' => 'POST',
                    'content' => http_build_query($data)
                ]
            ];

            $context = stream_context_create($options);
            $result = file_get_contents($url, false, $context);

            return json_decode($result)->success ?? false;
        } catch (Exception $e) {
            return null;
        }
    }

    public function actionFeedback()
    {
        $feedback = Yii::app()->request->getPost('feedback', null);

        if (!isset($feedback['google_recaptcha']) || empty($feedback['google_recaptcha'])) {
            $this->redirect(['site/index']);
        }

        $secret = '6LenIpUeAAAAALP1dMd6uHtNPxJTNfTV9JsgV6w0';

        $isValid = $this->isValidGoogleCaptcha($secret, $feedback['google_recaptcha']);

        if (!$isValid) {
            $this->redirect(['site/index']);
        }

        $page = Page::model()->getPage($feedback['page_alias']);
        $feedback['page_id'] = $page['page_id'];

        $mailer = new EsputnikMailer();
        $subject = 'Feedback';

        if ($page['feedback_file']) {
            $body = Yii::app()->getController()->renderPartial('//email/feedback', [
                'feedback' => $feedback,
                'page' => $page,
            ], true);

            $mailer->send([$feedback['email']], $subject, $body);
            $feedback['is_send'] = 1;
        }

        $body = Yii::app()->getController()->renderPartial('//email/feedbackAdmin', [
            'feedback' => $feedback,
            'page' => $page,
        ], true);

        $mailer->send(['pk@fresh.black'], $subject, $body);

        Feedback::model()->save($feedback);

        $this->render('thankYouFeedback');
    }

    /**
     * Get newest products.
     */
    public function actionNewest()
    {
        $categories = Yii::app()->params->categories;
        $categories_tree = Yii::app()->params->categories_tree;

        $this->pageTitle = 'Новинки';
        $this->pageDescription = '';
        $this->canonicalUrl = $this->createAbsoluteUrl('newest');

        $show_all = (bool) Yii::app()->request->getQuery('show_all', false);

        // get filters
        $filters = Yii::app()->request->getQuery('filter', array());

        // get sort
        $sort = Yii::app()->request->getQuery('sort');
        if (!in_array($sort, array('popular', 'price-asc', 'price-desc'))) {
            $sort = 'popular';
        }

        // set pages
        $pages = new CPagination();
        $pages->setPageSize($this->per_page);
        $pages->params = array();

        if (!empty($sort) && $sort != 'popular') {
            $pages->params['sort'] = $sort;
        }

        if (!empty($filters)) {
            $pages->params['filter'] = $filters;
        }

        // build facets
        $facets = Facet::model();
        $facets->setFacetsInput($filters);
        // $facets->getCategoryFacets($category['category_id']);

        $products = array();

        $total = Product::model()->getNewestProductsTotal($this->per_page);

        if ($total['total']) {
            $pages->setItemCount($total['total']);

            $offset = $pages->getOffset();
            $limit = $pages->getLimit();

            if ($show_all) {
                $limit = $total['total'];
            }

            // get products
            $products = Product::model()->getNewestProducts($offset, $limit, $sort);
        }

        $this->render('newestProducts', array(
            'categories' => $categories,
            'categories_tree' => $categories_tree,
            'products' => $products,
            'facets' => $facets,
            'sort' => $sort,
            'filters' => $filters,
            'pages' => $pages,
            'show_all' => $show_all,
        ));
    }

    /**
     * Get sale products.
     */
    public function actionSale()
    {
        $categories = Yii::app()->params->categories;
        $categories_tree = Yii::app()->params->categories_tree;
        $brands = Yii::app()->params->brands;

        $this->pageTitle = 'SALE';
        $this->pageDescription = '';
        $this->canonicalUrl = $this->createAbsoluteUrl('newest');

        // sale brands and categories
        $sale_categories = [];
        $sale_brands = [];

        foreach ($categories as $category) {
            if (!empty($category['category_discount'])) {
                $sale_categories[] = $category['category_id'];
            }
        }

        foreach ($brands as $brand) {
            if (!empty($brand['brand_discount'])) {
                $sale_brands[] = $brand['brand_id'];
            }
        }

        // get filters
        $filters = Yii::app()->request->getQuery('filter', array());

        // get sort
        $sort = Yii::app()->request->getQuery('sort');
        if (!in_array($sort, array('popular', 'price-asc', 'price-desc'))) {
            $sort = 'popular';
        }

        // set pages
        $pages = new CPagination();
        $pages->setPageSize($this->per_page);
        $pages->params = array();

        if (!empty($sort) && $sort != 'popular') {
            $pages->params['sort'] = $sort;
        }

        if (!empty($filters)) {
            $pages->params['filter'] = $filters;
        }

        // build facets
        $facets = Facet::model();
        $facets->setFacetsInput($filters);
        // $facets->getCategoryFacets($category['category_id']);

        $products = array();

        $total = Product::model()->getSaleProductsTotal($this->per_page, $sale_categories, $sale_brands);

        if ($total['total']) {
            $pages->setItemCount($total['total']);

            $offset = $pages->getOffset();
            $limit = $pages->getLimit();

            // get products
            $products = Product::model()->getSaleProducts($offset, $limit, $sort, $sale_categories, $sale_brands);
        }

        $this->render('saleProducts', array(
            'categories' => $categories,
            'categories_tree' => $categories_tree,
            'products' => $products,
            'facets' => $facets,
            'sort' => $sort,
            'filters' => $filters,
            'pages' => $pages,
        ));
    }

    public function actionParents()
    {
        $products = Product::model()->getProductsForParents();

        $this->render('parents', array(
            'products' => $products
        ));
    }

    public function actionFestivals()
    {
        if(Yii::app()->language != 'en') {
            throw new CHttpException(404, 'Page not found');
        }

        $this->layout = '//layouts/festivals';

        $this->render('festivals', array(
        ));
    }

    public function actionFestivalsFeedback()
    {
        $feedback = Yii::app()->request->getPost('feedback', []);

        Yii::app()->db
            ->createCommand("insert ignore into festivals_feedback set
              email = '{$feedback["email"]}',
              user_name = '{$feedback["user_name"]}',
              phone = '{$feedback["phone"]}',
              `type` = '{$feedback["type"]}' 
            ")
            ->query();

        $mailer = new EsputnikMailer();

        $body = Yii::app()->getController()->renderPartial('//email/festivals/feedback', [
            'feedback' => $feedback,
        ], true);

        $mailer->send([$feedback['email']], 'Welcome Aboard!', $body);

        $body = Yii::app()->getController()->renderPartial('//email/festivals/feedbackAdmin', [
            'feedback' => $feedback,
        ], true);

//        $mailer->send(['pk@fresh.black'], 'Festivals feedback, $body);
        $mailer->send(['support@fresh.black'], 'Festivals feedback', $body);

        return null;
    }

    /**
     * Get catalog category.
     *
     * @param string $alias category alias identifier.
     * @throws CHttpException 404 error code when a category is not found in DB.
     */
    public function actionCategory($alias)
    {
        $category_model = Category::model();

        // get category
        $category = $category_model->getCategoryByAlias($alias);

        if (!empty($category)) {
            $categories = Yii::app()->params->categories;
            $categories_tree = Yii::app()->params->categories_tree;

            $this->pageTitle = !empty($category['category_meta_title']) ? $category['category_meta_title'] : $category['category_name'];
            $this->pageDescription = $category['category_meta_description'];
            $this->canonicalUrl = $this->createAbsoluteUrl('category', array('alias' => $category['category_alias']));

            if (!empty($category['category_no_index'])) {
                $this->noIndex = true;
            }

            foreach ($categories[$category['category_id']]['parents'] as $index => $parent_id) {
                $this->breadcrumbs[$categories[$parent_id]['category_name']] = $this->createUrl('category', ['alias' => $categories[$parent_id]['category_alias']]);
            }

//			$this->breadcrumbs = [
//				$category['category_name'] => $this->canonicalUrl,
//			];

            $show_all = (bool) Yii::app()->request->getQuery('show_all', false);

            // get filters
            $filters = Yii::app()->request->getQuery('filter', array());

            // get sort
            $sort = Yii::app()->request->getQuery('sort');
            if (!in_array($sort, array('popular', 'price-asc', 'price-desc'))) {
                $sort = 'popular';
            }

            // set pages
            $pages = new CPagination();
            $pages->setPageSize($this->per_page);
            $pages->params = array(
                'alias' => $alias,
            );

            if (!empty($sort) && $sort != 'popular') {
                $pages->params['sort'] = $sort;
            }

            if (!empty($filters)) {
                $pages->params['filter'] = $filters;
            }

            // build facets
            $facets = Facet::model();
            $facets->setFacetsInput($filters);
            $facets->getCategoryFacets($category['category_id']);

            // count products by category
            $subcategories = [];

            if (isset($categories[$category['category_id']]['sub'])) {
                $subcategories = $categories[$category['category_id']]['sub'];
            } elseif (isset($categories[$category['parent_id']]['sub'])) {
                $subcategories = $categories[$category['parent_id']]['sub'];
            }

            $facets_empty = Facet::model();

            foreach ($subcategories as $index => $subcategory) {
                $subcategory_total = Product::model()->getCategoryProductsTotal($this->per_page, $subcategory['category_id'], $facets_empty);
                $subcategories[$index]['total'] = $subcategory_total['total'];
            }

            $products = array();

            $total = Product::model()->getCategoryProductsTotal($this->per_page, $category['category_id'], $facets);

            if ($total['total']) {
                $pages->setItemCount($total['total']);

                $offset = $pages->getOffset();
                $limit = $pages->getLimit();

                if ($show_all) {
                    $limit = $total['total'];
                }

                // get products
                $products = Product::model()->getCategoryProducts($offset, $limit, $category['category_id'], $facets, $sort);
            }

            $this->render('categoryProducts', array(
                'category' => $category,
                'categories' => $categories,
                'subcategories' => $subcategories,
                'categories_tree' => $categories_tree,
                'products' => $products,
                'facets' => $facets,
                'sort' => $sort,
                'filters' => $filters,
                'pages' => $pages,
                'show_all' => $show_all,
            ));
        } else {
            throw new CHttpException(404, 'Page not found');
        }
    }

    /**
     * Get brands list
     */
    public function actionBrands()
    {
        $this->pageTitle = 'Бренды';
        $this->pageDescription = '';
        $this->canonicalUrl = $this->createAbsoluteUrl('brands');

        $brands = Brand::model()->getBrands();

        $this->render('brands', array(
            'brands' => $brands,
        ));
    }

    /**
     * Get catalog brand.
     *
     * @param string $alias brand alias identifier.
     * @throws CHttpException 404 error code when a brand is not found in DB.
     */
    public function actionBrand($alias)
    {
        // get brand
        $brand = Brand::model()->getBrandByAlias($alias);

        if (!empty($brand)) {
            $categories = Yii::app()->params->categories;
            $categories_tree = Yii::app()->params->categories_tree;
            $brands = Yii::app()->params->brands;

            $this->pageTitle = !empty($brand['brand_meta_title']) ? $brand['brand_meta_title'] : $brand['brand_name'];
            $this->pageDescription = $brand['brand_meta_description'];
            $this->canonicalUrl = $this->createAbsoluteUrl('brand', array('alias' => $brand['brand_alias']));

            if (!empty($brand['brand_no_index'])) {
                $this->noIndex = true;
            }

            $show_all = (bool) Yii::app()->request->getQuery('show_all', false);

            // get filters
            $filters = Yii::app()->request->getQuery('filter', array());

            // get sort
            $sort = Yii::app()->request->getQuery('sort');
            if (!in_array($sort, array('popular', 'price-asc', 'price-desc'))) {
                $sort = 'popular';
            }

            // set pages
            $pages = new CPagination();
            $pages->setPageSize($this->per_page);
            $pages->params = array(
                'alias' => $alias,
            );

            if (!empty($sort) && $sort != 'popular') {
                $pages->params['sort'] = $sort;
            }

            if (!empty($filters)) {
                $pages->params['filter'] = $filters;
            }

            // build facets
            $facets = Facet::model();
            $facets->setFacetsInput($filters);
            // $facets->getCategoryFacets($category['category_id']);

            $products = array();

            $total = Product::model()->getBrandProductsTotal($this->per_page, $brand['brand_id'], $facets);

            if ($total['total']) {
                $pages->setItemCount($total['total']);

                $offset = $pages->getOffset();
                $limit = $pages->getLimit();

                if ($show_all) {
                    $limit = $total['total'];
                }

                // get products
                $products = Product::model()->getBrandProducts($offset, $limit, $brand['brand_id'], $facets, $sort);
            }

            $this->render('brandProducts', array(
                'brand' => $brand,
                'brands' => $brands,
                'categories' => $categories,
                'categories_tree' => $categories_tree,
                'products' => $products,
                'facets' => $facets,
                'sort' => $sort,
                'filters' => $filters,
                'pages' => $pages,
                'show_all' => $show_all,
            ));
        } else {
            throw new CHttpException(404, 'Page not found');
        }
    }

    /**
     * Get catalog category collection.
     *
     * @param string $category_alias category alias identifier.
     * @param string $collection_alias category alias identifier.
     * @throws CHttpException 404 error code when a category or collection is not found in DB.
     */
    public function actionCollection($category_alias, $collection_alias)
    {
        $category_model = Category::model();
        $collection_model = Collection::model();

        // get category
        $category = $category_model->getCategoryByAlias($category_alias);

        if (empty($category)) {
            throw new CHttpException(404, 'Page not found');
        }

        // get collection
        $collection = $collection_model->getCollectionByAlias($collection_alias);

        if (empty($collection)) {
            throw new CHttpException(404, 'Page not found');
        }

        $categories = Yii::app()->params->categories;
        $categories_tree = Yii::app()->params->categories_tree;

        $this->pageTitle = !empty($collection['collection_meta_title']) ? $collection['collection_meta_title'] : $collection['collection_title'];
        $this->pageDescription = $collection['collection_meta_description'];
        $this->canonicalUrl = $this->createAbsoluteUrl('collection', array('category_alias' => $category['category_alias'], 'collection_alias' => $collection['collection_alias']));

        if (!empty($collection['collection_no_index'])) {
            $this->noIndex = true;
        }

        // get photos
        $collection_photos = $collection_model->getCollectionPhotos($collection['collection_id']);

        // get products
        $products = Product::model()->getCategoryCollectionProducts($category['category_id'], $collection['collection_id']);

        $this->render('collection', array(
            'category' => $category,
            'categories' => $categories,
            'categories_tree' => $categories_tree,
            'collection' => $collection,
            'collection_photos' => $collection_photos,
            'products' => $products,
        ));
    }

    /**
     * Get product data and render it.
     *
     * @param string $alias product alias identifier.
     * @throws CHttpException 404 error code when a product is not found in DB.
     */
    public function actionProduct($alias)
    {
        $product_model = Product::model();

        $deliveryFrequency = DeliveryFrequency::model()->getDeliveryFrequency();

        // get product
        $product = $product_model->getProductByAlias($alias);

        $category_model = Category::model();
        $categories = $category_model->getCategoriesList();

        if (!empty($product)) {
            $this->pageTitle = !empty($product['product_meta_title']) ? $product['product_meta_title'] : $product['product_title'];
            $this->pageDescription = $product['product_meta_description'];
            $this->canonicalUrl = $this->createAbsoluteUrl('product', array('alias' => $product['product_alias']));

            if (!empty($product['product_no_index'])) {
                $this->noIndex = true;
            }

            $product_properties = $product_model->getProductProperties($product['product_id']);
            $product_photos = $product_model->getProductPhotos($product['product_id']);

            $product_variants = array();

            if ($product['product_price_type'] == 'variants') {
                $product_variants = $product_model->getProductVariants($product['product_id']);
            }

            $product_options = $product_model->getProductOptions($product['product_id']);

            $category = array();
            $brand = array();
            $collection = array();
            $collection_products = array();
            $related_products = array();

            // get category
            if (isset($categories[$product['category_id']])) {
                $category = $categories[$product['category_id']];

                $this->breadcrumbs = [
                    $category['category_name'] => $this->createAbsoluteUrl('site/category', ['alias' => $category['category_alias']]),
                    $product['product_title'] => $this->canonicalUrl,
                ];
            } else {
                $this->breadcrumbs = [
                    $product['product_title'] => $this->canonicalUrl,
                ];
            }

            /*
            // get brand
            $brand = Brand::model()->getBrandById($product['brand_id']);

            // get collection
            if (!empty($product['collection_id'])) {
                $collection = Collection::model()->getCollectionById($product['collection_id']);

                if (!empty($collection)) {
                    // get collection products
                    $collection_products = Product::model()->getCollectionProducts($product['collection_id'], $product['product_id']);
                }
            } */

            // get related products
            $related_products = $product_model->getProductsRelated($product['product_id']);

            $user = Yii::app()->user->id;

            if (!$user) {
                $sub = false;
            } else {
                $sub = Subscription::model()->getSubByUser($user);
            }

	   Yii::app()->params['googleLayout'] = (new GoogleSearchLayout())->productLayout($product);

            $this->render('product', array(
                'sub' => $sub,
                'categories' => $categories,
                'product' => $product,
                'deliveryFrequency' => $deliveryFrequency,
                'product_properties' => $product_properties,
                'product_photos' => $product_photos,
                'product_variants' => $product_variants,
                'product_options' => $product_options,
                // 'brand' => $brand,
                'category' => $category,
                // 'collection' => $collection,
                // 'collection_products' => $collection_products,
                'related_products' => $related_products,
            ));
        } else {
            $product = $product_model->getProductDisabledByAlias($alias);

            if($product) {
                $category = $categories[$product['category_id']];
                $url = $this->createAbsoluteUrl('site/category', ['alias' => $category['category_alias']]);
                $this->redirect($url);
            } else {
                throw new CHttpException(404, 'Page not found');
            }
        }
    }

    public function actionProductProperties()
    {
        $data = Yii::app()->request->getPost('data');

        $properties = Product::model()->getProductPropertiesByOption($data['product'], $data['option']);

        $this->json['data'] = $properties;
    }

    /**
     * Search products by keyword.
     */
    public function actionSearch()
    {
        $keyword = Yii::app()->request->getQuery('keyword', null);

        // get sort
        $sort = Yii::app()->request->getQuery('sort');
        if (!in_array($sort, array('popular', 'price-asc', 'price-desc'))) {
            $sort = 'popular';
        }

        $products = array();
        $errors = '';

        $this->pageTitle = 'Поиск';
        $this->pageDescription = '';
        $this->noIndex = true;

        $form = new SearchForm;
        $form->keyword = $keyword;

        $categories = Yii::app()->params->categories;
        $categories_tree = Yii::app()->params->categories_tree;

        if ($form->validate()) {
            $this->pageTitle .= ' «' . $form->keyword . '»';

            $products = Product::model()->getProductsByKeyword($form->keyword, $sort);
        } else {
            $validation_errors = $form->jsonErrors();
            $errors = implode("<br>\n", $validation_errors['messages']);
        }

        $this->render('search', array(
            'keyword' => $form->keyword,
            'categories' => $categories,
            'categories_tree' => $categories_tree,
            'products' => $products,
            'errors' => $errors,
            'sort' => $sort,
        ));
    }

    public function actionSubCart()
    {
        $cart_model = Cart::model();
        $action = Yii::app()->request->getPost('action');
        $cart = Yii::app()->request->getPost('cart');
        $is_popup = Yii::app()->request->getPost('is_popup');

        if (!empty($cart) && in_array($action, array('add', 'update', 'price', 'remove', 'add_custom_product'))) {
            $model = new CartForm($action);
            $model->attributes = $cart;

            if ($model->validate()) {
                if ($action == 'add_custom_product') {
                    $cart_model->addCustomProduct($model);
                } else {
                    $cart_model->$action($model);
                }
            }
        }

        $this->redirect(array('site/accountSubscription'));
    }

    /**
     * Cart items.
     */
    public function actionCart()
    {
        $cart_model = Cart::model();
        $action = Yii::app()->request->getPost('action');
        $cart = Yii::app()->request->getPost('cart');
        $is_popup = Yii::app()->request->getPost('is_popup');

        if (!empty($cart) && in_array($action, array('add', 'update', 'price', 'remove', 'add_custom_product'))) {
            $model = new CartForm($action);
            $model->attributes = $cart;

            if ($model->validate()) {
                if ($action == 'add_custom_product') {
                    $cart_model->addCustomProduct($model);
                } else {
                    $cart_model->$action($model);
                }

                if (Yii::app()->request->isAjaxRequest) {
                    $cart = $cart_model->getCartProducts();
                    $has_unavailable = $cart_model->hasUnavailableProducts($cart);

                    $this->json['unavailable'] = $has_unavailable ? 'У вас в корзине товары, которых нет в наличии. Для оформления заказа удалите недоступный товар.' : false;
                    $this->json['total'] = $cart_model->getTotal();
                    $this->json['price'] = number_format($cart_model->getPrice(), 0, '.', ' ');
                    $this->json['btn'] = $cart_model->getTotal();

                    if ($model->variant_id !== null && $model->option_id !== null) {
                        $cartWeightForDiscount = Cart::model()->getCartWeightForDiscount();
                    } else {
                        $cartWeightForDiscount = null;
                    }

                    if ($action == 'add') {
                        $this->json['html'] = $this->renderPartial('cartList', [
                            'cart' => $cart,
                            'total' => $this->json['total'],
                            'price' => $this->json['price'],
                            'discountWeight' => $cartWeightForDiscount,
                        ], true);
                    } else {
                        if ($this->json['total']) {
                            $this->json['html'] = $this->renderPartial('cartList', array(
                                'cart' => $cart,
                                'total' => $this->json['total'],
                                'price' => $this->json['price'],
                            ), true);
                        } else {
                            if ($is_popup) {
                                $this->json['html'] = '<p class="text-center">Ваша корзина пуста!</p>';
                            } else {
                                $this->json['html'] = '<p class="text-center">Ваша корзина пуста!</p>';
                            }
                        }
                    }

                    // usleep(350000);

                    return;
                } else {
                    $this->redirect(array('cart'));
                }
            } else {
                if (Yii::app()->request->isAjaxRequest) {
                    $validation_errors = $model->jsonErrors();
                    $errors = implode("\n", $validation_errors['messages']);

                    $this->json = array(
                        'error' => true,
                        'errorCode' => $errors,
                        'errorFields' => array(),
                    );

                    return;
                }
            }
        }

        $this->pageTitle = 'Корзина';
        $this->noIndex = true;

        $order_model = Order::model();
        $payment = $order_model->getPayment();
        $delivery = $order_model->getDelivery();
        $customer = $order_model->getUser();

        $np = new NovaPoshta();
        $np_cities = $np->getCities();
        $np_departments = array();

        if (!empty($user['np_city'])) {
            $np_city = $np->getCity($user['np_city']);

            if (!empty($np_city)) {
                $np_departments = $np->getDepartments($np_city['city_ref']);
            }
        }

        $cart = $cart_model->getCartProducts();
        $has_unavailable = $cart_model->hasUnavailableProducts($cart);

        $this->render('cart', array(
            'cart' => $cart,
            'has_unavailable' => $has_unavailable,
            'total' => $cart_model->getTotal(),
            'price' => $cart_model->getCurrencyPrice($cart),
            'payment' => $payment,
            'delivery' => $delivery,
            'customer' => $customer,
            'np_cities' => $np_cities,
            'np_departments' => $np_departments,
        ));
    }

    /**
     * Checkout process.
     */
    public function actionCheckout()
    {
        $cart_model = Cart::model();
        $cart = $cart_model->getCartProducts();
        $has_unavailable = $cart_model->hasUnavailableProducts($cart);

        if (empty($cart)) {
            if (Yii::app()->request->isAjaxRequest) {
                $this->json = [
                    'error' => true,
                    'errorCode' => Lang::t('checkout.tip.emptyCart'),
                    'errorFields' => [],
                ];
                return;
            } else {
                $this->redirect(array('index'));
            }
        }

        $order_model = Order::model();
        $order = Yii::app()->request->getPost('order');

        $discountSession = Discount::model()->getDiscount();

        if ($discountSession) {
            $discount = Discount::model()->getDiscountByCode($discountSession['discount_code']);
        } else {
            $discount = false;
        }

        $registration = Yii::app()->request->getPost('order');

        if (!empty($order)) {
            if (isset($registration['is_create_account']) && $registration['is_create_account'] == 1) {

                $user = User::model()->getUserByEmail($order['email']);

                if (empty($user)) {
                    $model = new RegistrationForm;
                    try {
                        $registration['password'] = random_int(100000, 999999);
                    } catch (Exception $e) {}
                    $registration['password_confirm'] = $registration['password'];

                    if (!empty($registration)) {
                        $model->attributes = $registration;

                        if ($model->validate()) {
                            User::model()->add($model);

//                            $identity = new UserIdentity($model->email, strval($model->password));
//
//                            if ($identity->authenticate()) {
//                                $duration = 3600 * 24 * 30;
//
//                                Yii::app()->user->login($identity, $duration);
//                            }

                            User::model()->sendMailFromChaCkoutPage($model);
                        }
                    }
                }
            }

            if ($has_unavailable) {
                $this->json = [
                    'error' => true,
                    'errorCode' => Lang::t('checkout.tip.notAllowedProduct'),
                    'errorFields' => [],
                ];
                return;
            }

            $model = new OrderForm();
            $model->attributes = $order;

            if ($model->validate()) {
                $order_model->setUser($model);
                $order_model->setDelivery($model);
                $order_model->setPayment($model);

                // $delivery_data = json_decode(Yii::app()->params->settings['delivery'], true);

                $products = [];

                foreach ($cart as $item) {
                    $products[] = [
                        'product' => $item['product_id'],
                        'qty' => $item['qty'],
                        'variant' => $item['variant']['variant_price'] ?? $item['price']
                    ];
                }

                $cart_price = $cart_model->getPrice($cart);
                $actionPrice = $cart_model->getActionPrice();

                if ((int)$actionPrice !== (int)$cart_price) {
                    $cart_price = $actionPrice;
                }

                $discount_value = Discount::model()->checkDiscount($discount, 'buy', $products);

                $order_id = $order_model->add($model, $cart_price, $cart, $discount, $discount_value);

                if ($discount && $discount['is_auto']) {
                    $user = User::model()->getUserById(Yii::app()->user->id);

                    if ($user) {
                        Discount::model()->setUserAuto($user['user_id']);
                    }
                }

                if ($order_id) {
                    Discount::model()->clear();
                    $thank_you = [
                        'order_id' => $order_id,
                    ];

                    $order = $order_model->getOrderById($order_id);

                    if (Yii::app()->params->settings['stock'] == 'order') {
                        $order_model->subtractOrderQty($order_id);
                    }

                    // $cart_price += $order['delivery_price'];

                    if ($discount && isset($discount_value['newPrice'])) {
                        $cart_price = $discount_value['newPrice'];
                    }

                    $order_products = $order_model->getOrderProducts($order_id);

                    $subject = Lang::t('email.subject.newOrder', ['{order_id}' => $order['order_id']]);
                    $body = Yii::app()->getController()->renderPartial('//email/order', [
                        'order' => $order,
                        'order_products' => $order_products,
                    ], true);

                    // send email to admins
                    $mailer = new EsputnikMailer();
                    $emails = explode(', ', Yii::app()->params->settings['notify_mail']);
                    $mailer->send($emails, $subject, $body);

                    // send email to client
                    $mailer->send([$order['email']], $subject, $body);

                    // online payment
                    if ($model->payment == 2 && !empty($order) && $cart_price < Yii::app()->params->settings['max_order_amount']) {
                        $payment = new Payment;

//                        if(in_array($order['email'], ['kykyllkan@gmail.com', 'claudiapod.intertech@gmail.com', 'alexandr.intertech@gmail.com', 'sohalife99@gmail.com'])) {
//                            $this->json['paymentForm'] = $payment->getFondyPaymentFormHtml($order_id, $cart_price, $order, $order_products);
//                        } else {
                            $this->json['paymentForm'] = $payment->getPaymentFormHtml($order_id, $cart_price, $order, $order_products);
//                        }

                        return;
                    }

                    $cart_model->clear();
                    $order_model->clear();

                    $utmParams = !empty(@$_COOKIE['utm_params']) ? "?{$_COOKIE['utm_params']}" : '';

                    $_SESSION['thankYou'] = $cart_price;
                    $order_model->setThankYou($thank_you);
                    $this->json['thankYou'] = $this->createUrl('thankyou').$utmParams;
                } else {
                    $this->json = [
                        'error' => true,
                        'errorCode' => Lang::t('checkout.error.orderFailed'), // Lang::t('checkout.error.couldNotCreateOrder'),
                        'errorFields' => [],
                    ];
                }
            } else {
                $errors = $model->jsonErrors();

                $order_model->setUser($model);

                if (!in_array('delivery', $errors['fields'])) {
                    $order_model->setDelivery($model);
                }

                if (!in_array('payment', $errors['fields'])) {
                    $order_model->setPayment($model);
                }

                $this->json = array(
                    'error' => true,
                    'errorCode' => '', // implode("<br>\n", $errors['messages']),
                    'errorFields' => $errors['fields'],
                );
            }

            return;
        }

        $this->pageTitle = Lang::t('seo.meta.titleCheckout');
        $this->noIndex = true;

        $this->layout = '//layouts/checkout';

        $customer = $order_model->getUser();

        $np = new NovaPoshta();
        $np_cities = $np->getCities();
        $np_departments = array();

        if (!empty($customer['np_city'])) {
            $np_city = $np->getCity($customer['np_city']);

            if (!empty($np_city)) {
                $np_departments = $np->getDepartments($np_city['city_ref']);
            }
        }

        $this->render('checkout', array(
            'cart' => $cart,
            'has_unavailable' => $has_unavailable,
            'discount' => $discount,
            'total' => $cart_model->getTotal(),
            'price' => $cart_model->getPrice(),
            'actionPrice' => $cart_model->getActionPrice(),
            'payment' => $order_model->getPayment(),
            'delivery' => $order_model->getDelivery(),
            'customer' => $customer,
            'np_cities' => $np_cities,
            'np_departments' => $np_departments,
        ));
    }

    /**
     * Thank you page.
     */
    public function actionThankYou()
    {
        $order_model = Order::model();
        $thank_you = $order_model->getThankYou();

        if ($thank_you === false) {
            $this->redirect(array('index'));
        }

        $payment_status = null;

        if (isset($thank_you['payment_status'])) {
            $payment_status = ($thank_you['payment_status'] == 'success') ? true : false;
        }

        $order = Order::model()->getOrderById($thank_you['order_id']);
        $products = Order::model()->getOrderProducts($thank_you['order_id']);

        $order['order_products'] = array_column($products, 'product_id');
        $_SESSION['thankYouProducts'] = array_column($products, 'product_id');

        $this->pageTitle = Lang::t('seo.meta.titleThankYou');

        $this->render('thankYou', ['thank_you' => $thank_you, 'order' => $order]);
    }

    /**
     * School.
     */
    public function actionSchool()
    {
        $this->pageTitle = Lang::t('seo.meta.titleSchool');
        $this->pageDescription = Lang::t('seo.meta.descriptionSchool');
        $this->canonicalUrl = $this->createAbsoluteUrl('school');

        $course_model = Course::model();
        $events = $course_model->getEvents();
        $courses = $course_model->getCourses();
        $authors = Author::model()->getAuthors();

        $this->breadcrumbs = [
            Lang::t('seo.meta.titleSchool') => $this->canonicalUrl,
        ];

        $this->render('school', [
            'events' => $events,
            'courses' => $courses,
            'authors' => $authors,
        ]);
    }

    /**
     * Get course data and render it.
     *
     * @param string $alias course alias identifier.
     * @throws CHttpException 404 error code when a course is not found in DB.
     */
    public function actionCourse($alias)
    {
        $course = Course::model()->getCourseByAlias($alias);

        if (empty($course)) {
            throw new CHttpException(404, 'Page not found');
        }

        $this->pageTitle = !empty($course['course_meta_title']) ? $course['course_meta_title'] : $course['course_name'];
        $this->pageDescription = $course['course_meta_description'];
        $this->canonicalUrl = $this->createAbsoluteUrl('course', ['alias' => $course['course_alias']]);

        if (!empty($course['course_no_index'])) {
            $this->noIndex = true;
        }

        if (!empty($course['course_image'])) {
            $course_image = json_decode($course['course_image'], true);
            $this->ogImage = Yii::app()->getBaseUrl(true) . Yii::app()->assetManager->getBaseUrl() . '/course/' . $course['course_id'] . '/' . $course_image['details']['1x']['path'];
        }

        $this->breadcrumbs = [
            Lang::t('seo.meta.titleSchool') => $this->createAbsoluteUrl('site/school'),
            $course['course_name'] => $this->canonicalUrl,
        ];

        $this->render('course', [
            'course' => $course,
        ]);
    }

    /**
     * Get news articles.
     */
    public function actionNews()
    {
        $this->pageTitle = Lang::t('seo.meta.titleNews');
        $this->pageDescription = Lang::t('seo.meta.descriptionNews');
        $this->canonicalUrl = $this->createAbsoluteUrl('news');

        $this->breadcrumbs = [
            Lang::t('seo.meta.titleNews') => $this->canonicalUrl,
        ];

        $categories = BlogCategory::model()->getBlogCategoriesList();
        $news = Blog::model()->getBlogs();

        $this->render('news', array(
            'categories' => $categories,
            'news' => $news,
        ));
    }

    /**
     * Get news articles by category.
     *
     * @param string $alias news category alias identifier.
     * @throws CHttpException 404 error code when a category is not found in DB.
     */
    public function actionNewsCategory($alias)
    {
        // get category
        $category = BlogCategory::model()->getBlogCategoryByAlias($alias);

        if (empty($category)) {
            throw new CHttpException(404, 'Page not found');
        }

        $this->pageTitle = !empty($category['category_meta_title']) ? $category['category_meta_title'] : $category['category_name'] . ' — ' . Lang::t('seo.meta.titleNews');
        $this->pageDescription = $category['category_meta_description'];
        $this->canonicalUrl = $this->createAbsoluteUrl('newscategory', array('alias' => $category['category_alias']));

        if (!empty($category['category_no_index'])) {
            $this->noIndex = true;
        }

        $this->breadcrumbs = [
            Lang::t('seo.meta.titleNews') => $this->createAbsoluteUrl('site/news'),
            $category['category_name'] => $this->canonicalUrl,
        ];

        $categories = BlogCategory::model()->getBlogCategoriesList();
        $news = Blog::model()->getCategoryBlogs($category['category_id']);

        $this->render('news', array(
            'category' => $category,
            'categories' => $categories,
            'news' => $news,
        ));
    }

    /**
     * Get blog article data and render it.
     *
     * @param string $alias blog article alias identifier.
     * @throws CHttpException 404 error code when an article is not found in DB.
     */
    public function actionNewsArticle($category_alias, $alias)
    {
        $news = Blog::model()->getBlogByAlias($alias);

        if (empty($news)) {
            throw new CHttpException(404, 'Page not found');
        }

        // get category
        $category = BlogCategory::model()->getBlogCategoryByAlias($category_alias);

        if (empty($category) || $news['category_alias'] != $category['category_alias']) {
            $this->redirect(['site/newsarticle', 'category_alias' => $news['category_alias'], 'alias' => $news['blog_alias']], true, 301);
        }

        $this->pageTitle = !empty($news['blog_meta_title']) ? $news['blog_meta_title'] : $news['blog_title'];
        $this->pageDescription = $news['blog_meta_description'];
        $this->canonicalUrl = $this->createAbsoluteUrl('newsarticle', ['category_alias' => $news['category_alias'], 'alias' => $news['blog_alias']]);

        if (!empty($news['blog_no_index'])) {
            $this->noIndex = true;
        }

        $this->breadcrumbs = [
            Lang::t('seo.meta.titleNews') => $this->createAbsoluteUrl('site/news'),
            $category['category_name'] => $this->createAbsoluteUrl('site/newscategory', ['alias' => $category['category_alias']]),
            $news['blog_title'] => $this->canonicalUrl,
        ];

        if (!empty($news['blog_photo'])) {
            $news_image = json_decode($news['blog_photo'], true);
            $this->ogImage = Yii::app()->getBaseUrl(true) . Yii::app()->assetManager->getBaseUrl() . '/blog/' . $news['blog_id'] . '/' . $news_image['details']['1x']['path'];
        }

        $this->render('newsArticle', array(
            'blog' => $news,
        ));
    }

    /**
     * Get base articles.
     */
    public function actionBase()
    {
        $this->pageTitle = Lang::t('seo.meta.titleBase');
        $this->pageDescription = Lang::t('seo.meta.descriptionBase');
        $this->canonicalUrl = $this->createAbsoluteUrl('base');

        $this->breadcrumbs = [
            Lang::t('seo.meta.titleBase') => $this->canonicalUrl,
        ];

        $categories = BaseCategory::model()->getBaseCategories();

        $this->render('base', array(
            'categories' => $categories,
        ));
    }

    /**
     * Get base articles by category.
     *
     * @param string $alias base category alias identifier.
     * @throws CHttpException 404 error code when a category is not found in DB.
     */
    public function actionBaseCategory($alias)
    {
        // get category
        $category = BaseCategory::model()->getBaseCategoryByAlias($alias);

        if (empty($category)) {
            throw new CHttpException(404, 'Page not found');
        }

        $this->pageTitle = !empty($category['category_meta_title']) ? $category['category_meta_title'] : $category['category_name'] . ' — ' . Lang::t('seo.meta.titleBase');
        $this->pageDescription = $category['category_meta_description'];
        $this->canonicalUrl = $this->createAbsoluteUrl('basecategory', array('alias' => $category['category_alias']));

        if (!empty($category['category_no_index'])) {
            $this->noIndex = true;
        }

        $this->breadcrumbs = [
            Lang::t('seo.meta.titleBase') => $this->createAbsoluteUrl('site/base'),
            $category['category_name'] => $this->canonicalUrl,
        ];

        $this->render('baseCategory', array(
            'category' => $category,
        ));
    }

    /**
     * Get base article data and render it.
     *
     * @param string $alias base article alias identifier.
     * @throws CHttpException 404 error code when an article is not found in DB.
     */
    public function actionBaseArticle($category_alias, $alias)
    {
        $base = Base::model()->getBaseByAlias($alias);

        if (empty($base)) {
            throw new CHttpException(404, 'Page not found');
        }

        // get category
        $category = BaseCategory::model()->getBaseCategoryByAlias($category_alias);

        if (empty($category) || $base['category_alias'] != $category['category_alias']) {
            $this->redirect(['site/basearticle', 'category_alias' => $base['category_alias'], 'alias' => $base['base_alias']], true, 301);
        }

        $this->pageTitle = !empty($base['base_meta_title']) ? $base['base_meta_title'] : $base['base_title'];
        $this->pageDescription = $base['base_meta_description'];
        $this->canonicalUrl = $this->createAbsoluteUrl('basearticle', ['category_alias' => $base['category_alias'], 'alias' => $base['base_alias']]);

        if (!empty($base['base_no_index'])) {
            $this->noIndex = true;
        }

        $this->breadcrumbs = [
            Lang::t('seo.meta.titleBase') => $this->createAbsoluteUrl('site/base'),
            $category['category_name'] => $this->createAbsoluteUrl('site/basecategory', ['alias' => $category['category_alias']]),
            $base['base_title'] => $this->canonicalUrl,
        ];

        if (!empty($base['base_photo'])) {
            $base_image = json_decode($base['base_photo'], true);
            $this->ogImage = Yii::app()->getBaseUrl(true) . Yii::app()->assetManager->getBaseUrl() . '/base/' . $base['base_id'] . '/' . $base_image['details']['1x']['path'];
        }

        $this->render('baseArticle', array(
            'base' => $base,
        ));
    }

    /**
     * Get page data and render it.
     *
     * @param string $alias page alias identifier.
     * @throws CHttpException 404 error code when the page is not found in DB.
     */
    public function actionPage($alias)
    {
        // get page
        $page = Page::model()->getPage($alias);

        if (!empty($page)) {
            $this->pageTitle = !empty($page['page_meta_description']) ? $page['page_meta_description'] : $page['page_title'];
            $this->pageDescription = $page['page_meta_description'];
            $this->canonicalUrl = $this->createAbsoluteUrl('page', array('alias' => $page['page_alias']));

            if (!empty($page['page_no_index'])) {
                $this->noIndex = true;
            }

            $this->breadcrumbs = [
                $page['page_title'] => $this->canonicalUrl,
            ];

            if ($page['page_type'] == 'about') {
                $members = Member::model()->getMembers();

                $this->render('about', array('page' => $page, 'members' => $members));
            } else {

                if ($page['page_alias'] == 'faq') {
                    $faqGroups = Faq::model()->getFaqGroups();
                    $faqQuest = Faq::model()->getFaqQuest();

                    $this->render(
                        'faq',
                        [
                            'page' => $page,
                            'faqGroups' => $faqGroups,
                            'faqQuest' => $faqQuest,
                        ]
                    );
                } else {
                    $this->render('page', array('page' => $page));
                }
            }
        } else {
            throw new CHttpException(404, 'Page not found');
        }
    }

    public function actionWishlist()
    {
        $this->redirect(['site/login']);
    }

    /**
     * Account dashboard.
     */
    public function actionAccount()
    {
        $this->pageTitle = Lang::t('account.title.accountTitle');
        $this->noIndex = true;
        $this->redirect(['site/accountSubscription']);
    }

    public function actionAccountPromo()
    {
        $user = Yii::app()->user->id;

        if (!$user) {
            return $this->redirect(['site/login']);
        }

        $user = User::model()->getUserById($user);

        $discounts = Discount::model()->getPersonalDiscounts($user);

        $this->render(
            'accountPromoCodes',
            [
                'discounts' => $discounts,
            ]
        );
    }

    public function actionAccountEdit()
    {
        $action = Yii::app()->request->getPost('action');
        $user = Yii::app()->request->getPost('user');

        if (!empty($user) && in_array($action, array('personal', 'password')) && Yii::app()->request->isAjaxRequest) {
            $model = new AccountForm($action);
            $model->attributes = $user;

            if ($model->validate()) {
                $user_model = User::model();

                if ($action == 'personal') {
                    $rs = $user_model->save($model);

                    if ($rs) {
                        $user_data = $user_model->getUser();

                        $this->json = array(
                            'type' => 'personal',
                            'html' => $this->renderPartial('accountPersonal', array('user' => $user_data), true),
                            'form_html' => $this->renderPartial('accountPersonalForm', array('user' => $user_data), true),
                        );
                    }
                } elseif ($action == 'password') {
                    $rs = $user_model->setPassword($model);

                    if ($rs) {
                        Yii::app()->user->logout(false);
                        $user = $model->getUser();

                        $identity = new UserIdentity($user['user_email'], $model->password);

                        if ($identity->authenticate()) {
                            // 30 days
                            $duration = 3600 * 24 * 30;
                            Yii::app()->user->login($identity, $duration);

                            $this->json = array(
                                'type' => 'password',
                                'msg' => Lang::t('account.tip.passwordChanged'),
                            );
                        } else {
                            $this->json = array(
                                'error' => true,
                                'errorCode' => $identity->errorMessage,
                                'errorFields' => array(),
                            );
                        }
                    }
                }

                if (!$rs) {
                    $this->json = array(
                        'error' => true,
                        'errorCode' => Lang::t('account.error.errorOccured'),
                        'errorFields' => array(),
                    );
                }
            } else {
                $errors = $model->jsonErrors();
                $this->json = array(
                    'error' => true,
                    'errorCode' => '', // implode("<br>\n", $errors['messages']),
                    'errorFields' => $errors['fields'],
                );
            }

            return;
        }

        $this->pageTitle = Lang::t('account.title.accountTitle');
        $this->noIndex = true;

        $account = array(
            'orders' => Order::model()->getUserOrders(6),
            'total' => Order::model()->getUserTotalAmount(),
            'user' => Yii::app()->user->user_data,
            'addresses' => UserAddress::model()->getUserAddresses(100),
            'cards' => UserCard::model()->getUserCards(100),
        );

        $this->render('accountEdit', $account);
    }

    /**
     * Account orders.
     */
    public function actionOrders()
    {
        $this->pageTitle = Lang::t('account.title.ordersHistory');
        $this->noIndex = true;

        $account = array(
            'orders' => Order::model()->getUserOrders(Yii::app()->user->id),
            'total' => Order::model()->getUserTotalAmount(),
            'user' => Yii::app()->user->user_data,
        );

        $this->render('accountOrders', $account);
    }

    /**
     * Customer order details.
     */
    public function actionOrder($order_id)
    {
        $order = Order::model()->getUserOrderById($order_id);

        if (empty($order)) {
            throw new CHttpException(404, 'Page not found');
        }

        $products = Order::model()->getUserOrderProducts($order_id);

        $this->pageTitle = Lang::t('accountOrder.meta.title', array('{order_id}' => $order_id));
        $this->noIndex = true;

        $this->render('accountOrder', array(
            'order' => $order,
            'products' => $products,
        ));
    }

    /**
     * Generate, download, send order invoice.
     */
    public function actionInvoice($order_id)
    {
        $action = Yii::app()->request->getPost('action');
        $email = Yii::app()->request->getPost('email');
        $message = Yii::app()->request->getPost('message');

        $order_model = Order::model();
        $order = $order_model->getUserOrderById($order_id);

        if (empty($order)) {
            throw new CHttpException(404, 'Page not found');
        }

        $pdf_path = Yii::app()->assetManager->getBasePath() . DS . 'order' . DS . $order['order_id'];

        if (!empty($order['invoice_pdf']) && is_file($pdf_path . DS . $order['invoice_pdf'])) {
            $pdf_file = $order['invoice_pdf'];
            $invoice_pdf = $pdf_path . DS . $pdf_file;
        } else {
            // generate PDF file
            $products = $order_model->getUserOrderProducts($order_id);
            $pdf_file = $order_model->generateInvoice($order, $products);

            if ($pdf_file !== false) {
                $invoice_pdf = $pdf_path . DS . $pdf_file;
            }
        }

        if (empty($invoice_pdf)) {
            throw new CHttpException(404, 'Page not found');
        }

        if ($action == 'send' && Yii::app()->request->isAjaxRequest) {
            if (!empty($email) && preg_match('/^([a-z0-9]([\-\_\.]*[a-z0-9])*)+@([a-z0-9]([\-]*[a-z0-9])*\.)+[a-z]{2,}$/i', $email)) {
                // send email
                $body = trim(trim($message) . "\n\n" . Lang::t('pdf.tip.invoiceAttachment'));
                $body = str_replace("\n", "<br>\n", $body);

                $mailer = new SendGridMailer();
                $emails = $mailer->prepareEmailsFromString($email);
                $mailer->send($emails, Lang::t('pdf.tip.emailSubject') . $id, $body, array(
                    array(
                        'name' => $pdf_file,
                        'file' => $invoice_pdf,
                    ),
                ));

                $this->json = array(
                    'message' => Lang::t('pdf.tip.invoiceSent'),
                );
            } else {
                $this->json = array(
                    'error' => true,
                    'errorMessage' => Lang::t('pdf.error.invalidEmail'),
                );
            }
        } else {
            // send file
            Yii::app()->request->sendFile($pdf_file, file_get_contents($invoice_pdf));
        }
    }

    /**
     * Account offers.
     */
    public function actionOffers()
    {
        $this->pageTitle = Lang::t('account.title.offersHistory');
        $this->noIndex = true;

        $account = array(
            'offers' => Order::model()->getUserOffers(),
            'total' => Order::model()->getUserTotalAmount(),
            'user' => Yii::app()->user->user_data,
        );

        $this->render('accountOffers', $account);
    }

    /**
     * Customer offer details.
     */
    public function actionOffer($offer_id)
    {
        $offer = Order::model()->getUserOfferById($offer_id);

        if (empty($offer)) {
            throw new CHttpException(404, 'Page not found');
        }

        $products = Order::model()->getUserOfferProducts($offer_id);

        $this->pageTitle = Lang::t('accountOffer.meta.title', array('{offer_id}' => $offer_id));
        $this->noIndex = true;

        $this->render('accountOffer', array(
            'offer' => $offer,
            'products' => $products,
        ));
    }

    /**
     * Generate, download, send offer invoice.
     */
    public function actionInvoiceOffer($offer_id)
    {
        $action = Yii::app()->request->getPost('action');
        $email = Yii::app()->request->getPost('email');
        $message = Yii::app()->request->getPost('message');

        $order_model = Order::model();
        $offer = $order_model->getUserOfferById($offer_id);

        if (empty($offer)) {
            throw new CHttpException(404, 'Page not found');
        }

        $pdf_path = Yii::app()->assetManager->getBasePath() . DS . 'offer' . DS . $offer['offer_id'];

        if (!empty($offer['offer_pdf']) && is_file($pdf_path . DS . $offer['offer_pdf'])) {
            $pdf_file = $offer['offer_pdf'];
            $offer_pdf = $pdf_path . DS . $pdf_file;
        } else {
            // generate PDF file
            $products = $order_model->getUserOfferProducts($offer_id);

            if (!empty($offer['user_id'])) {
                $seller = User::model()->getUserById($offer['user_id']);
            } else {
                $seller = array();
            }

            $pdf_file = $order_model->generateOffer($offer, $products, $seller);

            if ($pdf_file !== false) {
                $offer_pdf = $pdf_path . DS . $pdf_file;
            }
        }

        if (empty($offer_pdf)) {
            throw new CHttpException(404, 'Page not found');
        }

        if ($action == 'send' && Yii::app()->request->isAjaxRequest) {
            if (!empty($email) && preg_match('/^([a-z0-9]([\-\_\.]*[a-z0-9])*)+@([a-z0-9]([\-]*[a-z0-9])*\.)+[a-z]{2,}$/i', $email)) {
                // send email
                $mailer = new SendGridMailer();
                $emails = $mailer->prepareEmailsFromString($email);

                if (!empty($message)) {
                    $body = trim(trim($message) . "\n\n" . Lang::t('pdf.tip.invoiceAttachment'));
                    $body = '<p>' . str_replace("\n", "<br>\n", $body) . '</p>';
                } else {
                    $body = Lang::t('pdf.title.offerEmail');
                }

                $mailer->send($emails, Lang::t('pdf.title.offer', array('{offer_id}' => $offer['offer_id'])), $body, array(
                    array(
                        'name' => $pdf_file,
                        'file' => $offer_pdf,
                    ),
                ));

                $this->json = array(
                    'message' => Lang::t('pdf.tip.invoiceSent'),
                );
            } else {
                $this->json = array(
                    'error' => true,
                    'errorMessage' => Lang::t('pdf.error.invalidEmail'),
                );
            }
        } else {
            // send file
            Yii::app()->request->sendFile($pdf_file, file_get_contents($offer_pdf));
        }
    }

    /**
     * Login action.
     */
    public function actionLogin()
    {
        $this->pageTitle = Lang::t('login.meta.title');
        $this->pageDescription = '';

        $this->noIndex = true;

        $model = new LoginForm;
        $login = Yii::app()->request->getPost('login');

        $result = array();

        if (!empty($login)) {
            $model->attributes = $login;

            if ($model->validate()) {
                $identity = new UserIdentity($model->login, $model->password);

                if ($identity->authenticate()) {
                    // session duration is 30 days
                    $duration = 3600 * 24 * 30;
                    Yii::app()->user->login($identity, $duration);

                    if (isset($_SESSION['HTTP_REFERER'])) {
                        unset($_SESSION['HTTP_REFERER']);
                        $redirect_url = $this->createUrl('site/confirmSub');
                    } else {
                        $redirect_url = $this->createUrl('site/account');
                    }

                    if (Yii::app()->request->isAjaxRequest) {
                        $user = User::model()->getUser();
                        $result = array(
                            'success' => true,
                            'msg' => Lang::t('login.tip.loggedIn'),
                            'redirect' => $redirect_url,
                            'user' => [
                                'id'    => $user['user_id'],
                                'email' => $user['user_email'],
                                'phone' => $user['user_phone'],
                                'first_name' => $user['first_name'],
                                'last_name'  => $user['last_name'],
                            ]
                        );
                    } else {
                        $this->redirect($redirect_url);
                    }
                } else {
                    $model->addError('login', '');
                    $model->addError('password', '');

                    $errors = $model->jsonErrors();
                    $result = array(
                        'error' => true,
                        'errorCode' => $identity->errorMessage,
                        'errorFields' => $errors['fields'],
                    );
                }
            } else {
                $errors = $model->jsonErrors();
                $result = array(
                    'error' => true,
                    'errorCode' => '', // implode("<br>\n", $errors['messages']),
                    'errorFields' => $errors['fields'],
                );
            }
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->json = $result;
        } else {
            $this->render('login', array('model' => $model, 'result' => $result));
        }
    }

    /**
     * Registration action.
     */
    public function actionRegistration()
    {
        $this->pageTitle = Lang::t('registration.meta.title');
        $model = new RegistrationForm;
        $registration = Yii::app()->request->getPost('registration');

        $result = array();
        if (!empty($registration)) {
            $model->attributes = $registration;

            if ($model->validate()) {
                // add user
                $user_id = User::model()->add($model);
                if (!$user_id) {
                    $result = array(
                        'error' => true,
                        'errorCode' => 'Ошибка при регистрации. Свяжитесь с администрацией для решения проблемы.',
                        'errorFields' => array(),
                    );
                } else {
                    $identity = new UserIdentity($model->email, $model->password);

                    if ($identity->authenticate()) {
                        // session duration is 30 days
                        $duration = 3600 * 24 * 30;
                        Yii::app()->user->login($identity, $duration);

                        if (isset($_SESSION['HTTP_REFERER'])) {
                            unset($_SESSION['HTTP_REFERER']);
                            $redirect_url = $this->createUrl('site/confirmSub');
                        } else {
                            $redirect_url = $this->createUrl('site/account');
                        }

                        $this->redirect($redirect_url);
                    } else {
                        $errors = $model->jsonErrors();
                        $result = array(
                            'error'       => true,
                            'errorCode'   => $identity->errorMessage,
                            'errorFields' => $errors['fields'],
                        );
                    }
                }
            } else {
                $errors = $model->jsonErrors();
                $result = array(
                    'error' => true,
                    'errorCode' => '', // implode("<br>\n", $errors['messages']),
                    'errorFields' => $errors['fields'],
                );
            }
        }

        $this->render('registration', array('data' => $registration, 'result' => $result));
    }

    /**
     * Password reset action.
     */
    public function actionReset()
    {
        $this->pageTitle = Lang::t('resetPassword.meta.title');
        $this->pageDescription = '';

        $this->noIndex = true;

        $model = new ResetForm;
        $reset = Yii::app()->request->getPost('reset');
        $newpassword = Yii::app()->request->getPost('newpassword');

        $result = array();

        if (!empty($reset)) {
            $model->scenario = 'reset';
            $model->attributes = $reset;

            if ($model->validate()) {
                User::model()->setResetToken($model);

                if (Yii::app()->request->isAjaxRequest) {
                    $result = array(
                        'msg' => Lang::t('resetPassword.tip.sendInstructions'),
                    );
                } else {
                    Yii::app()->user->setFlash('reset', Lang::t('resetPassword.tip.sendInstructions'));
                    $this->redirect(array('site/reset'));
                }
            } else {
                $errors = $model->jsonErrors();
                $result = array(
                    'error' => true,
                    'errorCode' => '', // implode("<br>\n", $errors['messages']),
                    'errorFields' => $errors['fields'],
                );
            }
        } elseif (!empty($newpassword)) {
            $model->scenario = 'newpassword';
            $model->attributes = $newpassword;

            if ($model->validate()) {
                $rs = User::model()->setPassword($model, true);

                if ($rs) {
                    $user = $model->getUser();
                    $identity = new UserIdentity($user['user_email'], $model->password);

                    if ($identity->authenticate()) {
                        // session duration is 30 days
                        $duration = 3600 * 24 * 30;
                        Yii::app()->user->login($identity, $duration);

                        $redirect_url = $this->createUrl('site/account');
                    } else {
                        Yii::app()->user->setFlash('login_error', $identity->errorMessage);
                        $redirect_url = $this->createUrl('site/login');
                    }

                    if (Yii::app()->request->isAjaxRequest) {
                        $result = array(
                            'redirect' => $redirect_url,
                        );
                    } else {
                        $this->redirect($redirect_url);
                    }
                } else {
                    $result = array(
                        'error' => true,
                        'errorCode' => Lang::t('resetPassword.error.passwordResetError'),
                        'errorFields' => array(),
                    );
                }
            } else {
                $errors = $model->jsonErrors();
                $result = array(
                    'error' => true,
                    'errorCode' => '', // implode("<br>\n", $errors['messages']),
                    'errorFields' => $errors['fields'],
                );
            }
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->json = $result;
        } else {
            $template = 'resetPassword';

            $uid = Yii::app()->request->getQuery('uid');
            $token = Yii::app()->request->getQuery('token');

            if (!empty($uid) || !empty($token)) {
                $user = User::model()->getUserById($uid);

                if (!empty($user) && $user['user_reset_token'] == $token) {
                    $template = 'newPassword';

                    $result['uid'] = $uid;
                    $result['token'] = $token;
                } else {
                    Yii::app()->user->setFlash('reset_error', 'Invalid or expired reset link! Submit form again to reset password.');
                    $this->redirect(array('site/reset'));
                }
            }

            $this->render($template, array('model' => $model, 'result' => $result));
        }
    }

    /**
     * Logs out user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout(true);
        $this->redirect(Yii::app()->homeUrl);
    }

    /**
     * Sitemap.
     */
    public function actionSitemap()
    {
        $this->pageTitle = 'Карта сайта';
        $this->pageDescription = '';

        $categories = Yii::app()->params->categories;
        $brands = Brand::model()->getBrandsSitemap();
        $products = Product::model()->getProductsSitemap();
        $pages = Page::model()->getPagesSitemap();

        $this->render('sitemap', array(
            'categories' => $categories,
            'brands' => $brands,
            'products' => $products,
            'pages' => $pages,
        ));
    }

    /**
     * Sitemap XML.
     */
    public function actionSitemapXml()
    {
        // отключаем web-логи для ajax запросов
        foreach (Yii::app()->log->routes as $route) {
            if ($route instanceof CWebLogRoute || $route instanceof CProfileLogRoute) {
                $route->enabled = false;
            }
        }

        $date = date('c');

        $categories = Yii::app()->params->categories;
        $brands = Brand::model()->getBrandsSitemap();
        $products = Product::model()->getProductsSitemap();
        $pages = Page::model()->getPagesSitemap();

        // build xml output
        header('Content-type: text/xml');

        $dom = new DOMDocument('1.0', 'UTF-8');
        $dom->formatOutput = true;

        $urlset_el = $dom->createElement('urlset');
        $urlset_el->setAttribute('xmlns', 'https://www.sitemaps.org/schemas/sitemap/0.9/');
        $dom->appendChild($urlset_el);

        $url_el = $dom->createElement('url');
        $loc_el = $dom->createElement('loc', $this->createAbsoluteUrl('site/index'));
        $url_el->appendChild($loc_el);
        $priority_el = $dom->createElement('priority', '1.0');
        $url_el->appendChild($priority_el);
        $changefreq_el = $dom->createElement('changefreq', 'weekly');
        $url_el->appendChild($changefreq_el);
        $lastmod_el = $dom->createElement('lastmod', $date);
        $url_el->appendChild($lastmod_el);
        $urlset_el->appendChild($url_el);

        $url_el = $dom->createElement('url');
        $loc_el = $dom->createElement('loc', $this->createAbsoluteUrl('site/newest'));
        $url_el->appendChild($loc_el);
        $priority_el = $dom->createElement('priority', '0.9');
        $url_el->appendChild($priority_el);
        $changefreq_el = $dom->createElement('changefreq', 'weekly');
        $url_el->appendChild($changefreq_el);
        $lastmod_el = $dom->createElement('lastmod', $date);
        $url_el->appendChild($lastmod_el);
        $urlset_el->appendChild($url_el);

        if (!empty($categories)) {
            foreach ($categories as $category) {
                $url_el = $dom->createElement('url');
                $loc_el = $dom->createElement('loc', $this->createAbsoluteUrl('site/category', array('alias' => $category['category_alias'])));
                $url_el->appendChild($loc_el);
                $priority_el = $dom->createElement('priority', '0.9');
                $url_el->appendChild($priority_el);
                $changefreq_el = $dom->createElement('changefreq', 'weekly');
                $url_el->appendChild($changefreq_el);
                $lastmod_el = $dom->createElement('lastmod', $date);
                $url_el->appendChild($lastmod_el);
                $urlset_el->appendChild($url_el);
            }
        }

        $url_el = $dom->createElement('url');
        $loc_el = $dom->createElement('loc', $this->createAbsoluteUrl('site/brands'));
        $url_el->appendChild($loc_el);
        $priority_el = $dom->createElement('priority', '0.9');
        $url_el->appendChild($priority_el);
        $changefreq_el = $dom->createElement('changefreq', 'weekly');
        $url_el->appendChild($changefreq_el);
        $lastmod_el = $dom->createElement('lastmod', $date);
        $url_el->appendChild($lastmod_el);
        $urlset_el->appendChild($url_el);

        if (!empty($brands)) {
            foreach ($brands as $brand) {
                $url_el = $dom->createElement('url');
                $loc_el = $dom->createElement('loc', $this->createAbsoluteUrl('site/brand', array('alias' => $brand['brand_alias'])));
                $url_el->appendChild($loc_el);
                $priority_el = $dom->createElement('priority', '0.9');
                $url_el->appendChild($priority_el);
                $changefreq_el = $dom->createElement('changefreq', 'weekly');
                $url_el->appendChild($changefreq_el);
                $lastmod_el = $dom->createElement('lastmod', $date);
                $url_el->appendChild($lastmod_el);
                $urlset_el->appendChild($url_el);
            }
        }

        if (!empty($products)) {
            foreach ($products as $product) {
                $url_el = $dom->createElement('url');
                $loc_el = $dom->createElement('loc', $this->createAbsoluteUrl('site/product', array('alias' => $product['product_alias'])));
                $url_el->appendChild($loc_el);
                $priority_el = $dom->createElement('priority', '0.8');
                $url_el->appendChild($priority_el);
                $changefreq_el = $dom->createElement('changefreq', 'weekly');
                $url_el->appendChild($changefreq_el);
                $lastmod_el = $dom->createElement('lastmod', $date);
                $url_el->appendChild($lastmod_el);
                $urlset_el->appendChild($url_el);
            }
        }

        if (!empty($pages)) {
            foreach ($pages as $page) {
                $url_el = $dom->createElement('url');
                $loc_el = $dom->createElement('loc', $this->createAbsoluteUrl('site/page', array('alias' => $page['page_alias'])));
                $url_el->appendChild($loc_el);
                $priority_el = $dom->createElement('priority', '0.5');
                $url_el->appendChild($priority_el);
                $changefreq_el = $dom->createElement('changefreq', 'weekly');
                $url_el->appendChild($changefreq_el);
                $lastmod_el = $dom->createElement('lastmod', $date);
                $url_el->appendChild($lastmod_el);
                $urlset_el->appendChild($url_el);
            }
        }

        echo $dom->saveXML();
    }

    /**
     * FB Feed page.
     */
    public function actionFbFeed()
    {
        /***
         * Parse language from url address.
         */
        $language = str_replace(
            [
                '/fb_feed_',
                '.xml',
            ],
            '',
            Yii::app()->request->geturl()
        );

        /**
         * Get list products by language.
         */
        $products = Product::model()
            ->getProductsGoogleFeed(
                'ua' == $language ? 'uk' : $language
            )
        ;

        /**
         * Create XML outbut to browser.
         */
        header('Content-type: text/xml');

        /**
         * Build a main RSS XML dom.
         */
        $rss = new DOMDocument('1.0', 'UTF-8');
        $rss->formatOutput = true;

        /**
         * Build a main body XML.
         */
        $rssBody = $rss->createElement('rss');
        $rssBody->setAttribute('xmlns:g', 'http://base.google.com/ns/1.0');
        $rssBody->setAttribute('xmlns:c', 'http://base.google.com/cns/1.0');
        $rssBody->setAttribute('version', '2.0');


        /**
         * Build a chennal product.
         */
        $channel = $rss->createElement('channel');

        /**
         * Build link param.
         */
        $link = $rss->createElement(
            'link',
            $language == 'ua' ? Yii::app()->params['site'] : (Yii::app()->params['site'] . $language)
        );

        /**
         * Build link param.
         */
        $title = $rss->createElement(
            'title',
            $this->getFbFeedTitle($language)
        );

        $description = $rss->createElement(
            'description',
            $this->getFbFeedDescription($language)
        );

        $channel->appendChild($title);
        $channel->appendChild($link);
        $channel->appendChild($description);

        $product_model = Product::model();
        $assetsUrl = Yii::app()->assetManager->getBaseUrl();

        foreach ($products as $product) {
            /**
             * Build product item.
             */
            $item = $rss->createElement('item');

            $itemData = $rss->createElement(
                "g:identifier_exists",
                'false'
            );
            $item->appendChild($itemData);
            $itemData = $rss->createElement(
                "g:condition",
                'New'
            );
            $item->appendChild($itemData);

            foreach ($product as $column => $value) {
                if (in_array($column, [
                    'product_photo',
                    'old_price',
                    'custom_condition',
                ])) {
                    continue;
                }

                if (!$updateValue = $this->getValueByColumn($column, $value, $product)) {
                    if (in_array($column, [
                        'sale_price',
                        'availability',
                        'brand',
                    ])) {
                        continue;
                    }
                }

                if(in_array($column, ['title','description','link'])) {
                    $itemData = $rss->createElement(
                        $this->getColumn($column),
                        $updateValue
                    );
                } else {
                    if($column == 'price') {
                        $updateValue = round($updateValue).' UAH';
                    }

                    $itemData = $rss->createElement(
                        "g:{$this->getColumn($column)}",
                        $updateValue
                    );
                }

                $item->appendChild($itemData);

                if($column == 'image_link') {
                    $product_photos = $product_model->getProductPhotos($product['id']);

                    foreach ($product_photos as $photo) {
                        $photo_path = json_decode($photo['photo_path'], true);
                        $url = $assetsUrl.'/product/'.$product['id'].'/'.$photo_path['large']['1x'];

                        $itemData = $rss->createElement(
                            "g:additional_image_link",
                            $url
                        );
                        $item->appendChild($itemData);
                    }
                }
            }

            $itemData = $rss->createElement(
                "g:brand",
                'Fresh Black'
            );
            $item->appendChild($itemData);

            $channel->appendChild($item);
        }

        $rssBody->appendChild($channel);
        $rss->appendChild($rssBody);

        echo $rss->saveXML();
    }

    /**
     * Google Feed page.
     */
    public function actionGoogleFeed()
    {
        /***
         * Parse language from url address.
         */
        $language = str_replace(
            [
                '/google_feed_',
                '.xml',
            ],
            '',
            Yii::app()->request->geturl()
        );

        /**
         * Get list products by language.
         */
        $products = Product::model()
            ->getProductsGoogleFeed(
                'ua' == $language ? 'uk' : $language
            )
        ;

        /**
         * Create XML outbut to browser.
         */
        header('Content-type: text/xml');

        /**
         * Build a main RSS XML dom.
         */
        $rss = new DOMDocument('1.0', 'UTF-8');
        $rss->formatOutput = true;

        /**
         * Build a main body XML.
         */
        $rssBody = $rss->createElement('rss');
        $rssBody->setAttribute('xmlns:g', 'http://base.google.com/ns/1.0');
        $rssBody->setAttribute('version', '2.0');
        

        /**
         * Build a chennal product.
         */
        $channel = $rss->createElement('channel');

        /**
         * Build link param.
         */
        $link = $rss->createElement(
            'link',
            $language == 'ua' ? Yii::app()->params['site'] : (Yii::app()->params['site'] . $language)
        );

        /**
         * Build link param.
         */
        $title = $rss->createElement(
            'title',
            $this->getGoogleFeedTitle($language)
        );

        $channel->appendChild($title);
        $channel->appendChild($link);

        foreach ($products as $product) {

            /**
             * Build product item.
             */
            $item = $rss->createElement('item');

            foreach ($product as $column => $value) {
                if (in_array($column, [
                    'product_photo',
                    'old_price',
                    'custom_condition',
                ])) {
                    continue;
                }

                if (!$updateValue = $this->getValueByColumn($column, $value, $product)) {
                    if (in_array($column, [
                        'sale_price',
                        'availability',
                        'brand',
                    ])) {
                        continue;
                    }
                }

                $itemData = $rss->createElement(
                    "g:{$this->getColumn($column)}",
                    $updateValue
                );
                $item->appendChild($itemData);
            }

            $channel->appendChild($item);
        }

        $rssBody->appendChild($channel);
        $rss->appendChild($rssBody);

        echo $rss->saveXML();
    }

    public function getFbFeedTitle($language)
    {
        $translate = Translation::model()
            ->getTranslationByCode(
                'fb.feed.title',
                $language == 'ua' ? 'uk' : $language
            )
        ;

        return $translate && !empty($translate['translation_value']) ? $translate['translation_value'] : 'fb.feed.title';
    }

    public function getGoogleFeedTitle($language)
    {
        $translate = Translation::model()
            ->getTranslationByCode(
                'google.feed.title',
                $language == 'ua' ? 'uk' : $language
            )
        ;

        return $translate && !empty($translate['translation_value']) ? $translate['translation_value'] : '';
    }

    public function getFbFeedDescription($language)
    {
        $translate = Translation::model()
            ->getTranslationByCode(
                'fb.feed.description',
                $language == 'ua' ? 'uk' : $language
            )
        ;

        return $translate && !empty($translate['translation_value']) ? $translate['translation_value'] : 'fb.feed.description';
    }

    public function getGoogleFeedDescription($language)
    {
        $translate = Translation::model()
            ->getTranslationByCode(
                'google.feed.description',
                $language == 'ua' ? 'uk' : $language
            )
        ;

        return $translate && !empty($translate['translation_value']) ? $translate['translation_value'] : 'google.feed.description';
    }

    public function getColumn($column)
    {
        if (in_array($column, [
            'custom_condition',
            'old_price',
        ])) {
            return 'condition';
        }

        return $column;
    }

    public function getValueByColumn($column, $value, $product)
    {
        // if ('description' === $column) {
        //     return Yii::app()->params['site'] . $this->createUrl('product', [
        //         'alias' => $value
        //     ]);
        // }

        if ('description' === $column) {
            return strtr(strip_tags($value), ['&' => '-']);
        }

        if ('link' === $column) {
           return rtrim(Yii::app()->params['site'], '/') . $this->createUrl('product', [
                'alias' => $value
            ]);
        }

        if ('image_link' === $column) {
            $assetsUrl = Yii::app()->assetManager->getBaseUrl();
            $product_image = json_decode($product['product_photo'], true);

            return "{$assetsUrl}/product/{$value}/" . $product_image['path']['catalog']['1x'];
        }

        if ('custom_condition' === $column) {
            return true == $value ? 'new' : null;
        }

        if ('availability' === $column) {
            return 'in_stock' == $value ? 'in stock' : 'Out of stock';
        }

        if ('price' === $column) {
            if (!empty($product['old_price']) && $product['old_price'] > $product['sale_price']) {
                return $product['old_price'] . "UAH";
            }  else {
                return "{$value} UAH";
            }
        }

        if ('sale_price' === $column) {
            if (!empty($product['old_price']) && $product['old_price'] > $product['sale_price']) {
                return "{$value} UAH";
            }  else {
                return null;
            }
        }

        return $value;
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            /* if ($error['code'] == 404) {
                $request_uri = Yii::app()->request->getRequestUri();
                $base_url = Yii::app()->request->getBaseUrl();

                if (!empty($base_url)) {
                    $clean_uri = preg_replace('#^' . preg_quote($base_url, '#') . '#ui', '', $request_uri);
                } else {
                    $clean_uri = $request_uri;
                }

                if ($clean_uri != '/404') {
                    $this->redirect('/404', true, 301);
                }
            } */

            if (Yii::app()->request->isAjaxRequest) {
                $this->json = array(
                    'error' => true,
                    'errorCode' => $error['message'],
                    'errorFields' => array(),
                );
            } else {
                if ($error['code'] == 404) {
                    $this->render('//site/error404', $error);
                } else {
                    $this->render('//site/error', $error);
                }
            }
        }
    }

    /**
     * This method is invoked right after an action is executed.
     */
    protected function afterAction($action)
    {
        if (Yii::app()->request->isAjaxRequest) {
            header('Vary: Accept');

            if (strpos(Yii::app()->request->getAcceptTypes(), 'application/json') !== false) {
                header('Content-type: application/json; charset=utf-8');
            }

            if (isset($this->json['title'])) {
                $this->json['title'] .=  ' | ' . Yii::app()->name;
            }

            echo json_encode($this->json);
            Yii::app()->end();
        }
    }

    public function actionAccountaddress()
    {
        $action = Yii::app()->request->getPost('action');
        $user = Yii::app()->request->getPost('user');



        $this->pageTitle = Lang::t('account.title.accountUserAddressesTitle');
        $this->noIndex = true;

        $account = array(
            'addresses' => UserAddress::model()->getUserAddresses(6),
            'user' => Yii::app()->user->user_data,
        );

        $this->render('accountAddress', $account);
    }

    public function actionAccountaddressCreate()
    {
        $address = Yii::app()->request->getPost('address');

        if (!empty($address)) {
            $userAddress = UserAddress::model();
            $addressId = $userAddress->add($address);

            if ($addressId > 0) {
                $this->redirect('/accountedit');

                return;
            }
        }

        $this->pageTitle = Lang::t('account.title.accountUserAddressCreateTitle');
        $this->noIndex = true;

        $account = array(
            'user' => Yii::app()->user->user_data,
        );

        $this->render('accountAddressCreate', $account);
    }

    public function actionAccountaddressUpdate($alias)
    {
        $address = Yii::app()->request->getPost('address');

        if (!empty($address)) {
            $userAddress = UserAddress::model();
            $addressId = $userAddress->save($address);

            $this->redirect('/accountAddressUpdate/' . $address['id']);

            return;
        }

        $this->pageTitle = Lang::t('account.title.accountUserAddressEditTitle');
        $this->noIndex = true;

        $address = UserAddress::model()->getUserAddress($alias);

        $data = [
            'user' => Yii::app()->user->user_data,
            'address' => array_shift($address),
        ];

        $this->render('accountAddressUpdate', $data);
    }

    public function actionAccountAddressRemove($alias)
    {
        $userAddress = UserAddress::model();

        $addressId = $userAddress->delete($alias);

        $this->redirect('/accountedit');
    }

    public function actionAccountCardCreate()
    {
        $card = Yii::app()->request->getPost('card');

        if (!empty($card)) {
            $userCard = UserCard::model();
            $cardId = $userCard->add($card);

            if ($cardId > 0) {
                $this->redirect('/accountedit');

                return;
            }
        }

        $this->pageTitle = Lang::t('account.title.accountUserCardCreateTitle');
        $this->noIndex = true;

        $account = array(
            'user' => Yii::app()->user->user_data,
        );

        $this->render('accountCardCreate', $account);
    }

    public function actionAccountCardMain($alias)
    {
        $userCard = UserCard::model();

        $addressId = $userCard->save([
            'id' => $alias,
            'is_main' => true
        ]);

        $this->redirect('/accountedit');
    }

    public function actionAccountCardRemove($alias)
    {
        $userCard = UserCard::model();

        $addressId = $userCard->delete($alias);

        $this->redirect('/accountedit');
    }

    public function actionUserFeedback()
    {
        $feedback = Yii::app()->request->getPost('feedback', null);

        if ($feedback) {
            $feedback['user_id'] = Yii::app()->user->id;
            $result = UserFeedback::model()->saveFeedback($feedback);

            if ($result) {
                $feedback['result'] = UserFeedback::model()->getFeedback($feedback['user_id']);
            }
        }

        if(isset($_FILES['files']) && $feedback['result']) {
            $count = count($_FILES["files"]["name"]);

            for ($i = 0; $i < $count; $i++) {
                $temp = explode(".", $_FILES["files"]["name"][$i]);

                $newFileName = md5($_FILES['files']['name'][$i]) . '.' . end($temp);

                $file['file'] = $newFileName;
                $file['feedback_id'] = $feedback['result']['id'];

                $mainDir = realpath(Yii::getPathOfAlias('system') . DS . '..' . DS . 'www' . DS . 'assets');
                $uploadDir = $mainDir . '/uploads/';

                if ($uploadDir !== false && is_dir($uploadDir)) {
                    $uploadFile = $uploadDir . basename($newFileName);
                    move_uploaded_file($_FILES['files']['tmp_name'][$i], $uploadFile);
                    UserFeedback::model()->saveFile($file);
                } else {
                    mkdir($uploadDir);
                    $uploadFile = $uploadDir . basename($newFileName);
                    move_uploaded_file($_FILES['files']['tmp_name'][$i], $uploadFile);
                    UserFeedback::model()->saveFile($file);
                }
            }
        }

        return $this->redirect(array('site/userFeedbackPage'));
    }

    /**
     * @throws JsonException
     */
    public function actionChangeSubQty()
    {
        $body = Yii::app()->request->getRawBody();
        $userId = Yii::app()->user->id;
        $response = new stdClass();

        if (!empty($body) && $userId) {
            $data = json_decode($body, true, 512);

            if (isset($data['value']) && is_numeric($data['value']) && $data['value'] > 0) {
                $sub = Subscription::model()->getSubByUser($userId);
                $subProduct = Subscription::model()->getSubProduct($sub['id']);
                $result = Subscription::model()->updateSubQty($sub, $subProduct, $data['value']);

                if ($result) {
                    $response->status = true;
                    $response->message = Lang::t('layout.change.qty.message.success');
                    $response->value = $data['value'];
                    return $this->renderJSON($response);
                }
            }
        }

        $response->status = false;
        $response->message = Lang::t('layout.change.qty.message.false');
        return $this->renderJSON($response);
    }

    public function actionUserFeedbackPage()
    {
        $reasons = UserFeedback::model()->getReasons();

        $this->render(
            'accountFeedback',
            [
                'reasons' => $reasons
            ]
        );
    }

    public function actionSql1()
    {
        $date = Yii::app()->request->getQuery('date');

        if(empty($date)) return json_encode([]);

        $sql = "
            WITH
                FirstOrder AS(
                SELECT
                    email,
                    MIN(created) AS first_order_date
                FROM
                    nuare_black.order
                WHERE
                    status = 'paid'
                GROUP BY
                    email
            ),
            OrdersOnTargetDate AS(
                SELECT
                    email,
                    created,
                    price
                FROM
                    nuare_black.order
                WHERE
                    DATE(created) = :date
                    AND status = 'paid'
            ),
            FirstOrderOnTargetDate AS(
                SELECT
                    o.email,
                    o.created,
                    o.price
                FROM
                    OrdersOnTargetDate o
                JOIN FirstOrder f ON
                    o.email = f.email
                WHERE
                    DATE(f.first_order_date) = :date
            )
            SELECT
                'All' AS category,
                COUNT(DISTINCT email) AS total_users,
                COUNT(email) AS total_orders,
                SUM(price) AS total_revenue
            FROM
                OrdersOnTargetDate
            UNION ALL
            SELECT
                'FS' AS category,
                COUNT(DISTINCT email) AS total_fs_users,
                COUNT(email) AS total_fs_orders,
                SUM(price) AS total_fs_revenue
            FROM
                FirstOrderOnTargetDate;
        ";


        $data_rows = Yii::app()->db
            ->createCommand($sql)
            ->bindValue(':date', $date, PDO::PARAM_STR)
            ->queryAll();

        echo json_encode($data_rows);
        exit;
    }

    public function actionSql2()
    {
        $date = Yii::app()->request->getQuery('date');

        if(empty($date)) return json_encode([]);

        $sql = "
            SELECT
                o.order_id,
                o.first_name,
                o.last_name,
             o.phone,
                CONCAT_WS(', ',
                    o.np_city,
                    o.np_department,
                    o.np_address,
                    o.np_building,
                    o.np_apartment
                ) AS np_info,
                GROUP_CONCAT(CONCAT_WS(' ', oi.title, oi.variant_title, oi.options_title) SEPARATOR ', ') AS items,
                o.comment
            FROM
                nuare_black.order o
            JOIN
                nuare_black.order_item oi ON o.order_id = oi.order_id
            WHERE
                DATE(o.created) = :date
                AND o.status = 'paid'
            GROUP BY
                o.order_id;
        ";

        $data_rows = Yii::app()->db
            ->createCommand($sql)
            ->bindValue(':date', $date, PDO::PARAM_STR)
            ->queryAll();

        echo json_encode($data_rows, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        exit;
    }

    public function actionTestPage()
    {
        var_dump(stripos($_SERVER['QUERY_STRING'], 'utm_campaign'));
        if(stripos($_SERVER['QUERY_STRING'], 'utm_campaign') !== false) {
            setcookie("utm_params", $_SERVER['QUERY_STRING'], time()+(60*60*24*30*12), '/');
            $_SESSION['utm_params'] = $_SERVER['QUERY_STRING'];
            var_dump('test');
        }
        die(@$_GET['utm_campaign']);
        exit;
    }
}
