<?php
class ApiController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'apiAccess',
		);
	}

	public function filterApiAccess($filterChain)
	{
		$token = md5(strrev('black') . Yii::app()->request->getQuery('call', ''));

		if (Yii::app()->request->getQuery('token', '') != $token) {
			// доступ только по токену
			throw new CHttpException(401, 'Unauthorized');
		}

		$filterChain->run();
	}

	protected function beforeAction($action)
	{
		// отключаем web-логи для ajax запросов
		foreach (Yii::app()->log->routes as $route) {
			if ($route instanceof CWebLogRoute || $route instanceof CProfileLogRoute) {
				$route->enabled = false;
			}
        }

        return parent::beforeAction($action);
	}

	public function actionCall($call)
	{
		// set maximum execution time to 3 hours
		set_time_limit(60 * 60 * 6);

		$this->$call();
	}

	/**
	 * Update Nova Poshta cities, departments, streets
	 * 
	 * hash: 0769a6233d56880df3567e4a05512bff
	 */
	public function updateNpData()
	{
		(new NovaPoshta())->update();
		echo 'Success!';
	}
}
