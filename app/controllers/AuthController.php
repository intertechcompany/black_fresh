<?php

class AuthController extends Controller
{	
	public function actionFacebook()
	{
		$auth = new AuthUser();

		$auth->setDriver('facebook');

		$this->redirect($auth->getRedirectUrl());
	}

	public function actionGoogle()
	{
		$auth = new AuthUser();

		$auth->setDriver('google');

        $this->redirect($auth->getRedirectUrl());
	}

    public function actionGoogleLink()
    {
        $auth = new AuthUser();
        $auth->setDriver('google');
        $this->redirect($auth->getLinkRedirectUrl());
    }

    public function actionConfirmGoogle()
    {
        $auth = new AuthUser();
        $code = Yii::app()->request->getQuery('code');
        $auth->linkGoogle($code);

        $this->redirect(['site/accountSubscription']);
    }

    public function actionFbLink()
    {
        $auth = new AuthUser();
        $auth->setDriver('facebook');
        $this->redirect($auth->getLinkRedirectUrl());
    }

    public function actionConfirmFb()
    {
        $auth = new AuthUser();
        $auth->linkFacebook();

        $this->redirect(['site/accountSubscription']);
    }

	public function actionLogin()
    {
        $auth = new AuthUser();

        $code = Yii::app()->request->getQuery('code');

        if ($auth->login($code)) {
            if (isset($_SESSION['HTTP_REFERER'])) {
                unset($_SESSION['HTTP_REFERER']);
                $this->redirect($this->createUrl('site/confirmSub'));
            } else {
                $this->redirect($this->createUrl('site/account'));
            }
        } else {
            $this->redirect($this->createUrl('site/login'));
        }
    }

    public function actionLoginFb()
    {
        $auth = new AuthUser();

        $code = Yii::app()->request->getQuery('code');

        if ($auth->loginFb($code)) {
            if (isset($_SESSION['HTTP_REFERER'])) {
                unset($_SESSION['HTTP_REFERER']);
                $this->redirect($this->createUrl('site/confirmSub'));
            } else {
                $this->redirect($this->createUrl('site/account'));
            }
        } else {
            $this->redirect($this->createUrl('site/login'));
        }
    }

	public function actionCallback()
	{

	}
}
