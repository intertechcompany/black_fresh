<?php
class Page extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function getTopMenu()
	{
		$pages_menu = Yii::app()->db
			->createCommand("SELECT p.page_id, p.page_alias, pl.page_title 
							 FROM page as p 
							 JOIN page_lang as pl 
							 ON p.page_id = pl.page_id AND pl.language_code = :code AND pl.page_visible = 1 
							 WHERE p.active = 1 AND p.page_menu = 'top' 
							 ORDER BY p.page_position")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $pages_menu;
	}

	public function getBottomMenu1()
	{
		$pages_menu = Yii::app()->db
			->createCommand("SELECT p.page_id, p.page_alias, pl.page_title 
							 FROM page as p 
							 JOIN page_lang as pl 
							 ON p.page_id = pl.page_id AND pl.language_code = :code AND pl.page_visible = 1 
							 WHERE p.active = 1 AND p.page_menu = 'bottom1' 
							 ORDER BY p.page_position")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $pages_menu;
	}

	public function getBottomMenu2()
	{
		$pages_menu = Yii::app()->db
			->createCommand("SELECT p.page_id, p.page_alias, pl.page_title 
							 FROM page as p 
							 JOIN page_lang as pl 
							 ON p.page_id = pl.page_id AND pl.language_code = :code AND pl.page_visible = 1 
							 WHERE p.active = 1 AND p.page_menu = 'bottom2' 
							 ORDER BY p.page_position")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $pages_menu;
	}
	
	public function getBottomMenu3()
	{
		$pages_menu = Yii::app()->db
			->createCommand("SELECT p.page_id, p.page_alias, pl.page_title 
							 FROM page as p 
							 JOIN page_lang as pl 
							 ON p.page_id = pl.page_id AND pl.language_code = :code AND pl.page_visible = 1 
							 WHERE p.active = 1 AND p.page_menu = 'bottom3' 
							 ORDER BY p.page_position")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $pages_menu;
	}
	
	public function getPage($alias)
	{
		$page = Yii::app()->db
			->createCommand("SELECT p.*, pl.page_title, pl.page_intro, pl.page_description, pl.page_vacancy, pl.page_content as page_content_lang, pl.page_no_index, pl.page_meta_title, pl.page_meta_description 
							 FROM page as p 
							 JOIN page_lang as pl 
							 ON p.page_id = pl.page_id AND pl.language_code = :code AND pl.page_visible = 1 
							 WHERE p.active = 1 AND p.page_alias = :alias 
							 LIMIT 1")
			->bindValue(':alias', $alias, PDO::PARAM_STR)
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryRow();
			
		return $page;
	}

	public function getPagesSitemap()
	{
		$order_by = 'FIELD(page_menu, \'top\',\'bottom1\',\'bottom2\',\'bottom3\'), p.page_position';
		
		$pages = Yii::app()->db
			->createCommand("SELECT p.page_id, p.page_alias, pl.page_title, pl.page_no_index 
							 FROM page as p 
							 JOIN page_lang as pl 
							 ON p.page_id = pl.page_id AND pl.language_code = :code AND pl.page_visible = 1 
							 WHERE p.active = 1 
							 ORDER BY {$order_by}")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $pages;
	}
}