<?php

class UserCard extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function add($params)
	{
		$today = date('Y-m-d H:i:s');

		$params['user_id'] = Yii::app()->user->id;
        $params['created_at'] = $today;
        $params['updated_at'] = $today;
		
		try {
            $result = Yii::app()->db->createCommand()->insert('user_cards', $params);

            if ($params['is_main'] == 1) {
	        	$cardId = (int) Yii::app()->db->getLastInsertID();

	        	return $this->disCheckIsMainCards($cardId);
	        }

	        return $result;
        } catch (CDbException $ex) {
            return false;
        }

        return false;
	}

	public function save($model)
	{
		$model['updated_at'] = date('Y-m-d H:i:s');

		$builder = Yii::app()->db->schema->commandBuilder;

		$update_criteria = new CDbCriteria(
			[
				"condition" => "id = :id" , 
				"params" => [
					"id" => $model['id']
				]
			]
		);

		$this->disCheckIsMainCards($model['id']);

		try {
			return $builder->createUpdateCommand('user_cards', $model, $update_criteria)->execute();
		} catch (CDbException $ex) {
            return false;
        }

        return false;
	}

	public function delete($id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		
		$criteria = new CDbCriteria(
			[
				"condition" => "id = :id" , 
				"params" => [
					"id" => $id,
				]
			]
		);
		
		try {
			$builder->createDeleteCommand('user_cards', $criteria)->execute();

			$cards = $this->getUserCards(1);
			$card = array_shift($cards);

			return $this->disCheckIsMainCards($card['id']);
		}
		catch (CDbException $ex) {
			return false;
		}

		return false;
	}


	public function getUserCards($limit = 20)
	{
		$limit = 'LIMIT 0,' . $limit;

		return Yii::app()->db
			->createCommand("SELECT * FROM `user_cards` WHERE user_id = :user_id ORDER BY id DESC {$limit}")
			->bindValue(':user_id', (int) Yii::app()->user->id, PDO::PARAM_INT)
			->queryAll()
		;
	}

	public function getUserAddress($id)
	{
		return Yii::app()->db
			->createCommand("SELECT * FROM `user_cards` WHERE user_id = :user_id AND id = {$id} ORDER BY id DESC")
			->bindValue(':user_id', (int) Yii::app()->user->id, PDO::PARAM_INT)
			->queryAll()
		;
	}

	public function disCheckIsMainCards($id)
	{
		return Yii::app()->db
			->createCommand("UPDATE user_cards SET is_main = :is_main WHERE user_id = :user_id AND user_cards.id != :id")
			->bindValue(':id', $id, PDO::PARAM_STR)
			->bindValue(':is_main', 0, PDO::PARAM_STR)
			->bindValue(':user_id', (int) Yii::app()->user->id, PDO::PARAM_INT)
			->execute()
		;
	}
}
