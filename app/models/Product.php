<?php
class Product extends CModel
{
	private $per_page = 10;
	private $catalog_fields = [
		'p.product_id',
		'p.product_alias',
		'p.product_sku',
		'p.category_id',
		'p.brand_id',
		'p.collection_id',
		'p.product_extended',
		'p.product_photo',
		'p.product_price_type',
		'p.product_price',
		'p.product_price_old',
		'p.product_instock',
		'p.product_stock_qty',
		'p.product_weight',
		'pl.product_title',
		'pl.product_special_title',
		'pl.product_special_tip',
		'pl.product_tags',
	];

	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function subtractStockQty($order_product)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$product_id = $order_product['product_id'];
		$variant_id = $order_product['variant_id'];
		$quantity = $order_product['quantity'];
		
		if (!empty($variant_id)) {
			$variant_qty = $order_product['variant_stock_qty'] - $quantity;
			
			$update = array(
				'saved' => $today,
				'variant_instock' => ($variant_qty > 0) ? 'in_stock' : 'out_of_stock',
				'variant_stock_qty' => $variant_qty,
			);
	
			$criteria = new CDbCriteria(
				array(
					"condition" => "variant_id = :id" , 
					"params" => array(
						"id" => (int) $variant_id,
					)
				)
			);
			
			try {
				$variant_rs = $builder->createUpdateCommand('product_variant', $update, $criteria)->execute();
	
				if ($variant_rs) {
					return true;
				}
			} catch (CDbException $e) {
				
			}
		} else {
			$product_qty = $order_product['product_stock_qty'] - $quantity;
			
			$update = array(
				'saved' => $today,
				'product_instock' => ($product_qty > 0) ? 'in_stock' : 'out_of_stock',
				'product_stock_qty' => $product_qty,
			);
	
			$criteria = new CDbCriteria(
				array(
					"condition" => "product_id = :id" , 
					"params" => array(
						"id" => (int) $product_id,
					)
				)
			);
			
			try {
				$product_rs = $builder->createUpdateCommand('product', $update, $criteria)->execute();
	
				if ($product_rs) {
					return true;
				}
			} catch (CDbException $e) {
				
			}
		}

		return false;
	}

	public function updateProductsStockStatus($products)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');
		
		foreach ($products as $product_id => $product_type) {
			$to_last_size = false;
			
			if ($product_type == 'variants') {
				// get in stock value
				$variants_summary = Yii::app()->db
					->createCommand("SELECT v.variant_id, vv.variant_instock, SUM(v.variant_stock_qty) as variant_stock_qty 
						FROM product_variant as v
						JOIN (
							SELECT MIN(variant_instock) as variant_instock 
							FROM product_variant 
							WHERE product_id = :pid
						) as vv
						WHERE v.product_id = :pid 
						LIMIT 1")
					->bindValue(':pid', $product_id, PDO::PARAM_INT)
					->queryRow();
				
				$update_product = array(
					'saved' => $today,
					'product_instock' => $variants_summary['variant_instock'],
					'product_stock_qty' => $variants_summary['variant_stock_qty'],
				);

				$variants_available = Yii::app()->db
					->createCommand("SELECT COUNT(*) 
						FROM product_variant as v
						WHERE v.product_id = :pid AND v.variant_stock_qty > 0
						LIMIT 1")
					->bindValue(':pid', $product_id, PDO::PARAM_INT)
					->queryScalar();

				$to_last_size = ($variants_available == 1) ? true : false;
			} else {
				$product_summary = Yii::app()->db
					->createCommand("SELECT p.product_instock, p.product_stock_qty 
						FROM product as p
						WHERE p.product_id = :pid 
						LIMIT 1")
					->bindValue(':pid', $product_id, PDO::PARAM_INT)
					->queryRow();
				
				$update_product = array(
					'saved' => $today,
					'product_instock' => ($product_summary['product_stock_qty'] > 0) ? 'in_stock' : 'out_of_stock',
					'product_stock_qty' => $product_summary['product_stock_qty'],
				);

				$to_last_size = ($product_summary['product_stock_qty'] == 1) ? true : false;
			}

			// update product
			$update_criteria = new CDbCriteria(
				array(
					"condition" => "product_id = :product_id" , 
					"params" => array(
						"product_id" => $product_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('product', $update_product, $update_criteria)->execute();
			
				if (Yii::app()->params->settings['last_size'] && $rs) {
					if ($to_last_size) {
						// move to last size
						$insert_category = [
							'product_id' => $product_id,
							'category_id' => Yii::app()->params->settings['last_size_cid'],
						];

						try {
							$builder->createMultipleInsertCommand('category_product', $insert_category)->execute();
						} catch (CDbException $e) {
							
						}
					} else {
						// remove from last size
						$delete_criteria = new CDbCriteria(
							array(
								"condition" => "product_id = :product_id AND category_id = :category_id" , 
								"params" => array(
									"product_id" => $product_id,
									"category_id" => Yii::app()->params->settings['last_size_cid'],
								)
							)
						);
						
						try {
							$builder->createDeleteCommand('category_product', $delete_criteria)->execute();
						}
						catch (CDbException $e) {
							// ...
						}
					}
				}
			} catch (CDbException $e) {
				// ...
			}
		}
	}

	private function getCatalogFields()
	{
		return implode(', ', $this->catalog_fields);
	}

	public function getNewestProductsTotal($per_page)
	{
		$date = new DateTime('today', new DateTimeZone(Yii::app()->timeZone));
		$date->modify('-' . (int) Yii::app()->params->settings['newest'] . ' day');

		$total = Yii::app()->db
			->createCommand("SELECT COUNT(*)
							 FROM product as p 
							 WHERE p.active = 1 AND p.created >= :start_date") // p.product_newest = 1
			->bindValue(':start_date', $date->format('Y-m-d'), PDO::PARAM_INT)
			->queryScalar();
			
		return array(
			'total' => (int) $total,
			'pages' => ceil($total / $per_page),
		);
	}
	
	public function getNewestProducts($offset, $per_page, $sort)
	{
		$date = new DateTime('today', new DateTimeZone(Yii::app()->timeZone));
		$date->modify('-' . (int) Yii::app()->params->settings['newest'] . ' day');
		
		switch ($sort) {
			case 'price-asc':
				$order_by = 'p.product_price';
				break;
			case 'price-desc':
				$order_by = 'p.product_price DESC';
				break;
			default:
				$order_by = 'p.product_rating DESC, p.product_id DESC';
		}
		
		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 LEFT JOIN brand as b 
							 ON p.brand_id = b.brand_id AND b.active = 1 
							 LEFT JOIN brand_lang as bl 
							 ON b.brand_id = bl.brand_id AND bl.language_code = :code AND bl.brand_visible = 1 
							 WHERE p.active = 1 AND p.created >= :start_date
							 ORDER BY {$order_by} 
							 LIMIT {$offset},{$per_page}") // AND p.product_newest = 1 
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':start_date', $date->format('Y-m-d'), PDO::PARAM_INT)
			->queryAll();

		$products = $this->getProductsVariants($products);
			
		return $products;
	}
	
	public function getSaleProductsTotal($per_page, $categories, $brands)
	{
		if (empty($categories) && empty($brands)) {
			return array(
				'total' => 0,
				'pages' => 0,
			);
		}

		$where = [];

		if (!empty($categories)) {
			$additional_categories = "SELECT product_id FROM category_product WHERE category_id IN (" . implode(',', $categories) . ")";
			$where[] = "(p.category_id IN (" . implode(',', $categories) . ") OR p.product_id IN ({$additional_categories}))";
		}

        if (!empty($brands)) {
			$where[] = "p.brand_id IN (" . implode(',', $brands) . ")";
        }

		$total = Yii::app()->db
			->createCommand("SELECT COUNT(*)
							 FROM product as p 
							 LEFT JOIN brand as b 
							 ON p.brand_id = b.brand_id AND b.active = 1 
							 LEFT JOIN brand_lang as bl 
							 ON b.brand_id = bl.brand_id AND bl.language_code = :code AND bl.brand_visible = 1 
							 WHERE p.active = 1 AND (" . implode(' OR ', $where) . ")")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryScalar();
			
		return array(
			'total' => (int) $total,
			'pages' => ceil($total / $per_page),
		);
	}
	
	public function getSaleProducts($offset, $per_page, $sort, $categories, $brands)
	{
		if (empty($categories) && empty($brands)) {
			return [];
		}
		
		switch ($sort) {
			case 'price-asc':
				$order_by = 'p.product_price';
				break;
			case 'price-desc':
				$order_by = 'p.product_price DESC';
				break;
			default:
				$order_by = 'p.product_rating DESC, p.product_id DESC';
		}

		$where = [];

		if (!empty($categories)) {
			$additional_categories = "SELECT product_id FROM category_product WHERE category_id IN (" . implode(',', $categories) . ")";
			$where[] = "(p.category_id IN (" . implode(',', $categories) . ") OR p.product_id IN ({$additional_categories}))";
		}

        if (!empty($brands)) {
			$where[] = "p.brand_id IN (" . implode(',', $brands) . ")";
        }
		
		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 LEFT JOIN brand as b 
							 ON p.brand_id = b.brand_id AND b.active = 1 
							 LEFT JOIN brand_lang as bl 
							 ON b.brand_id = bl.brand_id AND bl.language_code = :code AND bl.brand_visible = 1 
							 WHERE p.active = 1 AND (" . implode(' OR ', $where) . ") 
							 ORDER BY {$order_by} 
							 LIMIT {$offset},{$per_page}")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		$products = $this->getProductsVariants($products);
			
		return $products;
	}

	public function getBestSellersProducts()
	{
		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 WHERE p.active = 1 AND p.product_bestseller = 1 
							 ORDER BY p.product_rating DESC, p.product_id DESC 
							 LIMIT 0,4")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		$products = $this->getProductsVariants($products);
		$products = $this->getListProductsProperties($products);
			
		return $products;
	}

	public function getSalesProducts()
	{
		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 WHERE p.active = 1 AND p.product_sale = 1 
							 ORDER BY p.product_rating DESC, p.product_id DESC 
							 LIMIT 0,20")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		$products = $this->getProductsVariants($products);
			
		return $products;
	}

	public function getCategoryProductsTotal($per_page, $category_id, $facets)
	{
		$categories = Yii::app()->params->categories;
		
		$join = '';
		$where = '';
		$additional_categories = "SELECT product_id FROM category_product WHERE category_id IN (" . implode(',', $categories[$category_id]['children']) . ")";

		if ($facets->hasFilter()) {
			$facets_sql = $facets->getFacetFilterSql();

			$join = $facets_sql['join'];
			$where = ' AND ' . $facets_sql['where'];
		}

		$total = Yii::app()->db
			->createCommand("SELECT COUNT(*)
							 FROM product as p 
							 {$join}
							 WHERE p.active = 1 AND (p.category_id IN (" . implode(',', $categories[$category_id]['children']) . ") OR p.product_id IN ({$additional_categories})) {$where}")
			->bindValue(':category_id', (int) $category_id, PDO::PARAM_INT)
			->queryScalar();
			
		return array(
			'total' => (int) $total,
			'pages' => ceil($total / $per_page),
		);
	}

	public function getCategoryProducts($offset, $per_page, $category_id, $facets, $sort)
	{
		$categories = Yii::app()->params->categories;

		$join = '';
		$where = '';
		$additional_categories = "SELECT product_id FROM category_product WHERE category_id IN (" . implode(',', $categories[$category_id]['children']) . ")";

		switch ($sort) {
			case 'price-asc':
				$order_by = 'p.product_price';
				break;
			case 'price-desc':
				$order_by = 'p.product_price DESC';
				break;
			default:
				$order_by = 'p.product_rating DESC, p.product_id DESC';
		}

		if ($facets->hasFilter()) {
			$facets_sql = $facets->getFacetFilterSql();

			$join = $facets_sql['join'];
			$where = ' AND ' . $facets_sql['where'];
		}

		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1  
							 {$join}
							 WHERE p.active = 1 AND (p.category_id IN (" . implode(',', $categories[$category_id]['children']) . ") OR p.product_id IN ({$additional_categories})) {$where} 
							 ORDER BY {$order_by} 
							 LIMIT {$offset},{$per_page}")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			// ->bindValue(':category_id', (int) $category_id, PDO::PARAM_INT)
			->queryAll();
		
		$products = $this->getProductsVariants($products);
		$products = $this->getListProductsProperties($products);
			
		return $products;
	}

	public function getBrandProductsTotal($per_page, $brand_id, $facets)
	{
		$join = '';
		$where = '';

		if ($facets->hasFilter()) {
			$facets_sql = $facets->getFacetFilterSql();

			$join = $facets_sql['join'];
			$where = ' AND ' . $facets_sql['where'];
		}

		$total = Yii::app()->db
			->createCommand("SELECT COUNT(*)
							 FROM product as p 
							 {$join}
							 WHERE p.active = 1 AND p.brand_id = :brand_id {$where}")
			->bindValue(':brand_id', (int) $brand_id, PDO::PARAM_INT)
			->queryScalar();
			
		return array(
			'total' => (int) $total,
			'pages' => ceil($total / $per_page),
		);
	}

	public function getBrandProducts($offset, $per_page, $brand_id, $facets, $sort)
	{
		$join = '';
		$where = '';

		switch ($sort) {
			case 'price-asc':
				$order_by = 'p.product_price';
				break;
			case 'price-desc':
				$order_by = 'p.product_price DESC';
				break;
			default:
				$order_by = 'p.product_rating DESC, p.product_id DESC';
		}

		if ($facets->hasFilter()) {
			$facets_sql = $facets->getFacetFilterSql();

			$join = $facets_sql['join'];
			$where = ' AND ' . $facets_sql['where'];
		}

		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 LEFT JOIN brand as b 
							 ON p.brand_id = b.brand_id AND b.active = 1 
							 LEFT JOIN brand_lang as bl 
							 ON b.brand_id = bl.brand_id AND bl.language_code = :code AND bl.brand_visible = 1 
							 {$join}
							 WHERE p.active = 1 AND p.brand_id = :brand_id {$where} 
							 ORDER BY {$order_by} 
							 LIMIT {$offset},{$per_page}")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':brand_id', (int) $brand_id, PDO::PARAM_INT)
			->queryAll();

		$products = $this->getProductsVariants($products);
			
		return $products;
	}

	public function getCategoryCollectionProducts($category_id, $collection_id)
	{
		$order_by = 'p.product_rating DESC, p.product_id DESC';

		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 WHERE p.active = 1 AND p.category_id = :category_id AND p.collection_id = :collection_id 
							 ORDER BY {$order_by}")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':category_id', (int) $category_id, PDO::PARAM_INT)
			->bindValue(':collection_id', (int) $collection_id, PDO::PARAM_INT)
			->queryAll();
			
		return $products;
	}

	public function getCollectionProducts($collection_id, $product_id)
	{
		$order_by = 'p.product_rating DESC, p.product_id DESC';

		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 WHERE p.active = 1 AND p.collection_id = :collection_id AND p.product_id != :product_id 
							 ORDER BY {$order_by}
							 LIMIT 0,10")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':collection_id', (int) $collection_id, PDO::PARAM_INT)
			->bindValue(':product_id', (int) $product_id, PDO::PARAM_INT)
			->queryAll();
			
		return $products;
	}

	public function getProductsRelated($product_id)
    {
        $order_by = 'p.product_rating DESC, p.product_id DESC';

        $products = Yii::app()->db
            ->createCommand("SELECT {$this->getCatalogFields()} 
								FROM product as p 
								JOIN product_lang as pl 
								ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
								JOIN related_products as rp 
								ON rp.related_id = p.product_id
								WHERE p.active = 1 AND rp.product_id = :product_id AND p.product_id != :product_id 
								ORDER BY {$order_by}
								")
            ->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
            ->bindValue(':product_id', (int) $product_id, PDO::PARAM_INT)
            ->queryAll();

        $products = $this->getProductsVariants($products);
        $products = $this->getListProductsProperties($products);

        return $products;
    }

	public function getRelatedProducts($product_id, $brand_id, $category_id)
	{
		if (empty($brand_id) && empty($category_id)) {
			return array();
		}
		
		$order_by = 'p.product_rating DESC, p.product_id DESC';

		if (!empty($brand_id) && !empty($category_id)) {
			$products = Yii::app()->db
				->createCommand("SELECT {$this->getCatalogFields()} 
								FROM product as p 
								JOIN product_lang as pl 
								ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
								LEFT JOIN brand as b 
								ON p.brand_id = b.brand_id AND b.active = 1 
								LEFT JOIN brand_lang as bl 
								ON b.brand_id = bl.brand_id AND bl.language_code = :code AND bl.brand_visible = 1 
								WHERE p.active = 1 AND p.brand_id = :brand_id AND p.category_id != :category_id AND p.product_id != :product_id 
								ORDER BY {$order_by}
								LIMIT 0,3")
				->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
				->bindValue(':brand_id', (int) $brand_id, PDO::PARAM_INT)
				->bindValue(':category_id', (int) $category_id, PDO::PARAM_INT)
				->bindValue(':product_id', (int) $product_id, PDO::PARAM_INT)
				->queryAll();
		} elseif (!empty($category_id)) {
			$products = Yii::app()->db
				->createCommand("SELECT {$this->getCatalogFields()} 
								FROM product as p 
								JOIN product_lang as pl 
								ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
								LEFT JOIN brand as b 
								ON p.brand_id = b.brand_id AND b.active = 1 
								LEFT JOIN brand_lang as bl 
								ON b.brand_id = bl.brand_id AND bl.language_code = :code AND bl.brand_visible = 1 
								WHERE p.active = 1 AND p.category_id = :category_id AND p.product_id != :product_id 
								ORDER BY {$order_by}
								LIMIT 0,4")
				->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
				->bindValue(':category_id', (int) $category_id, PDO::PARAM_INT)
				->bindValue(':product_id', (int) $product_id, PDO::PARAM_INT)
				->queryAll();
		}

		$products = $this->getProductsVariants($products);
		$products = $this->getListProductsProperties($products);
			
		return $products;
	}

	public function getProductsByIds($product_ids, $get_cart_properties = false, $isDiscount = false)
	{
	    $productsArr = [];
		$products = [];
		$productIdsList = $product_ids;

		$products_list = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 LEFT JOIN brand as b 
							 ON p.brand_id = b.brand_id AND b.active = 1 
							 LEFT JOIN brand_lang as bl 
							 ON b.brand_id = bl.brand_id AND bl.language_code = :code AND bl.brand_visible = 1 
							 WHERE p.active = 1 AND p.product_id IN (" . implode(',', $product_ids) . ")")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		if (!empty($products_list)) {
			$product_ids = array();

			foreach ($products_list as $product) {
				$product_id = $product['product_id'];

				$products[$product_id] = $product;
				$products[$product_id]['properties'] = array();

				if ($product['product_price_type'] != 'variants') {
					$product_ids[] = $product_id;
				}
			}

			if ($get_cart_properties && !empty($product_ids)) {
				// get product properties
				$products = $this->getProductsProperties($products, $product_ids);
			}
		}

		if ($isDiscount) {
            foreach ($productIdsList as $productId) {
                foreach ($products as $product) {
                    if ($productId == $product['product_id']) {
                        $productsArr[] = $product;
                    }
                }
            }

            return $productsArr;
        }

		return $products;
	}

	public function getProductsPropertiesList($product_ids)
	{
		$properties = [];
		
		$properties_list = Yii::app()->db
			->createCommand("SELECT pp.product_id, p.*, pl.property_title, v.value_id, v.value_icon, vl.value_title, v.is_delicious, v.delicious_icon 
							 FROM property_product as pp 
							 JOIN property as p 
							 ON pp.property_id = p.property_id 
							 JOIN property_lang as pl 
							 ON p.property_id = pl.property_id AND pl.language_code = :code AND pl.property_visible = 1 
							 JOIN property_value as v 
							 ON pp.value_id = v.value_id 
							 JOIN property_value_lang as vl 
							 ON v.value_id = vl.value_id AND vl.language_code = :code AND vl.value_visible = 1 
							 WHERE pp.product_id IN (" . implode(',', $product_ids) . ")  
							 ORDER BY p.property_top DESC, pl.property_title")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		if (!empty($properties_list)) {
			foreach ($properties_list as $property) {
				$product_id = $property['product_id'];
				$properties[$product_id][] = $property;
			}
		}

		return $properties;
	}

	public function getProductsProperties($products, $product_ids)
	{
		$properties_list = Yii::app()->db
			->createCommand("SELECT pp.product_id, p.property_id, pl.property_title, v.value_id, vl.value_title 
							 FROM property_product as pp 
							 JOIN property as p 
							 ON pp.property_id = p.property_id 
							 JOIN property_lang as pl 
							 ON p.property_id = pl.property_id AND pl.language_code = :code AND pl.property_visible = 1 
							 JOIN property_value as v 
							 ON pp.value_id = v.value_id 
							 JOIN property_value_lang as vl 
							 ON v.value_id = vl.value_id AND vl.language_code = :code AND vl.value_visible = 1 
							 WHERE pp.product_id IN (" . implode(',', $product_ids) . ") AND p.property_cart = 1 
							 ORDER BY p.property_top DESC, pl.property_title")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		if (!empty($properties_list)) {
			foreach ($properties_list as $property) {
				$product_id = $property['product_id'];

				if (isset($products[$product_id])) {
					$products[$product_id]['properties'][] = $property;
				}
			}
		}

		return $products;
	}

	public function getProductsByKeyword($keyword, $sort)
	{
		/*
		$keyword_plus = '+' . preg_replace('#\s+#u', ' +', $keyword);
		$keyword_quoted = '"' . $keyword . '"';
		$keyword_sql = '(' . $keyword_plus . ') (' . $keyword_quoted . ')';
		*/

		$keyword = addcslashes($keyword, '%_');
		$keyword_sql = preg_replace('#\s+#u', '%', $keyword);

		switch ($sort) {
			case 'price-asc':
				$order_by = 'p.product_price';
				break;
			case 'price-desc':
				$order_by = 'p.product_price DESC';
				break;
			default:
				$order_by = 'p.product_rating DESC, p.product_id DESC';
		}

		/*
		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 WHERE p.active = 1 AND MATCH (pl.product_title) AGAINST (:keyword IN BOOLEAN MODE) 
							 ORDER BY MATCH (pl.product_title) AGAINST (:keyword IN BOOLEAN MODE) DESC")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':keyword', $keyword_sql, PDO::PARAM_STR)
			->queryAll();
		*/
	
		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 LEFT JOIN brand as b 
							 ON p.brand_id = b.brand_id AND b.active = 1 
							 LEFT JOIN brand_lang as bl 
							 ON b.brand_id = bl.brand_id AND bl.language_code = :code AND bl.brand_visible = 1 
							 WHERE p.active = 1 AND (p.product_sku = :sku OR pl.product_brand LIKE :keyword) 
							 ORDER BY {$order_by}")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':sku', $keyword, PDO::PARAM_STR)
			->bindValue(':keyword', '%' . $keyword_sql . '%', PDO::PARAM_STR)
			->queryAll();
		
		$products = $this->getProductsVariants($products);
			
		return $products;
	}

	private function getProductsVariants($products)
	{
		if (Yii::app()->params->settings['quick_buy'] && !empty($products)) {
			// get variants for products
			$product_ids = array_column($products, 'product_id');

            foreach ($products as $index => $product) {
                if ($product['product_price_type'] == 'variants') {
					$variants = $this->getProductVariants($product['product_id']);
					$products[$index] = array_merge($products[$index], $variants);
                }
            }
		}

		return $products;
	}

	private function getProductsSizes($product_ids)
	{
		$sizes = [];
		
		$varaint_fields = [
			'v.variant_id',
			'v.product_id',
			'v.variant_sku',
			'v.variant_price',
			'v.variant_price_old',
			'v.variant_instock',
			'v.variant_stock_qty',
			'pvl.value_title',
		];
		
		$sizes_list = Yii::app()->db
			->createCommand("SELECT " . implode(', ', $varaint_fields) . " 
							 FROM product_variant as v 
							 JOIN product_variant_value as vv 
							 ON v.variant_id = vv.variant_id 
							 JOIN property_value as pv 
							 ON vv.value_id = pv.value_id 
							 JOIN property_value_lang as pvl 
							 ON pv.value_id = pvl.value_id AND pvl.language_code = :code AND pvl.value_visible = 1 
							 JOIN property as p 
							 ON pv.property_id = p.property_id 
							 WHERE v.product_id IN (" . implode(',' , $product_ids) . ") AND p.property_size = 1 
							 ORDER BY v.position")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		foreach ($sizes_list as $size) {
			$product_id = $size['product_id'];
			$variant_id = $size['variant_id'];

			$sizes[$product_id][$variant_id] = $size;
		}

		return $sizes;
	}

	private function getListProductsProperties($products)
	{
		if (!empty($products)) {
			// get properties for products
			$product_ids = array_column($products, 'product_id');

			$properties = $this->getProductsPropertiesList($product_ids);

			if (!empty($properties)) {
				foreach ($products as $index => $product) {
					if (isset($properties[$product['product_id']])) {
						$products[$index]['props'] = $properties[$product['product_id']];
					}
				}
			}
		}

		return $products;
	}

	public function getProductById($product_id)
	{
		$product = Yii::app()->db
			->createCommand("SELECT * FROM product as p JOIN product_lang as pl ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 WHERE p.active = 1 AND p.product_id = :product_id LIMIT 1")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':product_id', (int) $product_id, PDO::PARAM_INT)
			->queryRow();

		return $product;
	}

	public function getProductByAlias($alias)
	{
		$product = Yii::app()->db
			->createCommand("SELECT * FROM product as p JOIN product_lang as pl ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 WHERE p.active = 1 AND p.product_alias = :alias LIMIT 1")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':alias', $alias, PDO::PARAM_STR)
			->queryRow();

		return $product;
	}

    public function getProductDisabledByAlias($alias)
    {
        $product = Yii::app()->db
            ->createCommand("SELECT * FROM product as p JOIN product_lang as pl ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 WHERE p.active = 0 AND p.product_alias = :alias LIMIT 1")
            ->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
            ->bindValue(':alias', $alias, PDO::PARAM_STR)
            ->queryRow();

        return $product;
    }

	public function getProductProperties($product_id)
	{
		$product_properties = array();

		$values = Yii::app()->db
			->createCommand("SELECT pp.property_id, v.value_id, v.value_icon, vl.value_title, v.is_delicious, v.delicious_icon 
							 FROM property_product as pp 
							 JOIN property_value as v 
							 ON pp.value_id = v.value_id 
							 JOIN property_value_lang as vl 
							 ON v.value_id = vl.value_id AND vl.language_code = :code AND vl.value_visible = 1 
							 WHERE pp.product_id = :id 
							 ORDER BY v.value_top DESC, v.value_position, vl.value_title")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':id', (int) $product_id, PDO::PARAM_STR)
			->queryAll();

		if (!empty($values)) {
			$property_ids = array_column($values, 'property_id');

			$properties_list = Yii::app()->db
				->createCommand("SELECT p.*, pl.property_title 
								 FROM property as p 
								 JOIN property_lang as pl 
								 ON p.property_id = pl.property_id AND pl.language_code = :code AND pl.property_visible = 1 
								 WHERE p.property_id IN (" . implode(',', $property_ids) . ")  
								 ORDER BY p.property_top DESC, pl.property_title")
				->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
				->queryAll();

			if (!empty($properties_list)) {
				foreach ($properties_list as $property) {
					$property_id = $property['property_id'];

					$product_properties[$property_id] = $property;
				}

				foreach ($values as $value) {
					$property_id = $value['property_id'];

					if (isset($product_properties[$property_id])) {
						$product_properties[$property_id]['value_id'] = $value['value_id'];
						$product_properties[$property_id]['value_title'] = $value['value_title'];
						$product_properties[$property_id]['is_delicious'] = $value['is_delicious'];

						if ($value['is_delicious']) {
                            $product_properties[$property_id]['delicious'][$value['value_id']]['is_delicious'] = $value['is_delicious'];
                            $product_properties[$property_id]['delicious'][$value['value_id']]['delicious_icon'] = $value['delicious_icon'];
                        }
					}
				}
			}
		}

		return $product_properties;
	}

	public function getProductPhotos($product_id)
	{
		$product_photos = Yii::app()->db
			->createCommand("SELECT * FROM product_photo WHERE product_id = :id ORDER BY position")
			->bindValue(':id', (int) $product_id, PDO::PARAM_STR)
			->queryAll();

		return $product_photos;
	}

	public function getProductVariants($product_id)
	{
		$product_variants = array(
			'variants' => array(),
			'properties' => array(),
			'values' => array(),
		);

		$variants_list = Yii::app()->db
			->createCommand("SELECT pv.variant_id, pv.variant_sku, pv.variant_price, pv.variant_price_old, pv.variant_price_type, pv.variant_instock 
							 FROM product_variant as pv 
							 WHERE pv.product_id = :id 
							 ORDER BY pv.position")
			->bindValue(':id', (int) $product_id, PDO::PARAM_STR)
			->queryAll();

		if (!empty($variants_list)) {
			foreach ($variants_list as $variant) {
				$variant_id = $variant['variant_id'];
				
				$product_variants['variants'][$variant_id] = $variant;
				$product_variants['variants'][$variant_id]['sizes'] = array();
				$product_variants['variants'][$variant_id]['values'] = array();
				$product_variants['variants'][$variant_id]['photos'] = array();
			}

			$variant_ids = array_keys($product_variants['variants']);

			$variant_values = Yii::app()->db
				->createCommand("SELECT pvv.variant_id, p.property_size, v.value_id, v.property_id, vl.value_title 
								 FROM product_variant_value as pvv 
								 JOIN property_value as v 
								 ON pvv.value_id = v.value_id 
								 JOIN property_value_lang as vl 
								 ON v.value_id = vl.value_id AND vl.language_code = :code AND vl.value_visible = 1 
								 JOIN property as p 
								 ON v.property_id = p.property_id 
								 WHERE pvv.variant_id IN (" . implode(',', $variant_ids) . ")
								 ORDER BY v.value_top DESC, v.value_position, vl.value_title")
				->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
				->queryAll();

			if (!empty($variant_values)) {
				$values = array();
			
				foreach ($variant_values as $variant_value) {
					$variant_id = $variant_value['variant_id'];
					$property_id = $variant_value['property_id'];
					$value_id = $variant_value['value_id'];
					
					if (!isset($values[$property_id][$value_id])) {
						$values[$property_id][$value_id] = array(
							'value_id' => $value_id,
							'value_title' => $variant_value['value_title'],
						);
					}

					$product_variants['variants'][$variant_id]['values'][$property_id] = $value_id;

					if ($variant_value['property_size']) {
						$product_variants['variants'][$variant_id]['sizes'][] = $value_id;
					}
				}

				$property_ids = array_keys($values);

				$properties = Yii::app()->db
					->createCommand("SELECT p.property_id, pl.property_title 
									 FROM property as p 
									 JOIN property_lang as pl 
									 ON p.property_id = pl.property_id AND pl.language_code = :code AND pl.property_visible = 1
									 WHERE p.property_id IN (" . implode(',', $property_ids) . ")
									 ORDER BY p.property_top DESC, pl.property_title")
					->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
					->queryAll();

				foreach ($properties as $property) {
					$property_id = $property['property_id'];

					if (isset($values[$property_id])) {
						$product_variants['properties'][$property_id] = $property;
						$product_variants['properties'][$property_id]['values'] = $values[$property_id];
					}
				}
			}

			// get variant photos
			$variant_photos = Yii::app()->db
				->createCommand("SELECT variant_id, photo_path, photo_size FROM product_variant_photo WHERE variant_id IN (" . implode(',', $variant_ids) . ") ORDER BY position")
				->queryAll();

			foreach ($variant_photos as $variant_photo) {
				$variant_id = $variant_photo['variant_id'];

				if (isset($product_variants['variants'][$variant_id])) {
					$product_variants['variants'][$variant_id]['photos'][] = $variant_photo;
				}
			}
		}

		return $product_variants;
	}

	public function getProductOptions($product_id)
	{
		$product_options = array(
			'options' => array(),
			'properties' => array(),
		);

		$options = Yii::app()->db
			->createCommand("SELECT po.*, p.property_id, pl.property_title, pv.value_id, pvl.value_title 
							 FROM product_option as po 
							 JOIN property_value as pv 
							 ON po.value_id = pv.value_id 
							 JOIN property_value_lang as pvl 
							 ON pv.value_id = pvl.value_id AND pvl.language_code = :code 
							 JOIN property as p 
							 ON pv.property_id = p.property_id 
							 JOIN property_lang as pl 
							 ON p.property_id = pl.property_id AND pl.language_code = :code 
							 WHERE po.product_id = :product_id 
							 ORDER BY p.property_top DESC, pl.property_title, pv.value_top DESC, pv.value_position, pvl.value_title")
			->bindValue(':product_id', (int) $product_id, PDO::PARAM_INT)
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		if (!empty($options)) {
			foreach ($options as $option) {
				$product_options['options'][] = array(
					'option_id' => $option['option_id'],
					'option_price' => $option['option_price'],
					'value_id' => $option['value_id'],
				);

				$property_id = $option['property_id'];
				$value_id = $option['value_id'];

				if (!isset($product_options['properties'][$property_id])) {
					$product_options['properties'][$property_id] = array(
						'property_id' => $property_id,
						'property_title' => $option['property_title'],
						'values' => array(
							$value_id => array(
								'value_id' => $value_id,
								'value_title' => $option['value_title'],
							),
						),
					);
				} else {
					$product_options['properties'][$property_id]['values'][$value_id] = array(
						'value_id' => $value_id,
						'value_title' => $option['value_title'],
					);
				}
			}
		}

		return $product_options;
	}

	public function getProductVariant($product_id, $variant_id)
	{
		$variant = Yii::app()->db
			->createCommand("SELECT * FROM product_variant WHERE product_id = :product_id AND variant_id = :variant_id LIMIT 1")
			->bindValue(':product_id', (int) $product_id, PDO::PARAM_INT)
			->bindValue(':variant_id', (int) $variant_id, PDO::PARAM_INT)
			->queryRow();

		return $variant;
	}

	public function getProductOptionsData($product_id, $option_ids)
	{
		$options = Yii::app()->db
			->createCommand("SELECT * FROM product_option WHERE product_id = :product_id AND option_id IN (" . implode(',', $option_ids) . ")")
			->bindValue(':product_id', (int) $product_id, PDO::PARAM_INT)
			->queryAll();

		return $options;
	}

	public function getVariantsByIds($variant_ids)
	{
		$variants = array();

		$variants_list = Yii::app()->db
			->createCommand("SELECT pv.*, pvp.photo_path, pvp.photo_size 
							 FROM product_variant as pv 
							 LEFT JOIN product_variant_photo as pvp 
							 ON pv.variant_id = pvp.variant_id 
							 LEFT JOIN product_variant_photo as pvp_filter 
							 ON pvp.variant_id = pvp_filter.variant_id AND pvp.position > pvp_filter.position 
							 WHERE pv.variant_id IN (" . implode(',', $variant_ids) . ") AND pvp_filter.photo_id IS NULL")
			->queryAll();

		if (!empty($variants_list)) {
			foreach ($variants_list as $variant) {
				$variant_id = $variant['variant_id'];

				$variants[$variant_id] = $variant;
				$variants[$variant_id]['values'] = array();
			}

			// get properties and their values
			$variant_ids = array_keys($variants);

			$variant_values = Yii::app()->db
				->createCommand("SELECT pvv.variant_id, p.property_id, pl.property_title, v.value_id, vl.value_title 
								 FROM product_variant_value as pvv 
								 JOIN property_value as v 
								 ON pvv.value_id = v.value_id 
								 JOIN property_value_lang as vl 
								 ON v.value_id = vl.value_id AND vl.language_code = :code AND vl.value_visible = 1 
								 JOIN property as p 
								 ON v.property_id = p.property_id 
								 JOIN property_lang as pl 
								 ON p.property_id = pl.property_id AND pl.language_code = :code AND pl.property_visible = 1
								 WHERE pvv.variant_id IN (" . implode(',', $variant_ids) . ")
								 ORDER BY p.property_top DESC, pl.property_title, v.value_top DESC, v.value_position, vl.value_title")
				->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
				->queryAll();

			if (!empty($variant_values)) {
				foreach ($variant_values as $variant_value) {
					$variant_id = $variant_value['variant_id'];

					if (isset($variants[$variant_id])) {
						$variants[$variant_id]['values'][] = $variant_value;
					}
				}
			}
		}
			
		return $variants;
	}

	public function getOptionsByIds($option_ids)
	{
		$options = array();

		$options_list = Yii::app()->db
			->createCommand("SELECT po.*, p.property_id, pl.property_title, v.value_id, vl.value_title 
							 FROM product_option as po 
							 JOIN property_value as v 
							 ON po.value_id = v.value_id 
							 JOIN property_value_lang as vl 
							 ON v.value_id = vl.value_id AND vl.language_code = :code AND vl.value_visible = 1 
							 JOIN property as p 
							 ON v.property_id = p.property_id 
							 JOIN property_lang as pl 
							 ON p.property_id = pl.property_id AND pl.language_code = :code AND pl.property_visible = 1
							 WHERE po.option_id IN (" . implode(',', $option_ids) . ")
							 ORDER BY p.property_top DESC, pl.property_title, v.value_top DESC, v.value_position, vl.value_title")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		if (!empty($options_list)) {
			foreach ($options_list as $option) {
				$option_id = $option['option_id'];
				$options[$option_id] = $option;
			}
		}
			
		return $options;
	}

	public static function getDiscountPrice($product_price, $category_id = 0, $brand_id = 0)
	{
		$discount_data = array(
			'price' => $product_price,
			'discount' => 0,
		);

		$categories = Yii::app()->params->categories;
		$brands = Yii::app()->params->brands;

		$category_discount = 0;
		$brand_discount = 0;

		if (isset($categories[$category_id]) && !empty($categories[$category_id]['category_discount'])) {
			$category_discount = $categories[$category_id]['category_discount'];
		}
		
		if (isset($brands[$brand_id]) && !empty($brands[$brand_id]['brand_discount'])) {
			$brand_discount = $brands[$brand_id]['brand_discount'];
		}

		$discount = ($brand_discount > $category_discount) ? $brand_discount : $category_discount;

		if ($discount) {
			$discount_price = $product_price * (100 - $discount) / 100;
			$discount_price = round($discount_price, 2);
			
			$discount_data = array(
				'price' => $discount_price,
				'discount' => $discount,
			);
		}

		/* $user_discount = User::getUserDiscount();
		$categories = Yii::app()->params->categories;

		if ($user_discount && isset($categories[$category_id]) && $categories[$category_id]['category_discount']) {
			$category_discount = (int) $categories[$category_id]['category_discount'];
			$product_discount = $user_discount > $category_discount ? $category_discount : $user_discount;
			$discount_price = $product_price * (100 - $product_discount) / 100;
			$discount_price = round($discount_price, 2);

			$discount_data = array(
				'price' => $discount_price,
				'discount' => $product_discount,
			);
		} */

		return $discount_data;
	}

	public function getProductsSitemap()
	{
		$products = Yii::app()->db
			->createCommand("SELECT p.product_id, p.product_alias, pl.product_title, pl.product_no_index 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 WHERE p.active = 1 
							 ORDER BY p.product_id")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $products;
	}

	public function getProductsGoogleFeed($language = null)
	{
		return Yii::app()->db
			->createCommand("
				SELECT
					p.product_id as id,
					pl.product_title as title,
					pl.product_description as description,

					cl.category_name as product_type,

					p.product_alias as link,
					p.product_id as image_link,
					p.product_photo,
					p.product_newest as custom_condition,
					p.product_instock as availability,
					p.product_price as price,
					p.product_price as sale_price,
					p.product_price_old as old_price,
					bl.brand_name as brand
				FROM product as p

				-- Product land
				JOIN product_lang as pl 
			 	ON p.product_id = pl.product_id 
			 		AND pl.language_code = :code
			 		AND pl.product_visible = 1

		 		-- Category
			 	JOIN category as c
			 	ON p.category_id = c.category_id
			 	JOIN category_lang as cl 
			 	ON c.category_id = cl.category_id
			 		AND cl.language_code = :code
			 		AND cl.category_visible = 1

		 		-- Brand
		 		LEFT JOIN brand as b
		 		ON p.brand_id = b.brand_id
		 			AND b.active = 1
	 			LEFT JOIN brand_lang as bl 
			 	ON b.brand_id = bl.brand_id
			 		AND bl.language_code = :code
			 		AND bl.brand_visible = 1 

			 	WHERE p.active = 1 
			 	ORDER BY p.product_id
		 	")
			->bindValue(':code', $language ? $language : Yii::app()->language, PDO::PARAM_STR)
			->queryAll()
		;
	}

	public function getProductPropertiesByOption($productId, $optionId)
    {
        $arr = [];

        $data = Yii::app()->db
            ->createCommand("SELECT ppo.* FROM property_product_option as ppo where product_id = $productId and option_id = $optionId")
            ->queryAll();

        foreach ($data as $item) {
            $property = Yii::app()->db
                ->createCommand("SELECT pl.property_title from property_lang as pl where pl.property_id = :property_id and pl.language_code = :code")
                ->bindValue(':property_id', $item['property_id'])
                ->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
                ->queryRow();

            $propertyValue = Yii::app()->db
                ->createCommand("SELECT pvl.value_title from property_value_lang as pvl where pvl.value_id = :value_id and pvl.language_code = :code")
                ->bindValue(':value_id', $item['value_id'])
                ->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
                ->queryRow();

            $arr[] = [
                'title' => $property['property_title'],
                'value' => $propertyValue['value_title']
            ];
        }

        return $arr;
    }

    public function getProductsForParents()
    {
        $products = Yii::app()->db
            ->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 WHERE p.active = 1 AND p.is_for_parents = 1 
							 ORDER BY p.product_rating DESC, p.product_id DESC 
							 ")
            ->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
            ->queryAll();


        $products = $this->getProductsVariants($products);
        $products = $this->getListProductsProperties($products);

        return $products;
    }
}
