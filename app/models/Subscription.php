<?php


class Subscription extends CModel
{
    public function rules()
    {
        return array();
    }

    public function attributeNames()
    {
        return array();
    }

    public static function model()
    {
        return new self();
    }

    public function getFirstCartValue($cart)
    {
        $firstKey = key($cart);
        return $cart[$firstKey];
    }


    public function add($cart, $order)
    {
        $firstKey = key($cart);
        $cart = $cart[$firstKey];
        $date = new DateTime();
        $orderReference = md5($cart['product_id'] + $date->getTimestamp());

        $insertSubData = [
            'order_reference' => $orderReference,
            'user_id' => Yii::app()->user->id,
            'frequency_id' => $cart['frequency'],
            'price' => $cart['price'] * $cart['qty'],
            'status' => 'new',
            'delivery' => $order['delivery'],
            'phone' => $order['phone'],
            'main_product' => $cart['product_id'],
        ];

        if (isset($order['address'])) {
            $insertSubData['delivery_address'] = $order['address'];
        }
        if (isset($order['np_city'])) {
            $insertSubData['np_city'] = $order['np_city'];
        }
        if (isset($order['np_department'])) {
            $insertSubData['np_department'] = $order['np_department'];
        }

        if (!isset($order['np_department'])) {
            $insertSubData['np_department'] = $order['np_building'] . ' (Курьер)';
        }

        $builder = Yii::app()->db->schema->commandBuilder;
        $rs = $builder->createInsertCommand('sub_order', $insertSubData)->execute();

        if (!$rs) {
            return false;
        }

        $subId = (int) Yii::app()->db->getLastInsertID();

        $this->addSubProduct($cart, $subId);

        return $subId;
    }

    public function checkIfHaveSub($userId)
    {
        return Yii::app()->db
            ->createCommand("SELECT count(*) as total FROM sub_order WHERE user_id = :userId")
            ->bindValue(':userId', $userId)
            ->queryRow()
            ;
    }

    public function checkIfHaveSubHistory($userId)
    {
        return Yii::app()->db
            ->createCommand("SELECT count(*) as total FROM sub_user_history WHERE user_id = :userId")
            ->bindValue(':userId', $userId)
            ->queryRow();
    }

    public function getSubByUser($userId)
    {
        return Yii::app()->db
            ->createCommand("SELECT * FROM sub_order WHERE user_id = :userId")
            ->bindValue(':userId', $userId)
            ->queryRow()
            ;
    }

    public function saveReasonSub($userId, $reason, $sub, $result)
    {
        $insertData = [
            'user_id' => $userId,
            'reason' => $reason,
            'order_reference' => $sub['order_reference'],
            'payment_comment' => $result,
        ];

        $builder = Yii::app()->db->schema->commandBuilder;
        $rs = $builder->createInsertCommand('sub_remove_reason', $insertData)->execute();
    }

    public function updateSub($cart, $order, $sub)
    {
        $firstKey = key($cart);
        $cart = $cart[$firstKey];
        $date = new DateTime();
        $orderReference = md5($cart['product_id'] + $date->getTimestamp());

        $insertSubData = [
            'order_reference' => $orderReference,
            'frequency_id' => $cart['frequency'],
            'price' => $cart['price'] * $cart['qty'],
            'status' => 'new',
            'payment_status' => '',
            'payment' => '',
            'delivery' => $order['delivery'],
            'phone' => $order['phone'],
        ];

        $criteria = new CDbCriteria(
            [
                "condition" => "id = :id" ,
                "params" => [
                    "id" => $sub['id'],
                ]
            ]
        );

        if (isset($order['address'])) {
            $insertSubData['delivery_address'] = $order['address'];
        }
        if (isset($order['np_city'])) {
            $insertSubData['np_city'] = $order['np_city'];
        }
        if (isset($order['np_department'])) {
            $insertSubData['np_department'] = $order['np_department'];
        }

        if (!isset($order['np_department'])) {
            $insertSubData['np_department'] = $order['np_building'] . ' (Курьер)';
        }

        $builder = Yii::app()->db->schema->commandBuilder;
        $rs = $builder->createUpdateCommand('sub_order', $insertSubData, $criteria)->execute();

        if (!$rs) {
            return false;
        }

        $this->updateSubProduct($cart, $sub['id']);
        $this->removeNextPayments($sub);

        return $sub['id'];
    }

    public function removeNextPayments($sub)
    {
        $criteria = new CDbCriteria(
            [
                "condition" => "order_reference = :ref" ,
                "params" => [
                    "ref" => $sub['order_reference'],
                ]
            ]
        );

        $builder = Yii::app()->db->schema->commandBuilder;
        $rs = $builder->createDeleteCommand('sub_next_payments',$criteria)->execute();
    }

    public function updateSubProduct($cart, $subId)
    {
        $variant = $cart['variant']['values'][0];

        $option = $cart['options'];
        $firstKey = key($option);
        end($option);
        $lastKey = key($option);
        $milling = $option[$lastKey];
        $option = $option[$firstKey];


        $insertSubProduct = [
            'product_id' => $cart['product_id'],
            'variant_id' => $cart['variant_id'],
            'option_id' => $cart['option_id'],
            'quantity' => $cart['qty'],
            'price' => $cart['price'] * $cart['qty'],
            'variant_title' => $variant['property_title'] . ': ' . $variant['value_title'],
            'option_title' => $option['property_title'] . ': ' . $option['value_title'],
            'milling' => $milling['property_title'] . ': ' . $milling['value_title'],
            'title' => $cart['data']['product_title']
        ];

        $criteria = new CDbCriteria(
            [
                "condition" => "sub_id = :id" ,
                "params" => [
                    "id" => $subId,
                ]
            ]
        );

        $builder = Yii::app()->db->schema->commandBuilder;
        $rs = $builder->createUpdateCommand('sub_product', $insertSubProduct, $criteria)->execute();
    }

    public function createSubNextPayment($sub, $data)
    {
        $year = date("Y");
        $month = $data->month;
        $day = $data->day;

        $insert = [
            'order_reference' => $sub['order_reference'],
            'next_date' => "$year-$month-$day",
            'first_pay' => 0,
            'status' => 'new',
            'delivery_status' => 'new',
        ];

        $builder = Yii::app()->db->schema->commandBuilder;
        $rs = $builder->createInsertCommand('sub_next_payments', $insert)->execute();
    }

    public function addSubProduct($cart, $subId)
    {
        $variant = $cart['variant']['values'][0];

        $option = $cart['options'];
        $firstKey = key($option);
        end($option);
        $lastKey = key($option);
        $milling = $option[$lastKey];
        $option = $option[$firstKey];

        $insertSubProduct = [
            'sub_id' => $subId,
            'product_id' => $cart['product_id'],
            'variant_id' => $cart['variant_id'],
            'option_id' => $cart['option_id'],
            'quantity' => $cart['qty'],
            'price' => $cart['price'] * $cart['qty'],
            'variant_title' => $variant['property_title'] . ': ' . $variant['value_title'],
            'option_title' => $option['property_title'] . ': ' . $option['value_title'],
            'milling' => $milling['property_title'] . ': ' . $milling['value_title'],
            'title' => $cart['data']['product_title']
        ];

        $builder = Yii::app()->db->schema->commandBuilder;
        $rs = $builder->createInsertCommand('sub_product', $insertSubProduct)->execute();
    }

    public function getSubById($orderId)
    {
        return Yii::app()->db
            ->createCommand("SELECT so.*, u.* FROM sub_order as so left join user as u on so.user_id = u.user_id WHERE id = :orderId")
            ->bindValue(':orderId', (int) $orderId, PDO::PARAM_INT)
            ->queryRow()
        ;
    }

    public function getSubByOrderRef($orderRef)
    {
        return Yii::app()->db
            ->createCommand("SELECT so.*, u.* FROM sub_order as so left join user as u on so.user_id = u.user_id WHERE order_reference = :orderRef")
            ->bindValue(':orderRef',  $orderRef)
            ->queryRow()
            ;
    }

    public function getSubProduct($subId)
    {
        return Yii::app()->db
            ->createCommand("select sp.*, p.* from sub_product as sp
                                left join product as p
                                on p.product_id = sp.product_id
                                left join product_variant as pv 
                                ON sp.variant_id = pv.variant_id
                                where sp.sub_id = :subId")
            ->bindValue(':subId', (int) $subId, PDO::PARAM_INT)
            ->queryRow();
    }

    public function getNexSubPayments($userId)
    {
        return Yii::app()->db
            ->createCommand("SELECT snp.*, so.frequency_id FROM sub_next_payments as snp left join sub_order as so on so.order_reference = snp.order_reference 
                             WHERE so.user_id = :user and next_date > now()")
            ->bindValue(':user',  $userId)
            ->queryAll()
        ;
    }

    public function getNewSubProductsBySub($productNumber, $subProduct)
    {
        return Yii::app()->db
            ->createCommand("SELECT * from sub_product_new as spn join product as p on p.product_id = spn.product_id JOIN product_lang as pl ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 WHERE p.active = 1 AND spn.product_number = :product_number AND spn.sub_product = :sub_product")
            ->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
            ->bindValue(':product_number', $productNumber)
            ->bindValue(':sub_product', $subProduct)
            ->queryAll()
            ;
    }

    public function getNewSubProducts($productNumber)
    {
        return Yii::app()->db
            ->createCommand("SELECT * from sub_product_new as spn join product as p on p.product_id = spn.product_id JOIN product_lang as pl ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 WHERE p.active = 1 AND spn.product_number = :product_number")
            ->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
            ->bindValue(':product_number', $productNumber)
            ->queryAll()
            ;
    }

    public function pauseSub($subId, $nextDate)
    {
        $criteria = new CDbCriteria(
            [
                "condition" => "id = :id" ,
                "params" => [
                    "id" => $subId,
                ]
            ]
        );

        $update = [
            'next_date' => $nextDate,
            'status' => 'new',
            'payment' => null
        ];

        $builder = Yii::app()->db->schema->commandBuilder;
        $builder->createUpdateCommand('sub_next_payments', $update, $criteria)->execute();
    }

    public function updateSubQty($sub, $subProduct, $qty)
    {
        $product = Product::model()->getProductById($subProduct['product_id']);

        if (!$product) {
            return false;
        }

        $price = round((float) $product['product_price'] * (int) $qty);

        $criteriaSubProduct = new CDbCriteria(
            [
                "condition" => "sub_id = :sub_id" ,
                "params" => [
                    "sub_id" => (int) $sub['id'],
                ]
            ]
        );

        $updateSubProduct = [
            'quantity' => (int) $qty,
            'price' => $price,
        ];

        $criteriaSub = new CDbCriteria(
            [
                "condition" => "id = :id" ,
                "params" => [
                    "id" => $sub['id'],
                ]
            ]
        );

        $updateSub = [
            'price' => $price,
        ];

        $builder = Yii::app()->db->schema->commandBuilder;

        try {
            $builder->createUpdateCommand('sub_product', $updateSubProduct, $criteriaSubProduct)->execute();
            $builder->createUpdateCommand('sub_order', $updateSub, $criteriaSub)->execute();

            return true;
        } catch (Exception $exception) {
            return false;
        }
    }

    public function setUserSubDiscount($userId)
    {
        $insert = [
            'user_id' => $userId
        ];

        $builder = Yii::app()->db->schema->commandBuilder;

        try {
            $builder->createInsertCommand('sub_user_history', $insert)->execute();
        } catch (Exception $exception) {}
    }

    public function deleteSub($sub)
    {
        $this->deleteSubOrder($sub);
        $this->deleteSubProduct($sub);
        $this->deleteSubPayments($sub);
    }

    public function deleteSubProduct($sub)
    {
        $criteria = new CDbCriteria(
            [
                "condition" => "sub_id = :subId" ,
                "params" => [
                    "subId" => $sub['id'],
                ]
            ]
        );

        $builder = Yii::app()->db->schema->commandBuilder;
        $rs = $builder->createDeleteCommand('sub_product', $criteria)->execute();
    }

    public function deleteSubOrder($sub)
    {
        $criteria = new CDbCriteria(
            [
                "condition" => "id = :subId" ,
                "params" => [
                    "subId" => $sub['id'],
                ]
            ]
        );

        $builder = Yii::app()->db->schema->commandBuilder;
        $rs = $builder->createDeleteCommand('sub_order', $criteria)->execute();
    }

    public function deleteSubPayments($sub, $new = false)
    {
        if (!$new) {
            $criteria = new CDbCriteria(
                [
                    "condition" => "order_reference = :ref" ,
                    "params" => [
                        "ref" => $sub['order_reference'],
                    ]
                ]
            );
        } else {
            $criteria = new CDbCriteria(
                [
                    "condition" => "order_reference = :ref AND next_date > :date" ,
                    "params" => [
                        "ref" => $sub['order_reference'],
                        "date" => date('Y-m-d'),
                    ]
                ]
            );
        }


        $builder = Yii::app()->db->schema->commandBuilder;
        $rs = $builder->createDeleteCommand('sub_next_payments', $criteria)->execute();
    }

    public function getUserMark($userId, $productId)
    {
        return Yii::app()->db
            ->createCommand("SELECT * from sub_user_mark WHERE user_id = :userId AND product_id = :productId")
            ->bindValue(':userId', (int) $userId, PDO::PARAM_INT)
            ->bindValue(':productId', (int) $productId, PDO::PARAM_INT)
            ->queryRow()
        ;
    }

    public function saveUserMark($userId, $sub, $mark, $data = null)
    {
        $insert = [];

        if ($mark == 1) {
            $insert = [
                'user_id' => $userId,
                'product_id' => $sub['product_id'],
                'is_good' => $mark,
            ];
        } elseif ($mark == 0) {
            $insert = [
                'user_id' => $userId,
                'product_id' => $sub['product_id'],
                'is_good' => $mark,
                'answ_1' => $data[0]->result,
                'answ_2' => $data[1]->result,
                'answ_3' => $data[2]->result,
                'text_1' => $data[0]->text,
                'text_2' => $data[1]->text,
                'text_3' => $data[2]->text,
            ];
        }

        $builder = Yii::app()->db->schema->commandBuilder;
        $rs = $builder->createInsertCommand('sub_user_mark', $insert)->execute();
    }

    public function getSubQuestions()
    {
        return Yii::app()->db
            ->createCommand("SELECT * from sub_mark_questions")
            ->queryAll()
        ;
    }

    public function getAnswers()
    {
        return Yii::app()->db
            ->createCommand("SELECT * from sub_mark_answers")
            ->queryAll()
            ;
    }
}