<?php
class Member extends CModel
{
	private $per_page = 10;

	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function getMembers()
	{
		$members = Yii::app()->db
			->createCommand("SELECT m.*, ml.member_name, ml.member_description   
							 FROM member as m 
							 JOIN member_lang as ml 
							 ON m.member_id = ml.member_id AND ml.language_code = :code 
							 WHERE m.active = 1 
							 ORDER BY m.member_position, m.member_id DESC")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $members;
	}
}