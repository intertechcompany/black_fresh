<?php


class Quiz extends CModel
{
    public function rules()
    {
        return array();
    }

    public function attributeNames()
    {
        return array();
    }

    public static function model()
    {
        return new self();
    }

    public function getQuestions()
    {
        $questions = Yii::app()->db
            ->createCommand("SELECT q.* FROM quiz_questions as q")
            ->queryAll();

        return $questions;
    }

    public function getAnswers()
    {
        $questions = Yii::app()->db
            ->createCommand("SELECT a.* FROM quiz_answers as a")
            ->queryAll();

        return $questions;
    }

    public function getUserQuiz($userId)
    {
        return Yii::app()->db->createCommand("SELECT * FROM quiz_history WHERE user_id = $userId")
            ->queryRow();
    }

    public function saveAllAnswers($productId, $answ1, $answ2, $answ3, $answ4, $answ5, $answ6)
    {
        Yii::app()->db
            ->createCommand("INSERT into quiz_history_all (product_id, quest_1, quest_2, quest_3, quest_4, quest_5, quest_6) 
                                values ($productId, $answ1, $answ2, $answ3, $answ4, $answ5, $answ6)"
            )->execute()
        ;
    }

    public function saveAnswers($userId, $productId, $answ1, $answ2, $answ3, $answ4, $answ5, $answ6)
    {
        $quizHistory = $this->getUserQuiz($userId);

        if (!$quizHistory) {
            Yii::app()->db
                ->createCommand("INSERT into quiz_history (user_id, product_id, quest_1, quest_2, quest_3, quest_4, quest_5, quest_6) 
                                values ($userId, $productId, $answ1, $answ2, $answ3, $answ4, $answ5, $answ6)"
                )->execute();
        } else {
            $criteria = new CDbCriteria(
                [
                    "condition" => "id = :id" ,
                    "params" => [
                        "id" => $quizHistory['id'],
                    ]
                ]
            );

            $update = [
                'product_id' => $productId ? $productId : 0 ,
                'quest_1' => $answ1,
                'quest_2' => $answ2,
                'quest_3' => $answ3,
                'quest_4' => $answ4,
                'quest_5' => $answ5,
                'quest_6' => $answ6,
            ];

            $builder = Yii::app()->db->schema->commandBuilder;
            $builder->createUpdateCommand('quiz_history', $update, $criteria)->execute();
        }

    }

    public function getProduct($answ1, $answ2, $answ3, $answ4, $answ5, $answ6)
    {
        $product = Yii::app()->db
            ->createCommand("SELECT * FROM quiz_product as p WHERE answ_1 LIKE '%$answ1%' AND answ_2 LIKE '%$answ2%' AND answ_3 LIKE '%$answ3%' AND answ_4 LIKE '%$answ4%' AND 
                                answ_5 LIKE '%$answ5%' AND answ_6 LIKE '%$answ6%'")
            ->queryAll();

        return $product;
    }
}