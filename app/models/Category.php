<?php
class Category extends CModel
{
	const SORT_SUB_CATEGORIES = true;

	private $id_key = 'category_id';
	private $code_key = 'category_alias';
	private $categories;
	private $paths;
	private $tree;

	public function rules()
	{
		return array();
	}

	public function attributeNames()
	{
		return array();
	}

	public static function model()
	{
		return new self();
	}

	public function getCategoryByAlias($alias)
	{
		$category = Yii::app()->db
			->createCommand("SELECT c.*, cl.* 
							 FROM category as c 
							 JOIN category_lang as cl 
							 ON c.category_id = cl.category_id AND cl.language_code = :code AND cl.category_visible = 1 
							 WHERE c.active = 1 AND c.category_alias = :alias")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindParam(':alias', $alias, PDO::PARAM_STR)
			->queryRow();

		return $category;
    }

    public function getCategoryById($id)
    {
        return Yii::app()->db
            ->createCommand("SELECT * FROM category WHERE category_id = :id LIMIT 1")
            ->bindValue(':id', (int) $id, PDO::PARAM_INT)
            ->queryRow();
    }

    public function getCategoriesList()
	{
		if ($this->categories === null) {
			$this->buildTree();
		}

		return $this->categories;
	}

	public function getCategoriesPaths()
	{
		if ($this->paths === null) {
			$this->buildTree();
		}

		return $this->paths;
	}

	public function getCategoriesTree()
	{
		if ($this->tree === null) {
			$this->buildTree();
		}

		return $this->tree;
	}

    // helper methods
	private function buildTree()
	{
		// $categories_data = Yii::app()->cache->get('categories_' . Yii::app()->language);
		$categories_data = false;

		if ($categories_data !== false) {
			$this->categories = $categories_data['dataset'];
			$this->paths = $categories_data['paths'];
			$this->tree = $categories_data['tree'];

			return;
		}

		// prepare dataset
		$dataset = array();
		
		$data_rows = Yii::app()->db
			->createCommand("SELECT c.category_id, c.parent_id, c.active, c.category_alias, c.category_top, c.category_home, c.category_type, c.category_photo, cl.category_name, cl.category_description, cl.category_no_index 
							 FROM category as c 
							 JOIN category_lang as cl 
							 ON c.category_id = cl.category_id AND cl.language_code = :code AND cl.category_visible = 1 
							 WHERE c.active = 1 
							 ORDER BY c.parent_id, c.category_position")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		if (empty($data_rows)) {
			return;
		}

		foreach ($data_rows as $row) {
			$id = $row[$this->id_key];
			$dataset[$id] = $row;
		}

		$tree = array();
		$paths = array();

		foreach ($dataset as &$node) {
			$node['children'] = array();
			$node['parents'] = array();
			$node['path'] = '';

			if ($node['parent_id'] == 0) {
				// root node
				$tree[$node[$this->id_key]] = &$node;
			} else {
				// disabled category
				if (!isset($dataset[$node['parent_id']])) {
					continue;
				}

				// child node
				if (!isset($dataset[$node['parent_id']]['sub'])) {
					$dataset[$node['parent_id']]['sub'] = array();
				}

				$dataset[$node['parent_id']]['sub'][$node[$this->id_key]] = &$node;
			}
		}

		// remove node link
		unset($node);

		// collect ids and set parent nodes and paths
		$ids = array();
		$this->getNodesId($ids, $dataset, $paths, $tree);

		// set children nodes
		$ids = array_reverse($ids);

		foreach ($ids as $id) {
			$dataset[$id]['children'][] = $id;

			if ($dataset[$id]['parent_id'] > 0) {
				if (isset($dataset[$dataset[$id]['parent_id']]['children'])) {
					$dataset[$dataset[$id]['parent_id']]['children'] = array_merge($dataset[$dataset[$id]['parent_id']]['children'], $dataset[$id]['children']);
				} else {
					$dataset[$dataset[$id]['parent_id']]['children'] = $dataset[$id]['children'];
				}
			}
		}

		$categories_data = array(
			'dataset' => $dataset,
			'paths' => $paths,
			'tree' => $tree,
		);

		// Yii::app()->cache->set('categories_' . Yii::app()->language, $categories_data);

		$this->categories = $dataset;
		$this->paths = $paths;
		$this->tree = $tree;
	}

	private function getNodesId(&$ids, &$dataset, &$paths, $tree)
	{
		foreach ($tree as $id => $node) {
			$ids[] = $id;

			$dataset[$id]['parents'][] = $id;
			$dataset[$id]['path'] = '/' . $dataset[$id][$this->code_key];

			if ($dataset[$id]['parent_id'] > 0) {
				if (isset($dataset[$dataset[$id]['parent_id']]['parents'])) {
					$dataset[$id]['parents'] = array_merge($dataset[$dataset[$id]['parent_id']]['parents'], $dataset[$id]['parents']);
					$dataset[$id]['path'] = $dataset[$dataset[$id]['parent_id']]['path'] . $dataset[$id]['path'];
				}
			}

			$paths[$dataset[$id]['path']] = $id;

			if (isset($node['sub'])) {
				$this->getNodesId($ids, $dataset, $paths, $node['sub']);
			}
		}
	}

	public static function buildCategoriesTree($controller, $categories, $category_id)
	{
		$categories_html = '';

		foreach ($categories as $category) {
			$categories_html .= '<li>' . "\n";

			if ($category_id == $category['category_id']) {
				$categories_html .= '<a class="active" href="' . $controller->createUrl('category', array('alias' => $category['category_alias'])) . '">' . CHtml::encode($category['category_name']) . '</a>' . "\n";
			} else {
				$categories_html .= '<a href="' . $controller->createUrl('category', array('alias' => $category['category_alias'])) . '">' . CHtml::encode($category['category_name']) . '</a>' . "\n";
			}

			if (!empty($category['sub']) && in_array($category_id, $category['children'])) {
				if (Category::SORT_SUB_CATEGORIES) {
					usort($category['sub'], function($a, $b) {
						return strnatcmp($a['category_name'], $b['category_name']);
					});
				}
				
				$categories_html .= '<ul class="list-unstyled">' . "\n";
				$categories_html .= self::buildCategoriesTree($controller, $category['sub'], $category_id);
				$categories_html .= '</ul>' . "\n";
			}

			$categories_html .= '</li>' . "\n";
		}

		return $categories_html;
	}

	public static function renderBreadrumbs($controller, $categories, $parents, $category_id)
	{
		$breadcrumbs = '';

		foreach ($parents as $parent_id) {
			if (!isset($categories[$parent_id])) {
				continue;
			}

			if ($parent_id != $category_id) {
				$breadcrumbs .= '<a href="' . $controller->createUrl('category', array('alias' => $categories[$parent_id]['category_alias'])) . '">' . CHtml::encode($categories[$parent_id]['category_name']) . '</a>' . "\n";
				$breadcrumbs .= '/' . "\n";
			} else {
				$breadcrumbs .= CHtml::encode($categories[$parent_id]['category_name']);
			}
		}

		return $breadcrumbs;
	}
}