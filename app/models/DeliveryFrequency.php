<?php


class DeliveryFrequency extends CModel
{
    public function rules()
    {
        return array();
    }

    public function attributeNames()
    {
        return array();
    }

    public static function model()
    {
        return new self();
    }

    public function getDeliveryFrequency()
    {
        return Yii::app()->db
            ->createCommand("SELECT df.* FROM delivery_frequency as df")
            ->queryAll();
    }

    public function getDeliveryByFrequency($freq)
    {
        return Yii::app()->db
            ->createCommand("SELECT df.* FROM delivery_frequency as df WHERE frequency = $freq")
            ->queryRow();
    }
}