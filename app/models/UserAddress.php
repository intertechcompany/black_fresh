<?php

class UserAddress extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function add($params)
	{
		$today = date('Y-m-d H:i:s');

		$params['user_id'] = Yii::app()->user->id;
        $params['is_enabled'] = true;
        $params['created_at'] = $today;
        $params['updated_at'] = $today;
		
		try {
            return Yii::app()->db->createCommand()
        	->insert('user_addresses', $params);
        } catch (CDbException $ex) {
            return false;
        }

        return false;
	}

	public function save($model)
	{
		$model['updated_at'] = date('Y-m-d H:i:s');

		$builder = Yii::app()->db->schema->commandBuilder;

		$update_criteria = new CDbCriteria(
			[
				"condition" => "id = :id" , 
				"params" => [
					"id" => $model['id']
				]
			]
		);

		try {
			return $builder->createUpdateCommand('user_addresses', $model, $update_criteria)->execute();
		} catch (CDbException $ex) {
            return false;
        }

        return false;
	}

	public function delete($id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		
		$criteria = new CDbCriteria(
			[
				"condition" => "id = :id" , 
				"params" => [
					"id" => $id,
				]
			]
		);
		
		try {
			return $builder->createDeleteCommand('user_addresses', $criteria)->execute();
		}
		catch (CDbException $ex) {
			return false;
		}

		return false;
	}


	public function getUserAddresses($limit = 20)
	{
		$limit = 'LIMIT 0,' . $limit;

		return Yii::app()->db
			->createCommand("SELECT * FROM `user_addresses` WHERE user_id = :user_id ORDER BY id DESC {$limit}")
			->bindValue(':user_id', (int) Yii::app()->user->id, PDO::PARAM_INT)
			->queryAll()
		;
	}

	public function getUserAddress($id)
	{
		return Yii::app()->db
			->createCommand("SELECT * FROM `user_addresses` WHERE user_id = :user_id AND id = {$id} ORDER BY id DESC")
			->bindValue(':user_id', (int) Yii::app()->user->id, PDO::PARAM_INT)
			->queryAll()
		;
	}
}
