<?php
class Setting extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function getSettings()
	{
		$settings = Yii::app()->db
			->createCommand("SELECT * FROM setting WHERE setting_id = 1")
			->queryRow();
			
		return $settings;
	}

	public function save($model)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_setting = array(
			'saved' => $today,
		);

		foreach ($model as $setting_key => $setting_value) {
			$update_setting[$setting_key] = $setting_value;
		}

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "setting_id = :setting_id" , 
				"params" => array(
					"setting_id" => $model->setting_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('setting', $update_setting, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}
}
