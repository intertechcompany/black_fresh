<?php
class Facet extends CModel
{
	// define input data
	private $filter_inputs = array();
	
	// define states
	private $has_facets;
	private $has_filter;

	// define facet properties and ranges
	private $properties = array();
	private $values = array();
	private $selected = array();
	private $price = array(
		'min' => 0,
		'max' => 0,
		'from' => false,
		'to' => false,
	);
	private $length = array(
		'min' => 0,
		'max' => 0,
		'from' => false,
		'to' => false,
	);
	private $width = array(
		'min' => 0,
		'max' => 0,
		'from' => false,
		'to' => false,
	);

	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function getRangeNames()
	{
		return array();
		
		/* return array(
			'price',
			'length',
			'width',
		); */
	}

	public function hasFacets()
	{
		if ($this->has_facets !== null) {
			return $this->has_facets;
		}

		if (!empty($this->properties)) {
			return $this->has_facets = true;
		}

		$ranges = $this->getRangeNames();

		foreach ($ranges as $index) {
			if ($this->{$index}['min'] != $this->{$index}['max']) {
				return $this->has_facets = true;
			}
		}
	}

	public function hasFilter()
	{
		if ($this->has_filter !== null) {
			return $this->has_filter;
		}

		if (!empty($this->values)) {
			return $this->has_filter = true;
		}

		$ranges = $this->getRangeNames();

		foreach ($ranges as $index) {
			if ($this->{$index}['min'] != $this->{$index}['from'] || $this->{$index}['max'] != $this->{$index}['to']) {
				return $this->has_filter = true;
			}
		}
	}

	public function getSelectedFacets()
	{
		return $this->selected;
	}

	public function getFacetFilterSql()
	{
		$join = array();
		$where = array();

		if (!empty($this->values)) {
			foreach ($this->values as $property_id => $value_bucket) {
				$where[] = 'p.product_id IN (SELECT product_id FROM property_product WHERE value_id IN (' . implode(',', $value_bucket) . '))';
			}
		}

		$ranges = $this->getRangeNames();

		foreach ($ranges as $index) {
			if ($this->{$index}['min'] != $this->{$index}['from'] || $this->{$index}['max'] != $this->{$index}['to']) {
				$where[] = 'p.product_' . $index . ' BETWEEN ' . $this->{$index}['from'] . ' AND ' . $this->{$index}['to'];
			}
		}

		return array(
			'join' => implode(' ', $join),
			'where' => implode(' AND ', $where),
		);
	}

	public function getPropertiesFacets()
	{
		return $this->properties;
	}

	public function getPriceFacet()
	{
		return $this->price;
	}

	public function getLengthFacet()
	{
		return $this->length;
	}

	public function getWidthFacet()
	{
		return $this->width;
	}

	public function setFacetsInput($filters)
	{
		// collect properties
		if (isset($filters['p']) && is_array($filters['p'])) {
			$this->filter_inputs = $filters['p'];
		}

		// collect ranges data
		$ranges = $this->getRangeNames();

		foreach ($ranges as $index) {
			$range = isset($filters[$index]) ? $filters[$index] : false;

			if (!empty($range)) {
				foreach ($range as $range_index => $range_value) {
					if (isset($this->{$index}[$range_index])) {
						$this->{$index}[$range_index] = (int) $range_value;
					}
				}
			}
		}
	}

	public function getCategoryFacets($categoryId)
	{
//		$categories = Yii::app()->params->categories[$category_id]['children'];
//
//		$this->getCategoryProperties($categoryIds);
//		/* $this->getCategoryRanges($categories);
//		$this->getRanges(); */

        $categoryIds = $this->getCurretnAndChildsCategoryIds($categoryId);
		$this->getFilters($categoryIds);
	}

	public function getCurretnAndChildsCategoryIds($categoryId)
    {
        $categoryIds = [];
        $categoryIds[] = $categoryId;

        $categories = Yii::app()->db
            ->createCommand('SELECT c.category_id as id FROM category as c WHERE active = 1 AND parent_id = :id')
            ->bindValue(':id', $categoryId, PDO::PARAM_INT)
            ->queryAll();

        if (!empty($categories)) {
            foreach ($categories as $category) {
                if ($category['id'] !== $categoryId) {
                    $categoryIds[] = $category['id'];
                }
            }
        }

        return $categoryIds;
    }

	private function getCategoryProperties($category_ids)
	{
		$properties_set = Yii::app()->db
			->createCommand("SELECT p.property_id, p.property_size, p.property_color, pl.property_title, pv.value_id, pv.value_color, pv.value_multicolor, pvl.value_title   
							 FROM product  
							 JOIN property_product as pp 
							 ON product.product_id = pp.product_id 
							 JOIN property as p 
							 ON pp.property_id = p.property_id AND p.property_filter = 1 
							 JOIN property_lang as pl 
							 ON p.property_id = pl.property_id AND pl.language_code = :code 
							 JOIN property_value as pv 
							 ON pp.value_id = pv.value_id 
							 JOIN property_value_lang as pvl 
							 ON pv.value_id = pvl.value_id AND pvl.language_code = :code 
							 WHERE product.active = 1 AND product.category_id IN (" . implode(',', $category_ids) . ")
							 GROUP BY pp.value_id 
							 ORDER BY p.property_top DESC, pl.property_title, pv.value_top DESC, pv.value_position, pvl.value_title")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		if (!empty($properties_set)) {
			foreach ($properties_set as $property) {
				$property_id = $property['property_id'];
				$value_id = $property['value_id'];
				$selected = false;

				if (in_array($value_id, $this->filter_inputs)) {
					$selected = true;
					
					// add to values bucket
					$this->values[$property_id][] = $value_id;
					$this->selected[] = array(
						'value_id' => $value_id,
						'value_title' => $property['value_title'],
					);
				}

				if (isset($this->properties[$property_id])) {
					$this->properties[$property_id]['values'][$value_id] = array(
						'value_color' => $property['value_color'],
						'value_multicolor' => $property['value_multicolor'],
						'value_title' => $property['value_title'],
						'total' => 0,
						'selected' => $selected,
					);

					if ($selected) {
						$this->properties[$property_id]['selected'] = $property['property_id'];
					}
				} else {
					$this->properties[$property_id]['property_title'] = $property['property_title'];
					$this->properties[$property_id]['property_size'] = $property['property_size'];
					$this->properties[$property_id]['property_color'] = $property['property_color'];
					$this->properties[$property_id]['values'] = array(
						$value_id => array(
							'value_color' => $property['value_color'],
							'value_multicolor' => $property['value_multicolor'],
							'value_title' => $property['value_title'],
							'total' => 0,
							'selected' => $selected,
						),
					);

					if ($selected) {
						$this->properties[$property_id]['selected'] = $property['property_id'];
					}
				}
			}
		}
	}

	private function getCategoryRanges($category_ids)
	{
		// $facet_values not used now
		$facet_values = $this->values;

		$min_max_sql = $this->getMinMaxSql();
		$properties_filter = $this->preparePropertiesSql();

		$ranges = Yii::app()->db
			->createCommand("SELECT {$min_max_sql} 
							 FROM product as p
							 WHERE p.active = 1 AND p.category_id IN (" . implode(',', $category_ids) . ") {$properties_filter}")
			->queryRow();

		if (!empty($ranges)) {
			foreach ($ranges as $key => $value) {
				list($range_var, $index) = explode('_', $key);

				if (isset($this->{$range_var}[$index])) {
					$this->{$range_var}[$index] = (int) $value;
				}
			}
		}
	}

	private function getMinMaxSql()
	{
		$sql_segments = array();
		$ranges = $this->getRangeNames();

		foreach ($ranges as $index) {
			$sql_segments[] = 'MIN(p.product_' . $index . ') as ' . $index . '_min';
			$sql_segments[] = 'MAX(p.product_' . $index . ') as ' . $index . '_max';
		}

		return implode(', ', $sql_segments);
	}

	private function preparePropertiesSql($values = null, $key = 'p')
	{
		$sql_filters = array();
		$values = $values !== null ? $values : $this->values;
		
		if (!empty($values)) {
			foreach ($values as $property_id => $value_bucket) {
				$sql_filters[] = "AND {$key}.product_id IN (SELECT product_id FROM property_product WHERE value_id IN (" . implode(',', $value_bucket) . "))";
			}
		}

		return implode(' ', $sql_filters);
	}

	private function getRanges()
	{
		$ranges = $this->getRangeNames();

		foreach ($ranges as $index) {
			// is from value in range?
			if (empty($this->{$index}['from']) || $this->{$index}['from'] < $this->{$index}['min']) {
				$this->{$index}['from'] = $this->{$index}['min'];
			}

			// is to value in range?
			if (empty($this->{$index}['to']) || $this->{$index}['to'] > $this->{$index}['max']) {
				$this->{$index}['to'] = $this->{$index}['max'];
			}

			// "to" value should be greater or equal "from" value
			if ($this->{$index}['to'] < $this->{$index}['from']) {
				$this->{$index}['to'] = $this->{$index}['max'];
			}
		}
	}

	private function getFilters($category_ids)
	{
		$ranges_sql = $this->getRangesSql();

		if (empty($this->values)) {
			// get all values
			$values = Yii::app()->db
                ->createCommand("SELECT pp.property_id, pp.value_id, COUNT(pp.value_id) as total, pl.property_title, pvl.value_title
                                FROM product
                                JOIN property_product as pp ON product.product_id = pp.product_id
                                JOIN property as p ON pp.property_id = p.property_id
                                JOIN property_lang as pl ON pl.property_id = p.property_id AND pl.language_code = :code 
                                JOIN property_value_lang pvl on pp.value_id = pvl.value_id AND pvl.language_code = :code 
                                WHERE product.active = 1 AND product.category_id IN (" . implode(',', $category_ids) . ") AND p.property_filter = 1
                                GROUP BY pp.value_id")
                ->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
				->queryAll();

			$this->setPropertiesValues($values);
		} else {
			// get bucket values
			$this->getBucketValues($category_ids, $ranges_sql);
		}
	}

	private function getRangesSql()
	{
		$where = array();

		$ranges = $this->getRangeNames();

		foreach ($ranges as $index) {
			if ($this->{$index}['min'] != $this->{$index}['from'] || $this->{$index}['max'] != $this->{$index}['to']) {
				$where[] = 'AND product.product_' . $index . ' BETWEEN ' . $this->{$index}['from'] . ' AND ' . $this->{$index}['to'];
			}
		}

		return implode(' ', $where);
	}

	private function getBucketValues($category_ids, $ranges_sql)
	{
		$values_buckets = $this->values;

		foreach ($values_buckets as $property_id => $values) {
			$tmp_values_buckets = $values_buckets;
			unset($tmp_values_buckets[$property_id]);

			$filter_sql = $this->preparePropertiesSql($tmp_values_buckets, 'product');

			// get current property values
			$values = Yii::app()->db
				->createCommand("SELECT pp.property_id, pp.value_id, COUNT(pp.value_id) as total, pl.property_title, pvl.value_title
                                FROM product
                                JOIN property_product as pp ON product.product_id = pp.product_id
                                JOIN property as p ON pp.property_id = p.property_id
                                JOIN property_lang as pl ON pl.property_id = p.property_id AND pl.language_code = :code 
                                JOIN property_value_lang pvl on pp.value_id = pvl.value_id AND pvl.language_code = :code 
                                WHERE product.active = 1 AND product.category_id IN (" . implode(',', $category_ids) . ") AND p.property_filter = 1
                                 AND pp.property_id = :property_id {$filter_sql} {$ranges_sql} 
								 GROUP BY pp.value_id")
				->bindValue(':property_id', (int) $property_id, PDO::PARAM_INT)
                ->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
				->queryAll();

			$this->setPropertiesValues($values);
		}

		if (count($this->properties) > count($values_buckets)) {
			$properties = array_keys($values_buckets);
			$filter_sql = $this->preparePropertiesSql($values_buckets, 'product');

			// get rest properties values for selected values
			$values = Yii::app()->db
				->createCommand("SELECT pp.property_id, pp.value_id, COUNT(pp.value_id) as total, pl.property_title, pvl.value_title
                                FROM product
                                JOIN property_product as pp ON product.product_id = pp.product_id
                                JOIN property as p ON pp.property_id = p.property_id
                                JOIN property_lang as pl ON pl.property_id = p.property_id AND pl.language_code = :code 
                                JOIN property_value_lang pvl on pp.value_id = pvl.value_id AND pvl.language_code = :code 
                                WHERE product.active = 1 AND product.category_id IN (" . implode(',', $category_ids) . ") AND pp.property_id NOT IN (" . implode(',', $properties) . ") {$filter_sql} {$ranges_sql} 
								  AND p.property_filter = 1 GROUP BY pp.value_id")
                ->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
				->queryAll();

			$this->setPropertiesValues($values);
		}
	}

	private function setPropertiesValues($values)
	{
		if (!empty($values)) {
			foreach ($values as $value) {
				$property_id = $value['property_id'];
				$value_id = $value['value_id'];

                $selected = false;

                if (in_array($value_id, $this->filter_inputs)) {
                    $selected = true;

                    $this->values[$property_id][] = $value_id;
                    $this->selected[] = array(
                        'value_id' => $value_id,
                        'value_title' => $value['value_title'],
                    );
                }

                if (isset($this->properties[$property_id])) {
                    $this->properties[$property_id]['values'][$value_id] = [
                        'value_title' => $value['value_title'],
                        'total' => $value['total'],
                        'selected' => $selected,
                    ];

                    if ($selected) {
                        $this->properties[$property_id]['selected'] = $property_id;
                    }
                } else {
                    $this->properties[$property_id]['property_title'] = $value['property_title'];
                    $this->properties[$property_id]['values'] = [
                        $value_id => [
                            'value_title' => $value['value_title'],
                            'total' => $value['total'],
                            'selected' => $selected,
                        ]
                    ];

                    if ($selected) {
                        $this->properties[$property_id]['selected'] = $value['property_id'];
                    }
                }
			}
		}
	}
}