<?php


class Feedback extends CModel
{
    public function rules()
    {
        return array();
    }

    public function attributeNames()
    {
        return array();
    }

    public static function model()
    {
        return new self();
    }

    public function save($feedback)
    {
        $builder = Yii::app()->db->schema->commandBuilder;

        $builder->createInsertCommand('feedback', $feedback)->execute();
    }
}