<?php
class Banner extends CModel
{
	private $per_page = 10;

	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function getBanners()
	{
		$banners = Yii::app()->db
			->createCommand("SELECT b.*, bl.* 
							 FROM banner as b 
							 JOIN banner_lang as bl 
							 ON b.banner_id = bl.banner_id AND bl.language_code = :code AND bl.banner_visible = 1 
							 WHERE b.active = 1 
							 ORDER BY b.banner_place, b.banner_position, b.banner_id DESC")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $banners;
	}
}