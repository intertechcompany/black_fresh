<?php

class User extends CModel
{
    public function rules()
    {
        return array();
    }

    public function attributeNames()
    {
        return array();
    }

    public static function model()
    {
        return new self();
    }

    public static function getUserDiscount()
    {
        $discount = 0;

        if (!Yii::app()->user->isGuest) {
            $discount = (int)Yii::app()->user->user_data['discount'];
        }

        return $discount;
    }

    public function getUser()
    {
        $user = Yii::app()->db
            ->createCommand("SELECT * FROM user WHERE user_id = :id AND active = 1")
            ->bindValue(':id', (int)Yii::app()->user->id, PDO::PARAM_INT)
            ->queryRow();

        return $user;
    }

    public function getUserById($id)
    {
        $user = Yii::app()->db
            ->createCommand("SELECT * FROM user WHERE user_id = :id AND active = 1")
            ->bindValue(':id', (int)$id, PDO::PARAM_INT)
            ->queryRow();

        return $user;
    }

    public function getUserByLogin($login)
    {
        $user = Yii::app()->db
            ->createCommand("SELECT * FROM user WHERE user_login = :login")
            ->bindParam(':login', $login, PDO::PARAM_STR)
            ->queryRow();

        return $user;
    }

    public function removeUser($userId)
    {
        $criteria = new CDbCriteria(
            [
                "condition" => "user_id = :user_id" ,
                "params" => [
                    "user_id" => $userId,
                ]
            ]
        );

        $builder = Yii::app()->db->schema->commandBuilder;
        $builder->createDeleteCommand('user', $criteria)->execute();
    }

    public function updateUserSocial($userId, $value, $isFacebook = false)
    {
        $criteria = new CDbCriteria(
            [
                "condition" => "user_id = :user_id" ,
                "params" => [
                    "user_id" => (int) $userId,
                ]
            ]
        );

        if (!$isFacebook) {
            $update = [
                'google_login' => true,
                'google_email' => $value,
            ];
        } else {
            $update = [
                'fb_login' => true,
                'fb_id' => $value,
            ];
        }

        $builder = Yii::app()->db->schema->commandBuilder;

        try {
            $builder->createUpdateCommand('user', $update, $criteria)->execute();

            return true;
        } catch (Exception $exception) {
            return false;
        }
    }

    public function getUserByEmailSocial($email)
    {
        return Yii::app()->db
            ->createCommand("SELECT * FROM user WHERE google_email = :email")
            ->bindParam(':email', $email, PDO::PARAM_STR)
            ->queryRow();
    }

    public function getUserByEmail($email)
    {
        $user = Yii::app()->db
            ->createCommand("SELECT * FROM user WHERE user_email = :email")
            ->bindParam(':email', $email, PDO::PARAM_STR)
            ->queryRow();

        return $user;
    }

    public function getUserByPhone($phone)
    {
        return Yii::app()->db
            ->createCommand("SELECT * FROM user WHERE user_phone = :phone")
            ->bindParam(':phone', $phone, PDO::PARAM_STR)
            ->queryRow();
    }

    public function getUserByFbId($id)
    {
        $user = Yii::app()->db
            ->createCommand("SELECT * FROM user WHERE fb_id = :id")
            ->bindParam(':id', $id, PDO::PARAM_STR)
            ->queryRow();

        return $user;
    }

    public function getUserByLoginOrEmail($login)
    {
        $user = Yii::app()->db
            ->createCommand("SELECT * FROM user WHERE user_login = :login OR user_email = :login")
            ->bindParam(':login', $login, PDO::PARAM_STR)
            ->queryRow();

        return $user;
    }

    public function saveToken($token, $id)
    {
        $rs = Yii::app()->db
            ->createCommand("UPDATE user SET user_token = :token WHERE user_id = :id")
            ->bindValue(':token', $token, PDO::PARAM_STR)
            ->bindValue(':id', (int)$id, PDO::PARAM_INT)
            ->execute();

        return (bool)$rs;
    }

    public function isValidToken($id, $token)
    {
        $user = Yii::app()->db
            ->createCommand("SELECT COUNT(*) as total, saved FROM user WHERE user_id = :id AND active = 1 AND user_token = :token")
            ->bindValue(':id', (int)$id, PDO::PARAM_INT)
            ->bindValue(':token', $token, PDO::PARAM_STR)
            ->queryRow();

        if ($user['total']) {
            $is_valid = true;
            Yii::app()->user->setState('saved', $user['saved']);
        } else {
            $is_valid = false;
            Yii::app()->user->setState('saved', null);
        }

        return $is_valid;
    }

    public function add($model)
    {
        $builder = Yii::app()->db->schema->commandBuilder;
        $today = date('Y-m-d H:i:s');
        $insert_user = array(
            'active' => 1,
            'created' => $today,
            'saved' => $today,
            'user_email' => $model->email,
            'user_phone' => $model->phone,
            'user_password' => $this->hashPassword($model->password),
            'user_first_name' => $model->first_name,
            'user_last_name' => $model->last_name,
        );

        try {
            $rs = $builder->createInsertCommand('user', $insert_user)->execute();
            $user_id = (int)Yii::app()->db->getLastInsertID();
        } catch (CDbException $e) {
            return false;
        }

        return $user_id;
    }

    public function addBySocial($data)
    {
        $builder = Yii::app()->db->schema->commandBuilder;
        $today = date('Y-m-d H:i:s');
        $insert_user = [
            'active' => 1,
            'created' => $today,
            'saved' => $today,
            'user_email' => $data['email'],
            'google_email' => $data['email'],
            'user_first_name' => $data['given_name'],
            'user_last_name' => $data['family_name'],
            'google_login' => 1
        ];

        try {
            $rs = $builder->createInsertCommand('user', $insert_user)->execute();
            $user_id = (int)Yii::app()->db->getLastInsertID();
        } catch (CDbException $e) {
            return false;
        }

        return $user_id;
    }

    public function addBySocialFb($data)
    {
        $builder = Yii::app()->db->schema->commandBuilder;
        $today = date('Y-m-d H:i:s');
        $insert_user = [
            'active' => 1,
            'created' => $today,
            'saved' => $today,
            'fb_id' => $data['id'],
            'user_email' => (isset($data['email']) && $data['email'] !== '') ? $data['email'] : $data['id'] . '@facebook.com',
            'user_first_name' => $data['first_name'],
            'user_last_name' => $data['last_name'],
            'fb_login' => 1
        ];

        try {
            $rs = $builder->createInsertCommand('user', $insert_user)->execute();
            $user_id = (int)Yii::app()->db->getLastInsertID();
        } catch (CDbException $e) {
            return false;
        }

        return $user_id;
    }

    public function sendMailFromChaCkoutPage($model)
    {
        $user = User::model()->getUserByEmail($model->email);

        if ($user) {
            $subject = Lang::t('auth.emailSubject.accountCrateSuccess');

            $body = Lang::t('registration.emailBody.accountCreateSuccess', array(
                '{name}' => $user['user_first_name'],
                '{password}' => $model->password,
            ));

            $mailer = new EsputnikMailer();

            $emails = $mailer->prepareEmailsFromString($user['user_email']);

            $mailer->send([$user['user_email']], $subject, $body);
        }
    }

    public function save($model)
    {
        $builder = Yii::app()->db->schema->commandBuilder;

        $skip_attributes = array();
        $int_attributes = array();
        $date_attributes = array();

        $update = array(
            'saved' => date('Y-m-d H:i:s'),
        );

        foreach ($model as $field => $value) {
            if (in_array($field, $skip_attributes)) {
                continue;
            } elseif (in_array($field, $int_attributes)) {
                $update[$field] = (int)$value;
            } elseif (in_array($field, $date_attributes)) {
                if (empty($value)) {
                    $update[$field] = '0000-00-00';
                } else {
                    $date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
                    $update[$field] = $date->format('Y-m-d');
                }
            } else {
                $update[$field] = $value;
            }
        }

        $criteria = new CDbCriteria(
            array(
                "condition" => "user_id = :id",
                "params" => array(
                    "id" => (int)Yii::app()->user->id,
                )
            )
        );

        try {
            $rs = $builder->createUpdateCommand('user', $update, $criteria)->execute();

            if ($rs) {
                return true;
            }
        } catch (CDbException $e) {

        }

        return false;
    }

    public function setPassword($model, $reset = false)
    {
        $builder = Yii::app()->db->schema->commandBuilder;
        $saved = date('Y-m-d H:i:s');
        $user = $model->getUser();

        $update = array(
            'saved' => $saved,
            'user_password' => $this->hashPassword($model->password),
        );

        if ($reset) {
            $update['user_reset_token'] = '';
        }

        $criteria = new CDbCriteria(
            array(
                "condition" => "user_id = :id",
                "params" => array(
                    "id" => (int)$user['user_id'],
                )
            )
        );

        try {
            $rs = $builder->createUpdateCommand('user', $update, $criteria)->execute();

            if ($rs) {
                if ($reset) {
                    // send success message
                    $subject = Lang::t('auth.emailSubject.resetSuccess');

                    $body = Lang::t('auth.emailBody.resetSuccess', array(
                        '{name}' => $user['user_first_name'],
                        '{password}' => $model->password,
                    ));

                    $mailer = new EsputnikMailer();
                    $mailer->send([$user['user_email']], $subject, $body);
                }

                return true;
            }
        } catch (CDbException $e) {

        }

        return false;
    }

    public function setResetToken($model)
    {
        $builder = Yii::app()->db->schema->commandBuilder;
        $user = $model->getUser();
        $token = $this->randomString(12);

        $update = array(
            'user_reset_token' => $token,
        );

        $criteria = new CDbCriteria(
            array(
                "condition" => "user_id = :id",
                "params" => array(
                    "id" => (int)$user['user_id'],
                )
            )
        );

        try {
            $rs = $builder->createUpdateCommand('user', $update, $criteria)->execute();

            if ($rs) {
                // send reset link
                $subject = Lang::t('auth.emailSubject.resetAction');

                $body = Lang::t('auth.emailBody.resetAction', array(
                    '{name}' => $user['user_first_name'],
                    '{reset_url}' => Yii::app()->createAbsoluteUrl('site/reset', array('uid' => $user['user_id'], 'token' => $token)),
                ));

                $mailer = new EsputnikMailer();

                $mailer->send([$user['user_email']], $subject, $body);

                return true;
            }
        } catch (CDbException $e) {

        }

        return false;
    }

    /**
     * Checks if the given password is correct.
     * @param string the password to be validated
     * @param string the password hash
     * @return boolean whether the password is valid
     */
    public function validatePassword($password, $password_hash)
    {
        return CPasswordHelper::verifyPassword($password, $password_hash);
    }

    /**
     * Generates the password hash.
     * @param string password
     * @return string hash
     */
    public function hashPassword($password)
    {
        return CPasswordHelper::hashPassword($password, 10);
    }

    private function randomString($length = 8)
    {
        // digits
        $digits = range(0, 9);
        shuffle($digits);

        // small letters
        $lower = range('a', 'z');
        shuffle($lower);

        // caption letters
        $upper = range('A', 'Z');
        shuffle($upper);

        $total_characters = floor($length / 3);
        $rest_characters = $length - $total_characters * 3;

        $chars = array_merge(
            array_slice($digits, 0, $total_characters),
            array_slice($lower, 0, $total_characters + $rest_characters),
            array_slice($upper, 0, $total_characters)
        );
        shuffle($chars);
        $string = implode($chars);

        return $string;
    }
}
