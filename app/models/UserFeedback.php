<?php


class UserFeedback extends CModel
{
    public function rules()
    {
        return array();
    }

    public function attributeNames()
    {
        return array();
    }

    public static function model()
    {
        return new self();
    }

    public function saveFeedback($data)
    {
        $builder = Yii::app()->db->schema->commandBuilder;

        return $builder->createInsertCommand('user_feedback', $data)->execute();
    }

    public function getFeedback($userId)
    {
        return Yii::app()->db
            ->createCommand("SELECT f.id FROM user_feedback AS f WHERE f.user_id = $userId ORDER BY id DESC LIMIT 1")
            ->queryRow();
    }

    public function saveFile($data)
    {
        $builder = Yii::app()->db->schema->commandBuilder;

        $builder->createInsertCommand('user_feedback_file', $data)->execute();
    }

    public function getReasons()
    {
        return Yii::app()->db
            ->createCommand("SELECT r.* FROM user_feedback_reason as r")
            ->queryAll();
    }
}