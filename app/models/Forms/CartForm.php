<?php

/**
 * CartForm class.
 * CartForm is the data structure for keeping
 * subscribe form data. It is used by the 'subscribe' action of 'AjaxController'.
 */
class CartForm extends FormModel
{
	public $product_id;
	public $variant_id;
	public $option_id;
	public $frequency;
	public $qty;
	public $price;
	public $title;

	private $product;
	private $variant;
	private $options;
	private $discount;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'qty',
				'filter',
				'filter' => 'intval',
			),
			array(
				'price',
				'filter',
				'filter' => 'floatval',
			),
			array(
				'product_id',
				'isValidProduct',
				'message' => Lang::t('cart.error.invalidProduct'),
				'on' => 'add, update, price',
			),
			array(
				'variant_id',
				'isValidVariant',
				'message' => Lang::t('cart.error.invalidVariant'),
				'on' => 'add, update, price',
			),
			array(
				'option_id',
				'isValidOptions',
				'message' => Lang::t('cart.error.invalidOption'),
				'on' => 'add, update, price',
			),
			array(
				'qty',
				'isValidQty',
				'on' => 'add, update',
			),
            array(
                'frequency',
                'filter',
				'filter' => 'intval'
            ),
			/* array(
				'qty',
				'numerical',
				'min' => 1,
				'integerOnly' => true,
				'message' => Lang::t('cart.error.invalidQuantity'),
				'tooSmall' => Lang::t('cart.error.invalidQuantityTooSmall'),
				'on' => 'add',
			),
			array(
				'qty',
				'numerical',
				'min' => 0,
				'integerOnly' => true,
				'message' => Lang::t('cart.error.invalidQuantity'),
				'tooSmall' => Lang::t('cart.error.invalidQuantityTooSmall0'),
				'on' => 'update',
			), */
			array(
				'price',
				'isValidPrice',
				'message' => Lang::t('cart.error.invalidPrice'),
				'on' => 'price, add_custom_product',
			),
			array(
				'title, price',
				'required',
				'message' => Lang::t('global.error.invalidData'),
				'on' => 'add_custom_product',
			),
			array(
				'product_id, variant_id, option_id, qty, price',
				'safe',
				'on' => 'remove',
			),
		);
	}
	
	public function isValidProduct($attribute, $params)
	{
		if (preg_match('#^C_[0-9abcdef]{13}#ui', $this->$attribute)) {
			// custom product
			return;
		}

		$this->$attribute = (int) $this->$attribute;
		$this->product = Product::model()->getProductById($this->$attribute);

		if (empty($this->product)) {
			$this->addError($attribute, $params['message']);
		}
	}

	public function isValidVariant($attribute, $params)
	{
		if (empty($this->product) || $this->product['product_price_type'] != 'variants') {
			return;
		}

		$this->variant = Product::model()->getProductVariant($this->product_id, $this->$attribute);

		if (empty($this->variant)) {
			$this->addError($attribute, $params['message']);
		}
	}

	public function isValidOptions($attribute, $params)
	{
		if (empty($this->$attribute)) {
			return;
		}

		$options = Product::model()->getProductOptionsData($this->product_id, $this->$attribute);

		if (empty($options) || count($options) != count($this->$attribute)) {
			$this->addError($attribute, $params['message']);
		}

		$this->options = $options;
	}

	public function isValidQty($attribute, $params)
	{
		// validate minimal qty
		if ($this->scenario == 'add' && $this->$attribute < 1) {
			$this->addError($attribute, Lang::t('cart.error.invalidQuantityTooSmall'));
			return;
		} elseif ($this->scenario == 'update' && $this->$attribute < 0) {
			$this->addError($attribute, Lang::t('cart.error.invalidQuantityTooSmall0'));
			return;
		}

		// validate available qty
		if (Yii::app()->params->settings['stock'] != 'none') {
			$product = $this->getProduct();
			$variant = $this->getVariant();

			if (empty($product)) {
				return;
			}

			$available_qty = !empty($variant) ? $variant['variant_stock_qty'] : $product['product_stock_qty'];

			if ($this->scenario == 'add') {
				if ($available_qty < 1) {
					$this->addError($attribute, !empty($variant) ? 'Выбранный размер не доступен для покупки! Пожалуйста, обновите страницу' : 'Товар не доступен для покупки! Пожалуйста, обновите страницу');
				}
			}
		}
	}

	public function isValidPrice($attribute, $params)
	{
		if (Yii::app()->user->isGuest) {
			$this->addError($attribute, Lang::t('global.error.invalidData'));
		}

		if (preg_match('#^C_[0-9abcdef]{13}#ui', $this->$attribute)) {
			// custom product
			if ($this->$attribute < 1) {
				$this->$attribute = 1;
			} else if ($this->$attribute > 999999) {
				$this->$attribute = 999999;
			}
		} elseif (!empty($this->product)) {
			$regular_price = !empty($this->variant) ? $this->variant['variant_price'] : $this->product['product_price'];

			if (!empty($this->options)) {
				foreach ($this->options as $option) {
					$regular_price += (float) $option['option_price'];
				}
			}

			$max_price = (float) $regular_price;
			$discount = Product::getDiscountPrice($max_price, $this->product['category_id'], $this->product['brand_id']);
			$min_price = (float) $discount['price'];

			if ($this->$attribute < $min_price) {
				$this->$attribute = $min_price;
			} else if ($this->$attribute > $max_price) {
				$this->$attribute = $max_price;
			}

			// calculate discount
			if ($this->$attribute == $max_price) {
				$this->discount = 0;
			} elseif ($this->$attribute == $min_price) {
				$this->discount = $discount['discount'];
			} else {
				$this->discount = round(100 - $this->$attribute * 100 / $max_price);
			}
		}
	}

	public function getProduct()
	{
		return $this->product;
	}

	public function getVariant()
	{
		return $this->variant;
	}

	public function getOptions()
	{
		return $this->options;
	}

    public function getFrequency()
    {
        return $this->frequency;
    }

	public function getDiscount()
	{
		return $this->discount;
	}
}