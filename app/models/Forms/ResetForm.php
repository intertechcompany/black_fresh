<?php

/**
 * ResetForm class.
 * ResetForm is the data structure for keeping
 * forgot form data. It is used by the 'forgot' action of 'SiteController'.
 */
class ResetForm extends FormModel
{
	public $login;
	public $user_id;
	public $token;
	public $password;
	public $password_confirm;

	private $user;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'login',
				'required',
				'message' => Lang::t('resetPassword.error.enterLogin'),
				'on' => 'reset',
			),
			array(
				'login',
				'isValidUser',
				'message' => Lang::t('resetPassword.error.noUser'),
				'skipOnError' => true,
				'on' => 'reset',
			),
			array(
				'user_id',
				'isValidToken',
				'message' => Lang::t('resetPassword.error.invalidLink'),
				'on' => 'newpassword',
			),
			array(
				'token',
				'safe',
				'on' => 'newpassword',
			),
			array(
				'password',
				'required',
				'message' => Lang::t('resetPassword.error.enterNewPassword'),
				'on' => 'newpassword',
			),
			array(
				'password_confirm',
				'required',
				'message' => Lang::t('resetPassword.error.confirmNewPassword'),
				'on' => 'newpassword',
			),
			array(
				'password_confirm',
				'compare',
				'compareAttribute' => 'password',
				'message' => Lang::t('resetPassword.error.passwordDoNotMatch'),
				'skipOnError' => true,
				'on' => 'newpassword',
			),
		);
	}

	public function isValidUser($attribute, $params)
	{
		$this->user = User::model()->getUserByLoginOrEmail($this->$attribute);

		if (empty($this->user)) {
			$this->addError($attribute, $params['message']);

			return false;
		}

		return true;
	}

	public function isValidToken($attribute, $params)
	{
		$this->user = User::model()->getUserById($this->$attribute);

		if (empty($this->user) || $this->user['user_reset_token'] != $this->token) {
			$this->addError($attribute, $params['message']);

			return false;
		}

		return true;
	}

	public function getUser()
	{
		return $this->user;
	}
}