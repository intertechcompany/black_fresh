<?php

/**
 * DiscountForm class.
 * DiscountForm is the data structure for keeping
 * course discount form data. It is used by the 'courseRegistration' action of 'AdminController'.
 */
class DiscountForm extends FormModel
{
	public $code;

	private $discount;
	
	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'code',
				'isValidDiscount',
			),
		);
	}

	public function isValidDiscount($attribute, $params)
	{
		$this->discount = Discount::model()->getDiscountByCode($this->$attribute);

        if (!empty($this->discount)) {
			$now = time();
			
			if ($this->discount['discount_allowed_uses'] > 0 && $this->discount['discount_allowed_uses'] == $this->discount['activated']) {
				// validate uses
				$this->addError($attribute, 'Промокод вже активовано!');
			} elseif ($this->discount['discount_start'] != '0000-00-00' && strtotime($this->discount['discount_start']) > $now) {
				// validate start date
				$this->addError($attribute, 'Вийшов термін промокоду!');
			} elseif ($this->discount['discount_end'] != '0000-00-00' && strtotime($this->discount['discount_end']) < $now) {
				// validate end date
				$this->addError($attribute, 'Вийшов термін промокоду!');
			}
        } else {
            $this->addError($attribute, 'Невірний промокод!');
        }
    }

	public function getDiscount()
	{
		return $this->discount;
	}
}
