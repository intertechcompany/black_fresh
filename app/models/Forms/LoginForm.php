<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends FormModel
{
	public $login;
	public $password;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'login',
				'required',
				'message' => Lang::t('login.error.enterLogin'),
			),
			array(
				'password',
				'required',
				'message' => Lang::t('login.error.enterPassword'),
			),
		);
	}
}