<?php
/**
 * RegistrationForm class.
 * RegistrationForm is the data structure for keeping
 * login form data. It is used by the 'login' action of 'SiteController'.
 */
class RegistrationForm extends FormModel
{
    public $first_name;
    public $last_name;
    public $email;
    public $phone;
    public $password;
    public $password_confirm;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return [
            [
                'email, phone, first_name, last_name, password, password_confirm',
                'required',
                'message' => 'Заполните все поля формы!',
            ],
            [
                'email',
                'email',
                'pattern' => '/^([a-z0-9]([\-\_\.]*[a-z0-9])*)+@([a-z0-9]([\-]*[a-z0-9])*\.)+[a-z]{2,}$/i',
                'message' => 'Некорректный email!',
                'skipOnError' => true,
            ],
            [
                'email',
                'isValidEmail',
                'message' => 'Пользователь с таким email уже зарегистрирован!',
                'skipOnError' => true,
            ],
            [
                'phone',
                'isValidPhone',
                'message' => 'Пользователь с таким телефоном уже зарегистрирован!',
                'skipOnError' => true,
            ],
            [
                'password_confirm',
                'compare',
                'compareAttribute' => 'password',
                'message' => Lang::t('account.error.passwordsDoNotMatch'),
                'skipOnError' => true,
                'on' => 'password',
            ],
        ];
    }

    public function isValidEmail($attribute, $params)
    {
        $user = User::model()->getUserByEmail($this->$attribute);

        if (!empty($user)) {
            $this->addError($attribute, $params['message']);
        }
    }

    public function isValidPhone($attribute, $params)
    {
        $user = User::model()->getUserByPhone($this->$attribute);

        if (!empty($user)) {
            $this->addError($attribute, $params['message']);
        }
    }
}