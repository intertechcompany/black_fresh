<?php

/**
 * OrderForm class.
 * OrderForm is the data structure for keeping
 * write mail form data. It is used by the 'mail' action of 'AjaxController'.
 */
class OrderForm extends FormModel
{
	public $delivery;
	public $payment;
	public $first_name;
	public $last_name;
	public $full_name;
	public $phone;
	public $email;
	public $country;
	public $zip;
	public $region;
	public $city;
	public $address;
	public $np_city;
	public $np_department;
	public $np_address;
	public $np_building;
	public $np_apartment;
	public $comment;
	public $gift_text;

	public $is_offline;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'payment',
				'filter',
				'filter' => 'intval',
			),
			array(
				'payment',
				'in',
				'range' => array(1,2),
				'allowEmpty' => false,
				'message' => Lang::t('checkout.error.invalidPaymentMethod'),
			),
			array(
				'delivery',
				'filter',
				'filter' => 'intval',
			),
			array(
				'delivery',
				'in',
				'range' => array(1,2,3,4),
				'allowEmpty' => false,
				'message' => Lang::t('checkout.error.invalidDeliveryMethod'),
			),
			array(
				'first_name, last_name, phone, email',
				'required',
				'message' => Lang::t('checkout.error.required'),
			),
			/* array(
				'first_name, last_name',
				'match',
				'pattern' => '/^[\p{L}]+$/ui',
				'message' => 'Допускаються тільки літери!',
				'skipOnError' => true,
			), */
			array(
				'full_name',
				'filter',
				'filter' => array($this, 'fullName'),
			),
			array(
				'phone',
				'match',
				'pattern' => '/^\+380 \d{2} \d{3} \d{4}$/ui',
				'message' => Lang::t('checkout.error.phone'),
				'skipOnError' => true,
			),
			array(
				'email',
				'email',
				'pattern' => '/^([a-z0-9]([\-\_\.]*[a-z0-9])*)+@([a-z0-9]([\-]*[a-z0-9])*\.)+[a-z]{2,}$/i',
				'message' => Lang::t('checkout.error.email'),
				'skipOnError' => true,
			),
			/* array(
				'country, city, address',
				'isValidWorldwideDelivery',
				'message' => Lang::t('checkout.error.required'),
			), */
			array(
				'address',
				'isValidCourierDelivery',
				'message' => Lang::t('checkout.error.required'),
			),
			array(
				'np_city',
				'isValidNpDelivery',
				'message' => Lang::t('checkout.error.city'),
			),
			array(
				'np_department',
				'isValidNpDelivery',
				'message' => Lang::t('checkout.error.department'),
			),
            array(
                'phone',
                'isValidPhone',
                'message' => Lang::t('checkout.error.phone.validation.number'),
            ),
			array(
				'zip, region, np_building, np_apartment, comment, gift_text, is_offline',
				'safe',
			),
		);
	}

	public function isValidWorldwideDelivery($attribute, $params)
	{
		if ($this->delivery == 3 && empty($this->$attribute)) {
			$this->addError($attribute, $params['message']);
			return;
		}
	}
	
	public function isValidCourierDelivery($attribute, $params)
	{
		if ($this->delivery == 2 && empty($this->$attribute)) {
			$this->addError($attribute, $params['message']);
			return;
		}
	}

	public function isValidNpDelivery($attribute, $params)
	{
		if ($attribute == 'np_city') {
			if ($this->delivery == 3 && empty($this->$attribute)) {
				$this->addError($attribute, $params['message']);
				return;
			}
		} elseif ($attribute == 'np_department') {
			if ($this->delivery == 3 && empty($this->$attribute)) {
				$this->addError($attribute, $params['message']);
				return;
			}
		} elseif ($attribute == 'np_address') {
			if ($this->delivery == 1 && empty($this->$attribute)) {
				$this->addError($attribute, $params['message']);
				return;
			}
		}
	}

	public function fullName()
	{
		return $this->first_name . ' ' . $this->last_name;
	}

	public function isValidPhone($attribute, $params)
    {
        $test = $this->phone;
        $res = strpos($this->phone, "+380 0");
        if (strpos($this->phone, "+380 0") !== false) {
            $this->addError($attribute, $params['message']);
        }
    }
}