<?php

/**
 * RequestForm class.
 */
class RequestForm extends FormModel
{
    public $course_id;
    public $first_name;
    public $last_name;
    public $phone;
    public $email;
    public $code;

    private $course;
    private $discount;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array(
                'course_id',
                'isValidCourse',
                'message' => Lang::t('course.error.invalidData'),
            ),
            array(
                'first_name, last_name, phone, email',
                'required',
                'message' => Lang::t('course.error.required'),
            ),
            /* array(
				'first_name, last_name',
				'match',
				'pattern' => '/^[\p{L}]+$/ui',
				'message' => 'Допускаються тільки літери!',
				'skipOnError' => true,
            ), */
            array(
				'phone',
				'match',
				'pattern' => '/^\+380 \d{2} \d{3} \d{4}$/ui',
				'message' => Lang::t('course.error.phone'),
				'skipOnError' => true,
			),
            array(
                'email',
                'email',
                'pattern' => '/^([a-z0-9]([\-\_\.]*[a-z0-9])*)+@([a-z0-9]([\-]*[a-z0-9])*\.)+[a-z]{2,}$/i',
                'message' => Lang::t('course.error.email'),
                'skipOnError' => true,
            ),
            array(
                'code',
                'isValidDiscount',
                'message' => Lang::t('course.error.invalidData'),
            )
        );
    }

    public function isValidCourse($attribute, $params)
	{
		$this->course = Course::model()->getCourseById($this->$attribute);
		
		if (empty($this->course) || strtotime($this->course['course_date']) <= time()) {
			$this->addError($attribute, $params['message']);
		}
    }

    public function isValidDiscount($attribute, $params)
    {
        $this->discount = Discount::model()->getDiscountByCode($this->$attribute);
        $checkValid = [];
        $now = time();

        if (empty($this->discount)) {
            $this->addError($attribute, Lang::t('course.error.discount.empty'));
        } elseif ($this->discount['is_school'] != 1) {
            $this->addError($attribute, Lang::t('course.error.discount.service'));
        } elseif ($this->discount['discount_allowed_uses'] > 0 && $this->discount['discount_allowed_uses'] == $this->discount['activated']) {
            // validate uses
            $this->addError($attribute, 'Промокод вже активовано!');
        } elseif ($this->discount['discount_start'] != '0000-00-00' && strtotime($this->discount['discount_start']) > $now) {
            // validate start date
            $this->addError($attribute, 'Вийшов термін промокоду!');
        } elseif ($this->discount['discount_end'] != '0000-00-00' && strtotime($this->discount['discount_end']) < $now) {
            // validate end date
            $this->addError($attribute, 'Вийшов термін промокоду!');
        }

        if ($this->discount['is_auto']) {
            $checkValid['auto'] = Discount::model()->checkAuto($this->discount);
        }

        if ($this->discount['is_auto_season']) {
            $checkValid['season'] = Discount::model()->checkSeason($this->discount);
        }

        if ($this->discount['is_personal']) {
            $checkValid['personal'] = Discount::model()->checkPersonal($this->discount);
        }

        if (!empty($checkValid) && ((isset($checkValid['auto']) && $checkValid['auto']) || (isset($checkValid['season']) && $checkValid['season']) || (isset($checkValid['personal']) && $checkValid['personal']))) {
            return;
        } else {
            if (!empty($checkValid)) {
                $today = date('Y-m-d');

                if (isset($checkValid['auto']) && !$checkValid['auto']) {
                    if (!Yii::app()->user->id) {
                        $this->addError($attribute, Lang::t('checkout.tip.discount.error.user.login'));
                    } else {
                        $this->addError($attribute, Lang::t('checkout.tip.discount.error.user.used'));
                    }
                }

                if (isset($checkValid['season']) && !$checkValid['season']) {
                    if ($this->discount['season_date_start'] > $today) {
                        $this->addError($attribute, Lang::t('checkout.tip.discount.error.season.start'));
                    } elseif ($this->discount['season_date_end'] < $today) {
                        $this->addError($attribute, Lang::t('checkout.tip.discount.error.season.end'));
                    }
                }

                if (isset($checkValid['personal']) && !$checkValid['personal']) {
                    $this->addError($attribute, Lang::t('checkout.tip.discount.error.user.personal'));
                }
            }
        }
    }

    public function getCourse()
    {
        return $this->course;
    }
}
