<?php

/**
 * SubscriberForm class.
 */
class SubscriberForm extends FormModel
{
    public $email;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array(
                'email',
                'required',
                'message' => Lang::t('subscribe.error.required'),
            ),
            array(
                'email',
                'email',
                'pattern' => '/^([a-z0-9]([\-\_\.]*[a-z0-9])*)+@([a-z0-9]([\-]*[a-z0-9])*\.)+[a-z]{2,}$/i',
                'message' => Lang::t('subscribe.error.email'),
                'skipOnError' => true,
            ),
            array(
                'email',
                'isSubscribed',
                'message' => Lang::t('subscribe.error.alreadySubscribed'),
                'skipOnError' => true,
            ),
        );
    }

    public function isSubscribed($attribute, $params)
	{
		$subscriber = Subscriber::model()->getSubscriberByEmail($this->$attribute);
		
		if (!empty($subscriber)) {
			$this->addError($attribute, $params['message']);
		}
	}
}
