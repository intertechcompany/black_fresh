<?php

/**
 * AccountForm class.
 * AccountForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AccountController'.
 */
class AccountForm extends FormModel
{
	public $user_first_name;
	public $user_last_name;
	public $user_email;
	public $user_phone;

	public $password_current;
	public $password;
	public $password_confirm;

	private $user;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'user_first_name',
				'required',
				'message' => Lang::t('account.error.enterFirstName'),
				'on' => 'personal',
			),
			array(
				'user_email',
				'required',
				'message' => Lang::t('account.error.enterEmail'),
				'on' => 'personal',
			),
			array(
				'user_email',
				'email',
				'pattern' => '/^([a-z0-9]([\-\_\.]*[a-z0-9])*)+@([a-z0-9]([\-]*[a-z0-9])*\.)+[a-z]{2,6}$/i',
				'message' => Lang::t('account.error.invalidEmail'),
				'skipOnError' => true,
				'on' => 'personal',
			),
			array(
				'user_email',
				'isValidEmail',
				'message' => Lang::t('account.error.anotherEmail'),
				'skipOnError' => true,
				'on' => 'personal',
			),
			array(
				'user_last_name, user_phone',
				'safe',
				'on' => 'personal',
			),
			array(
				'password_current',
				'required',
				'message' => Lang::t('account.error.currentPassword'),
				'on' => 'password',
			),
			array(
				'password_current',
				'isCorrectPassword',
				'message' => Lang::t('account.error.invalidCurrentPassword'),
				'skipOnError' => true,
				'on' => 'password',
			),
			array(
				'password',
				'required',
				'message' => Lang::t('account.error.newPassword'),
				'on' => 'password',
			),
			array(
				'password_confirm',
				'required',
				'message' => Lang::t('account.error.confirmNewPassword'),
				'on' => 'password',
			),
			array(
				'password',
				'compare',
				'compareAttribute' => 'password_current',
				'operator' => '!=',
				'message' => Lang::t('account.error.equivalentPasswords'),
				'skipOnError' => true,
				'on' => 'password',
			),
			array(
				'password_confirm',
				'compare',
				'compareAttribute' => 'password',
				'message' => Lang::t('account.error.passwordsDoNotMatch'),
				'skipOnError' => true,
				'on' => 'password',
			),
		);
	}

	public function isValidEmail($attribute, $params)
	{
		$user = User::model()->getUserByEmail($this->$attribute);
		
		if (!empty($user) && Yii::app()->user->id != $user['user_id']) {
			$this->addError($attribute, $params['message']);

			return false;
		}
	}
	
	public function isCorrectPassword($attribute, $params)
	{
		$this->user = Yii::app()->user->user_data;
		$this->user['user_id'] = Yii::app()->user->id;

		$user_model = User::model();
		$user = $user_model->getUser();
					
		if (!$user_model->validatePassword($this->$attribute, $user['user_password'])) {
			$this->addError($attribute, $params['message']);
		}
	}

	public function getUser()
	{
		return $this->user;
	}
}