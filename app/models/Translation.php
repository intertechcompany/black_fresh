<?php
class Translation extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function getTranslationByCode($code, $lang)
	{
		$translation = Yii::app()->db
			->createCommand("SELECT t.translation_id, t.translation_type, t.translation_code, tl.language_code, tl.translation_value FROM translation as t JOIN translation_lang as tl ON t.translation_id = tl.translation_id AND tl.language_code = :lang WHERE t.translation_code = :code")
			->bindValue(':lang', $lang, PDO::PARAM_STR)
			->bindValue(':code', $code, PDO::PARAM_STR)
			->queryRow();
			
		return $translation;
	}

	public function warmUpCache()
	{
		$builder = Yii::app()->db->schema->commandBuilder;

		Yii::app()->cache->delete('_t');

		$lang_codes = array();
		$translations_cache = array();
		$js_translation = array();

		// get all translations
		$translations = Yii::app()->db
			->createCommand("SELECT t.translation_id, t.translation_code, tl.language_code, tl.translation_value FROM translation as t JOIN translation_lang as tl ON t.translation_id = tl.translation_id ORDER BY t.translation_id")
			->queryAll();

		if (!empty($translations)) {
			foreach ($translations as $translation) {
				$code = $translation['translation_code'];
				$lang = $translation['language_code'];

				$lang_codes[$lang] = $lang;

				$translations_cache[$code][$lang] = $translation['translation_value'];

				// add js value
				if (!isset($js_translation[$lang])) {
					$js_translation[$lang] = array();
				}

				list($type, $group, $variable) = array_pad(explode('.', $code), 3, '');

				if ($type == 'javaScript') {
					if (!empty($variable)) {
						$js_translation[$lang][$group][$variable] = $translation['translation_value'];
					} else {
						$js_translation[$lang][$group] = $translation['translation_value'];
					}
				}
			}
		}

		Yii::app()->cache->set('_t', $translations_cache);

		$lang_path = Yii::app()->assetManager->getBasePath() . DS . 'static' . DS . 'lang';

		// prepare js
		$new_js = array();

		foreach ($lang_codes as $lang_code) {
			/*
			$json_data = array(
				'mfp_close' => isset($translations_cache['javaScript.magnificPopup.close'][$lang_code]) ? $translations_cache['javaScript.magnificPopup.close'][$lang_code] : '',
				'mfp_loading' => isset($translations_cache['javaScript.magnificPopup.loading'][$lang_code]) ? $translations_cache['javaScript.magnificPopup.loading'][$lang_code] : '',
				'mfp_prev' => isset($translations_cache['javaScript.magnificPopup.prev'][$lang_code]) ? $translations_cache['javaScript.magnificPopup.prev'][$lang_code] : '',
				'mfp_next' => isset($translations_cache['javaScript.magnificPopup.next'][$lang_code]) ? $translations_cache['javaScript.magnificPopup.next'][$lang_code] : '',
				'mfp_counter' => isset($translations_cache['javaScript.magnificPopup.counter'][$lang_code]) ? $translations_cache['javaScript.magnificPopup.counter'][$lang_code] : '',
				'mfp_image_err' => isset($translations_cache['javaScript.magnificPopup.imageError'][$lang_code]) ? $translations_cache['javaScript.magnificPopup.imageError'][$lang_code] : '',
				'mfp_ajax_err' => isset($translations_cache['javaScript.magnificPopup.ajaxError'][$lang_code]) ? $translations_cache['javaScript.magnificPopup.ajaxError'][$lang_code] : '',
				'pswp_close' => isset($translations_cache['javaScript.photoSwipe.close'][$lang_code]) ? $translations_cache['javaScript.photoSwipe.close'][$lang_code] : '',
				'pswp_fullscreen' => isset($translations_cache['javaScript.photoSwipe.fullscreen'][$lang_code]) ? $translations_cache['javaScript.photoSwipe.fullscreen'][$lang_code] : '',
				'pswp_zoom' => isset($translations_cache['javaScript.photoSwipe.zoom'][$lang_code]) ? $translations_cache['javaScript.photoSwipe.zoom'][$lang_code] : '',
				'pswp_prev' => isset($translations_cache['javaScript.photoSwipe.prev'][$lang_code]) ? $translations_cache['javaScript.photoSwipe.prev'][$lang_code] : '',
				'pswp_next' => isset($translations_cache['javaScript.photoSwipe.next'][$lang_code]) ? $translations_cache['javaScript.photoSwipe.next'][$lang_code] : '',
				'std_ajax_err' => isset($translations_cache['javaScript.ajaxError'][$lang_code]) ? $translations_cache['javaScript.ajaxError'][$lang_code] : '',
				'std_page_load_err' => isset($translations_cache['javaScript.pageLoadError'][$lang_code]) ? $translations_cache['javaScript.pageLoadError'][$lang_code] : '',
			);
			*/
			
			$new_js[$lang_code] = 'var _t = ' . json_encode($js_translation[$lang_code]) . ';';
		}

		$rebuild_js = false;

		// get current version of js translations
		$js_ver = Yii::app()->params->settings['js_rev'];

		if (!empty($js_ver)) {
			foreach ($lang_codes as $lang_code) {
				if (!is_file($lang_path . DS . $js_ver . DS . $lang_code . '.js')) {
					$rebuild_js = true;
					break;
				}

				// read file
				$old_js = file_get_contents($lang_path . DS . $js_ver . DS . $lang_code . '.js');

				if (strcmp($old_js, $new_js[$lang_code]) !== 0) {
					$rebuild_js = true;
					break;
				}
			}
		} else {
			$rebuild_js = true;
		}

		if ($rebuild_js) {
			// remove old files
			if (is_dir($lang_path . DS . $js_ver)) {
				CFileHelper::removeDirectory($lang_path . DS . $js_ver);
			}

			// rebuild js lang files
			
			// digits
			$digits = range(0, 9);
			shuffle($digits);
			
			// small letters
			$lower = range('a', 'z');
			shuffle($lower);

			// caption letters
			$upper = range('A', 'Z');
			shuffle($upper);
			
			$chars = array_merge(array_slice($lower, 0, 4), array_slice($digits, 0, 2), array_slice($upper, 0, 2));
			shuffle($chars);
			$new_js_hash = implode($chars);

			$dir_rs = CFileHelper::createDirectory($lang_path . DS . $new_js_hash, 0755, true);

			if ($dir_rs) {
				// write to files
				foreach ($lang_codes as $lang_code) {
					file_put_contents($lang_path . DS . $new_js_hash . DS . $lang_code . '.js', $new_js[$lang_code]);
				}

				$update_setting = array(
					'saved' => date('Y-m-d H:i:s'),
					'setting_js_rev' => $new_js_hash,
				);

				$update_criteria = new CDbCriteria(
					array(
						"condition" => "setting_id = :setting_id" , 
						"params" => array(
							"setting_id" => 1,
						)
					)
				);

				try {
					$builder->createUpdateCommand('setting', $update_setting, $update_criteria)->execute();
				} catch (CDbException $e) {
					// ...
				}
			}
		}
	}
}