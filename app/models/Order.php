<?php

class Order extends CModel
{
	public function rules()
	{
        return array();
    }
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
        return new self();
    }
	
	public function setUser($model)
	{
		$_SESSION['order']['user'] = array(
			'first_name' => $model->first_name,
			'last_name' => $model->last_name,
			'phone' => $model->phone,
			'email' => $model->email,
			'country' => $model->country,
			'zip' => $model->zip,
			'region' => $model->region,
			'city' => $model->city,
			'address' => $model->address,
			'np_city' => $model->np_city,
			'np_department' => $model->np_department,
			'np_address' => $model->np_address,
			'np_building' => $model->np_building,
			'np_apartment' => $model->np_apartment,
			'comment' => $model->comment,
		);
	}

	public function getUser()
	{
		if (!empty($_SESSION['order']['user'])) {
			$user = array(
				'first_name' => $_SESSION['order']['user']['first_name'],
				'last_name' => $_SESSION['order']['user']['last_name'],
				'phone' => $_SESSION['order']['user']['phone'],
				'email' => $_SESSION['order']['user']['email'],
				'country' => $_SESSION['order']['user']['country'],
				'zip' => $_SESSION['order']['user']['zip'],
				'region' => $_SESSION['order']['user']['region'],
				'city' => $_SESSION['order']['user']['city'],
				'address' => $_SESSION['order']['user']['address'],
				'np_city' => $_SESSION['order']['user']['np_city'],
				'np_department' => $_SESSION['order']['user']['np_department'],
				'np_address' => $_SESSION['order']['user']['np_address'],
				'np_building' => $_SESSION['order']['user']['np_building'],
				'np_apartment' => $_SESSION['order']['user']['np_apartment'],
				'comment' => $_SESSION['order']['user']['comment'],
			);
		} else {
			$user = array(
				'first_name' => '',
				'last_name' => '',
				'phone' => '',
				'email' => '',
				'country' => '',
				'zip' => '',
				'region' => '',
				'city' => '',
				'address' => '',
				'np_city' => '',
				'np_department' => '',
				'np_address' => '',
				'np_building' => '',
				'np_apartment' => '',
				'comment' => '',
			);
		}
		
		return $user;
	}
	
	public function setDelivery($model)
	{
		$_SESSION['order']['delivery'] = $model->delivery;
	}

	public function getDelivery()
	{
		if (!empty($_SESSION['order']['delivery'])) {
			$delivery = $_SESSION['order']['delivery'];
		} else {
			$delivery = 0;
		}
		
		return $delivery;
	}
	
	public function setPayment($model)
	{
		$_SESSION['order']['payment'] = $model->payment;
	}

	public function getPayment()
	{
		if (!empty($_SESSION['order']['payment'])) {
			$payment = $_SESSION['order']['payment'];
		} else {
			$payment = 0;
		}
		
		return $payment;
	}
	
	public function setThankYou($thank_you)
	{
		$_SESSION['thank_you'] = $thank_you;
	}

	public function getThankYou()
	{
		if (isset($_SESSION['thank_you'])) {
			$thank_you = $_SESSION['thank_you'];
			unset($_SESSION['thank_you']);

			return $thank_you;
		}
		
		return false;
	}
	
	public function clear()
	{
		unset($_SESSION['order']);
	}

	public function add($model, $price, $cart_items, $discount, $discount_value) 
	{
		// add order data to db
		$order_id = null;
		$today = date('Y-m-d H:i:s');
		$user_id = (!Yii::app()->user->isGuest) ? Yii::app()->user->id : 0;
		
		// collect order items
		$insert_items = array();
		$seller_price = 0;
		$index = 0;

		// collect order item options
		$insert_item_options = array();
		
		foreach ($cart_items as $cart_item) {
		    $newPrice = 0;
		    $discountValue = 0;

			$product = $cart_item['data'];
			$variant = $cart_item['variant'];
			$options = $cart_item['options'];
			$regular_price = !empty($variant) ? $variant['variant_price'] : $product['product_price'];

			$insert_item_options[$index] = array();

			if (!empty($options)) {
				$options_values = array();

				foreach ($options as $option) {
					$regular_price += (float) $option['option_price'];
					$options_values[] = $option['property_title'] . ': ' . $option['value_title'];
					$insert_item_options[$index][] = array(
						'order_item_id' => null,
						'option_id' => $option['option_id'],
						'option_price' => (float) $option['option_price'],
						'option_title' => $option['property_title'] . ': ' . $option['value_title'],
					);
				}

				$options_title = implode("\n", $options_values);
			} else {
				$options_title = '';
			}

			// $item_discount = Product::getDiscountPrice($regular_price, $product['category_id'], $product['brand_id']);
			$item_discount['price'] = 0;

			if (!empty($variant['values'])) {
				$variant_values = array();

				foreach ($variant['values'] as $value) {
					$variant_values[] = CHtml::encode($value['property_title'] . ': ' . $value['value_title']);
				}
			
				$variant_title = implode("\n", $variant_values);
			} else {
				$variant_title = '';
			}

			if (preg_match('#^C_[0-9abcdef]{13}#ui', $cart_item['product_id'])) {
				$cart_item['product_id'] = 0;
			}

			if (!empty($discount)) {
                if ($discount['discount_type'] == 'percentage') {
                    $discountValue = $discount['discount_value'];
                    $newPrice = $cart_item['price'] - ($cart_item['price'] * $discount['discount_value'] / 100);
                } else {
                    $newPrice = $cart_item['price'] - $discount['discount_value'];
                    $discountValue = 100 - ($newPrice / $cart_item['price'] * 100);
                }
            }

			if (!empty($discount) && ($newPrice == 0 || $newPrice < 0)) {
                $newPrice = 2;
            }

			$insert_items[] = array(
				'order_id' => null,
				'product_id' => $cart_item['product_id'],
				'variant_id' => $cart_item['variant_id'],
				'quantity' => $cart_item['qty'],
				'price' => $newPrice ? $newPrice : $cart_item['price'],
				'seller_price' => Yii::app()->user->isGuest ? 0 : $item_discount['price'],
				'discount' => $cart_item['discount'],
				'title' => $product['product_title'],
				'variant_title' => $variant_title,
				'options_title' => $options_title,
                'is_discount' => !empty($discount),
                'discount_value' => round($discountValue),
			);

			if (!Yii::app()->user->isGuest) {
				$seller_price += $item_discount['price'] * $cart_item['qty'];
			}

			$index++;
		}

		$delivery_data = $this->getDeliveryData($model->delivery);

		// insert to client order
		$insert_order = array(
			'created' => $today,
			'saved' => $today,
			'is_new' => 1,
			'user_id' => $user_id,
			'currency' => Yii::app()->params->currency,
			'price' => $price,
			'discount_id' => !empty($discount) ? $discount['discount_id'] : 0,
			'discount_value' => !empty($discount) ? $discount_value['value'] : 0,
			'delivery_price' => $delivery_data['value'],
			'delivery_currency' => $delivery_data['currency'],
			'status' => 1,
		);

		foreach ($model as $attribute => $value) {
			$insert_order[$attribute] = $value;
		}
		
		$builder = Yii::app()->db->schema->commandBuilder;
		
		try {
			$rs = $builder->createInsertCommand('order', $insert_order)->execute();

			if ($rs) {
				$order_id = (int) Yii::app()->db->getLastInsertID();
				$total_items = 0;

				if (!empty($discount)) {
					Discount::model()->activate();
				}

				foreach ($insert_items as $index => $insert_item) {
					$insert_item['order_id'] = $order_id;

					$rs = $builder->createInsertCommand('order_item', $insert_item)->execute();

					if ($rs) {
						$total_items++;
						$order_item_id = (int) Yii::app()->db->getLastInsertID();

						if (!empty($insert_item_options[$index])) {
							foreach ($insert_item_options[$index] as $option_index => $insert_item_option) {
								$insert_item_options[$index][$option_index]['order_item_id'] = $order_item_id;
							}

							$builder->createMultipleInsertCommand('order_item_option', $insert_item_options[$index])->execute();
						}
					}
				}

				if ($total_items != count($cart_items)) {
					// something went wrong... not all certificates have been saved
					$order_id = false;

					// delete an order and send notification to admin
					// ...
				}
			}
		} catch (CDbException $e) {
			return false;
		}
		
		return $order_id;
	}

	public function subtractOrderQty($order_id)
	{
		$product_model = Product::model();
		$products_to_update = [];
		$products = $this->getOrderProducts($order_id);

		foreach ($products as $product) {
			$product_id = $product['product_id'];
			$variant_id = $product['variant_id'];
			$quantity = $product['quantity'];

			$qty_rs = $product_model->subtractStockQty($product);

			if ($qty_rs) {
				$products_to_update[$product_id] = !empty($variant_id) ? 'variants' : 'item';
			}
		}

		if (!empty($products_to_update)) {
			$product_model->updateProductsStockStatus($products_to_update);
		}
	}

	public function getDeliveryData($delivery)
	{
		if ($delivery == 1) {
			return [
				'value' => 0,
				'currency' => 'uah',
			];
		} elseif ($delivery == 2) {
			return [
				'value' => Yii::app()->params->settings['courier'],
				'currency' => 'uah',
			];
		} elseif ($delivery == 3) {
			return [
				// 'value' => Yii::app()->params->settings['np_address'],
				// 'value' => Yii::app()->params->settings['np_department'],
				'value' => 0,
				'currency' => 'uah',
			];
		} elseif ($delivery == 4) {
            return [
                'value' => 0,
                'currency' => 'uah',
            ];
        }
	}

	public function getOrderDeliveryPrice($order)
	{
		$delivery_price = (float) $order['delivery_price'];
		
		if (Yii::app()->params->currency != $order['delivery_currency']) {
			$currency_code = Yii::app()->params->currency;
			$currency_rates = Yii::app()->params->settings['currency'];
			
			$delivery_price = $delivery_price * $currency_rates[$order['delivery_currency']] / $currency_rates[$currency_code];
			
			if (!empty($currency_rates['margin']) && $currency_code == 'uah') {
				$delivery_price = $delivery_price * (1 + $currency_rates['margin'] / 100);
			}

			$delivery_price = round($delivery_price, 2);
		}

		return $delivery_price;
	}

	public function updateStatus($order_id, $status, $comment = '')
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = new DateTime('now', new DateTimeZone(Yii::app()->timeZone));
		$today = $today->format('Y-m-d H:i:s');

		$update = array(
			'saved' => $today,
			'status' => $status,
			'payment_comment' => trim($comment),
		);

		$criteria = new CDbCriteria(
			array(
				"condition" => "order_id = :id" , 
				"params" => array(
					"id" => (int) $order_id,
				)
			)
		);
		
		try {
			$transaction_rs = $builder->createUpdateCommand('order', $update, $criteria)->execute();

			if ($transaction_rs) {
				return true;
			}
		} catch (CDbException $e) {
			
		}

		return false;
	}

	public function getOrderById($order_id)
	{
		$order = Yii::app()->db
			->createCommand("SELECT * FROM `order` WHERE order_id = :order_id")
			->bindValue(':order_id', (int) $order_id, PDO::PARAM_INT)
			->queryRow();

		return $order;
	}

	public function getOrderProducts($order_id)
	{
		$order_products = Yii::app()->db
			->createCommand("SELECT oi.*, p.product_photo, p.product_alias, p.product_instock, p.product_stock_qty, p.product_price_type, pv.variant_instock, pv.variant_stock_qty 
							 FROM order_item as oi 
							 LEFT JOIN product as p 
							 ON oi.product_id = p.product_id 
							 LEFT JOIN product_variant as pv 
							 ON oi.variant_id = pv.variant_id 
							 WHERE oi.order_id = :order_id 
							 ORDER BY oi.order_item_id")
			->bindValue(':order_id', (int) $order_id, PDO::PARAM_INT)
			->queryAll();
		
		return $order_products;
	}

	public function getUserOrders($limit = 0)
	{
		if (!empty($limit)) {
			$limit = 'LIMIT 0,' . $limit;
		} else {
			$limit = '';
		}

		$orders = Yii::app()->db
			->createCommand("SELECT * FROM `order` WHERE user_id = :user_id ORDER BY order_id DESC {$limit}")
			->bindValue(':user_id', (int) Yii::app()->user->id, PDO::PARAM_INT)
			->queryAll();
		
		return $orders;
	}

	public function getUserOrderById($order_id)
	{
		$order = Yii::app()->db
			->createCommand("SELECT * FROM `order` WHERE user_id = :user_id AND order_id = :order_id LIMIT 1")
			->bindValue(':user_id', (int) Yii::app()->user->id, PDO::PARAM_INT)
			->bindValue(':order_id', (int) $order_id, PDO::PARAM_INT)
			->queryRow();
		
		return $order;
	}

	public function getUserOrderProducts($order_id)
	{
		$order_products_list = Yii::app()->db
			->createCommand("SELECT oi.*, p.product_alias, p.product_sku, p.product_photo, p.product_pack_size, p.product_price_type, p.product_length, p.product_width, pl.product_title, pl.product_cart 
							 FROM order_item as oi 
							 LEFT JOIN product as p 
							 ON oi.product_id = p.product_id AND p.active = 1 
							 LEFT JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 WHERE oi.order_id = :order_id 
							 ORDER BY oi.order_item_id")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':order_id', (int) $order_id, PDO::PARAM_INT)
			->queryAll();

        $order_products = array();
        $product_ids = array();

		if (!empty($order_products_list)) {
			foreach ($order_products_list as $order_product) {
				$product_id = $order_product['product_id'];
				$variant_id = $order_product['variant_id'];

				$order_products[$product_id . '_' . $variant_id] = $order_product;
				$order_products[$product_id . '_' . $variant_id]['properties'] = array();

				if (!$order_product['variant_id']) {
					$product_ids[] = $product_id;
				}

				// get options
				$order_products[$product_id . '_' . $variant_id]['options'] = $this->getOrderItemOptions($order_product['order_item_id']);
			}

			if (!empty($product_ids)) {
				// get product properties
				$order_products = Product::model()->getProductsProperties($order_products, $product_ids);
			}

			$variant_ids = array_filter(array_column($order_products, 'variant_id'), function($v) {
    			return $v > 0;
			});

			if (!empty($variant_ids)) {
				$variants = Product::model()->getVariantsByIds($variant_ids);

				foreach ($order_products as $key => $order_product) {
					$order_products[$key]['variant'] = array();
					$variant_id = $order_product['variant_id'];

					if ($variant_id && isset($variants[$variant_id])) {
						$order_products[$key]['variant'] = $variants[$variant_id];
					}
				}
			}
		}
		
		return $order_products;
	}

	public function getOrderItemOptions($order_item_id)
	{
		$options = array();

		$options_list = Yii::app()->db
			->createCommand("SELECT oio.*, po.option_price as original_option_price, p.property_id, pl.property_title, v.value_id, vl.value_title 
							 FROM order_item_option as oio 
							 LEFT JOIN product_option as po 
							 ON oio.option_id = po.option_id 
							 LEFT JOIN property_value as v 
							 ON po.value_id = v.value_id 
							 LEFT JOIN property_value_lang as vl 
							 ON v.value_id = vl.value_id AND vl.language_code = :code AND vl.value_visible = 1 
							 LEFT JOIN property as p 
							 ON v.property_id = p.property_id 
							 LEFT JOIN property_lang as pl 
							 ON p.property_id = pl.property_id AND pl.language_code = :code AND pl.property_visible = 1
							 WHERE oio.order_item_id = :order_item_id
							 ORDER BY p.property_top DESC, pl.property_title, v.value_top DESC, vl.value_title")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':order_item_id', (int) $order_item_id, PDO::PARAM_INT)
			->queryAll();

		if (!empty($options_list)) {
			foreach ($options_list as $option) {
				$option_id = $option['option_id'];
				$options[$option_id] = $option;
			}
		}
			
		return $options;
	}

	public function getUserTotalAmount()
	{
		$total = Yii::app()->db
			->createCommand("SELECT SUM(price) as amount FROM `order` WHERE user_id = :user_id AND status IN ('paid', 'completed')")
			->bindValue(':user_id', (int) Yii::app()->user->id, PDO::PARAM_INT)
			->queryRow();
		
		return $total;
	}
}
