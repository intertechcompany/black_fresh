<?php
class Course extends CModel
{
	private $per_page = 10;

	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function getEvents()
	{
		$courses = Yii::app()->db
			->createCommand("SELECT c.*, cl.course_name  
							 FROM course as c 
							 JOIN course_lang as cl 
							 ON c.course_id = cl.course_id AND cl.language_code = :code 
							 WHERE c.active = 1 AND c.course_type = 'event' 
							 ORDER BY c.course_date, c.course_id")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		foreach ($courses as $id => $course) {
			$courses[$id]['authors'] = $this->getCourseAuthors($course['course_id']);
		}
			
		return $courses;
	}

	public function getCourses()
	{
		$courses = Yii::app()->db
			->createCommand("SELECT c.*, cl.course_tip, cl.course_name  
							 FROM course as c 
							 JOIN course_lang as cl 
							 ON c.course_id = cl.course_id AND cl.language_code = :code 
							 WHERE c.active = 1 AND c.course_type = 'full' 
							 ORDER BY c.course_position, c.course_id DESC")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
		
		foreach ($courses as $id => $course) {
			$courses[$id]['authors'] = $this->getCourseAuthors($course['course_id']);
		}
			
		return $courses;
	}

	public function getCourseAuthors($course_id)
	{
		$authors = Yii::app()->db
			->createCommand("SELECT al.author_name 
							 FROM author_course as ac 
							 JOIN author as a 
							 ON ac.author_id = a.author_id 
							 JOIN author_lang as al 
							 ON a.author_id = al.author_id AND al.language_code = :code 
							 WHERE ac.course_id = :course_id AND a.active = 1 
							 ORDER BY al.author_name")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':course_id', (int) $course_id, PDO::PARAM_INT)
			->queryColumn();
		
		return $authors;
	}

	public function getCourseById($course_id)
	{
		$course = Yii::app()->db
			->createCommand("SELECT * FROM course as c JOIN course_lang as cl ON c.course_id = cl.course_id AND cl.language_code = :code WHERE c.active = 1 AND c.course_id = :course_id LIMIT 1")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':course_id', (int) $course_id, PDO::PARAM_INT)
			->queryRow();

		return $course;
	}

	public function getCourseByAlias($alias)
	{
		$course = Yii::app()->db
			->createCommand("SELECT c.*, cl.course_name, cl.course_tip, cl.course_place, cl.course_content as course_content_lang, cl.course_no_index, cl.course_meta_title, cl.course_meta_description, cl.course_meta_keywords 
							 FROM course as c 
							 JOIN course_lang as cl 
							 ON c.course_id = cl.course_id AND cl.language_code = :code 
							 WHERE c.active = 1 AND c.course_alias = :alias  
							 LIMIT 1")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':alias', $alias, PDO::PARAM_STR)
			->queryRow();

		if (!empty($course)) {
			$course['authors'] = $this->getCourseAuthors($course['course_id']);
		}

		return $course;
	}
}