<?php
class Transaction extends CModel
{
	public function rules() {
		return array();
	}
	
	public function attributeNames() {
		return array();
	}
	
	public static function model() {
		return new self();
	}
	
	public function getTransactionById($id)
	{
		$transaction = Yii::app()->db
			->createCommand("SELECT * FROM transaction WHERE transaction_id = :id")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();
			
		return $transaction;
	}

	public function add($order_id, $price)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = new DateTime('now', new DateTimeZone(Yii::app()->timeZone));
		$today = $today->format('Y-m-d H:i:s');

		$insert_transaction = array(
			'created'   => $today,
			'saved'     => $today,
			'order_id'  => $order_id,
			'price'     => $price,
		);
		
		try {
			$transaction_rs = $builder->createInsertCommand('transaction', $insert_transaction)->execute();

			if ($transaction_rs) {
				$transaction_id = (int) Yii::app()->db->getLastInsertID();
				
				return $transaction_id;
			}
		} catch (CDbException $e) {
			
		}

		return false;
	}

	public function update($transaction_id, $status, $data)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = new DateTime('now', new DateTimeZone(Yii::app()->timeZone));
		$today = $today->format('Y-m-d H:i:s');

		$update = array(
			'saved' => $today,
			'status' => $status,
			'data' => $data,
		);

		$criteria = new CDbCriteria(
			array(
				"condition" => "transaction_id = :id" , 
				"params" => array(
					"id" => (int) $transaction_id,
				)
			)
		);
		
		try {
			$transaction_rs = $builder->createUpdateCommand('transaction', $update, $criteria)->execute();

			if ($transaction_rs) {
				return true;
			}
		} catch (CDbException $e) {
			
		}

		return false;
	}
}
