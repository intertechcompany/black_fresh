<?php
class Discount extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function init()
	{
		if (!isset($_SESSION['discount'])) {
			$_SESSION['discount'] = [];
		}
	}

	public static function getDiscountValue($price)
	{
		if (empty($_SESSION['discount'])) {
			return 0;
		}

		$discount = $_SESSION['discount'];
		
		if ($discount['discount_type'] == 'percentage') {
			$discount_value = $price * -$discount['discount_value'] / 100;
		} else {
			$discount_value = -$discount['discount_value'];
		}

		return round($discount_value);
	}

	public function getDiscountByCode($dicount_code)
	{
		$discount = Yii::app()->db
			->createCommand("SELECT * FROM discount WHERE LOWER(discount_code) LIKE LOWER('%$dicount_code')  AND active = 1 LIMIT 1")
			->queryRow();

		if ($discount) {
            $personal = Yii::app()->db
                ->createCommand("SELECT dp.user_id as id FROM discount_personal as dp WHERE dp.discount_id = :id")
                ->bindValue(':id', (int) $discount['discount_id'], PDO::PARAM_INT)
                ->queryAll();

            $categories = Yii::app()->db
                ->createCommand("SELECT dc.category_id as id FROM discount_categories as dc WHERE dc.discount_id = :id")
                ->bindValue(':id', (int) $discount['discount_id'], PDO::PARAM_INT)
                ->queryAll();

            foreach ($categories as $category) {
                $discount['categories'][] = $category['id'];
            }

            foreach ($personal as $user) {
                $discount['personal'][] = $user['id'];
            }
        }

		return $discount;
	}

	public function getPersonalDiscounts($user)
    {
        $today = date('Y-m-d');
        $registerDiscounts = null;

        if (!$user['register_discount']) {
            $registerDiscounts = Yii::app()->db
                ->createCommand("SELECT * FROM discount WHERE is_auto = 1 AND active = 1")
                ->queryAll()
            ;
        }

        $personalDiscounts = Yii::app()->db
            ->createCommand("SELECT d.* FROM discount as d join discount_personal as dp on dp.discount_id = d.discount_id where dp.user_id = :id and d.is_personal = 1")
            ->bindParam('id', $user['user_id'])
            ->queryAll()
        ;

        $seasonsDiscounts = Yii::app()->db
            ->createCommand("SELECT * FROM discount WHERE is_auto_season = 1 AND active = 1 AND season_date_start <= '$today' AND season_date_end >= '$today'")
            ->queryAll()
        ;

        return [
            'register' => $registerDiscounts ?? null,
            'personal' => $personalDiscounts ?? null,
            'seasons' => $seasonsDiscounts ?? null
        ];
    }

	public function getDiscount()
	{
		$discount = $_SESSION['discount'];
		
		if (!empty($discount)) {
			$form = new DiscountForm();
			$form->attributes = [
				'code' => $discount['discount_code'],
			];

			if (!$form->validate()) {
				$this->clear();
			}
		}
		
		return $_SESSION['discount'];
	}

	public function checkDiscount($discount, $type, $products = null)
    {
        $data = [];
        $checkValid = [];

        if ($discount) {
            if ($discount['is_auto']) {
                $checkValid['auto'] = Discount::model()->checkAuto($discount);
            } else {
                $checkValid['auto'] = false;
            }

            if ($discount['is_auto_season']) {
                $checkValid['season'] = Discount::model()->checkSeason($discount);
            } else {
                $checkValid['season'] = false;
            }

            if ($discount['is_personal']) {
                $checkValid['personal'] = Discount::model()->checkPersonal($discount);
            } else {
                $checkValid['personal'] = false;
            }

            if (!empty($checkValid) && ($checkValid['auto'] || $checkValid['season'] || $checkValid['personal'])) {
                if ($type == 'buy') {
                    $data = Discount::model()->setBuyDiscount($discount, $products);
                } elseif ($type == 'sub') {
                    $data = Discount::model()->setBuyDiscount($discount, $products, 'sub');
                }

                return $data;
            } else {
                if ($type == 'buy') {
                    $data = Discount::model()->setBuyDiscount($discount, $products);
                } elseif ($type == 'sub') {
                    $data = Discount::model()->setBuyDiscount($discount, $products, 'sub');
                }

                if (!$data) {
                    return false;
                }

                return $data;
            }
        }
    }

    public function checkPersonal($discount)
    {
        $user = User::model()->getUserById(Yii::app()->user->id);

        if (!$user) {
            return  false;
        }

        if (!in_array($user['user_id'], $discount['personal'])) {
            return false;
        }

        return true;
    }

    public function checkAuto($discount)
    {
        $user = User::model()->getUserById(Yii::app()->user->id);

        if (!$user) {
            return false;
        }

        if ($user['register_discount']) {
            return false;
        }

        return true;
    }

    public function checkSeason($discount)
    {
        $today = date('Y-m-d');

        if ($discount['season_date_start'] > $today) {
            return false;
        } elseif ($discount['season_date_end'] < $today) {
            return false;
        } else {
            return true;
        }
    }

	public function setBuyDiscount($discount, $productsData, $type = 'buy')
    {
        $priceProducts = 0;
        $priceCategoryProducts = 0;
        $newPrice = 0;
        $value = 0;

        if (Yii::app()->user->id) {
            $sub = Subscription::model()->getSubByUser(Yii::app()->user->id);
        } else {
            $sub = false;
        }

        if (empty($productsData)) {
            return [
                'newPrice' => $newPrice,
                'value' => $value
            ];
        }

        $productsId = [];

        foreach ($productsData as $item) {
            $productsId[] = $item['product'];
        }

        $products = Product::model()->getProductsByIds($productsId, false, true);
        $productsInCategories = [];

        if (!empty($discount['categories'])) {
            foreach ($products as $key => $product) {
                $productCategory = Category::model()->getCategoryById($product['category_id']);
                $productParent = [];

                if ($productCategory['parent_id']) {
                    $productParent = Category::model()->getCategoryById($productCategory['parent_id']);
                }

                if (in_array($productCategory['category_id'], $discount['categories']) || (!empty($productParent) && in_array($productParent['category_id'], $discount['categories']))) {
                    $productsInCategories[] = $product;
                    unset($products[$key]);
                }
            }

            foreach ($productsInCategories as $inCategory) {
                foreach ($productsData as $key => $data) {
                    if ($data['product'] == $inCategory['product_id']) {
                        $priceCategoryProducts += (int) $productsData[$key]['variant'] * $productsData[$key]['qty'];
                        unset($productsData[$key]);
                    }
                }
            }

            foreach ($products as $item) {
                foreach ($productsData as $key => $data) {
                    if ($data['product'] == $item['product_id']) {
                        $priceProducts += (int) $productsData[$key]['variant'] * $productsData[$key]['qty'];
                    }
                }
            }

            if ($discount['discount_type'] == 'percentage') {
                $priceDiscount = $priceCategoryProducts;

                if ($type == 'sub') {
                    if (!$sub) {
                        $priceDiscount =  round($priceDiscount - ($priceDiscount * 0.3));
                    }
                }

                $value = round($priceCategoryProducts * $discount['discount_value'] / 100);
                $priceDiscount = $priceDiscount - $value;

                $newPrice = $priceDiscount + $priceProducts;
            } else {

                $priceDiscount = $priceCategoryProducts;

                if ($type == 'sub') {
                    if (!$sub) {
                        $priceDiscount =  round($priceDiscount - ($priceDiscount * 0.3));
                    }
                }

                $value = $discount['discount_value'];
                $priceDiscount = $priceDiscount - $value;

                $newPrice = $priceDiscount + $priceProducts;
            }

            if ($newPrice == 0 || $newPrice < 2) {
                if ($newPrice < 0) {
                    if ($type == 'buy') {
                        $value = Cart::model()->getPrice() - 2;
                    } elseif ($type == 'sub') {
                        if (!$sub) {
                            $value = round(Cart::model()->getSubPrice() - (Cart::model()->getSubPrice() * 0.3) - 2);
                        }
                    } else {
                        $value = 0;
                    }
                } else {
                    $value = $value - 2;
                }
                $newPrice = 2;
            }
        }

        return [
            'newPrice' => $newPrice,
            'value' => $value
        ];
    }

	public function apply($discount)
	{
		$_SESSION['discount'] = $discount;
	}

	public function clear()
	{
		$_SESSION['discount'] = [];
	}

	public function activate()
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_discount = array(
			'saved' => $today,
			'activated' => $_SESSION['discount']['activated'] + 1,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "discount_id = :discount_id" , 
				"params" => array(
					"discount_id" => (int) $_SESSION['discount']['discount_id'],
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('discount', $update_discount, $update_criteria)->execute();

			if ($rs) {
				$this->clear();
				
				return true;
			}
		} catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function setUserAuto($id)
    {
        $builder = Yii::app()->db->schema->commandBuilder;
        $today = date('Y-m-d H:i:s');

        $update_discount = array(
            'saved' => $today,
            'register_discount' => 1,
        );

        $update_criteria = new CDbCriteria(
            array(
                "condition" => "user_id = :id" ,
                "params" => array(
                    "id" => (int) $id,
                )
            )
        );

        $rs = $builder->createUpdateCommand('user', $update_discount, $update_criteria)->execute();
    }
}
