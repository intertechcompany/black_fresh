<?php


class Faq extends CModel
{
    public function rules()
    {
        return array();
    }

    public function attributeNames()
    {
        return array();
    }

    public static function model()
    {
        return new self();
    }

    public function getFaqGroups()
    {
        return Yii::app()->db
            ->createCommand("SELECT f.*, fl.faq_title FROM faq_group as f JOIN faq_group_lang as fl ON f.faq_id = fl.faq_id AND fl.language_code = :code where f.active = 1 ORDER BY f.order ASC")
            ->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
            ->queryAll();
    }

    public function getFaqQuest()
    {
        return Yii::app()->db
            ->createCommand("SELECT f.*, fl.quest_title, fl.quest_description, fgl.faq_title FROM faq_question as f JOIN faq_group_lang fgl ON fgl.faq_id = f.group_id and fgl.language_code = :code JOIN faq_question_lang as fl ON f.faq_id = fl.faq_id AND fl.language_code = :code where f.active = 1")
            ->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
            ->queryAll();
    }
}