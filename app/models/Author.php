<?php
class Author extends CModel
{
	private $per_page = 10;

	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function getAuthors()
	{
		$authors = Yii::app()->db
			->createCommand("SELECT a.*, al.author_name, al.author_description   
							 FROM author as a 
							 JOIN author_lang as al 
							 ON a.author_id = al.author_id AND al.language_code = :code 
							 WHERE a.active = 1 
							 ORDER BY a.author_position, a.author_id DESC")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $authors;
	}
}