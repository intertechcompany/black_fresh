<?php
class Header extends CWidget
{ 
	public function run()
	{
		$categories = Yii::app()->params->categories_tree;
		$pages = Page::model()->getTopMenu();
		$cart_total = Cart::model()->getTotal();

		$this->render('header', [
			'categories' => $categories,
			'pages' => $pages,
			'cart_total' => $cart_total,
			'registration' => true,
		]);
	}
}