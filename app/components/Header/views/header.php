<style >
	.login-style {
		border: 1px solid var(--black-color);    
		padding-right: 20px;
    	margin-right: 5px;   
	    line-height: 2;
	    border-radius: 16px;
	    padding-left: 20px;
	    text-decoration: none;
	}
	.login-style:hover {
	    background-color: var(--black-color);
	    color: var(--white-color);
	    text-decoration: none;
	}
	.header__catalog {
		margin-left: 5px !important;
	}
	.header__menu-item {
		margin-left: 20px;
	}
	.header__menu {
		padding-right: 0 !important;
	}
	@media (max-width: 1200px) {
		.header__menu-item {
			margin-left: 13px;
			font-size: 12px;
		}
	}
	@media (max-width: 1000px) {
		.header__catalog {
			margin-left: 25px;
		}
		.login-style {
			margin-left: 10px;
		}
	}
	@media (max-width: 1000px) {
		.header__catalog {
			margin-left: 25px !important;
		}
	}
	
	@media (max-width: 420px) {
		.header__catalog-btn:after {
			display: none;
		}
	}
	
	



</style>
<?php
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
	$controller = Yii::app()->getController();
?>
<?php if (isset(Yii::app()->params->settings['show_news']) && Yii::app()->params->settings['show_news'] !== null) { ?>
    <?php if (in_array($controller->route, ['site/category', 'site/product', 'site/school', 'site/course', 'site/index'])) { ?>
        <div class="header-tip"><?php echo Lang::t('layout.tip.headerContact', ['{phone}' => CHtml::encode(Yii::app()->params->settings['phone'])])?></div>
    <?php } ?>
<?php } ?>
<header class="header<?php if ($controller->route != 'site/index') { ?> header--sticky<?php } ?>">
	<div class="wrap">
		<div class="header__container">
			<a href="<?=Yii::app()->createUrl('site/index')?>" class="header__logo"></a>
			<a href="#" class="header__menu-toggle"><i></i></a>

			<?php if (!empty($categories)) {?>
			<div class="header__catalog">
				<a href="#" class="header__catalog-btn"><?=Lang::t('layout.btn.catalog')?></a>
				<div id="dropdown" class="dropdown">
					<ul class="dropdown__menu list-unstyled">
						<?php foreach ($categories as $category) { ?>
						<li><a href="<?=Yii::app()->createUrl('site/category', ['alias' => $category['category_alias']])?>"><?=CHtml::encode($category['category_name'])?></a></li>
						<?php } ?>
					</ul>
					<div class="dropdown__side">
						<?php $i = 0; ?>
						<?php foreach ($categories as $index => $category) { ?>
						<div class="dropdown__section<?php if ($i == 0) { ?> dropdown__section--visible<?php } ?>">
							<div class="dropdown__section-img">
								<?php if (!empty($category['category_photo'])) { ?>
								<?php
									$category_photo = json_decode($category['category_photo'], true);
									$category_photo_1x = $assetsUrl . '/category/' . $category['category_id'] . '/' . $category_photo['1x']['path'];
									$category_photo_2x = '';

									if (isset($category_photo['2x'])) {
										$category_photo_2x = $assetsUrl . '/category/' . $category['category_id'] . '/' . $category_photo['2x']['path'];
									}
								?>
								<picture>
									<?php /* <source type="image/webp" srcset="images/menu_section.webp, images/menu_section@2x.webp 2x"> */ ?>
									<?php if (!empty($category_photo_2x)) { ?>
									<source srcset="<?=$category_photo_1x?>, <?=$category_photo_2x?> 2x">
									<?php } ?>
									<img src="<?=$category_photo_1x?>" alt="<?=CHtml::encode($category['category_name'])?>">
								</picture>
								<?php } ?>
							</div>
							<div class="dropdown__section-content">
								<div class="dropdown__section-title"><?=CHtml::encode($category['category_name'])?></div>
								<div class="dropdown__section-text">
									<?=$category['category_description']?>
								</div>
							</div>
						</div>
						<?php $i++; } ?>
					</div>
				</div>
			</div>
			<?php } ?>

			<div class="mobile-menu mobile-menu--hidden">
				<ul class="mobile-menu__list list-unstyled">
					<li><a href="/quiz"><?=Lang::t('layout.link.subquiz')?></a></li>
<!--					<li><a href="--><?//=Yii::app()->createUrl('site/school')?><!--">--><?//=Lang::t('layout.link.school')?><!--</a></li>-->
					<?php /* <li><a href="<?=Yii::app()->createUrl('site/base')?>"><?=Lang::t('layout.link.base')?></a></li> */ ?>
					<?php if (!empty($pages)) {?>
					<?php foreach ($pages as $page) { ?>
					<li><a href="<?=Yii::app()->createUrl('site/page', ['alias' => $page['page_alias']])?>"><?=CHtml::encode($page['page_title'])?></a></li>
					<?php } ?>
					<?php } ?>
					<li><a href="<?=Yii::app()->createUrl('site/news')?>"><?=Lang::t('layout.link.news')?></a></li>
<!--                    --><?php //if(!empty(Yii::app()->params->settings['is_show_parents_in_header'])) { ?>
<!--                        <li><a href="--><?php //=Yii::app()->createUrl('site/parents')?><!--">--><?php //=Lang::t('layout.link.parents')?><!--</a></li>-->
<!--                    --><?php //} ?>
				</ul>

				<ul class="mobile-menu__lang list-unstyled">
					<?php
						// $current_url_path = Yii::app()->getRequest()->getBaseUrl() . '/' . Yii::app()->getRequest()->getPathInfo();
						$current_url_path = Yii::app()->getRequest()->getPathInfo();
					?>
					<?php foreach (Yii::app()->params->langs as $lang_code => $lang) { ?>
					<?php
						if (empty($lang['url'])) {
							$lang_url = '/' . $current_url_path;
						} else {
							$lang_url = '/' . $lang['url'] . '/' . $current_url_path;
						}
					?>
					<li><a href="<?=$lang_url?>"<?php if ($lang_code == Yii::app()->language) { ?> class="mobile-menu__lang-active"<?php } ?>><?=CHtml::encode($lang['name'])?></a></li>
					<?php } ?>
				</ul>
			</div>

			<ul class="header__menu list-unstyled">
				<li class="header__menu-item"><a href="/quiz" class="header__menu-link"><?=Lang::t('layout.link.subquiz')?></a></li>
<!--				<li class="header__menu-item"><a href="--><?php ////echo Yii::app()->createUrl('site/school')?><!--" class="header__menu-link">--><?//=Lang::t('layout.link.school')?><!--</a></li>-->
				<?php /* <li class="header__menu-item"><a href="<?=Yii::app()->createUrl('site/base')?>" class="header__menu-link"><?=Lang::t('layout.link.base')?></a></li> */ ?>
				<?php if (!empty($pages)) {?>
				<?php foreach ($pages as $page) { ?>
				<li class="header__menu-item"><a href="<?=Yii::app()->createUrl('site/page', ['alias' => $page['page_alias']])?>" class="header__menu-link"><?=CHtml::encode($page['page_title'])?></a></li>
				<?php } ?>
				<?php } ?>
				<li class="header__menu-item"><a href="<?=Yii::app()->createUrl('site/news')?>" class="header__menu-link"><?=Lang::t('layout.link.news')?></a></li>
<!--                --><?php //if(!empty(Yii::app()->params->settings['is_show_parents_in_header'])) { ?>
<!--				<li class="header__menu-item"><a href="--><?php //=Yii::app()->createUrl('site/parents')?><!--" class="header__menu-link">--><?php //=Lang::t('layout.link.parents')?><!--</a></li>-->
<!--                --><?php //} ?>
			</ul>

			<div class="header__lang">
				<a href="#" class="header__lang-active"><?=Yii::app()->params->langs[Yii::app()->language]['name']?></a>
				<ul class="header__langs list-unstyled">
					<?php
						// $current_url_path = Yii::app()->getRequest()->getBaseUrl() . '/' . Yii::app()->getRequest()->getPathInfo();
						$current_url_path = Yii::app()->getRequest()->getPathInfo();
					?>
					<?php foreach (Yii::app()->params->langs as $lang_code => $lang) { ?>
					<?php
						if ($lang_code == Yii::app()->language) {
							continue;
						}

						if (empty($lang['url'])) {
							$lang_url = '/' . $current_url_path;
						} else {
							$lang_url = '/' . $lang['url'] . '/' . $current_url_path;
						}
					?>
					<li><a href="<?=$lang_url?>"><?=CHtml::encode($lang['name'])?></a></li>
					<?php } ?>
				</ul>
			</div>
<?php if (Yii::app()->user && isset(Yii::app()->user->user_data)) : ?>
				<a class="login-style" href="<?=$registration ? Yii::app()->createUrl('login') : '#'?>" class="header__cart">
					<?=CHtml::encode(Yii::app()->user->user_data['user_first_name'])?>
				</a>
			<?php else : ?>

				<a class="login-style" href="<?=$registration ? Yii::app()->createUrl('login') : '#'?>" class="header__cart">
					<?=Lang::t('layout.btn.login')?>
				</a>
			<?php endif; ?>
			<a href="<?=$cart_total ? Yii::app()->createUrl('checkout') : '#'?>" class="header__cart<?php if ($cart_total) { ?> header__cart--products<?php } ?>">
				<span class="header__cart-tip"><?=Lang::t('layout.btn.cart')?></span><span class="header__cart-counter"><?=$cart_total?></span>
			</a>

			
		</div>
	</div>
</header>
<!-- /.header -->
