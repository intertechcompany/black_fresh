<?php
/**File:: protected/components/UrlManager.php (Yii v1.1.14 compatible . May be less...)

::RUSSIAN DESCRIPTION::
В Yii наши URL по-умолчанию выглядят так http://site.com/index.php?param1=a или http://site.com/index.php/param1/a, но обычно мы хотим избавится от имени входного скрипта(index.php) в URL.
И для этого устанавливаем в настройках параметр 'URLManager'=>array(...,'showScriptName' => false,...), но этот способ работает только для формирования и приема URL(http://site.com/param1/a).
Но по-прежнему позволяет обращаться к нашему адресу по его старому значению http://site.com/index.php/param1/a .
ЭТОТ СКРИПТ закрывает эту маленькую огрешность - т.е. не разрешает обращаться к URL в котором по-прежнему присутствует имя входного скрипта после site.com

Расширяет возможности базового класса CUrlManager.
Делается проверка запроса Url на обращение к нему по его дубликату с использованием имени файла входного скрипта(обычно index.php).
При выявлении дубликата Url делается редирект(HTTP 301) на оригинальный Url ЛИБО отдает ошибку HTTP 404 (в зависимости от указанных настроек).


::ENGLISH DESCRIPTION::
In Yii our URL default looks like http://site.com/index.php?param1=a or http://site.com/index.php/param1/a, but usually we want to get rid of the entry script (index.php) in the URL.
For this we setting parameter 'URLManager' => array (..., 'showScriptName' => false, ...), but this method works only for the generation and reception of URL (http://site.com/param1/a).
But as before, gives you access to our address at his old value http://site.com/index.php/param1/a .
THIS SCRIPT closes this little bug - does not refer to the URL which is still present the entry script name after the site.com

Extends the base class CUrlManager
A check request Url to access it by using a duplicate Url with the FILENAME entry script (usually index.php)
In identifying duplicate Url does a redirect(HTTP 301) to the original Url OR gives an HTTP 404 (depending on the settings specified)


 *  *
 * Parses the user request.
 * @param CHttpRequest $request the request application component
 */

/* //в КОНФИГУРАЦИИ ОБЯЗАТЕЛЬНО НЕОБХОДИМО УКАЗАТЬ ЛИШЬ путь к данному классу, а остальные значения переменных можно исправить прямо в этом файле,
 *  но для большей гибкости, основные настройки вынесены в файл конфигурации main.php в секцию 'components'=>array('urlManager'=>array(...СЮДА...))
 * //В файл конфигурации main.php добавить настройки (основные параметры: 'class','showScriptName','actAddressedDoubleUrl','scriptNameEntry')
 *
      	'components'=>array(
                .........
		'urlManager'=>array(
                        'class' => 'application.components.UrlManager',//указываем наш новый класс
			'urlFormat'=>'path',
                        'showScriptName'=>false,
                        'urlSuffix'=>'.php',//добавил расширение .php как и у входного скрипта для проверки ситуаций когда действие выглядит как имя входного скрипта index.php
                        'useStrictParsing'=>true,//не пропускает те Url у которых нету 'urlSuffix'
                        'actAddressedDoubleUrl'=>'301',//default:'301'; alternate: '404' - действие необходимое при обращении к странице по дубликату ее URL с использованием имени входного скрипта(обычно index.php)
                        //'scriptNameEntry'=>'hidden-index.php',//default:'index.php';имя файла входного скрипта(точки входа) - УКАЗАТЬ если отличается от index.php
			'caseSensitive'=>false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',

			),
		),
                .........
*
*/
class UrlManager extends CUrlManager
{
	/**
	 * @var string Default:'301'; alternate: '404' - Выбор действия необходимого при обращении к странице по дубликату её URL с использованием имени входного скрипта (обычно index.php)
	 */
	public $actAddressedDoubleUrl = '301';
	/**
	 * @var string Default:'index.php';Имя файла входного скрипта(точки входа)
	 */
	public $scriptNameEntry = 'index.php';

	public function addressedDoubleUrl($request)
	{
		$requestUri = Yii::app()->request->requestUri;
		
		if (Yii::app()->urlManager->showScriptName === false && strpos($requestUri, $this->scriptNameEntry) !== false) {
			if ($this->actAddressedDoubleUrl == '301') {
				$cleanRequestUri = str_replace('/index.php', '', $requestUri);
				if (strpos($cleanRequestUri, '/') !== 0) {
					$cleanRequestUri = '/' . $cleanRequestUri;
				}
				Yii::app()->request->redirect($cleanRequestUri, true, 301);
			} elseif ($this->actAddressedDoubleUrl == '404') {
				throw new CHttpException(404, Yii::t('yii', 'Unable to resolve the request "{route}".', array('{route}' => $request->requestUri)));
			}
			
			return false;
		}
		
		return true;
	}

	public function parseUrl($request)
	{
		if($request->requestUri === '/') {
			//отключает обязательное использование суффикса, если используется "пустое"(''=>'site/index) правило в urlManager [Фикс Yii бага]
			Yii::app()->urlManager->useStrictParsing = false;
		}
		
		if ($this->addressedDoubleUrl($request)) {
			return parent::parseUrl($request);
		}
	}
}