<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout = '//layouts/main';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu = array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs = array();
	
	/**
	 * @var string the default meta description of the current page
	 */
	public $pageDescription = '';
	/**
	 * @var string the default meta keywords of the current page
	 */
	public $pageKeywords = '';
	
	/**
	 * @var string canonical url for page
	 */
	public $canonicalUrl = '';

	/**
	 * @var bool meta noindex page flag
	 */
	public $noIndex = false;

	/**
	 * @var string open graph URL
	 */
	public $ogUrl = '';

	/**
	 * @var string open graph title
	 */
	public $ogTitle = '';

	/**
	 * @var string open graph description
	 */
	public $ogDescription = '';

	/**
	 * @var string open graph image
	 */
	public $ogImage = '';
}