<?php

class NovaPoshta
{
	const KYIV_REF = '8d5a980d-391c-11dd-90d9-001a92567626';
	
	private $builder;
	private $today;
	private $get_addresses = false;
	
	public function __construct()
	{
        require_once Yii::getPathOfAlias('application.vendor.GuzzleHttp') . DS . 'autoload.php';
    }
	
	public function getCities($city_name = '')
	{
		$city_name = trim($city_name);
		$field = (Yii::app()->language == 'ru') ? 'city_name_ru' : 'city_name';

		if (!empty($city_name)) {
			$city_name = addcslashes($city_name, '%_');
		
			$cities = Yii::app()->db
				->createCommand("SELECT * FROM np_city WHERE {$field} LIKE :city_name ORDER BY {$field}")
				->bindValue(':city_name', '%' . $city_name . '%', PDO::PARAM_STR)
				->queryAll();
		} else {
			$cities = Yii::app()->db
				->createCommand("SELECT * FROM np_city ORDER BY {$field}")
				->queryAll();
		}
			
		return $cities;
	}
	
	public function getCity($city_name)
	{
		$field = (Yii::app()->language == 'ru') ? 'city_name_ru' : 'city_name';
		
		$city = Yii::app()->db
			->createCommand("SELECT * FROM np_city WHERE {$field} = :city_name LIMIT 1")
			->bindValue(':city_name', $city_name, PDO::PARAM_STR)
			->queryRow();
			
		return $city;
	}

	public function getCityById($city_id)
	{
		$city = Yii::app()->db
			->createCommand("SELECT * FROM np_city WHERE id = :city_id LIMIT 1")
			->bindValue(':city_id', (int) $city_id, PDO::PARAM_INT)
			->queryRow();
			
		return $city;
	}
	
	public function getCityByRef($city_ref)
	{
		$city = Yii::app()->db
			->createCommand("SELECT * FROM np_city WHERE city_ref = :city_ref LIMIT 1")
			->bindValue(':city_ref', $city_ref, PDO::PARAM_STR)
			->queryRow();
			
		return $city;
	}

	public function getDepartments($city_ref)
	{
		$departments = Yii::app()->db
			->createCommand("SELECT * FROM np_department WHERE city_ref = :city_ref ORDER BY department_num")
			->bindValue(':city_ref', $city_ref, PDO::PARAM_STR)
			->queryAll();
			
		return $departments;
	}

	public function getDepartmentByRef($department_ref)
	{
		$department = Yii::app()->db
			->createCommand("SELECT * FROM np_department WHERE department_ref = :department_ref LIMIT 1")
			->bindValue(':department_ref', $department_ref, PDO::PARAM_STR)
			->queryRow();
			
		return $department;
	}

	public function getDepartment($department_name)
	{
		$field = (Yii::app()->language == 'ru') ? 'department_name_ru' : 'department_name';
		
		$department = Yii::app()->db
			->createCommand("SELECT * FROM np_department WHERE {$field} = :department_name LIMIT 1")
			->bindValue(':department_name', $department_name, PDO::PARAM_STR)
			->queryRow();
			
		return $department;
	}

	public function getStreets($city_ref, $street_name = '')
	{
		$street_name = trim($street_name);
		$field = (Yii::app()->language == 'ru') ? 'street_full_name_ru' : 'street_full_name';
		$order_field = (Yii::app()->language == 'ru') ? 'street_name_ru' : 'street_name';

		if (!empty($street_name)) {
			$street_name = addcslashes($street_name, '%_');
			$street_name = preg_replace('#\s+#u', '%', $street_name);
		
			$streets = Yii::app()->db
				->createCommand("SELECT * FROM np_street WHERE city_ref = :city_ref AND {$field} LIKE :street_name ORDER BY {$order_field}")
				->bindValue(':city_ref', $city_ref, PDO::PARAM_STR)
				->bindValue(':street_name', '%' . $street_name . '%', PDO::PARAM_STR)
				->queryAll();
		} else {
			$streets = Yii::app()->db
				->createCommand("SELECT * FROM np_street WHERE city_ref = :city_ref ORDER BY {$order_field}")
				->bindValue(':city_ref', $city_ref, PDO::PARAM_STR)
				->queryAll();
		}
			
		return $streets;
	}

	public function getStreetByRef($street_ref)
	{
		$street = Yii::app()->db
			->createCommand("SELECT * FROM np_street WHERE street_ref = :street_ref LIMIT 1")
			->bindValue(':street_ref', $street_ref, PDO::PARAM_STR)
			->queryRow();
			
		return $street;
	}

	public function getDeliveryDate($city_ref)
	{
		$delivery_date = array();

		// $delivery_np_time = (int) str_replace(':', '', Yii::app()->params->settings['delivery_np_time']);
		$delivery_np_time = (int) str_replace(':', '', '12:00');
		$current_time = (int) date('Hi');
		
		if ($current_time < $delivery_np_time) {
			$date = new DateTime('today', new DateTimeZone(Yii::app()->timeZone));
		} else {
			$date = new DateTime('tomorrow', new DateTimeZone(Yii::app()->timeZone));
		}

		$delivery_date_response = $this->getNpData(array(
			'modelName' => 'InternetDocument',
			'calledMethod' => 'getDocumentDeliveryDate',
			'methodProperties' => array(
				'DateTime' => $date->format('d.m.Y'),
				'ServiceType' => 'WarehouseWarehouse',
				'CitySender' => self::KYIV_REF,
				'CityRecipient' => $city_ref,
			),
			'apiKey' => Yii::app()->params->np['api_key'],
		));

		if (!empty($delivery_date_response) && $delivery_date_response['success'] === true && !empty($delivery_date_response['data'])) {
			$delivery_date = $delivery_date_response['data'];
		}

		return $delivery_date;
	}

	public function getDeliveryPrice($city_ref, $force = false)
	{
		$delivery_price = array();

		// calculate weight and price
		$cart_model = Cart::model();
		$cart_weight = $cart_model->getCartWeight();
		$cart_cost = $cart_model->getPrice();

		/* $delivery_free = (int) Yii::app()->params->settings['delivery_free'];

		if ($cart_cost >= $delivery_free && !$force) {
			return array();
		} */

		$delivery_price_response = $this->getNpData(array(
			'modelName' => 'InternetDocument',
			'calledMethod' => 'getDocumentPrice',
			'methodProperties' => array(
				'CitySender' => '8d5a980d-391c-11dd-90d9-001a92567626', // Kyiv
				'CityRecipient' => $city_ref,
				'ServiceType' => 'WarehouseWarehouse',
				'CargoType' => 'Parcel',
				'SeatsAmount' => 1,
				'Weight' => (float) $cart_weight,
				'Cost' => (int) $cart_cost,
			),
			'apiKey' => Yii::app()->params->np['api_key'],
		));

		if (!empty($delivery_price_response) && $delivery_price_response['success'] === true && !empty($delivery_price_response['data'])) {
			$delivery_price = $delivery_price_response['data'];
		}

		return $delivery_price;
	}

	public function getDeliveryStatus($np_document_number)
	{
		$delivery_status = array();

		$delivery_status_response = $this->getNpData(array(
			'modelName' => 'TrackingDocument',
			'calledMethod' => 'getStatusDocuments',
			'methodProperties' => array(
				'Documents' => array(
					array(
						'DocumentNumber' => $np_document_number,
						'Phone' => '',
					),
				),
			),
			'apiKey' => Yii::app()->params->np['api_key'],
		));

		if (!empty($delivery_status_response) && $delivery_status_response['success'] === true && !empty($delivery_status_response['data'])) {
			$delivery_status = $delivery_status_response['data'];
		}

		return $delivery_status;
	}

	public function createDocument($order, $order_items, $is_dropship = false, $user = array())
	{
		$document = array();

		// delivery date
		/* $delivery_np_time = (int) str_replace(':', '', Yii::app()->params->settings['delivery_np_time']); */
		$delivery_np_time = (int) str_replace(':', '', '12:00');
		$order_time = (int) date('Hi', strtotime($order['created']));
		
		if ($order_time < $delivery_np_time) {
			$date = new DateTime('today', new DateTimeZone(Yii::app()->timeZone));
		} else {
			$date = new DateTime('tomorrow', new DateTimeZone(Yii::app()->timeZone));
		}

		// weight, price, description
		$weight = 0;
		$price = 0;
		$description = array();

		foreach ($order_items as $order_item) {
			$price += $order_item['price'];
			$description[] = $order_item['product_keyword'];

			if (in_array($order_item['category_id'], array(15,16,17,18,19))) {
				$weight += 2;
			} else {
				$weight += 0.5;
			}
		}

		// payer type and payment method
		$delivery_free = (int) Yii::app()->params->settings['delivery_free'];

		if ($price >= $delivery_free) {
			$payer_type = 'Sender';
		} else {
			$payer_type = 'Recipient';
		}

		// dropship delivery
		if ($is_dropship) {
			if ($user['delivery_np']) {
				$payer_type = 'Sender';
			} else {
				$payer_type = 'Recipient';
			}
		}

		if ($payer_type == 'Sender' || trim($order['payment']) == 'Наличными') {
			$payment_method = 'Cash';
		} else {
			$payment_method = 'NonCash';
		}

		// EDRPOU fix for non cash payments
		$payment_method = 'Cash';

		// city and department refs
		$np_city = $order['np_city'];
		$np_dept = $order['np_department'];

		$city = $this->getCity($np_city);
		$department = $this->getDepartment($np_dept);

		if (empty($city) || empty($department)) {
			return array();
		}

		if (!empty($order['middle_name'])) {
			$full_name = trim($order['name']) . ' ' . $order['middle_name'] . ' ' . trim($order['last_name']);
		} else {
			$full_name = trim($order['name']) . ' ' . trim($order['last_name']);
		}

		$np_data = array(
			'NewAddress' => '1',
			'PayerType' => $payer_type,
			'PaymentMethod' => $payment_method,
			'CargoType' => 'Parcel',
			'Weight' => (string) $weight,
			'ServiceType' => 'WarehouseWarehouse',
			'SeatsAmount' => '1',
			'Description' => 'Запчастини',
			'Cost' => (string) $price,
			'CitySender' => self::KYIV_REF,
			'Sender' => '2356efee-736c-11e7-ab8b-005056b2fc3d',
			'SenderAddress' => '169227ee-e1c2-11e3-8c4a-0050568002cf',
			'ContactSender' => '55844bef-8d73-11e7-8ba8-005056881c6b',
			'SendersPhone' => '380951350872',
			'Recipient' => $department['department_ref'],
			'RecipientCity' => $city['city_ref'],
			'RecipientCityName' => $np_city,
			'RecipientArea' => '',
			'RecipientAreaRegions' => '',
			'RecipientAddress' => $department['department_ref'],
			'RecipientAddressName' => '',
			'RecipientHouse' => '',
			'RecipientFlat' => '',
			'RecipientName' => $full_name,
			'RecipientType' => 'PrivatePerson',
			'RecipientsPhone' => preg_replace('#[^\d]#', '', $order['phone']),
			'DateTime' => $date->format('d.m.Y'),
		);

		// оплата наложенным платежом
		if (trim($order['payment']) == 'Наличными') {
			$np_data['BackwardDeliveryData'] = array(
				array(
					'PayerType' => 'Recipient',
					'CargoType' => 'Money',
					'RedeliveryString' => (string) $price,
				),
			);
		}

		$document_response = $this->getNpData(array(
			'modelName' => 'InternetDocument',
			'calledMethod' => 'save',
			'methodProperties' => $np_data,
			'apiKey' => Yii::app()->params->np['api_key'],
		));

		if (!empty($document_response) && $document_response['success'] === true && !empty($document_response['data'])) {
			$document = $document_response['data'];
		} else {
			Yii::log(print_r($document_response, true), 'error', 'app');
		}

		return $document;
	}

	public function update()
	{
		@set_time_limit(60 * 60); // 1 hour
		$this->builder = Yii::app()->db->schema->commandBuilder;
		$this->today = date('Y-m-d H:i:s');

		$this->createTmpTables();
		$this->setNpCities();
		$this->setNpDepartments();
		$this->moveTables();
	}

	private function setNpCities()
	{
		$cities = $this->getNpCities();

		if (!empty($cities)) {
			foreach ($cities as $city) {
				$insert = array(
		  			'created' => $this->today,
					'saved' => $this->today,
					'city_ref' => $city['Ref'],
					// 'city_name' => trim(preg_replace('#\((.*)\)#', '', $city['Description'])),
					// 'city_name_ru' => trim(preg_replace('#\((.*)\)#', '', $city['DescriptionRu'])),
					'city_name' => $city['Description'],
					'city_name_ru' => $city['DescriptionRu'],
					'city_id' => $city['CityID'],
		  		);

		  		try {
					$this->builder->createInsertCommand('np_city_tmp', $insert)->execute();
					
					if ($this->get_addresses) {
						$this->setNpAddresses($city['Ref']);
					}
				} catch (CDbException $e) {
					// ...
				}
			}
		}	
	}

	private function setNpDepartments()
	{
		$departments = $this->getNpDepartments();

		if (!empty($departments)) {
			foreach ($departments as $dept) {
				/* if (!in_array($dept['TypeOfWarehouse'], array(
					'6f8c7162-4b72-4b0a-88e5-906948c6a92f',
					'841339c7-591a-42e2-8233-7a0a00f0ed6f',
					'9a68df70-0267-42a8-bb5c-37f427e36ee4',
				))) {
					continue;
				} */

				$insert = array(
					'created' => $this->today,
					'saved' => $this->today,
					'department_ref' => $dept['Ref'],
					'department_key' => $dept['SiteKey'],
					'department_num' => $dept['Number'],
					'department_name' => $dept['Description'],
					'department_name_ru' => $dept['DescriptionRu'],
					'city_ref' => $dept['CityRef'],
				);

				try {
					$this->builder->createInsertCommand('np_department_tmp', $insert)->execute();
				} catch (CDbException $e) {
					// ...
				}	
			}
		}
	}

	private function setNpAddresses($city_ref)
	{
		$addresses = $this->getNpAddresses($city_ref);

		if (!empty($addresses)) {
			foreach ($addresses as $address) {
				$insert = array(
					'created' => $this->today,
					'saved' => $this->today,
					'street_ref' => $address['Ref'],
					'street_name' => $address['Description'],
					'street_full_name' => $address['Description'] . ' ' . $address['StreetsType'],
					'street_type_ref' => $address['StreetsTypeRef'],
					'street_type' => $address['StreetsType'],
					'city_ref' => $city_ref,
				);

				try {
					$this->builder->createInsertCommand('np_street_tmp', $insert)->execute();
				} catch (CDbException $e) {
					// ...
				}	
			}
		}
	}

	private function createTmpTables()
	{
		// drop tmp tables
		$tables = Yii::app()->db
			->createCommand("SHOW TABLES")
			->queryColumn();
		
		if (in_array('np_city_tmp', $tables)) {
			Yii::app()->db
				->createCommand()
				->dropTable('np_city_tmp');
		}
		
		if (in_array('np_department_tmp', $tables)) {
			Yii::app()->db
				->createCommand()
				->dropTable('np_department_tmp');
		}
		
		if (in_array('np_street_tmp', $tables)) {
			Yii::app()->db
				->createCommand()
				->dropTable('np_street_tmp');
		}
		
		// cities
		$np_city_fields = array(
			'id' => 'int(11) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY',
			'created' => 'datetime NOT NULL DEFAULT \'0000-00-00 00:00:00\'',
			'saved' => 'datetime NOT NULL DEFAULT \'0000-00-00 00:00:00\'',
			'city_ref' => 'varchar(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
			'city_name' => 'varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
			'city_name_ru' => 'varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
			'city_id' => 'int(11) NOT NULL',
		);

		Yii::app()->db
			->createCommand()
			->createTable('np_city_tmp', $np_city_fields, 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

		/*Yii::app()->db
			->createCommand()
			->addPrimaryKey('id', 'np_city_tmp', 'id');*/

		Yii::app()->db
			->createCommand()
			->createIndex('created', 'np_city_tmp', 'created');

		Yii::app()->db
			->createCommand()
			->createIndex('city_ref', 'np_city_tmp', 'city_ref');

		Yii::app()->db
			->createCommand()
			->createIndex('city_name', 'np_city_tmp', 'city_name');

		Yii::app()->db
			->createCommand()
			->createIndex('city_name_ru', 'np_city_tmp', 'city_name_ru');

		// departments
		$np_department_fields = array(
			'id' => 'int(11) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY',
			'created' => 'datetime NOT NULL DEFAULT \'0000-00-00 00:00:00\'',
			'saved' => 'datetime NOT NULL DEFAULT \'0000-00-00 00:00:00\'',
			'department_ref' => 'varchar(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
			'department_key' => 'varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
			'department_num' => 'int(11) NOT NULL',
			'department_name' => 'varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
			'department_name_ru' => 'varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
			'city_ref' => 'varchar(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
		);

		Yii::app()->db
			->createCommand()
			->createTable('np_department_tmp', $np_department_fields, 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

		/*Yii::app()->db
			->createCommand()
			->addPrimaryKey('id', 'np_department_tmp', 'id');*/

		Yii::app()->db
			->createCommand()
			->createIndex('created', 'np_department_tmp', 'created');

		Yii::app()->db
			->createCommand()
			->createIndex('department_ref', 'np_department_tmp', 'department_ref');

		Yii::app()->db
			->createCommand()
			->createIndex('department_name', 'np_department_tmp', 'department_name');
		
		Yii::app()->db
			->createCommand()
			->createIndex('department_name_ru', 'np_department_tmp', 'department_name_ru');

		Yii::app()->db
			->createCommand()
			->createIndex('city_ref', 'np_department_tmp', 'city_ref');

		// streets
		$np_street_fields = array(
			'id' => 'int(11) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY',
			'created' => 'datetime NOT NULL DEFAULT \'0000-00-00 00:00:00\'',
			'saved' => 'datetime NOT NULL DEFAULT \'0000-00-00 00:00:00\'',
			'street_ref' => 'varchar(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
			'street_name' => 'varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
			'street_full_name' => 'varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
			'street_type_ref' => 'varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
			'street_type' => 'varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
			'city_ref' => 'varchar(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
		);

		Yii::app()->db
			->createCommand()
			->createTable('np_street_tmp', $np_street_fields, 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

		Yii::app()->db
			->createCommand()
			->createIndex('created', 'np_street_tmp', 'created');

		Yii::app()->db
			->createCommand()
			->createIndex('street_ref', 'np_street_tmp', 'street_ref');

		Yii::app()->db
			->createCommand()
			->createIndex('street_name', 'np_street_tmp', 'street_name');

		Yii::app()->db
			->createCommand()
			->createIndex('city_ref', 'np_street_tmp', 'city_ref');
	}

	private function moveTables()
	{
		// drop obsolete tables
		$tables = Yii::app()->db
			->createCommand("SHOW TABLES")
			->queryColumn();
		
		if (in_array('np_city', $tables)) {
			Yii::app()->db
				->createCommand()
				->dropTable('np_city');
		}

		Yii::app()->db
			->createCommand()
			->renameTable('np_city_tmp', 'np_city');

		if (in_array('np_department', $tables)) {
			Yii::app()->db
				->createCommand()
				->dropTable('np_department');
		}

		Yii::app()->db
			->createCommand()
			->renameTable('np_department_tmp', 'np_department');
		
		if (in_array('np_street', $tables)) {
			Yii::app()->db
				->createCommand()
				->dropTable('np_street');
		}

		Yii::app()->db
			->createCommand()
			->renameTable('np_street_tmp', 'np_street');
	}

	private function getNpCities()
	{
		$cities = array();

		$cities_response = $this->getNpData(array(
			'modelName' => 'Address',
			'calledMethod' => 'getCities',
			'apiKey' => Yii::app()->params->np['api_key'],
		));

		if (!empty($cities_response) && $cities_response['success'] === true && !empty($cities_response['data'])) {
			$cities = $cities_response['data'];
		}

		return $cities;
	}

	private function getNpDepartments()
	{
		$departments = array();

		$departments_response = $this->getNpData(array(
			'modelName' => 'AddressGeneral',
			'calledMethod' => 'getWarehouses',
			'apiKey' => Yii::app()->params->np['api_key'],
		));

		if (!empty($departments_response) && $departments_response['success'] === true && !empty($departments_response['data'])) {
			$departments = $departments_response['data'];
		}

		return $departments;
	}

	private function getNpAddresses($city_ref)
	{
		$addresses = array();

		$addresses_response = $this->getNpData(array(
			'modelName' => 'Address',
			'calledMethod' => 'getStreet',
			'methodProperties' => array(
				'CityRef' => $city_ref,
				// 'Language' => 'ru',
			),
			'apiKey' => Yii::app()->params->np['api_key'],
		));

		if (!empty($addresses_response) && $addresses_response['success'] === true && !empty($addresses_response['data'])) {
			$addresses = $addresses_response['data'];

			if ($addresses_response['info']['totalCount'] > 500) {
				$pages = ceil($addresses_response['info']['totalCount'] / 500);

				for ($i = 2; $i <= $pages; $i++) {
					$addresses_response = $this->getNpData(array(
						'modelName' => 'Address',
						'calledMethod' => 'getStreet',
						'methodProperties' => array(
							'CityRef' => $city_ref,
							'Page' => $i,
						),
						'apiKey' => Yii::app()->params->np['api_key'],
					));

					if (!empty($addresses_response) && $addresses_response['success'] === true && !empty($addresses_response['data'])) {
						$addresses = array_merge($addresses, $addresses_response['data']);
					}
				}
			}
		}

		return $addresses;
	}

	private function getNpData($request_body)
	{
		$client = new GuzzleHttp\Client(array(
			'timeout' => 15.0,
		));

		try {
			$response = $client->post('https://api.novaposhta.ua/v2.0/json/', array(
				'headers' => array(
					'Content-Type'  => 'application/json',
				),
				'body' => json_encode($request_body),
			));

			return json_decode($response->getBody(), true);
		} catch (GuzzleHttp\Exception\RequestException $e) {
			$exception = "Status code: " . $e->getCode() . ". Error message: " . $e->getMessage();

			if ($e->hasResponse()) {
				$exception .= $e->getResponse()->getBody();
			}

			Yii::log($exception, 'error', 'NovaPoshta');
		}
	}
}
