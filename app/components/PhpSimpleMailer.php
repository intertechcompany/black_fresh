<?php
class PhpSimpleMailer extends Mailer
{
	/**
	 * Send message via SendGrid API.
	 * 
	 * @param Array $to_list an array of email addresses. 
	 * @param string $subject the subject of your email.
	 * @param string $body the body of your email.
	 * @return boolean success send or failed.
	 */
	public function send(array $to_list, $subject, $body)
	{
		$mailer = Yii::createComponent('ext.Mailer.EMailer', true);

		try {
			$mailer->IsMail();
			
			// $from = 'noreply@' . Yii::app()->request->getServerName();
			$from = Yii::app()->params->mailer['from_email'];
			$from_name = Yii::app()->params->mailer['from_name'];
			
			$mailer->Sender = $from;
			$mailer->SetFrom($from, $from_name);
			$mailer->AddReplyTo($from, $from_name);

			foreach ($to_list as $email) {
				$email = trim($email);

				if (!empty($email)) {
					$mailer->AddAddress($email);
				}
			}
			
			$mailer->CharSet = 'UTF-8';
			$mailer->ContentType = 'text/html';

			$mailer->Subject = $subject;
			$mailer->Body = $this->wrapEmailBody($subject, $body);

			$mailer->Send();

			return true;
		} catch (phpmailerException $e) {
			
		}

		return false;
	}
}