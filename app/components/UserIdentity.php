<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	const ERROR_NOT_ACTIVE = 3;
	
	private $id;

	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$user_model = User::model();
        $user = $user_model->getUserByEmail($this->username);

		if (empty($user)) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
			$this->errorMessage = Lang::t('login.error.invalidLogin');
		} else if (!$user_model->validatePassword($this->password, $user['user_password'])) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
			$this->errorMessage = Lang::t('login.error.invalidLogin');
		} else if ($user['active'] != 1) {
			$this->errorCode = self::ERROR_NOT_ACTIVE;
			$this->errorMessage = Lang::t('login.error.accountBlocked');
		} else {
			$this->id = $user['user_id'];
			
			// generate token
			$token = md5($user['created'] . $user['user_password']);
			$this->setState('token', $token);
			$user_model->saveToken($token, $this->id);
			
			$this->errorCode = self::ERROR_NONE;
		}
		
		return $this->errorCode == self::ERROR_NONE;
	}

    public function authenticateSocial()
    {
        $user_model = User::model();
        $user = $user_model->getUserByEmail($this->username);

        if (empty($user)) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
            $this->errorMessage = Lang::t('login.error.invalidLogin');
        } else if ($user['active'] != 1) {
            $this->errorCode = self::ERROR_NOT_ACTIVE;
            $this->errorMessage = Lang::t('login.error.accountBlocked');
        } else {
            $this->id = $user['user_id'];

            // generate token
            $token = md5($user['created'] . $user['user_email'] . (new DateTime())->format('Y-m-d H:i:s'));
            $this->setState('token', $token);
            $user_model->saveToken($token, $this->id);

            $this->errorCode = self::ERROR_NONE;
        }

        return $this->errorCode == self::ERROR_NONE;
    }

	/**
	 * @return integer the ID of the user record
	 */
	public function getId()
	{
		return $this->id;
	}
}