<?php
/**
 * Lang class
 */
class Lang
{
	/**
	 * Get the translation by code.
	 * @param string $code the translation code.
	 * @param array $params parameters to be applied to the message using <code>strtr</code>.
	 * The first parameter can be a number without key.
	 * And in this case, the method will call {@link CChoiceFormat::format} to choose
	 * an appropriate message translation.
	 * Starting from version 1.1.6 you can pass parameter for {@link CChoiceFormat::format}
	 * or plural forms format without wrapping it with array.
	 * This parameter is then available as <code>{n}</code> in the message translation string.
	 * @return string translated text/html or original code if the translation is not found.
	 */
	public static function t($code, $params = array(), $lang = '')
	{
		$lang = !empty($lang) ? $lang : Yii::app()->language;
		$code = trim($code);
		$message = $code;

		if (isset(Yii::app()->params->translations[$code][$lang])) {
			$message = Yii::app()->params->translations[$code][$lang];
			
			if (!empty($params)) {
				$message = Yii::t('app', $message, $params);
			}

			return $message;
		}
		
		if (!empty($code)) {
			// get translate message if cache is not defined
			$translate_message = Translation::model()->getTranslationByCode($code, $lang);
				
			if (!empty($translate_message['translation_value'])) {
				if (!empty($params)) {
					$message = Yii::t('app', $translate_message['translation_value'], $params);
				}
				else {
					$message = $translate_message['translation_value'];
				}
			}
		}

		if (!empty(Yii::app()->params->highlight) && $message != $code) {
            if (strpos($code, 'placeholder') !== false || strpos($code, 'alt') !== false) {
                $message .= ' — ' . $code;
            } else {
                $message .= '<br><span class="__highlight">' . $code . '<input type="text" readonly value="' . $code . '"></span>';
            }
		}
		
		return $message;
	}

	/**
	 * Convert date from UTC to current time zone.
	 * @param string $date datetime UTC timezone.
	 * @param string $format datetime format.
	 * @return string foramtted datetime for curent timezone.
	 */
	public static function dateFromUTC($date, $format)
	{
		if (Yii::app()->timeZone != 'UTC') {
			$datetime_object = new DateTime($date, new DateTimeZone('UTC'));
			$datetime_object->setTimeZone(new DateTimeZone(Yii::app()->timeZone));
		}
		else {
			$datetime_object = new DateTime($date);
		}

		return $datetime_object->format($format);
	}

	/**
	 * Convert date from current time zone to UTC.
	 * @param string $date datetime for current timezone.
	 * @param string $format datetime format.
	 * @return string foramtted datetime for curent timezone.
	 */
	public static function dateToUTC($date, $format)
	{
		if (Yii::app()->timeZone != 'UTC') {
			$datetime_object = new DateTime($date, new DateTimeZone(Yii::app()->timeZone));
			$datetime_object->setTimeZone(new DateTimeZone('UTC'));
		}
		else {
			$datetime_object = new DateTime($date);
		}

		return $datetime_object->format($format);
	}
}