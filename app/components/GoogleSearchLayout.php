<?php

class GoogleSearchLayout
{
    private $clearHtmlAttributes = [
        '<p>',
        '</p>',
        '<strong>',
        '</strong>'
    ];

    /**
     * @param array|null $product
     * @return array
     */
    public function productLayout(?array $product): array
    {
        $data = [
            'result' => false,
            'content' => ''
        ];

        if (!empty($product)) {
            $data['result'] = true;
            $data['content'] = $this->prepareLayoutScript(
                json_encode(
                    $this->prepareProductData($product)
                )
            );
        }

        return $data;
    }

    private function prepareProductData(array $product): array
    {
        return [
            '@context' => 'https://schema.org/',
            '@type' => 'Product',
            'name' => $product['product_title'] ?? '',
            'description' => str_replace(
                $this->clearHtmlAttributes,
                ' ',
                $product['product_tip'] ?? ''
            ),
            'brand' => [
                '@type' => 'Brand',
                'name' => $product['product_brand'] ?? ''
            ],
            'sku' => $product['product_alias'] ?? '',
            'image' => [
                $this->getProductImage($product),
            ],
            'offers' => [
                '@type' => 'Offer',
                'url' => $this->prepareProductUrl($product),
                'priceCurrency' => 'UAH',
                'price' => round($product['product_price']),
                'itemCondition' => 'https://schema.org/NewCondition',
                'availability' => 'https://schema.org/InStock'
            ]
        ];
    }

    private function prepareLayoutScript(string $jsonData): string
    {
        return '<script type="application/ld+json">' . $jsonData . '</script>';
    }

    private function getProductImage(array $product): string
    {
        $img = '';
        $assetsUrl = Yii::app()->assetManager->getBaseUrl();
        $productPhotos = Product::model()->getProductPhotos($product['product_id']);

        if (!empty($productPhotos)) {
            foreach ($productPhotos as $photo) {
                if (!isset($photo['photo_path'])) {
                    continue;
                }

                $photoPath = json_decode($photo['photo_path'], true);

                if (!empty($photoPath)) {
                    $img = $assetsUrl . '/product/'
                        . $product['product_id']
                        . '/' . ($photoPath['large']['1x'] ?? '');
                }

                if ($img !== '') {
                    break;
                }
            }
        }

        return $img;
    }

    private function prepareProductUrl(array $product): string
    {
        $siteUrl = Yii::app()->params['site'] ?? null;
        $url = Yii::app()->createUrl('site/product', ['alias' => $product['product_alias']]);

        if ($siteUrl && !strpos($url, $siteUrl)) {
            return str_replace(['//', 'https:/'], ['/', 'https://'], $siteUrl . $url);
        }

        return $url;
    }
}