<?php
class Breadcrumbs extends CWidget
{
    public $breadcrumbs = [];

    public function run()
    {
        $this->render('breadcrumbs', [
            'breadcrumbs' => $this->breadcrumbs,
        ]);
    }
}