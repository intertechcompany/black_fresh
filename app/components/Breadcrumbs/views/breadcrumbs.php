<?php
    $controller = Yii::app()->getController();
    $counter = 2;
?>
<div class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
    <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="<?=$controller->createAbsoluteUrl('site/index')?>">
            <span itemprop="name"><?=Lang::t('layout.link.home')?></span>
        </a>
        <meta itemprop="position" content="1">
    </span>
    <?php foreach ($breadcrumbs as $label => $url) { ?>
    &gt;
    <span itemscope itemprop="itemListElement" itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="<?=$url?>">
            <span itemprop="name"><?=CHtml::encode($label)?></span>
        </a>
        <meta itemprop="position" content="<?=$counter?>">
    </span>
    <?php $counter++; } ?>
</div>