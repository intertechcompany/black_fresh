<footer class="footer<?php if (Yii::app()->getController()->route != 'site/index') { ?> footer--page<?php } ?>">
    <div class="wrap">
        <div class="footer__head"></div>
        <div class="footer__groups">
            <ul class="footer__group list-unstyled">
                <li class="footer__group-item footer__group-item--title"><?=Lang::t('layout.tip.footerShop')?></li>
                <?php foreach ($categories as $category) { ?>
                <li class="footer__group-item"><a href="<?=Yii::app()->createUrl('site/category', ['alias' => $category['category_alias']])?>"><?=CHtml::encode($category['category_name'])?></a></li>
                <?php } ?>
                <li class="footer__group-item"><i class="icon-inline icon-mastercard-secure"></i><i class="icon-inline icon-visa-secure"></i></li>
            </ul>
            <?php /* <ul class="footer__group list-unstyled">
                <li class="footer__group-item footer__group-item--title"><?=Lang::t('layout.link.base')?></li>
                <?php foreach ($base_categories as $category) { ?>
                <li class="footer__group-item"><a href="<?=Yii::app()->createUrl('site/basecategory', ['alias' => $category['category_alias']])?>"><?=CHtml::encode($category['category_name'])?></a></li>
                <?php } ?>
            </ul> */ ?>
            <ul class="footer__group list-unstyled">
                <li class="footer__group-item footer__group-item--title"><?=Lang::t('layout.tip.footerCompany')?></li>
<!--                <li class="footer__group-item"><a href="--><?//=Yii::app()->createUrl('site/school')?><!--">--><?//=Lang::t('layout.link.school')?><!--</a></li>-->
                <?php /* <li class="footer__group-item"><a href="<?=Yii::app()->createUrl('site/base')?>"><?=Lang::t('layout.link.base')?></a></li> */ ?>
                <li class="footer__group-item"><a href="<?=Yii::app()->createUrl('site/news')?>"><?=Lang::t('layout.link.news')?></a></li>
                <?php foreach ($pages as $page) { ?>
                <li class="footer__group-item"><a href="<?=Yii::app()->createUrl('site/page', ['alias' => $page['page_alias']])?>"><?=CHtml::encode($page['page_title'])?></a></li>
                <?php } ?>
            </ul>
            <ul class="footer__group list-unstyled"></ul>
            <ul class="footer__group list-unstyled">
                <li class="footer__group-item footer__group-item--title"><?=Lang::t('layout.tip.footerContacts')?></li>
                <?php if (!empty(Yii::app()->params->settings['facebook'])) { ?>
                <li class="footer__group-item"><a href="<?=CHtml::encode(Yii::app()->params->settings['facebook'])?>" target="_blank" rel="nofollow">Facebook</a></li>
                <?php } ?>
                <?php if (!empty(Yii::app()->params->settings['instagram'])) { ?>
                <li class="footer__group-item"><a href="<?=CHtml::encode(Yii::app()->params->settings['instagram'])?>" target="_blank" rel="nofollow">Instagram</a></li>
                <?php } ?>
                <?php if (!empty(Yii::app()->params->settings['phone'])) { ?>
                <li class="footer__group-item"><a href="tel:+<?=preg_replace('#[^\d]#', '', CHtml::encode(Yii::app()->params->settings['phone']))?>"><?=CHtml::encode(Yii::app()->params->settings['phone'])?></a></li>
                <?php } ?>
                <?php if (!empty(Yii::app()->params->settings['mail'])) { ?>
                <li class="footer__group-item"><a href="mailto:<?=CHtml::encode(Yii::app()->params->settings['mail'])?>"><?=CHtml::encode(Yii::app()->params->settings['mail'])?></a></li>
                <?php } ?>
                <li class="footer__group-item"><?=Lang::t('layout.tip.footerAddress')?></li>
                <li class="footer__group-item footer__group-item--dev"><?=Lang::t('layout.tip.footerSite')?> – <a href="http://www.otherland.studio/" target="_blank" rel="nofollow">OtherLand</a></li>
                <li style="margin-top: 10px;" class="footer__group-item footer__group-item--dev"><?=Lang::t('layout.tip.footerCoding')?> – <a href="https://intertech.company/" target="_blank" rel="nofollow">Intertech</a></li>
            </ul>
        </div>
    </div>
</footer>
<!-- /.footer -->
