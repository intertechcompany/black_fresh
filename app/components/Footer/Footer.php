<?php
class Footer extends CWidget
{ 
	public function run()
	{
		$pages = Page::model()->getBottomMenu1();
		$categories = Yii::app()->params->categories_tree;
		$base_categories = BaseCategory::model()->getBaseCategoriesList();

		$this->render('footer', [
			'pages' => $pages,
			'categories' => $categories,
			'base_categories' => $base_categories,
		]);
	}
}