<?php
class CartSide extends CWidget
{ 
	public function run()
	{
		$cart_model = Cart::model();

		$cart = $cart_model->getCartProducts();
		$total = $cart_model->getTotal();
		$price = $cart_model->getPrice();

		$this->render('cartSide', [
			'cart' => $cart,
			'total' => $total,
			'price' => $price,
            'utmParams' => !empty(@$_COOKIE['utm_params']) ? "?{$_COOKIE['utm_params']}" : ''
		]);
	}
}
