<?php

use WayForPay\SDK\Collection\ProductCollection;
use WayForPay\SDK\Credential\AccountSecretCredential;
use WayForPay\SDK\Domain\Client;
use WayForPay\SDK\Domain\PaymentSystems;
use WayForPay\SDK\Domain\Product;
use WayForPay\SDK\Wizard\PurchaseWizard;

class Payment extends CApplicationComponent
{
    public function __construct()
    {
        require_once Yii::getPathOfAlias('application.vendor.WayForPay') . DS . 'autoload.php';
    }

    /**
     * @throws CException
     */
    public function getData($isRawData = false): array
    {
        $default = [
            'merchantAccount' => '',
            'orderReference' => '',
            'merchantSignature' => '',
            'amount' => 0,
            'currency' => '',
            'authCode' => 0,
            'email' => '',
            'phone' => '',
            'createdDate' => 0,
            'processingDate' => 0,
            'cardPan' => '',
            'cardType' => '',
            'issuerBankCountry' => '',
            'issuerBankName' => '',
            'recToken' => '',
            'transactionStatus' => '',
            'reason' => '',
            'reasonCode' => 0,
            'fee' => 0,
            'paymentSystem' => '',
            'repayUrl' => '',
        ];

        if ($isRawData) {
            $data = json_decode(Yii::app()->request->getRawBody(), true);

            if ($data === null) {
                $this->log(
                    'Payment response parse',
                    Yii::app()->request->getRawBody()
                );
                throw new CException(json_last_error_msg(), json_last_error());
            }
        } else {
            $data = $_POST;
        }

        return array_merge($default, $data);
    }

    /**
     * @param string $message
     * @param null $data
     * @param string $level
     * @param string $category
     * @param bool $returnPrint
     */
    private function log(
        string $message = '',
        $data = null,
        string $level = 'error',
        string $category = 'wfp',
        bool   $returnPrint = true
    ): void
    {
        Yii::log($message . "\n" . print_r($data, $returnPrint), $level, $category);
    }

    public function getPaymentFormHtml($order_id, $amount, $order, $products, $sub = false)
    {
        $controller = Yii::app()->getController();

        $credential = new AccountSecretCredential(Yii::app()->params->wfp['account'], Yii::app()->params->wfp['secret']);

        $products_collection = [];

        foreach ($products as $product) {
            $product_title = $product['title'];

            if (!empty($product['variant_title']) || !empty($product['options_title'])) {
                $product_subtitle = '';

                if (!empty($product['variant_title'])) {
                    $product_subtitle .= ' ' . $product['variant_title'];
                }

                if (!empty($product['options_title'])) {
                    $product_subtitle .= ' ' . $product['options_title'];
                }

                $product_title .= ' (' . trim(htmlspecialchars_decode($product_subtitle, ENT_QUOTES)) . ')';
            }

            $product_title = str_replace(["\r", "\n"], ["", " "], $product_title);

            $products_collection[] = new Product($product_title, (float)$product['price'], (int)$product['quantity']);
        }

        if ($sub) {
            $paymentSystem = new PaymentSystems(['card'], 'card');

            $form = PurchaseWizard::get($credential)
                ->setOrderReference($order_id['order_reference'])
                ->setAmount($amount)
                ->setCurrency('UAH')
                ->setOrderDate(new DateTime())
                ->setMerchantDomainName(Yii::app()->request->getServerName())
                ->setMerchantTransactionType('SALE')
                ->setClient(new Client(
                    $order['first_name'],
                    $order['last_name'],
                    $order['email'],
                    $order['phone']
                ))
                ->setProducts(new ProductCollection($products_collection))
                ->setReturnUrl($controller->createAbsoluteUrl('payment/resultSub'))
                ->setServiceUrl($controller->createAbsoluteUrl('payment/callbackSub'))
                ->setPaymentSystems($paymentSystem)
                ->setLanguage(Yii::app()->language == 'uk' ? 'UA' : mb_strtoupper(Yii::app()->language, 'utf-8'))
                ->getForm()
                ->getAsString();
        } else {
            $form = PurchaseWizard::get($credential)
                ->setOrderReference($order_id)
                ->setAmount($amount)
                ->setCurrency('UAH')
                ->setOrderDate(new DateTime())
                ->setMerchantDomainName(Yii::app()->request->getServerName())
                ->setMerchantTransactionType('SALE')
                ->setClient(new Client(
                    $order['first_name'],
                    $order['last_name'],
                    $order['email'],
                    $order['phone']
                ))
                ->setProducts(new ProductCollection($products_collection))
                ->setReturnUrl($controller->createAbsoluteUrl('payment/result'))
                ->setServiceUrl($controller->createAbsoluteUrl('payment/api'))
                ->setLanguage(Yii::app()->language == 'uk' ? 'UA' : mb_strtoupper(Yii::app()->language, 'utf-8'))
                ->getForm()
                ->getAsString();
        }

        return $form;
    }

    /**
     * @param array $paymentData
     * @throws PaymentException
     */
    function callback(array $paymentData): void
    {
        $this->log(
            'Payment callback start',
            $paymentData,
            'info'
        );

        $this->validatePaymentData($paymentData);

        $orderId = (int)str_replace('FB_', '', $paymentData['orderReference']);
        $orderModel = Order::model();
        $order = $orderModel->getOrderById($orderId);

        $this->validateOrder($order, $orderId, $paymentData);

        if (in_array($order['status'], ['paid', 'completed', 'cancelled'])) {
            return;
        }

        $paymentStatus = $paymentData['transactionStatus'] ?? '';

        if ($paymentStatus === 'Approved') {
            $status = 'paid';
        } elseif ($paymentStatus === 'InProcessing' || $paymentStatus === 'Pending') {
            $status = 'processing';
        } else {
            $status = 'payment_error';
        }

        $comment = json_encode($paymentData);

        $orderModel->updateStatus($orderId, $status, $comment);

        if ($status === 'paid') {
            if (Yii::app()->params->settings['stock'] === 'payment') {
                $orderModel->subtractOrderQty($orderId);
            }

            $orderProducts = $orderModel->getOrderProducts($orderId);
            $this->sendSuccessEmail($order, $orderProducts);
        }

        $this->log(
            'Payment callback end success',
            [
                'status' => $status,
            ],
            'info'
        );
    }

    /**
     * @param array $data
     * @param bool $isSub
     * @throws PaymentException
     */
    private function validatePaymentData(array $data, bool $isSub = false): void
    {
        if (!isset($data['orderReference'])) {
            $this->log(
                'No order reference found',
                $data
            );
            throw new PaymentException('No order reference found');
        }

        if (!$isSub && isset($data['transactionStatus']) && $this->isValidSignature($data) === false) {
            $this->log(
                'Signature dose not match',
                $data
            );
            throw new PaymentException('Invalid signature.');
        }
    }

    /**
     * @param $data
     * @return bool
     */
    private function isValidSignature($data): bool
    {
        $signatureFields = [
            Yii::app()->params->wfp['account'],
            $data['orderReference'],
            $data['amount'],
            $data['currency'],
            $data['authCode'],
            $data['cardPan'],
            $data['transactionStatus'],
            $data['reasonCode'],
        ];

        $valid_signature = hash_hmac(
            'md5',
            implode(';', $signatureFields),
            Yii::app()->params->wfp['secret']
        );

        return $data['merchantSignature'] === $valid_signature;
    }

    /**
     * @param $order
     * @param $orderId
     * @param $paymentData
     * @throws PaymentException
     */
    private function validateOrder($order, $orderId, $paymentData): void
    {
        if (empty($order)) {
            $this->log(
                'Order not found by this id',
                [
                    'id' => $orderId,
                    'orderReference' => $paymentData['orderReference'] ?? ''
                ]
            );
            throw new PaymentException('Order not found');
        }
    }

    /**
     * @param $order
     * @param $order_products
     */
    private function sendSuccessEmail($order, $order_products): void
    {
        $subject = Lang::t('email.subject.paidOrder', ['{order_id}' => $order['order_id']]);
        $body = Yii::app()->getController()->renderPartial('//email/order', [
            'order' => $order,
            'order_products' => $order_products,
        ], true);

        try {
            // send email to admins
            $mailer = new EsputnikMailer();
            $emails = explode(', ', Yii::app()->params->settings['notify_mail']);
            $mailer->send($emails, $subject, $body);

            // send email to client
            $mailer->send([$order['email']], $subject, $body);
        } catch (Exception $exception) {
            $this->log(
                'Send email error',
                [
                    'message' => $exception->getMessage(),
                    'order' => $order
                ]
            );
        }
    }

    /**
     * @param $paymentData
     * @return string
     */
    public function response($paymentData): string
    {
        $time = time();

        $response = [
            "orderReference" => $paymentData['orderReference'],
            "status" => 'accept',
            "time" => $time,
            "signature" => hash_hmac(
                'md5',
                implode(';', [$paymentData['orderReference'], 'accept', $time]),
                Yii::app()->params->wfp['secret']
            ),
        ];

        return json_encode($response);
    }

    /**
     * @param array $paymentData
     * @throws PaymentException
     */
    public function actionResultSub(array $paymentData)
    {
        $this->log(
            'User sub payment result start',
            $paymentData,
            'info'
        );

        $this->validatePaymentData($paymentData, true);

        $orderRef = $paymentData['orderReference'];
        $sub = Subscription::model()->getSubByOrderRef($orderRef);
        $subProducts = Subscription::model()->getSubProduct($sub['id']);

        $this->validateSub($sub, $orderRef, $paymentData);

        $sub['payment'] = json_encode($paymentData);
        $sub['payment_status'] = $paymentData['transactionStatus'];

        $criteria = new CDbCriteria();
        $criteria->condition = "order_reference=:ref";
        $criteria->params = [':ref' => $orderRef];

        $builder = Yii::app()->db->schema->commandBuilder;

        try {
            $builder->createUpdateCommand('sub_order', $sub, $criteria)->execute();
        } catch (\Exception $exception) {
            $this->log(
                'Error when update sub',
                [
                    'sub' => $sub,
                    'message' => $exception->getMessage(),
                ],
                'error',
                'sub'
            );
        }

        $_SESSION['thankYou'] = $sub['price'];
        $_SESSION['thankYouProducts'] = array_column($subProducts, 'product_id');

        $this->clearCart();

        $this->log(
            'User sub payment result end',
            $paymentData,
            'info'
        );

        $utmParams = !empty(@$_COOKIE['utm_params']) ? "?{$_COOKIE['utm_params']}" : '';

        Yii::app()->getController()
            ->redirect(Yii::app()
                    ->getController()
                    ->createUrl('site/thankYouSub').$utmParams
            );

//        Yii::app()->getController()->redirect(['site/thankYouSub']);
    }

    /**
     * @param $sub
     * @param $subRef
     * @param $paymentData
     * @throws PaymentException
     */
    private function validateSub($sub, $subRef, $paymentData): void
    {
        if (empty($sub)) {
            $this->log(
                'Sub not found by this ref',
                [
                    'orderReference' => $subRef ?? '',
                    'paymentData' => $paymentData ?? []
                ]
            );
            throw new PaymentException('Sub not found');
        }
    }

    private function clearCart(): void
    {
        Cart::model()->clear();
        Order::model()->clear();
    }

    /**
     * @param array $paymentData
     * @throws PaymentException
     */
    public function result(array $paymentData)
    {
        $this->log(
            'User order payment result start',
            $paymentData,
            'info'
        );

        $this->validatePaymentData($paymentData);

        $orderId = (int)str_replace('FB_', '', $paymentData['orderReference']);
        $orderModel = Order::model();
        $order = $orderModel->getOrderById($orderId);
        $products = $orderModel->getOrderProducts($orderId);

        $_SESSION['thankYouProducts'] = array_column($products, 'product_id');

        $this->validateOrder($order, $orderId, $paymentData);

        $orderModel->setThankYou([
            'order_id' => $orderId,
            'order_status' => $order['status'],
            'payment_status' => $paymentData['transactionStatus'] ?? '',
            'payment_message' => $paymentData['reason'] ?? '',
            'order_products' => array_column($products, 'product_id')
        ]);

        $this->clearCart();

        $this->log(
            'User order payment result end',
            [
                'orderStatus' => $order['status']
            ],
            'info'
        );

        $utmParams = !empty(@$_COOKIE['utm_params']) ? "?{$_COOKIE['utm_params']}" : '';

        Yii::app()->getController()
            ->redirect(Yii::app()
                ->getController()
                ->createUrl('site/thankyou').$utmParams
            );
    }
}

class PaymentException extends CException
{

}
