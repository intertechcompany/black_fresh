<?php

use WayForPay\SDK\Domain\Client;

class AuthUser extends CApplicationComponent
{
    public $driver;

    public function __construct()
    {
        require_once Yii::getPathOfAlias('application.vendor.graph-sdk') . DS . 'autoload.php';
    }

    public function setDriver($driver)
    {
        $this->driver = $driver;
    }

    public function getRedirectUrl()
    {
        if ('facebook' == $this->driver) {
            return $this->getFacebookUrl();
        }

        if ('google' == $this->driver) {
            return $this->getGoogleUrl();
        }

        return null;
    }

    public function getLinkRedirectUrl()
    {
        if ('facebook' === $this->driver) {
            return $this->getFacebookLinkUrl();
        }

        if ('google' === $this->driver) {
            return $this->getGoogleLinkUrl();
        }

        return null;
    }

    public function getFacebookUrl()
    {
        $params = [
            'client_id' => '271812324255386',
            'redirect_uri' => 'https://fresh.black/auth/loginFb',
            'scope' => 'public_profile',
            'response_type' => 'code',
            'state' => '123'
        ];

        return 'https://www.facebook.com/dialog/oauth?' . urldecode(http_build_query($params));
    }

    public function getFacebookLinkUrl()
    {
        $params = [
            'client_id' => '271812324255386',
            'redirect_uri' => 'https://fresh.black/auth/confirmFb',
            'scope' => 'public_profile',
            'response_type' => 'code',
            'state' => '123'
        ];

        return 'https://www.facebook.com/dialog/oauth?' . urldecode(http_build_query($params));
    }

    public function getGoogleUrl()
    {
        $params = [
            'client_id'     => '672236738468-ktemnnoeb3hv4lseatg32dcg0v2tgpdf.apps.googleusercontent.com',
            'redirect_uri'  => 'https://fresh.black/auth/login',
            'response_type' => 'code',
            'scope' => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile',
            'state' => '123'
        ];

        // https://snipp.ru/php/oauth-google

        return 'https://accounts.google.com/o/oauth2/auth?' . urldecode(http_build_query($params));
    }

    public function getGoogleLinkUrl()
    {
        $params = [
            'client_id' => '672236738468-ktemnnoeb3hv4lseatg32dcg0v2tgpdf.apps.googleusercontent.com',
            'redirect_uri' => 'https://fresh.black/auth/confirmGoogle',
            'response_type' => 'code',
            'scope' => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile',
            'state' => '123'
        ];

        return 'https://accounts.google.com/o/oauth2/auth?' . urldecode(http_build_query($params));
    }

    public function loginFb($code)
    {
        $data = $this->getUserTokenFb();

        if (!empty($data['access_token'])) {
            $info = $this->getUserDataFb($data);

            if (isset($info['email'])) {
                $user = User::model()->getUserByEmail($info['email']);
            } else {
                $user = User::model()->getUserByFbId($info['id']);
            }

            if ($user) {
                if (!$this->identify($user)) {
                    return false;
                }
            } else {
                $userId = User::model()->addBySocialFb($info);

                if (!$userId) {
                    return false;
                }

                $user = User::model()->getUserById($userId);

                if (!$this->identify($user)) {
                    return false;
                }
            }
            return true;
        }

        return false;
    }

    public function linkGoogle($code)
    {
        $data = $this->getUserToken($code, true);
        $userId = Yii::app()->user->id;

        if (!empty($data['access_token']) && $userId) {
            $info = $this->getUserData($data);

            if (!empty($info)) {
                $updateResult = false;
                $user = User::model()->getUserById($userId);
                $socialUser = User::model()->getUserByEmail($info['email']);

                if (empty($socialUser)) {
                    $socialUser = User::model()->getUserByEmailSocial($info['email']);
                }

                if (!empty($socialUser)) {
                    $updateResult = User::model()->updateUserSocial($user['user_id'], $socialUser['user_email']);
                }

                if (!empty($socialUser) && $user['user_id'] !== $socialUser['user_id']) {
                    $socialSub = Subscription::model()->getSubByUser($socialUser['user_id']);

                    if (!empty($socialSub)) {
                        $result = $this->removeSubPayment($socialSub);

                        Subscription::model()->deleteSub($socialSub);
                        Subscription::model()->saveReasonSub($socialUser, 'Link account', $socialSub, $result);
                    }

                    if ($updateResult) {
                        User::model()->removeUser($socialUser['user_id']);
                    }
                }
            }
        }
    }

    public function linkFacebook()
    {
        $userId = Yii::app()->user->id;

        $data = $this->getUserTokenFb(true);

        if (!empty($data['access_token'])) {
            $info = $this->getUserDataFb($data);

            if (!empty($info)) {
                $updateResult = false;
                $user = User::model()->getUserById($userId);

                if (isset($info['email'])) {
                    $socialUser = User::model()->getUserByEmail($info['email']);
                } else {
                    $socialUser = User::model()->getUserByFbId($info['id']);
                }

                if (!empty($socialUser)) {
                    $updateResult = User::model()->updateUserSocial($user['user_id'], $socialUser['fb_id'], true);
                }

                if (!empty($socialUser) && $user['user_id'] !== $socialUser['user_id']) {
                    $socialSub = Subscription::model()->getSubByUser($socialUser['user_id']);

                    if (!empty($socialSub)) {
                        $result = $this->removeSubPayment($socialSub);

                        Subscription::model()->deleteSub($socialSub);
                        Subscription::model()->saveReasonSub($socialUser, 'Link account', $socialSub, $result);
                    }

                    if ($updateResult) {
                        User::model()->removeUser($socialUser['user_id']);
                    }
                }
            }
        }
    }

    public function login($code)
    {
        $data = $this->getUserToken($code);

        if (!empty($data['access_token'])) {

            $info = $this->getUserData($data);

            $user = User::model()->getUserByEmail($info['email']);

            if (empty($user)) {
                $user = User::model()->getUserByEmailSocial($info['email']);
            }

            if ($user) {
                if (!$this->identify($user)) {
                    return false;
                }
            } else {
                $userId = User::model()->addBySocial($info);

                if (!$userId) {
                    return false;
                }

                $user = User::model()->getUserById($userId);

                if (!$this->identify($user)) {
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    public function identify($user)
    {
        if (!$user['user_email']) {
            $identity = new UserIdentity($user['fb_id'], $user['user_password']);
        } else {
            $identity = new UserIdentity($user['user_email'], $user['user_password']);
        }

        if ($identity->authenticateSocial()) {
            $duration = 3600 * 24 * 30;
            Yii::app()->user->login($identity, $duration);

            return true;
        } else {
            return false;
        }
    }

    public function getUserTokenFb($confirm = false)
    {
        $params = [
            'client_id' => '271812324255386',
            'client_secret' => '1d04fbf164496b796e6de24b08e057cb',
            'redirect_uri' => 'https://fresh.black/auth/loginFb',
            'code' => $_GET['code']
        ];

        if ($confirm) {
            $params['redirect_uri'] = 'https://fresh.black/auth/confirmFb';
        }

        $data = file_get_contents('https://graph.facebook.com/oauth/access_token?' . urldecode(http_build_query($params)));
        $data = json_decode($data, true);

        return $data;
    }

    public function getUserDataFb($data)
    {
        $params = array(
            'access_token' => $data['access_token'],
            'fields' => 'id,email,first_name,last_name,picture'
        );

        $info = file_get_contents('https://graph.facebook.com/me?' . urldecode(http_build_query($params)));
        $info = json_decode($info, true);

        return $info;
    }

    public function getUserData($data)
    {
        $params = array(
            'access_token' => $data['access_token'],
            'id_token' => $data['id_token'],
            'token_type' => 'Bearer',
            'expires_in' => 3599
        );

        $info = file_get_contents('https://www.googleapis.com/oauth2/v1/userinfo?' . urldecode(http_build_query($params)));

        return json_decode($info, true);
    }

    public function getUserToken($code, $confirm = false)
    {
        $params = [
            'client_id' => '672236738468-ktemnnoeb3hv4lseatg32dcg0v2tgpdf.apps.googleusercontent.com',
            'client_secret' => 'LDzwOV_0g5wQ5EShL6OLm4Vt',
            'redirect_uri' => 'https://fresh.black/auth/login',
            'grant_type' => 'authorization_code',
            'code' => $code
        ];

        if ($confirm) {
            $params['redirect_uri'] = 'https://fresh.black/auth/confirmGoogle';
        }

        $ch = curl_init('https://accounts.google.com/o/oauth2/token');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $data = curl_exec($ch);
        curl_close($ch);

        return json_decode($data, true);
    }

    public function removeSubPayment($sub)
    {
        $account = Yii::app()->params->wfp['account'];
        $password = Yii::app()->params->wfp['password'];

        $url = 'https://api.wayforpay.com/regularApi';

        $data = [
            "requestType" => "REMOVE",
            "merchantAccount" => $account,
            "merchantPassword" => $password,
            "orderReference" => $sub['order_reference']
        ];

        $options = [
            'http' => [
                'method' => 'POST',
                'content' => json_encode($data),
                'header' => "Content-Type: application/json\r\n" . "Accept: application/json\r\n"
            ]
        ];

        $context = stream_context_create($options);
        return file_get_contents($url, false, $context);
    }
}
