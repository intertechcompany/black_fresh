<?php

/**
 * FormModel class.
 * FormModel extends default validation class CFormModel.
 */
class FormModel extends CFormModel
{
	private $_myErrors = array();
	private $_errorFields = array();

	public function afterValidate()
	{	
        return parent::afterValidate();
    }
	
	public function jsonErrors()
	{
		if ($this->hasErrors()) {
			$errors_arr = $this->getErrors();

			// $this->_errorFields = array_keys($errors_arr);
			
			foreach ($errors_arr as $field => $errors) {
				if (!empty($errors)) {
					$tmp_messages = array();

					foreach ($errors as $error) {
						$this->_myErrors[] = CHtml::encode($error);
						$tmp_messages[] = CHtml::encode($error);
					}

					$this->_errorFields[$field] = array_unique($tmp_messages);
				}
			}
		}

		$json_errors = array(
			'messages' => array_unique($this->_myErrors),
			'fields' => $this->_errorFields,
		);
		
		return $json_errors;
	}

	public function getHtmlErrors()
	{
		return $this->jsonErrors();
	}
}