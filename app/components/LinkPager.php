<?php
class LinkPager extends CLinkPager {
	/**
	 * @var string hidden navigation element class for mobile devices.
	 */
	public $internalHiddenPageCssClass;
	
	/**
	 * @var string the type of pagination.
	 */
	public $type;
	
	/**
	 * @var integer the number of start selected page for ajax type.
	 */
	public $page_start = 0;
	
	/**
	 * Initializes the pager by setting some default property values.
	 */
	public function init()
	{
		parent::init();
		unset($this->htmlOptions['id']);
	}
	
	/**
	 * Executes the widget.
	 * This overrides the parent implementation by displaying the generated page buttons.
	 */
	public function run()
	{
		$this->registerClientScript();
		$buttons = $this->createPageButtons();
		
		if (empty($buttons)) {
			return;
		}

		$buttons = CHtml::tag('ul', $this->htmlOptions, implode('', $buttons));
		
		echo $this->header;
		echo $buttons;
		echo $this->footer;
	}
	
	/**
	 * Creates the page buttons.
	 * @return array a list of page buttons (in HTML code).
	 */
	protected function createPageButtons()
	{
		if(($pageCount=$this->getPageCount())<=1)
			return array();

		list($beginPage,$endPage)=$this->getPageRange();
		$currentPage=$this->getCurrentPage(false); // currentPage is calculated in getPageRange()
		$buttons=array();

		// first page
		// $buttons[]=$this->createPageButton($this->firstPageLabel,0,$this->firstPageCssClass,$currentPage<=0,false);

		// prev page
		if(($page=$currentPage-1)<0)
			$page=0;
		$buttons[]=$this->createPageButton($this->prevPageLabel,$page,$this->previousPageCssClass,$currentPage<=0,false);

		// internal pages
		for($i=$beginPage;$i<=$endPage;++$i) {
			if ($this->type == 'ajax' && ($i + 1) >= $this->page_start && $i <= $currentPage) {
				// selected
				$buttons[]=$this->createPageButton($i+1,$i,$this->internalPageCssClass,false,true);
			} else {
				$visible_indexes = array();

				if ($beginPage == $currentPage) {
					$visible_indexes = array(0, 1, 2);
				} elseif ($endPage == $currentPage) {
					$visible_indexes = array($endPage - 2, $endPage - 1, $endPage);
				} else {
					$visible_indexes = array($currentPage - 1, $currentPage, $currentPage + 1);
				}

				if (!in_array($i, $visible_indexes)) {
					$buttons[]=$this->createPageButton($i+1,$i,$this->internalHiddenPageCssClass,false,$i==$currentPage);
				} else {
					$buttons[]=$this->createPageButton($i+1,$i,$this->internalPageCssClass,false,$i==$currentPage);
				}
			}
		}

		// next page
		if(($page=$currentPage+1)>=$pageCount-1)
			$page=$pageCount-1;
		$buttons[]=$this->createPageButton($this->nextPageLabel,$page,$this->nextPageCssClass,$currentPage>=$pageCount-1,false);

		// last page
		// $buttons[]=$this->createPageButton($this->lastPageLabel,$pageCount-1,$this->lastPageCssClass,$currentPage>=$pageCount-1,false);

		return $buttons;
	}
	
	/**
	 * Creates a page button.
	 * You may override this method to customize the page buttons.
	 * @param string $label the text label for the button
	 * @param integer $page the page number
	 * @param string $class the CSS class for the page button.
	 * @param boolean $hidden whether this page button is visible
	 * @param boolean $selected whether this page button is selected
	 * @return string the generated button
	 */
	protected function createPageButton($label, $page, $class, $hidden, $selected)
	{
		if ($selected) {
			$class = !empty($class) ? ' class="active ' . CHtml::encode($class) . '"' : ' class="active"';
		}
		else {
			$class = !empty($class) ? ' class="' . CHtml::encode($class) . '"' : '';
		}

		// show html entity if elements has label and not hidden
		if (!$hidden && !empty($label)) {
			if ($selected) {
				return '<li' . $class . '>' . CHtml::tag('span', array(), $label) . '</li>';
			}
			else {
				return '<li' . $class . '>' . CHtml::link($label, $this->createPageUrl($page)) . '</li>';
			}
		}
	}
}