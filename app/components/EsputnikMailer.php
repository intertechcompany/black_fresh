<?php
class EsputnikMailer extends Mailer
{
	private $login;
	private $password;
	private $from_email;
	private $from_name;

	public function __construct($from_email = '', $from_name = '')
	{
		require_once Yii::getPathOfAlias('application.vendor.GuzzleHttp') . DS . 'autoload.php';
		
		$this->login = Yii::app()->params->mailer['esputnik']['login'];
		$this->password = Yii::app()->params->mailer['esputnik']['password'];
		$this->from_email = !empty($from_email) ? $from_email : Yii::app()->params->mailer['from_email'];
		$this->from_name = !empty($from_name) ? $from_name : Yii::app()->params->mailer['from_name'];
	}

	/**
	 * Send message via SendGrid API.
	 * 
	 * @param Array $to_list an array of email addresses. 
	 * @param string $subject the subject of your email.
	 * @param string $body the body of your email.
	 * @return boolean success send or failed.
	 */
	public function send(array $to_list, $subject, $body, $plain = '')
	{
        if (Yii::app()->params->dev) {
			Yii::log($this->from_email . ' ' . $this->from_name . "\n\n" . var_export($to_list, true) . "\n\n" . $subject . "\n\n" . $body, 'error', 'esputnik');
			
			return false;
		}
		
		$client = new GuzzleHttp\Client(array(
			'timeout' => 15.0,
		));

		$request_body = [
			'from' => '"' . $this->from_name . '" <' . $this->from_email . '>',
			'subject' => $subject,
			'htmlText' => $body, 
			'plainText' => $plain,
			'emails' => $to_list,
		];

		try {
			$response = $client->post('https://esputnik.com/api/v1/message/email', [
				'headers' => [
					'Content-Type'  => 'application/json',
					'Accept'        => 'application/json',
				],
				'auth' => [
					$this->login, 
					$this->password,
				],
				'body' => json_encode($request_body),
			]);

			Yii::log($response->getBody(), 'info', 'esputnik');

			return true;
		} catch (GuzzleHttp\Exception\RequestException $e) {
			$exception = "Status code: " . $e->getCode() . ". Error message: " . $e->getMessage();

			if ($e->hasResponse()) {
				$exception .= $e->getResponse()->getBody();
			}

			Yii::log($exception, 'error', 'esputnik');
		}
			
		return false;
	}
}
