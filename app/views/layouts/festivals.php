<?php /* @var $this Controller */ ?>
<?php
if ($this->route == 'site/index') {
    $meta_title = $this->pageTitle;
} else {
    $meta_title = $this->pageTitle . ' | ' . Yii::app()->name;
}

$meta_description = $this->pageDescription ?: '';

$staticUrl = Yii::app()->assetManager->getBaseUrl() . '/static/' . Yii::app()->params->settings['rev'];

$this->ogTitle = !empty($this->ogTitle) ? $this->ogTitle : $this->pageTitle;
$this->ogDescription = !empty($this->ogDescription) ? $this->ogDescription : $meta_description;
$this->ogUrl = Yii::app()->getRequest()->getBaseUrl(true) . '/' . Yii::app()->getRequest()->getPathInfo();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width,initial-scale=1,minimum-scale=1" name="viewport">
    <meta content="ie=edge" http-equiv="X-UA-Compatible">
    <title><?=Lang::t('festivals.title')?></title>
    <meta content="<?=Lang::t('festivals.description')?>" name="description">
    <meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1,maximum-scale=1,minimum-scale=1">
    <link rel="icon" type="image/png" href="/festival-assets/images/icons/favicon.png" />
    <link href="/festival-assets/css/index.css" rel="stylesheet">

<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '164049038258643');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=164049038258643&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->
</head>
<body>
<?php echo $content; ?>
<script defer="defer" src="/festival-assets/js/188.js"></script>
<script defer="defer" src="/festival-assets/js/index.js"></script>
</body>
</html>
