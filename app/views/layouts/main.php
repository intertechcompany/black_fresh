<?php /* @var $this Controller */ ?>
<?php
    if ($this->route == 'site/index') {
        $meta_title = $this->pageTitle;
    } else {
        $meta_title = $this->pageTitle . ' | ' . Yii::app()->name;
    }

    $categories = Yii::app()->params->categories_tree;

    $meta_description = $this->pageDescription ?: '';

    $staticUrl = Yii::app()->assetManager->getBaseUrl() . '/static/' . Yii::app()->params->settings['rev'];

    $this->ogTitle = !empty($this->ogTitle) ? $this->ogTitle : $this->pageTitle;
    $this->ogDescription = !empty($this->ogDescription) ? $this->ogDescription : $meta_description;
    $this->ogUrl = Yii::app()->getRequest()->getBaseUrl(true) . '/' . Yii::app()->getRequest()->getPathInfo();

    $cart_total = Cart::model()->getTotal();
?><!DOCTYPE html>
<html>
<head>
    <title><?=CHtml::encode($meta_title)?></title>
    <meta name="description" content="<?=CHtml::encode($meta_description)?>">
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="<?=$staticUrl?>/css/style.min.css">
    <script src="<?=$staticUrl?>/js/modernizr.min.js"></script>

    <?php if (!empty($this->canonicalUrl)) { ?><link rel="canonical" href="<?=$this->canonicalUrl?>"><?php } ?>
    <?php if (!empty($this->noIndex)) { ?><meta name="robots" content="noindex,nofollow"><?php } ?>

    <link rel="shortcut icon" href="/favicon.ico" sizes="16x16 32x32" type="image/x-icon">
    <link rel="apple-touch-icon" href="/apple-touch-icon.png">

    <meta property="og:url" content="<?=CHtml::encode($this->ogUrl)?>">
    <meta property="og:title" content="<?=CHtml::encode($this->ogTitle)?>">
    <meta property="og:type" content="website">
    <meta property="og:description" content="<?=CHtml::encode($this->ogDescription)?>">
    <?php if (!empty($this->ogImage)) { ?>
    <meta property="og:image" content="<?=CHtml::encode($this->ogImage)?>">
    <?php } ?>

    <?php if(!$this->noIndex) { ?>
        <?php if (!in_array($this->route, ['site/cart', 'site/checkout'])
            && strpos($this->route, 'site/account') === false
            && strpos($this->route, 'site/user') === false
        ) { ?>
            <meta name="robots" content="index, follow">
        <?php } else { ?>
            <meta name="robots" content="noindex, nofollow">
        <?php } ?>
    <?php } ?>

    <?php if (!empty(Yii::app()->params->highlight)) { ?>
    <style>
        .__highlight {
            display: inline-block;
            position: absolute;
            height: 16px;
            padding: 1px 5px;
            color: #000;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
            line-height: 14px;
        }

        .__highlight input {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 16px;
            border: 0;
            background-color: yellow;
            padding: 0 5px;
        }
    </style>
    <?php } ?>

    <?php if ($this->route == 'site/index') { ?>
        <!-- Hotjar Tracking Code for https://fresh.black/ -->
        <script>
          (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:2063009,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
          })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script>
    <?php } ?>

    <?php if (!Yii::app()->params->dev) { ?>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PWKS6V7');</script>
    <!-- End Google Tag Manager -->
    
    <style>
    	@media (min-width: 768px)
	  .footer__head:before {
	    width: 133px;
	    height: 51px !important;
	}
    </style>

    <?php if ($this->route == 'site/parents') { ?>
        <link rel="stylesheet" href="<?=$staticUrl?>/fonts/stylesheet.css">
    <?php } ?>

    <?php
        if (isset(Yii::app()->params['googleLayout']) && Yii::app()->params['googleLayout']['result'] === true) {
            echo Yii::app()->params['googleLayout']['content'];
        }
    ?>

    <style>
        .header__catalog {
            z-index: 1;
        }
        .product-card__info {
            background-color: #56e1ff;
        }
        .header__logo {
            position: relative;
            background-image: url(/img/new_logo_freshblack_block_black.png);
            background-repeat: no-repeat;
            background-size: 82%;
        }
        .footer__head:before {
            content: '';
            display: block;
            background-image: url(/img/new_logo_freshblack_block_black.png);
            background-repeat: no-repeat;
            background-size: 100%;
        }
        .subscribe:before {
            content: '';
            display: block;
            background-image: url(/img/new_okay_freshblack_black.png);
            background-repeat: no-repeat;
            background-size: 100%;
        }
        .mission:before {
            content: '';
            display: block;
            background-image: url(/img/new_okay_freshblack_black.png);
            background-repeat: no-repeat;
            background-size: 100%;
        }
    </style>
</head>
<body>
    <?php if (!Yii::app()->params->dev) { ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PWKS6V7"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?php } ?>
    
    <div class="container">
        <?php $this->widget('application.components.Header.Header'); ?>
        
        <?php echo $content; ?>

        <?php if ($this->route != 'site/thankyou' && $this->route != 'site/parents') { ?>
        <?php $this->widget('application.components.Footer.Footer'); ?>
        <?php } ?>
    </div>

    <?php $this->widget('application.components.CartSide.CartSide'); ?>

    <script src="<?=$staticUrl?>/js/app.min.js"></script>

    <?php if (!Yii::app()->params->dev) { ?>
    <?=Yii::app()->params->settings['ga']?>
    <?php } ?>

    <?php } ?>

    <?php if (!empty(Yii::app()->params->highlight)) { ?>
    <script>
        $(function() {
            $('.__highlight').on('click', 'input', function(e) {
                e.preventDefault();
                this.setSelectionRange(0, this.value.length);
            });
        });
    </script>
    <?php } ?>
</body>
</html>
