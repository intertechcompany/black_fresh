<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" style="outline-style: none; margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; border-width: 0; vertical-align: baseline">
<head style="outline-style: none">
    <!--[if gte mso 9
      ]><xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG />
            <o:PixelsPerInch>96</o:PixelsPerInch>
          </o:OfficeDocumentSettings>
        </xml><!
    [endif]-->
    <!-- fix outlook zooming on 120 DPI windows devices -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- So that mobile will display zoomed in -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- enable media queries for windows phone 8 -->
    <meta name="format-detection" content="date=no">
    <!-- disable auto date linking in iOS 7-9 -->
    <meta name="format-detection" content="telephone=no">
    <!-- disable auto telephone linking in iOS 7-9 -->

</head>
<body style="font-family: arial;font-size: 14px;color: #000;">
<table align="center" style="width: 100%;max-width: 600px;">
    <tbody>
    <tr>
        <td style="font-size: 6px;line-height: 10px;padding: 8px 0 14px;border-bottom-width: 1px;border-bottom-style: solid;border-bottom-color: rgb(0, 0, 0);">
            <img src="https://fresh.black/assets/static/v1.0.27/images/new_logo.png" width="100" alt="logo">
        </td>
    </tr>
    <tr>
        <td style="padding-bottom: 20px;padding-top: 20px;">
            <p style="font-family: arial;font-size: 20px;margin: 0;font-weight: 400;font-style: normal;text-align: center;"><?=Lang::t('email.body.orderNum', ['{order_id}' => $order['order_id']])?></p>
        </td>
    </tr>
    <tr>
        <td style="padding-bottom: 50px;">
            <p style="font-family: arial;font-size: 15px;">
                <?php if ($order['status'] == 'paid') { ?>
                    <?=Lang::t('email.body.paid')?>
                <?php } else { ?>
                    <?=Lang::t('email.body.thankYou', ['{first_name}' => $order['first_name']])?>
                <?php } ?>
            </p>
        </td>
    </tr>
    <tr>
        <td style="padding-bottom: 20px;">
            <p style="font-family: arial;font-size: 15px;"><?=Lang::t('email.body.products')?></p>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" style="border-bottom-width: 1px;border-bottom-style: solid;border-bottom-color: rgb(0, 0, 0);">
                <tbody>
                <?php
                $assetsUrl = Yii::app()->assetManager->getBaseUrl();
                ?>
                <?php foreach ($order_products as $order_product) { ?>
                    <?php
                    if (!empty($order_product['product_photo'])) {
                        $product_photo = json_decode($order_product['product_photo'], true);
                        $product_photo = $assetsUrl . '/product/' . $order_product['product_id'] . '/' . $product_photo['path']['catalog_alt']['1x'];
                    } else {
                        $product_photo = '';
                    }

                    if (!empty($order_product['product_alias'])) {
                        $product_link = $this->createAbsoluteUrl('site/product', array('alias' => $order_product['product_alias']));
                    } else {
                        $product_link = $this->createAbsoluteUrl('site/index');
                    }
                    ?>
                    <tr>
                        <td width="20%" style="padding-bottom: 20px;">
                            <a href="<?=$product_link?>" target="_blank">
                                <img src="<?=$product_photo?>" width="80" height="94" alt="product img">
                            </a>
                        </td>
                        <td width="20%" style="padding-bottom: 20px;">
                            <a href="<?=$product_link?>" target="_blank" style="color: #000;text-decoration: none;"><?=$order_product['title']?></a>
                        </td>
                        <td width="20%" style="padding-bottom: 20px;"><?=$order_product['variant_title']?></td>
                        <td width="20%" style="padding-bottom: 20px;"><?=$order_product['quantity']?></td>
                        <td width="20%" style="padding-bottom: 20px;"><?=$order_product['price']?> <?=Lang::t('layout.tip.uah')?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding-top: 20px;padding-bottom: 52px;font-weight: bold;"><?=Lang::t('email.body.amount')?> <?=$order['price']?> <?=Lang::t('layout.tip.uah')?></td>
    </tr>
    <tr>
        <td style="padding-bottom: 22px;">
            <p style="font-family: arial;font-size: 14px;font-weight: bold;margin: 0 0 5px;"><?=Lang::t('email.body.orderClientInfo')?></p>
            <p style="font-family: arial;font-size: 14px;margin: 0 0 5px;"><?=$order['full_name']?></p>
            <p style="font-family: arial;font-size: 14px;margin: 0 0 5px;">
                <a href="mailto:<?=$order['email']?>" target="_blank"><?=$order['email']?></a>
            </p>
            <p style="font-family: arial;font-size: 14px;margin: 0 0 5px;"><a href="tel:+<?=$order['phone']?>" target="_blank" style="color: #000;text-decoration: none;"><?=$order['phone']?></a></p>
        </td>
    </tr>
    <?php if(isset($order['gift_text']) && $order['gift_text'] !== '') { ?>
        <tr>
            <td style="padding-bottom: 22px;">
                <p style="font-family: arial;font-size: 14px;font-weight: bold;margin: 0 0 5px;"><?=Lang::t('email.body.orderClientInfo.giftText')?></p>
                <p style="font-family: arial;font-size: 14px;margin: 0 0 5px;"><?=$order['gift_text']?></p>
            </td>
        </tr>
    <?php } ?>
    <?php if($order['comment'] !== '') { ?>
        <tr>
            <td style="padding-bottom: 22px;">
                <p style="font-family: arial;font-size: 14px;font-weight: bold;margin: 0 0 5px;"><?=Lang::t('email.body.orderClientInfo.comment')?></p>
                <p style="font-family: arial;font-size: 14px;margin: 0 0 5px;"><?=$order['comment']?></p>
            </td>
        </tr>
    <?php } ?>
    <tr>
        <td style="padding-bottom: 22px;">
            <p style="font-family: arial;font-size: 14px;font-weight: bold;margin: 0 0 5px;"><?=Lang::t('email.body.orderDeliveryInfo')?></p>
            <?php if ($order['delivery'] == 1) { ?>
                <p style="font-family: arial;font-size: 14px;margin: 0 0 5px;"><?=Lang::t('email.body.orderDeliveryPickup')?></p>
            <?php } if ($order['delivery'] == 2) { ?>
                <p style="font-family: arial;font-size: 14px;margin: 0 0 5px;"><?=Lang::t('email.body.orderDeliveryCourier')?></p>
                <p style="font-family: arial;font-size: 14px;margin: 0 0 5px;"><?=Lang::t('email.body.orderDeliveryAddress')?> <?=$order['address']?></p>
            <?php } if ($order['delivery'] == 3) { ?>
                <p style="font-family: arial;font-size: 14px;margin: 0 0 5px;"><?=Lang::t('email.body.orderDeliveryNp')?></p>
                <p style="font-family: arial;font-size: 14px;margin: 0 0 5px;"><?=Lang::t('email.body.orderDeliveryCity')?> <?=$order['np_city']?></p>
                <p style="font-family: arial;font-size: 14px;margin: 0 0 5px;"><?=Lang::t('email.body.orderDeliveryDepartment')?> <?=$order['np_department']?></p>
            <?php } ?>
        </td>
    </tr>
    <tr>
        <td style="padding-bottom: 22px;">
            <p style="font-family: arial;font-size: 14px;font-weight: bold;margin: 0 0 5px;"><?=Lang::t('email.body.orderPaymentInfo')?></p>
            <p style="font-family: arial;font-size: 14px;margin: 0 0 5px;">
                <?php if ($order['payment'] == 1) { ?>
                    <?=Lang::t('email.body.orderPaymentCash')?>
                <?php } if ($order['payment'] == 2) { ?>
                    <?=Lang::t('email.body.orderPaymentCard')?>
                <?php } ?>
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" style="margin-top: 20px;border-top-width: 1px;border-top-style: solid;border-top-color: rgb(0, 0, 0);">
                <tbody>
                <tr>
                    <td style="padding-top: 20px;">
                        <?php if (!empty(Yii::app()->params->settings['phone'])) { ?>
                            <a href="tel:+<?=preg_replace('#[^\d]#', '', CHtml::encode(Yii::app()->params->settings['phone']))?>" target="_blank" style="color: #000;text-decoration: none;margin-right: 5px;">
                                <?=CHtml::encode(Yii::app()->params->settings['phone'])?>
                            </a>
                        <?php } ?>
                        <?php if (!empty(Yii::app()->params->settings['mail'])) { ?>
                            <a href="mailto:<?=CHtml::encode(Yii::app()->params->settings['mail'])?>" target="_blank" style="color: #000;text-decoration: none;margin-right: 5px;">
                                <?=CHtml::encode(Yii::app()->params->settings['mail'])?>
                            </a>
                        <?php } ?>
                        <?php if (!empty(Yii::app()->params->settings['facebook'])) { ?>
                            <a href="<?=CHtml::encode(Yii::app()->params->settings['facebook'])?>" target="_blank" style="color: #000;text-decoration: none;margin-right: 5px;">Facebook</a>
                        <?php } ?>
                        <?php if (!empty(Yii::app()->params->settings['instagram'])) { ?>
                            <a href="<?=CHtml::encode(Yii::app()->params->settings['instagram'])?>" target="_blank" style="color: #000;text-decoration: none;margin-right: 5px;">Instagram</a>
                        <?php } ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
