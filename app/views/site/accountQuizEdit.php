<?php
/* @var $this SiteController */
$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>

<style>

    @media only screen and (max-width: 2200px) {
        .quiz-answer {
            height: 60px;
            width: 540px;
        }

        .result-product-img {
            width: 551px;
            height: 661px;
        }

        .product-description {
            width: 460px;
        }

        .div-desc {
            margin-left: 50px;
            width: 400px;
        }

        .check {
            margin: 5px 25px 3px;
        }
    }

    @media only screen and (max-width: 1900px) {
        .quiz-answer {
            height: 60px;
            width: 540px;
        }

        .result-product-img {
            width: 551px;
            height: 661px;
        }

        .product-description {
            width: 460px;
        }

        .div-desc {
            margin-left: 50px;
            width: 400px;
        }

        .check {
            margin: 5px 25px 3px;
        }
    }
    @media only screen and (max-width: 1600px) {
        .quiz-answer {
            width: 540px;
            height: 60px;
        }

        .result-product-img {
            width: 551px;
            height: 561px;
        }

        .product-description {
            width: 460px;
        }

        .div-desc {
            margin-left: 50px;
            width: 400px;
        }

        .check {
            margin: 5px 25px 3px;
        }
    }
    @media only screen and (max-width: 1199px) {
        .quiz-answer {
            width: 455px;
            height: 60px;
        }

        .result-product-img {
            width: 450px;
            height: 535px
        }

        .product-description {
            width: 460px;
        }

        .div-desc {
            margin-left: 50px;
            width: 400px;
        }

        .check {
            margin: 5px 25px 3px;
        }
    }
    @media only screen and (max-width: 990px) {
        .quiz-answer {
            width: 340px;
            height: 60px;
        }

        .result-product-img {
            width: 335px;
            height: 440px;
        }

        .result-product-div {
            width: 375px;
            height: 380px;
        }

        .check {
            margin: 5px 25px 3px;
        }
    }
    @media only screen and (max-width: 767px) {
        .quiz-answer {
            width: 290px;
            height: 60px;
        }

        #quiz-body {
            display: block;
        }

        .result-product-img {
            width: 450px;
            height: 535px;
            margin-left: 110px;
        }

        .product-description {
            width: 460px;
        }

        .div-desc {
            margin-left: 50px;
            width: 400px;
        }

        .check {
            margin: 5px 25px 3px;
        }
    }
    @media only screen and (max-width: 600px) {
        .quiz-answer {
            width: 240px;
            height: 60px;
        }

        .result-product-img {
            width: 400px;
            height: 430px;
            margin-left: 120px;
        }

        .product-description {
            width: 400px;
        }

        .div-desc {
            margin-left: 50px;
        }

        .check {
            margin: 5px 25px 3px;
        }
    }

    @media only screen and (max-width: 500px) {
        .quiz-answer {
            width: 180px;
            height: 60px;
        }

        .product-description {
            width: 340px;
        }

        .result-product-img {
            width: 360px;
            height: 360px;
            margin-left: 60px;
        }

        .div-desc {
            margin-left: 0;
            width: 400px;
        }

        .check {
            margin: 5px 25px 3px;
        }
    }

    @media only screen and (max-width: 400px) {
        .quiz-answer {
            width: 138px;
            height: 60px;
        }

        .div-desc {
            margin-left: 0;
            width: auto;

        }

        .result-product-img {
            width: 241px;
            height: 268px;
            margin-left: 20px;

        }

        .check {
            margin: -2px 12px 3px;
        }
    }

    .title {
        font-family: "Apercu Pro", serif;
        font-style: normal;
        font-weight: normal;
        word-wrap: break-word;
        /*line-height: 50px;*/
        text-transform: uppercase;
    }

    .title .title-page {
        font-size: 40px;
    }

    .title .quiz-question {
        font-size: 34px;
    }

    .user-answer {
        color: grey;
    }

    .question {
        width: 785px;
        cursor: pointer;
    }

    .quiz-answer:hover {
        border: 1px solid #0098FF;
        cursor: pointer;
    }

    .quiz-answer {
        border: 1px solid #000000;
        margin-top: 20px;
        display: inline-block;
        height: 90px;
        width: 375px;
    }

    .quiz-new-answer {
        color: #0098FF;
    }

    .quiz-answer-text {
        margin-left: 20px;
        color: grey;
    }

    .quiz-body {
        display: inline-block;
        margin-left: 100px;
        margin-top: 20px;
        width: 68%;
    }

    .span-toggle {
        font-size: 34px;
        margin-top: -40px;
        margin-right: 20px;
        float: right;
    }

    .toggled {
        transform: rotate(180deg);
    }

    hr {
        border-bottom: 2px solid black;
        width: 100%;
    }

    .product-div {
        display: inline-block;
    }

    .result-radio {
        width: 100px;
        height: 25px;
    }

    .result-span {
        width: 190px;
    }

    .product-tip {
        margin: 20px 0 0;
    }

    .result-header {
        font-family: "Apercu Pro", serif;
        font-style: normal;
        font-weight: normal;
        word-wrap: break-word;
        font-size: 25px;
        /*line-height: 50px;*/
        text-transform: uppercase;
        margin-bottom: -40px;
        /*margin-bottom: 25px;*/
    }

    .result-product-name {
        font-family: "Apercu Pro", serif;
        font-style: normal;
        font-weight: normal;
        word-wrap: break-word;
        font-size: 42px;
        /*line-height: 50px;*/
        text-transform: uppercase;
        margin-bottom: 25px;
        /*margin-left: 40px;*/
    }

    p {
        margin-left: 40px;
        margin-bottom: 10px;
    }

    .result-product-btn {
        width: 241px;
        margin-top: 25px;
        margin-left: 40px;
    }

    .result-product-div {
        display: inline-block;
    }
    #quiz-edit-body a {
        color: #0098FF;
    }
</style>

<main class="home">
    <section class="banner" style="background-image: none;">
        <div class="wrap" id="wrap">
            <?php $this->renderPartial('accountMenu'); ?>
            <div id="quiz-edit-body" class="quiz-body"></div>
        </div>
        <div class="wrap">
            <div id="quiz-tip" style="display: none;">
                <hr class="hr">
                <section class="product__section">
                    <div class="product__subtitle"><?=Lang::t('quiz.tip.title')?></div>

                    <ol class="product__delivery list-unstyled">
                        <li>
                            <span class="product__delivery-title"><?=Lang::t('quiz.tip.first.title')?></span>
                            <span class="product__delivery-tip"><?=Lang::t('quiz.tip.first.text')?></span>
                        </li>
                        <li>
                            <span class="product__delivery-title"><?=Lang::t('quiz.tip.second.title')?></span>
                            <span class="product__delivery-tip"><?=Lang::t('quiz.tip.second.text')?></span>
                        </li>
                        <li>
                            <span class="product__delivery-title"><?=Lang::t('quiz.tip.third.title')?></span>
                            <span class="product__delivery-tip"><?=Lang::t('quiz.tip.third.text')?></span>
                        </li>
                    </ol>
                </section>
            </div>
        </div>
    </section>
</main>
<script>
    function toggle(divId, spanId) {
        const x = document.getElementById(divId);
        const toggle = document.getElementById(spanId);

        console.log('toggle');

        if (x.style.display === "none") {
            x.style.display = "block";
            toggle.classList.remove('toggled');
        } else {
            x.style.display = "none";
            toggle.classList.add('toggled');
        }
    }

    let questions;
    let answers;
    let userAnswer;
    let language = {};
    let questionCount = 0;
    let newAnswers = [];

    window.onload = async function() {
        const quizBody = document.getElementById('quiz-edit-body');
        if (quizBody) {
            await getCurrentLanguage();
            await getAnswers();
            await getQuestions();
            await getUserAnswer();
        }
    }

    async function getCurrentLanguage()
    {
        let currentUrl = window.location.href;
        let url = new URL(currentUrl);
        let requestUrl = '';

        if (url.pathname.includes('en')) {
            requestUrl = new URL('/en/language/current', url);
        } else if(url.pathname.includes('ru')) {
            requestUrl = new URL('/ru/language/current', url);
        } else  {
            requestUrl = new URL('/language/current', url);
        }

        try {
            const response = await fetch(requestUrl, {method: 'GET'});
            language = await response.json();
        } catch (e) {
            console.log(e);
        }
    }

    async function getQuestions() {
        try {
            const response = await fetch('/questions', {method: 'GET'});
            questions = await response.json();
            questionCount = questions.length;
        } catch (e) {
            console.log(e);
        }
    }

    async function getUserAnswer() {
        try {
            const response = await fetch('/quiz/user/answer', {method: 'GET'});
            userAnswer = await response.json();
            createBody();
        } catch (e) {
            console.log(e);
        }
    }

    async function getAnswers() {
        try {
            const response = await fetch('/answers', {method: 'GET'});
            answers = await response.json();
        } catch (e) {
            console.log(e);
        }
    }

    function createBody() {
        const quizBody = document.getElementById('quiz-edit-body');
        const btnBack = document.createElement('a');
        const title = document.createElement('h1');

        btnBack.href = '/accountSubscription';

        if (language.lang === 'ru') {
            btnBack.textContent = 'Назад';
            title.textContent = 'Результаты теста';
        } else if (language.lang === 'uk') {
            btnBack.textContent = 'Назад';
            title.textContent = 'Результати теста';
        } else if (language.lang === 'en') {
            btnBack.textContent = 'Back';
            title.textContent = 'Test result';
        } else {
            console.log('language error');
        }

        title.className = 'title title-page';

        quizBody.appendChild(btnBack);
        quizBody.appendChild(title);

        createQuestions(quizBody);
    }

    function createQuestions(quizBody) {

        for (let i = 0; i < questions.length; i++) {
            const divQuestion = document.createElement('div');
            const hr = document.createElement('hr');
            const title = document.createElement('h2');
            const spanUser = document.createElement('span');
            const spanNew = document.createElement('span');
            const spanNewAnswer = document.createElement('span');
            const spanToggle = document.createElement('span');

            divQuestion.className = 'question';
            divQuestion.onclick = () => { toggle('div-' + i, 'span-' + i) };

            hr.className = 'hr';

            if (language.lang === 'ru') {
                title.textContent = questions[i].title_ru;
                let j = i + 1;
                spanUser.textContent = 'Ваш ответ: ' + answers[userAnswer['quest_' + j]].title_ru;
                spanNew.textContent =  'Новый ответ: ';
            } else if (language.lang === 'uk') {
                title.textContent = questions[i].title_uk;
                setUserAnswer(i, 'uk', spanUser)
                spanNew.textContent =  'Нова відповідь: ';
            } else if (language.lang === 'en') {
                title.textContent = questions[i].title_en;
                let j = i + 1;
                spanUser.textContent = 'Your answer: ' + answers[userAnswer['quest_' + j]].title_en;
                spanNew.textContent =  'New answer: ';
            } else {
                console.log('language error');
            }

            spanNew.style.marginLeft = '20px';

            spanNewAnswer.className = 'quiz-new-answer';
            spanNewAnswer.id = 'new-' + questions[i].id;


            spanToggle.className = 'span-toggle toggled';
            spanToggle.id = 'span-' + i;
            spanToggle.textContent = '^';

            divQuestion.appendChild(hr);
            divQuestion.appendChild(title);
            divQuestion.appendChild(spanUser);
            divQuestion.appendChild(spanNew);
            spanNew.appendChild(spanNewAnswer);
            divQuestion.appendChild(spanToggle);
            quizBody.appendChild(divQuestion);

            createAnswers(questions[i].id, quizBody, i);
        }
        createButton();
    }

    function setUserAnswer(i, language, span) {
        let j = i + 1;

        answers.forEach(function (item) {
            if (item.id === userAnswer['quest_' + j]) {
                if (language === 'uk') {
                    span.textContent = 'Ваша відповідь: ' + item['title_' + language];
                } else if (language === 'ru') {
                    span.textContent = 'Ваш ответ: ' + item['title_' + language];
                } else if (language === 'en') {
                    span.textContent = 'Your answer: ' + item['title_' + language];
                }
            }
        });
    }

    function createAnswers(questionId, quizBody, i) {
        const answerBody = document.createElement('div');
        const divColumn = document.createElement('div');

        answerBody.className = 'question';
        answerBody.style.display = 'none';
        answerBody.id = 'div-' + i;

        divColumn.style.columnCount = 2;

        answers.forEach(function (item) {
            if (item.question_id === questionId) {
                const answerDiv = document.createElement('div');
                const answerTitle = document.createElement('p');
                const title = document.createElement('span');
                const text = document.createElement('p');

                answerDiv.className = 'quiz-answer';
                answerDiv.dataset.question = questionId;
                answerDiv.dataset.answer = item.id;
                answerDiv.onclick = () => { saveAnswer(questionId, item.id); }

                text.className = 'quiz-answer-text';

                if (language.lang === 'ru') {
                    title.textContent = item.title_ru;
                    text.textContent = item.text_ru;
                } else if (language.lang === 'uk') {
                    title.textContent = item.title_uk;
                    text.textContent = item.text_ua;
                } else if (language.lang === 'en') {
                    title.textContent = item.title_en;
                    text.textContent = item.text_en;
                } else {
                    console.log('language error');
                }

                if (item.icon) {
                    const icon = document.createElement('img');

                    icon.src = '/' + item.icon;
                    icon.style.width = '30px';
                    icon.style.height = '30px';

                    answerTitle.appendChild(icon);
                }

                title.style.marginLeft = '10px';
                title.id = 'title-' + item.id;
                answerTitle.style.marginLeft = '10px';

                answerDiv.appendChild(answerTitle);
                answerTitle.appendChild(title);
                answerDiv.appendChild(text);

                divColumn.appendChild(answerDiv);
            }

            answerBody.appendChild(divColumn);
            quizBody.appendChild(answerBody);
        })
    }

    function createButton() {
        const quizBody = document.getElementById('quiz-edit-body');
        const button = document.createElement('button');

        button.className = 'product__btn btn';
        button.style.width = '250px';
        button.style.marginTop = '20px';
        button.textContent = 'Зберегти';
        button.onclick = () => { send(); }

        quizBody.appendChild(button);
    }

    async function send() {
        newAnswers[0] = 'edit';
        try {
            const response = await fetch('/answers/new', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(newAnswers)
            });

            const data = await response.json();

            if (data.message === 'success') {
                createResult(data);
            } else if (data.message === 'empty') {
                createEmptyResult();
            }

        } catch (e) {
            console.log(e);
        }
    }

    function clearBody() {
        const body = document.getElementById('wrap');
        body.innerHTML = '';

        const quizBody = document.createElement('div');
        quizBody.id = 'quiz-body';

        body.appendChild(quizBody);
    }

    let type = '';

    function changeView()
    {
        if (window.screen.width > 990) {
            if (resultView) {
                document.getElementById('quiz-body').style.display = 'inline-flex';
            }
        } else {
            document.getElementById('quiz-body').style.display = 'block';
        }
    }

    window.addEventListener('resize', function(event){
        changeView();
    });

    function createResult(product) {
        clearBody();
        // document.getElementById('under-quiz-title').textContent = '';
        // document.getElementById('under-quiz-text').textContent = '';

        const quizBody = document.getElementById('quiz-body');

        let imageJson = JSON.parse(product.product_photo);

        // Part 1
        const divProductImg = document.createElement('div');
        const productImg = document.createElement('img');

        divProductImg.className = 'product-div';

        productImg.src = product.assetUrl + '/product/' + product.product_id + '/' + imageJson.path.original['1x'];
        productImg.className = 'result-product-img';


        // Part 2
        const divProduct = document.createElement('div');
        const header = document.createElement('h5');
        const productTitle = document.createElement('h4');
        const productTip = document.createElement('p');

        // product reason
        const divReasonHeader = document.createElement('div');
        const reasonToggle = document.createElement('span');
        const reasonHeader = document.createElement('h4');
        const divReasonBody = document.createElement('div');
        const reasonText = document.createElement('p');

        //product description
        const divProductHeader = document.createElement('div');
        const productToggle = document.createElement('span');
        const productHeader = document.createElement('h4');
        const divProductBody = document.createElement('div');
        const productText = document.createElement('p');

        divProduct.className = 'product-div div-desc';

        header.className = 'result-header';

        productTitle.textContent = product.product_brand;
        productTitle.className = 'result-product-name';

        productTip.className = 'product-tip';

        // product reason
        divReasonHeader.style.cursor = 'pointer';
        divReasonHeader.onclick = () => { toggle('div-1', 'span-1') };

        reasonToggle.className = 'span-toggle toggled';
        reasonToggle.id = 'span-1';
        reasonToggle.textContent = '^';
        reasonToggle.style.marginTop = '-50px';

        divReasonBody.style.display = 'none';
        divReasonBody.id = 'div-1';

        reasonText.style.marginLeft = 0;

        divReasonHeader.appendChild(reasonHeader);
        divReasonHeader.appendChild(reasonToggle);

        divReasonBody.appendChild(reasonText);

        //product description
        divProductHeader.style.cursor = 'pointer';
        divProductHeader.onclick = () => { toggle('div-2', 'span-2') };

        productToggle.className = 'span-toggle toggled';
        productToggle.id = 'span-2';
        productToggle.textContent = '^';
        productToggle.style.marginTop = '-50px';

        divProductBody.style.display = 'none';
        divProductBody.id = 'div-2';

        productText.textContent = product.product_tip;
        productText.style.marginLeft = 0;
        productText.className = 'product-description';

        divProductHeader.appendChild(productHeader);
        divProductHeader.appendChild(productToggle);

        divProductBody.appendChild(productText);

        let currentUrl = window.location.href;
        let url = new URL(currentUrl);
        let requestUrl = new URL('product/' + product.product_alias + '?action=subscription&type=' + type, url);

        const productButton = document.createElement('a');
        productButton.href = requestUrl.href;
        productButton.className = 'product__btn btn result-product-btn';
        productButton.style.width = '50%';

        if (language.lang === 'ru') {
            header.textContent = 'Твой кофе:';

            productTip.innerHTML = 'Получи скидку 30% на свою первую подписку:'
                + '<br>' + '- Подписывайтесь и оценивайте кофе'
                + '<br>' + '- Никогда не платите за доставку подписки'
                + '<br>' + '- Выберите удобный для вас график поставок'
                + '<br>' + '- Свежая обжарки и доставка прямо к вам'
            ;

            reasonHeader.textContent = 'УЗНАЙ БОЛЬШЕ ПРО СВОЮ ПОДПИСКУ:';
            productHeader.textContent = 'БОЛЬШЕ ОБ ЭТОМ СОРТЕ';
            productButton.textContent = 'Оформить подписку';
            reasonText.textContent = product.text.text_ru;
        } else if (language.lang === 'uk') {
            header.textContent = 'Твоя кава:';

            productTip.innerHTML = 'Отримай знижку 30% на свою першу підписку:'
                + '<br>' + '- Підписуйтесь та оцінюйте каву'
                + '<br>' + '- Ніколи не платіть за доставку підписки'
                + '<br>' + '- Оберіть зручний для вас графік доставок'
                + '<br>' + '- Свіже обсмаження та доставка прямо до вас'
            ;

            reasonHeader.textContent = 'ДІЗНАЙСЯ БІЛЬШЕ ПРО СВОЮ ПІДПИСКУ:';
            productHeader.textContent = 'БІЛЬШЕ ПРО ЦЕЙ СОРТ';
            productButton.textContent = 'Оформити підписку';
            reasonText.textContent = product.text.text_uk;
        } else if (language.lang === 'en') {
            header.textContent = 'Your coffee:';

            productTip.innerHTML = 'Get a 30% discount on your first subscription:'
                + '<br>' + '- Subscribe and rate coffee'
                + '<br>' + '- Never pay for a subscription delivery'
                + '<br>' + '- Choose a convenient delivery schedule for you'
                + '<br>' + '- Fresh roasted and delivery directly to you'
            ;

            reasonHeader.textContent = 'WHY DO YOU LIKE THIS COFFEE?';
            productHeader.textContent = 'LEARN MORE ABOUT YOUR SUBSCRIPTION:';
            productButton.textContent = 'Subscribe';
            reasonText.textContent = product.text.text_en;
        }

        quizBody.appendChild(divProductImg);
        quizBody.appendChild(divProduct);

        document.getElementById('quiz-tip').style.display = 'block';

        // Part 1
        divProductImg.appendChild(productImg);

        //Part 2
        divProduct.appendChild(header);
        divProduct.appendChild(productTitle);
        productVariants(divProduct, product);
        divProduct.appendChild(productTip);

        if (product.text.text_uk) {
            divProduct.appendChild(document.createElement('hr'));
            divProduct.appendChild(divReasonHeader);
            divProduct.appendChild(divReasonBody);
        }

        divProduct.appendChild(document.createElement('hr'));
        divProduct.appendChild(divProductHeader);
        divProduct.appendChild(divProductBody);
        divProduct.appendChild(productButton);

        setData(product);
        resultView = true;
        document.getElementById('quiz-body').style.display = 'inline-flex';
        document.getElementById('quiz-body').style.marginTop = '20px';
        changeView();
    }

    function productVariants(divProduct, product) {
        // product variants
        for (let i = 0; i < 2; i++) {
            const label = document.createElement('label');
            const input = document.createElement('a');
            const span = document.createElement('span');

            label.className = 'product__radio result-radio';

            if (i === 0) {
                label.style.marginLeft = '0';
                label.style.marginTop = '0';
            } else {
                label.style.marginLeft = '60px';
                label.style.marginTop = '0';
            }

            let currentUrl = window.location.href;
            let url = new URL(currentUrl);

            input.href = new URL('product/' + product.product_alias + '?action=subscription&type=' + type, url);
            input.id = 'value-' + i;
            input.style.textDecoration = 'none';

            span.id = 'span-radio-' + i;
            span.className = 'result-span';

            input.appendChild(span);
            label.appendChild(input);

            if (i > 0) {
                label.style.display = 'none';
            }

            // label.appendChild(span);
            divProduct.appendChild(label);
        }
    }

    function setData(product) {
        const productVariants = product.variants.variants;
        let count = 0;

        Object.keys(productVariants).forEach(function (key){
            let text = '';
            let currency = '';
            if (count === 0) {
                if (language.lang === 'ru') {
                    text = '200 г / ';
                    currency = 'грн';
                } else if (language.lang === 'uk') {
                    text = '200 г / ';
                    currency = 'грн';
                } else if (language.lang === 'en') {
                    text = '200 g / ';
                    currency = 'uah';
                }
            } else {
                if (language.lang === 'ru') {
                    text = '1 кг / ';
                    currency = 'грн';
                } else if (language.lang === 'uk') {
                    text = '1 кг / ';
                    currency = 'грн';
                } else if (language.lang === 'en') {
                    text = '1 kg / ';
                    currency = 'uah';
                }
            }
            let price = productVariants[key].variant_price - (productVariants[key].variant_price * 0.3);
            let html = text + "<small style='text-decoration: line-through; color: #1f78d8; font-size: 15px;'>" + (productVariants[key].variant_price).replace('.00', '') + "</small>" + ' ' + Math.round(price) + " " + currency;
            document.getElementById('span-radio-' + count).innerHTML = html;
            count++;
        });
    }

    function saveAnswer(questionId, answerId) {
        newAnswers[questionId] = answerId;
        console.log(newAnswers);

        document.getElementById('new-' + questionId).textContent = document.getElementById('title-' + answerId).textContent;
    }
</script>
