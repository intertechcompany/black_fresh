<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<div class="breadcrumbs">
	<a href="<?=Yii::app()->homeUrl?>"><?=Lang::t('layout.link.breadcrumbsHome')?></a> 
	<span class="b-sep">|</span> 
	<?=Category::renderBreadrumbs($this, $categories, $categories[$category['category_id']]['parents'], $category['category_id'])?>
</div>

<div class="category-head clearfix">
	<h1 class="ch-title"><?=CHtml::encode($category['category_name'])?></h1>
</div>

<?php if (!empty($categories[$category['category_id']]['sub'])) { ?>
<div id="home-categories" class="home-categories">
	<?php $this->renderPartial('categoriesList', array('categories' => $categories[$category['category_id']]['sub'])); ?>
</div>
<!-- /.home-categories -->
<?php } else { ?>
<p><?=Lang::t('category.tip.noCategories')?></p>
<?php } ?>