<style>
    .addresses-items span {
        color: #000;
    }
    .account-menu-box {
        margin-top: 29px;
        float: left;
        display: inline-block;
        width: 21%;
    }
    .account-info {
        display: inline-block;
        margin-left: 100px;
        width: 68%;
        margin-top: 5px;
    }
    .account-password {
        display: inline-block;
        margin-left: 335px;
    }
    #account-addresses {
        display: inline-block;
        margin-left: 335px;
    }
    #account-cartds {
        display: inline-block;
        margin-left: 335px;
        /*       width: 70%;*/
    }
    .adress-tex-message {
        font-size: 15px;
        line-height: 22px;
        color: #808080;
        margin-top: 0;
    }
    .add-new-info a {
        color:#0098FF;
    }
    .add-new-info {
        margin-top: 20px;
        width: 100%
    }
    #account-password {
        width: 75%;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-direction: row;
        flex-direction: row;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
        -ms-flex-line-pack: stretch;
        align-content: stretch;
        -webkit-box-align: start;
        -ms-flex-align: start;
        align-items: flex-start;
    }
    .width-field {
        width: 50%;
        margin-bottom: 20px;
    }
    .user-cards-style {
        width: 100%;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-direction: row;
        flex-direction: row;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
        -ms-flex-line-pack: stretch;
        align-content: stretch;
        -webkit-box-align: start;
        -ms-flex-align: start;
        align-items: flex-start;
    }
    .width-cards {
        background: #fff;
        padding: 10px;
        margin-top: 5px;
        border: 2px solid #000;
        width: 385px;
        margin-right: 10px;
    }
    #af-password_current {
        width: 270px;
        height: 40px;
    }
    #af-newpassword {
        width: 270px;
        height: 40px;
    }
    #af-newpassword_confirm {
        width: 270px;
        height: 40px;
    }
    .flex-width {
        width: 100%;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-direction: row;
        flex-direction: row;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
        -ms-flex-line-pack: stretch;
        align-content: stretch;
        -webkit-box-align: start;
        -ms-flex-align: start;
        align-items: flex-start;
    }
    .adress-width {
        background: #fff;
        padding: 10px;
        margin-top: 5px;
        border: 2px solid #000;
        width: 380px;
        height: 175px;
        margin-right: 10px;
    }
    .width-btn {
        width: 100%;
    }
    .main-adress {
        float: right;
        font-size: 12px;
        line-height: 8px;
    }
    @media (max-width: 1200px) {
        .account-password {
            margin-left: 298px;
        }
        .width-field {
            width: 46%;
        }
        #account-password {
        width: 100%;
        }
        #account-addresses {
            margin-left: 298px;
        }
        .adress-width {
            width: 316px;
            height: 175px;
        }
    }
    @media (max-width: 1000px) {
        .account-info {
            width: 74%;
        }
        .account-password {
            margin-left: 183px;
        }
        .account-title {
            font-size: 20px;
        }
        .width-field {
            width: 50%
        }
        #af-password_current, #af-newpassword, #af-newpassword_confirm{
            width: 240px;
        }
        #account-addresses {
            margin-left: 183px;
        }
        .adress-width {
            width: 245px;
        }
    }

    @media (max-width: 700px) {
        .account-password {
            margin-left: 157px;
        }
        #account-addresses {
            margin-left: 157px;
        }
        .width-field {
            width: 47%; 
        }
        #af-password_current, #af-newpassword, #af-newpassword_confirm{
            width: 200px;
            height: 35px;
            font-size: 16px;
        }
        .adress-width {
            width: 100%;
            font-size: 12px;
            height: 140px;
        }
    }
    @media (max-width: 620px) {
        .account-password {
            margin-left: 148px;
        }
        .width-field {
            width: 51%;
        }
        #af-password_current, #af-newpassword, #af-newpassword_confirm{
            width: 240px;
        }
        #account-addresses {
            margin-left: 148px;
        }
        .account-info {
            margin-left: 5px;
            padding-left: 50px;
        }
    }
    @media (max-width: 550px) {
        .account-password {
            padding-left: 0 !important;
            margin-left: 0 !important;
        }
        #account-addresses {
            padding-left: 14px;

        }
        .account-menu-box {
        	width: 100% !important;
        }
        .box-content li {
        	font-size: 16px !important;
    		line-height: 23px !important;
        }
        .account-info {
        	margin-left: 0 !important;
    		padding-left: 0 !important;
        }
        #account-addresses {
			margin-left: 0;
            padding-left: 0;     	
        }
    }
    @media (max-width: 460px) {
        .account-password  {
            float: left;
            margin-left: 0;
            padding-left: 0;
        }
        #account-addresses {
            float: left;
            margin-left: 0;
            padding-left: 0;
        }
    }




</style>

<?php
/* @var $this SiteController */
$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<div class="wrap account-edit">
    <div class="content-divider"></div>
    <?php $this->renderPartial('accountMenu'); ?>
    <div class="account-wrap clearfix">
        <div id="account-personal" class="account-personal account-info">
            <h1 class="account-title"><?=Lang::t('account.title.personalData')?></h1>

            <div class="account-personal-data">
                <p class="success-message"></p>
                <form id="account-details" action="<?=$this->createUrl('site/accountedit')?>" class="account-form hidden" novalidate>
                    <?php $this->renderPartial('accountPersonalForm', array('user' => $user)); ?>
                </form>
            </div>
        </div>
        <div id="account-personal" class="account-personal account-password">
            <h1 class="account-title"><?=Lang::t('account.title.personalDataPassword')?></h1>

            <div class="account-personal-data">
                <form id="account-password" action="<?=$this->createUrl('site/accountedit')?>" class="account-form hidden" novalidate>
                    <div class="width-field"><label for="af-password_current"><?=Lang::t('account.tip.labelCurrentPassword')?></label>

                        <input id="af-password_current" type="password" name="user[password_current]" value="">
                        <span class="error error-password_current"></span>
                    </div>
                    <div class="width-field"><label for="af-newpassword"><?=Lang::t('account.tip.labelNewPassword')?></label>

                        <input id="af-newpassword" type="password" name="user[password]" value="">
                        <span class="error error-password"></span>
                    </div>
                    <div class="width-field">
                        <label for="af-newpassword_confirm"><?=Lang::t('account.tip.labelConfirmNewPassword')?></label>

                        <input id="af-newpassword_confirm" type="password" name="user[password_confirm]" value="">
                        <span class="error error-password_confirm"></span>


                        <input type="hidden" name="action" value="password">

                    </div>
                    <div class="width-btn"> <button class="btn mt-btn"><?=Lang::t('account.btn.changePassword')?></button></div>

                </form>
            </div>
        </div>

        <div id="account-addresses">
            <div class="account-wrap clearfix">
                <div id="account-personal" class="account-personal">
                    <h1 class="account-title"><?=Lang::t('account.title.userAddresses')?></h1>


                    <div class="account-personal-data flex-width">
                        <?php foreach ($addresses as $addrss) : ?>
                            <div
                                    class="addresses-items adress-width"
                                    style="
                                    background: #fff;
                                    padding: 20px;
                                    margin-top: 5px;
                                    border: 1px solid #000;
                                "
                            >
                                <div>
                                    <div>
                                        <span><?= $addrss['first_name'] ?> <?= $addrss['last_name'] ?></span>
                                        <?php if (!empty($addrss['is_main']) && $addrss['is_main'] == 1) : ?>
                                        <span class="main-adress" >
                                            <?= Lang::t('account.addresses.is.main') ?>
                                        </span>
                                    <?php endif; ?>
                                    </div>
                                    <div>
                                        <span><?= $addrss['flat'] ?> <?= $addrss['street'] ?></span>
                                    </div>
                                    <div>
                                        <span><?= $addrss['city'] ?>, <?= $addrss['region'] ?> <?= $addrss['zip_code'] ?></span>
                                    </div>
                                    <div>
                                        <span><?= $addrss['country'] ?></span>
                                    </div>
                                </div>

                                <div class="add-new-info">
                                    <a
                                            href="<?=$this->createUrl('accountAddressUpdate/' . $addrss['id'])?>"
                                            style="
                                            text-decoration: none;
                                        "
                                    >
                                        <?=Lang::t('account.addresses.update')?>
                                    </a>
                                    /
                                    <a 
                                            href="<?=$this->createUrl('/accountAddressRemove/' . $addrss['id'])?>"
                                            style="
                                            text-decoration: none;
                                        "
                                    >
                                        <?=Lang::t('account.addresses.remove')?>
                                    </a>
                                </div>
                            </div>
                        <?php endforeach; ?>

                        <div class="add-new-info">
                            <a
                                    href="<?=$this->createUrl('site/accountAddressCreate')?>"
                                    style="
                                    text-decoration: none;
                                "
                            >
                                <?=Lang::t('account.addresses.create')?> +
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- <div id="account-cartds">
            <div class="account-wrap clearfix">
                <div id="account-personal" class="account-personal">
                    <h1 class="account-title"><?=Lang::t('account.title.userCards')?></h1>

                    <div class="account-personal-data user-cards-style">
                        <?php foreach ($cards as $card) : ?>
                            <div
                                    class="addresses-items width-cards"
                            >
                                <div>
                                    <div>
                                        <span><?= $card['number'] ?></span>
                                    </div>
                                    <div>
                                        <span><?= date('m / Y', strtotime($card['date'])) ?></span>
                                    </div>
                                </div>
                                <div class="add-new-info">
                                    <?php if (!empty($card['is_main']) && $card['is_main'] == 1) : ?>
                                        <span>
                                            <?= Lang::t('account.card.is.main') ?>
                                        </span>
                                    <?php endif; ?>



                                    <a
                                            href="<?=$this->createUrl('/accountCardRemove/' . $card['id'])?>"
                                            style="
                                            text-decoration: none;
                                        "
                                    >
                                        <?=Lang::t('account.card.remove')?>
                                    </a>

                                    <?php if (empty($card['is_main']) || $card['is_main'] == 0) : ?>
                                        /
                                        <a
                                                href="<?=$this->createUrl('/accountCardMain/' . $card['id'])?>"
                                                style="
                                                text-decoration: none;
                                            "
                                        >
                                            <?=Lang::t('account.card.check.is.main')?>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>

                        <div class="add-new-info">
                            <a
                                    href="<?=$this->createUrl('accountCardCreate')?>"
                                    style="
                                    text-decoration: none;
                                "
                            >
                                <?=Lang::t('account.card.create')?> +
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<script>
    $('#account-details, #account-password').on('submit', function(e) {
        e.preventDefault();
        let form = $(this);

        $.ajax({
            url: window.location.href,
            method: 'post',
            data: form.serialize(),
            dataType: 'json',
            success: function(data, textStatus, jqXHR)
            {
                if (typeof data.error !== 'undefined' && data.error == true) {

                    form.find('.error').hide();

                    $.each(data.errorFields, function (key, val) {
                        let element = form.find('.error-' + key);

                        element.text(val[0]);
                        element.show();
                    });
                }

                if (typeof data.msg !== 'undefined' && data.msg != '') {
                    form.parent().find('.success-message').text(data.msg);
                }
            },
            error: function(data) {
                alert(data.error);
            }
        });
    });
</script>