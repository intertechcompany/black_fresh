<style>
    .account-menu-box {
        margin-top: 30px;
        float: left;
        display: inline-block;
        width: 21%;
    }
    h1 {
        margin-top: 25px;
        margin-bottom: 25px;
    }
    .box-content ul {
        list-style: none;
        padding-left: 0;

    }
    .personal-cabinet-title {
        font-size: 19px;
        line-height: 22px;
        margin-bottom: 20px;
    }
    .box-content li {
        margin-bottom: 16px;
        font-size: 18px;
        line-height: 22px;
    }
    .box-content li a {
        text-decoration: none;
    }
    @media (max-width: 1000px) {
        .personal-cabinet-title {
            font-size: 14px;
        }
        .box-content li {
            margin-bottom: 10px;
            font-size: 13px;
            line-height: 17px;
        }
    }
    @media (max-width: 400px) {
        .account-menu-box {
            width: 100%;
        }
    }

</style>
<div class="account-menu-box">
    <div class="box-heading personal-cabinet-title"></div>
    <div class="box-content">
        <ul>
            <?php
                foreach ($faqGroups as $faqGroup) {
            ?>
            <li>
                <a role="button" onclick="scrollGroup(<?= $faqGroup['faq_id'] ?>)" style="cursor: pointer;"><?php echo $faqGroup['faq_title'] ?></a>
            </li>
            <?php } ?>
        </ul>
    </div>
</div>

<script>
    function scrollGroup(id) {
        document.getElementById('group-' + id).scrollIntoView({block: "center", behavior: "smooth"});
    }
</script>