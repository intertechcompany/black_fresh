<?php /* @var $this SiteController */ ?>
<main class="thank-you wrap">
	<div class="thank-you__container">
		<h1 class="thank-you__title"><?=Lang::t('thankYou.feedback.new')?></h1>
		<div class="thank-you__tip">
			<?=str_replace(["\r", "\n"], ["", "<br>\n"], Lang::t('thankYou.feedback.manager', ['{phone}' => preg_replace('#[^\d]#', '', CHtml::encode(Yii::app()->params->settings['phone'])), '{phone_formatted}' => CHtml::encode(Yii::app()->params->settings['phone'])]))?>
		</div>
		<div class="thank-you__back"><a href="<?=$this->createUrl('site/index')?>" class="btn"><?=Lang::t('thankYou.btn.backToHome')?></a></div>
	</div>

	<form id="subscribe" class="subscribe" action="<?=$this->createUrl('ajax/subscribe')?>" method="post" novalidate>
		<div class="subscribe__title"><?=Lang::t('home.tip.subscribeTitle')?></div>
		<div class="subscribe__content">
			<ul class="subscribe__special list-unstyled">
				<li><?=Lang::t('home.tip.followInsta')?> <a href="<?=CHtml::encode(Yii::app()->params->settings['instagram'])?>" target="_blank" rel="nofollow">@fresh.black.okay</a></li>
				<li><?=Lang::t('home.tip.followFacebook')?> <a href="<?=CHtml::encode(Yii::app()->params->settings['facebook'])?>" target="_blank" rel="nofollow">fresh.black.okay</a></li>
				<li><?=Lang::t('home.tip.subscribe')?></li>
			</ul>
			<div class="subscribe__group">
				<input type="email" class="subscribe__input" name="subscribe[email]" placeholder="Email" required>
				<button class="subscribe__btn">&gt;</button>
			</div>
		</div>
	</form>
	<!-- /.subscribe -->
</main>