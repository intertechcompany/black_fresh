<?php
/* @var $this SiteController */
$assetsUrl = Yii::app()->assetManager->getBaseUrl();
$staticUrl = $assetsUrl . '/static/' . Yii::app()->params->settings['rev'];

$productPhoto = json_decode($product['product_photo']);

$lang = Yii::app()->language;

if ($sub) {
    switch ($lang) {
        case 'uk': {
            $deliveryTitle = $deliveryFreq['title_uk'];
            break;
        }
        case 'ru': {
            $deliveryTitle = $deliveryFreq['title_ru'];
            break;
        }
        case 'en': {
            $deliveryTitle = $deliveryFreq['title_en'];
            break;
        }
    }
}

?>

<style>

    @media (max-width: 600px) {
        .social {
            flex-direction: column;
            flex-wrap: wrap;
        }
    }

    .social {
        margin-top: 50px;
        display: flex;
        align-content: center;
        justify-content: center;
        align-items: flex-start;
    }

    .socials-div-enter-facebook {
        background-color: #3D5D9C;
        color: #fff;
        text-align: center;
        padding: 16px 39px 16px 39px;
        border-radius: 100px;
        font-size: 15px;
        margin-bottom: 10px;
        width: 250px;
    }
    .socials-div-enter-google {
        background-color: #DC4D28;
        color: #fff;
        text-align: center;
        padding: 16px 39px 16px 39px;
        border-radius: 100px;
        font-size: 15px;
        margin-bottom: 10px;
        width: 250px;
    }
    .socials-div-enter-google a {
        color: #fff;
        text-decoration: none;
        text-align: center;
    }
    .socials-div-enter-facebook a {
        color: #fff;
        text-decoration: none;
        text-align: center;
    }

    .change-input {
        border: 1px solid #000;
        min-width: 100px;
        width: 100px;
        height: 32px;
        padding: 0 10px;
        text-align: center;
        border-radius: 16px !important;

        font-family: inherit;
        font-size: 100%;
        line-height: 1.15;
        margin-right: 10px;
    }

    .title {
        font-family: "Apercu Pro", serif;
        font-style: normal;
        font-weight: normal;
        word-wrap: break-word;
        font-size: 34px;
        /*line-height: 50px;*/
        text-transform: uppercase;
    }

    .title-modal {
        font-family: "Apercu Pro", serif;
        font-style: normal;
        font-weight: normal;
        word-wrap: break-word;
    }

    .title.product-title {
        margin-top: 1px;
        word-break: break-word;
        max-width: 400px;
    }

    .custom-title {
        font-family: "Apercu Pro", serif;
        font-style: normal;
        font-weight: normal;
        word-wrap: break-word;
        font-size: 18px;
        text-transform: uppercase;
    }

    .product-img {
        width: 335px;
        height: 407px;
        /*margin-top: -378px;*/
        border: 1px solid var(--black-color);
    }

    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    /* Modal Content/Box */
    .modal-content {
        background-color: #fefefe;
        margin: 15% auto; /* 15% from the top and centered */
        padding: 20px;
        border: 1px solid #888;
        width: 700px; /* Could be more or less, depending on screen size */
    }

    /* The Close Button */
    .close {
        color: black;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: black;
        text-decoration: none;
        cursor: pointer;
    }

    /* modal content */

    .modal-value:hover {
        border: 1px solid #0098FF;
        cursor: pointer;
    }

    .modal-value {
        border: 1px solid #000000;
        margin-top: 20px;
        display: inline-block;
        height: 60px;
        width: 320px;
    }

    .modal-p {
        margin-left: 10px;
    }

    .modal-span {
        margin-left: 10px;
    }

    .check {
        display: inline-block;
        width: 10px;
        height: 20px;
        margin: 5px 25px 3px;
        border: solid #0098FF;
        border-width: 0 3px 3px 0;
        transform: rotate( 45deg);
        float: right;
    }

    .btn-white {
        width: 250px;
        background-color: white;
        color: black;
        border: 1px solid black;
    }
    .link-blue {
        color: #0098FF;
        text-decoration: none;
        padding-left: 0;
    }
    .text-al-div {
        width: 250px;
        margin: 0 auto;
        text-align: left;
        margin-top: 8px;
    }
    .account-menu-box {
        margin-top: 30px;
        float: left;
        display: inline-block;
    }
    h1 {
        margin-top: 25px;
        margin-bottom: 25px;
    }
    ul {
        list-style: none;
        padding-left: 0;
        padding-right: 40px;
    }
    .personal-cabinet-title {
        font-size: 18px;
        line-height: 22px;
        margin-bottom: 20px;
    }
    .box-content li {
        margin-bottom: 16px;
        font-size: 18px;
        line-height: 22px;
    }
    .box-content li a {
        text-decoration: none;
    }
    .subscription-div {
        display: inline-block;
        margin-left: 45px;
        margin-top: 15px;
        width: 855px;
    }
    .next-sub {
        font-size: 20px;
        line-height: 22px;
        margin-top: 30px;
        margin-bottom: 10px;
    }
    .date-next-sub {
        font-size: 15px;
        line-height: 22px;
        margin-bottom: 10px;
    }


    .month {
        border: 1.02632px solid rgba(0, 0, 0, 0.3);
        background: #ffffff;
        width: 287.37px;
        box-sizing: border-box;
        padding: 12px;

        display: flex;
        flex-direction: column;
    }

    .calendar-title {
        text-align: center;
        padding: 5px;
    }

    .calendar {
        display: flex;
        justify-content: center;
        align-items: center;
        flex-wrap: wrap;
        height: 100%;
    }

    .day-button {
        border: 1.02632px solid #b3b3b3;
        box-sizing: border-box;
        border-radius: 2.05263px;
        width: 30.79px;
        height: 30.79px;
        margin: 3px;
        display: flex;
        justify-content: center;
    }

    .day-button span {
        align-self: center;
    }

    .day-title {
        box-sizing: border-box;
        border-radius: 2.05263px;
        width: 30.79px;
        height: 30.79px;
        margin: 3px;
        display: flex;
        justify-content: center;
    }
    .day-title span {
        align-self: center;
    }
    .product__radio span {
        height: 27px;
        font-size: 13px;
    }
    .cc-3 {
        column-count: <?php if (!empty($rndProducts)) {echo count($rndProducts);} else { echo '3';} ?>
    }
    .product-also-h4  {
        margin-bottom: 30px;
        height: 35px;
    }
    .right-after-img {
        display: inline-block; 
        margin-left: 60px;
     }
    #modal-btn-pause, #modal-btn-remove {
        width: 300px;
        background-color: white;
        color: black;
        border: 1px solid black;
     }
    .div-with-btn {
        margin-top: 50px;
        text-align: center;
        margin-bottom: -70px;
    }
    .hr-between {
        border-bottom: 2px solid black; 
        width: 100%; 
        margin-top: 50px;
     }
     .btn-without-sub {
        width: 300px;
     }
     
    @media (max-width: 1200px) {
        .subscription-div {
            margin-left: 0px;
        }
    }

    /*@media (min-width: 1200px) {
        .header__menu-item {
            margin-left: 12px;
        }
    }*/


    @media (max-width: 1000px) {
        .title {
            font-size: 23px;
            margin-bottom: 23px;
            margin-top: 15px;
        }
        .product-img {
            width: 290px;
            height: 300px;
            margin-top: -300px;
        }
        .title.product-title {
            margin-bottom: 35px;
            max-width: 300px;
        }
        .title.like-coffee {
            margin-bottom: 35px;
        }
        .like-dislike {
            height: 35px !important;
        }
        .product-also-h4 {
            margin-bottom: 0;
        }
        .cc-3 {
            column-count: 1;
        }
        .subscription-div {
            width: 100%;
        }
        .right-after-img {
            margin-left: 30px;
        }
    }
    @media (max-width: 730px) {
       .product-img {
        margin-top: 0;
       } 
    }
    @media (max-width: 700px) {
        .btn-without-sub {
            width: 270px
        }
        .title {
            margin-bottom: 10px;
        }
        .product-img {
            width: 370px;
            height: auto;
            margin-top: 0;
        }
        .right-after-img{
            margin-left: 10px;
        }
        .title.product-title {
            font-size: 24px;
        }
        .right-after-img p {
            font-size: 13px;
            margin-top: -32px !important;
        }
        .title.like-coffee {
            font-size: 17px;
            margin-bottom: 42px;
        }
        .like-dislike {
            height: 28px !important;
        }
        .next-sub {
            font-size: 19px;
            margin-top: 17px;
            margin-bottom: 5px;
        }
        #modal-btn-pause, #modal-btn-remove {
        width: 260px;
        }
    }
    @media (max-width: 690px) {
          .btn-without-sub {
            width: 240px
        }
    }
  
    @media (max-width: 560px) {
        .product-img {
            margin-top: 0;
            width: 370px;
            height: auto;
        }
        .right-after-img p {
            font-size: 11px;
        }
        .right-after-img hr {
            width: 220px !important;
        }
        .link-blue {
            font-size: 13px;
        }
        .hr-between {
            margin-top: 90px;
        }
        #modal-btn-pause, #modal-btn-remove {
            margin-top: 15px;
        }
        .div-with-btn {
            margin-top: 0;
        }
        .btn-without-sub {
            width: 230px;
        }
    }
    @media (max-width: 500px) {
        .product-img {
            margin-top: 0px;
            margin-bottom: 10px;
        }
        #modal-btn-pause, #modal-btn-remove {
            margin-top: 15px;
        }
        .subscription-div {
            text-align: center;
        }
        .btn-without-sub {
            margin-bottom: 10px;
            width: 100%;
        }
    }
.product-card__radio input:checked+span, .product__radio input:checked+span {
    background-color: #fff;
    background-color: var(--white-color);
    color: #000;
    color: var(--black-color);
}

@media (max-width: 400px) {
    .product-img {
        width: 320px;
    }
}

</style>

<div class="wrap account-menu">
    <div class="content-divider"></div>
    <div class="account-menu-box" style="margin-top: 20px; float: left; display: inline-block; width: 220px;">
        <div class="box-heading personal-cabinet-title"><?= Lang::t('layout.title.account') ?></div>
        <div class="box-content">
            <ul>
                <li><a href="<?= $this->createUrl('site/accountedit') ?>"><?= Lang::t('layout.title.accountEdit') ?></a></li>
                <li><a href="<?= $this->createUrl('site/accountSubscription') ?>"><?= Lang::t('layout.title.editSubscription') ?></a></li>
                <li><a href="<?= $this->createUrl('site/accountPromo') ?>"><?= Lang::t('layout.title.promoCode') ?></a></li>
                <li><a href="<?= $this->createUrl('/user-feedback') ?>"><?= Lang::t('layout.title.feedback') ?></a></li>
                <li><a href="<?= $this->createUrl('site/logout') ?>"><?= Lang::t('layout.title.logout') ?></a></li>
            </ul>
        </div>
    </div>
    <div class="subscription-div" >
        <?php if ($sub) { ?>
            <?php if (!empty($_GET['sub']) && $_GET['sub'] == 'exist') { ?>
                <span style="font-size: 22px; width: auto;"><?= Lang::t('layout.title.subscription.exist') ?></span>
                <br>
                <span style="font-size: 14px; width: auto;"><?= Lang::t('layout.subtitle.subscription.exist') ?></span>
            <?php } ?>
            <h4 class="title" style="margin-top: 30px;"><?= Lang::t('layout.title.subscription') ?></h4>
            <div style="display: flex; flex-wrap: wrap;">
                <div style="display: inline-block">
                    <?php if (!empty($productPhoto->path)) { ?>
                        <img class="product-img" src="<?php echo $assetsUrl . "/product/". $product['product_id']. "/" . $productPhoto->path->original->{'1x'}; ?>" alt="">
                    <?php } ?>
                </div>
                <div class="right-after-img">
                    <h4 class="title product-title"><?php echo $subProduct['title'] ?></h4>
                    <p style="margin-top: -38px;"><?php echo str_replace("Об'єм:", "", $subProduct['variant_title'])  . " / " . $subProduct['price'] . " грн / " . $deliveryTitle ?></p>
                    <hr style="border-bottom: 2px solid black; width: 100%;">
                    <h5 class="title like-coffee" style="margin-top: -5px;"><?= Lang::t('layout.doyou.likecoffe') ?></h5>
                    <div style="margin-top: -35px;">
                        <div>
                            <?php if ($userMark == false) { ?>
                                <img class="like-dislike"  onclick="goodMark()" style="height: 40px;cursor: pointer;" src="/assets/quiz/subtract.svg" alt="">
                                <img  class="like-dislike" onclick="badMark()" style="height: 40px;cursor: pointer;" src="/assets/quiz/subtract-bad.svg" alt="">
                            <?php } else {
                                $lang = Yii::app()->language;
                                $title = '';
                                $answer = '';

                                if ($lang == 'uk') {
                                    $title = 'Ваша оцінка:';
                                    if ($userMark['is_good'] == 1) {
                                        $answer = 'Мені подобається';
                                    } else {
                                        $answer = 'Мені не подобається';
                                    }
                                } elseif ($lang == 'ru') {
                                    $title = 'Ваша оценка:';
                                    if ($userMark['is_good'] == 1) {
                                        $answer = 'Мне нравиться';
                                    } else {
                                        $answer = 'Мне не нравиться';
                                    }
                                } elseif ($lang == 'en') {
                                    $title = 'Your mark:';
                                    if ($userMark['is_good'] == 1) {
                                        $answer = 'I like';
                                    } else {
                                        $answer = 'I do not like';
                                    }
                                }
                                ?>
                                <p><?php echo $title . ' ' . $answer; ?></p>
                            <?php }?>
                        </div>
                        <div style="margin-top: 20px;">
                            <a class="link-blue" href="<?= $this->createUrl('site/quizEditPage') ?>"><?= Lang::t('layout.title.change.results') ?></a>
                        </div>
                    </div>
                    <hr style="border-bottom: 2px solid black; width: 100%;">
                    <div>
                        <h5 class="next-sub"><?= Lang::t('layout.next.subscription') ?></h5>
                        <div class="date-next-sub"> <span ><?php if ($next != false) {echo date('d.m.Y', strtotime($next['next_date']));} elseif ($sub['payment_status'] != 'Approved') { echo "Подписка не оформленна"; } else { echo "Обрабатывается"; } ?></span> </div>
                        <div class="date-next-sub"> <span ><?php if ($next != false) {echo Lang::t('layout.sub.product.qty') . ': ' . '<span id="qty-value">' . $subProduct['quantity'] . '</span> ' . Lang::t('layout.sub.product.qty.value'); } ?></span> </div>
                        <div class="div-with-blue-link">
                            <a class="link-blue" href="<?= $this->createUrl('site/product', ['alias' => $product['product_alias']]) ?>">
                                <?= Lang::t('layout.quickly.buy') ?>
                            </a>
                        </div>

                        <?php if ($next != false) { ?>
                            <a class="link-blue" href="#" onclick="getNextPayment()"><?= Lang::t('layout.change.schedule') ?></a>
                        <?php } ?>

                        <?php if ($next != false) { ?>
                            <a class="link-blue" id="change-qty-btn" style="cursor: pointer"><?= Lang::t('layout.change.qty') ?></a>
                        <?php } ?>

                        <div id="change-block" style="margin-top: 10px; display: none">
                            <input type="number" id="change-qty" class="change-input" value="">
                            <button
                                    id="change-qty-save"
                                    class="product__btn btn"
                                    style="height: 32px; line-height: 1; width: 140px; min-width: 140px;"
                                    data-url="<?= $this->createUrl('site/changeSubQty') ?>"
                            >
                                <?= Lang::t('layout.change.qty.save') ?>
                            </button>
                        </div>

                        <div id="change-sub-message" class="date-next-sub" style="display: none"></div>
                    </div>
                </div>
            </div>
            <div class="social">
                <?php if (isset($user['fb_login']) && $user['fb_login'] !== "1") { ?>
                    <div class="socials-div-enter-facebook"><a href="<?=$this->createUrl('auth/fbLink')?>"> <?= Lang::t('login.social.facebook.connect') ?></a></div>
                <?php } ?>
                <?php if (isset($user['google_login']) && $user['google_login'] !== "1") { ?>
                    <div class="socials-div-enter-google"><a href="<?=$this->createUrl('auth/googleLink')?>"><?= Lang::t('login.social.google.connect') ?></a></div>
                <?php } ?>
            </div>
            <?php if ((isset($user['fb_login']) && $user['fb_login'] !== "1") || (isset($user['google_login']) && $user['google_login'] !== "1")) { ?>
                <p><?= Lang::t('social.connect.message') ?></p>
            <?php } ?>
        <?php } else { ?>
            <h4 class="title"><?= Lang::t('layout.no.subscription.title') ?></h4>
            <p><?= Lang::t('layout.no.subscription.text') ?></p>
            <div style="margin-top: 50px; text-align: left;">
                <a href="<?= $this->createUrl('site/quiz') ?>" class="product__btn btn btn-without-sub" ><?= Lang::t('layout.subscription.quiz') ?></a>
                <a href="<?= $this->createUrl('site/category', ['alias' => 'coffee']) ?>" class="product__btn btn btn-without-sub"><?= Lang::t('layout.subscription.catalog') ?></a>
            </div>
            <div class="social">
                <?php if (isset($user['fb_login']) && $user['fb_login'] !== "1") { ?>
                    <div class="socials-div-enter-facebook"><a href="<?=$this->createUrl('auth/fbLink')?>"> <?= Lang::t('login.social.facebook.connect') ?></a></div>
                <?php } ?>
                <?php if (isset($user['google_login']) && $user['google_login'] !== "1") { ?>
                    <div class="socials-div-enter-google"><a href="<?=$this->createUrl('auth/googleLink')?>"><?= Lang::t('login.social.google.connect') ?></a></div>
                <?php } ?>
            </div>
            <?php if ((isset($user['fb_login']) && $user['fb_login'] !== "1") || (isset($user['google_login']) && $user['google_login'] !== "1")) { ?>
                <p><?= Lang::t('social.connect.message') ?></p>
            <?php } ?>
        <?php } ?>
        <hr class="hr-between">
        <div style="text-align: center;">
            <?php if ($sub) { ?>
                <h5 class="custom-title"><?= Lang::t('layout.see.another') ?></h5>
            <?php } else { ?>
                <h5 class="custom-title"><?= Lang::t('layout.see.new') ?></h5>
            <?php } ?>
            <div class="cc-3">
                <?php if (!empty($rndProducts[0]) && !empty($rndProducts[0]['product'])) { ?>
                <div class="product-also" style="text-align: center; margin-top: -20px; height: 540px;">
                    <h4 class="product-also-h4" ><?= Lang::t('layout.see.another.product1') ?></h4>
                    <?php $this->renderPartial('productsListSub', array('products' => [$rndProducts[0]['product']])); ?>
                    <h4 style="font-size: 22px; line-height: 20px; max-height: 40px;"><?=CHtml::encode($rndProducts[0]['product']['product_title'])?></h4>
                    <div class="text-al-div">
                        <form action="<?=$this->createUrl('site/subCart')?>" method="post">
                            <div class="product__buy-content product__buy-content--radio" style="margin-top: -20px; margin-bottom: 20px;">
                                <div class="product-div-radio">
                                    <?php
                                    $in_stock = false;
                                    $property_title = '';
                                    $variants = [];

                                    $productVariants = $rndProducts[0]['product_variants'];

                                    foreach ($productVariants['variants'] as $variant) {
                                        $variant_id = $variant['variant_id'];
                                        $values = array_values($variant['values']);

                                        $value_id = !empty($values) ? $values[0] : 0;
                                        $value_title = '';

                                        foreach ($productVariants['properties'] as $property) {
                                            foreach ($property['values'] as $value_index => $value) {
                                                if ($value_index == $value_id) {
                                                    $property_title = $property['property_title'];
                                                    $value_title = $value['value_title'];
                                                }
                                            }
                                        }

                                        $variants[] = [
                                            'id' => $variant_id,
                                            'title' => $value_title,
                                            'price' => $variant['variant_price'],
                                            'price_old' => $variant['variant_price_old'],
                                            'in_stock' => ($variant['variant_instock'] == 'in_stock') ? true : false,
                                        ];
                                    }
                                    $count = 0;
                                    ?>
                                    <?php foreach ($variants as $variant) {  if ($count == 0) {?>
                                        <label for="value-<?=$variant['id']?>" class="product__radio <?php if (!$variant['in_stock']) { ?> product-card__radio--disabled<?php } ?>" style="width: 100px; height: 25px;">
                                            <input type="radio" style="visibility:hidden;" id="value-<?=$variant['id']?>" name="cart[variant_id]" value="<?=$variant['id']?>"<?php if ($variant['in_stock'] && !$in_stock) { $in_stock = true; ?> checked<?php } elseif (!$variant['in_stock']) { ?> disabled<?php } ?>>
                                            <span style="width: 138px; "><?=CHtml::encode($variant['title'])?>/<?php echo number_format($variant['price'], 0, '.', ' ');?>грн</span>
                                        </label>
                                    <?php } $count++;} ?>

                                </div>
                            </div>
                            <input type="hidden" name="cart[product_id]" value="<?php echo $rndProducts[0]['product']['product_id'];?>">
                            <input type="hidden" name="cart[qty]" value="1">
                            <input type="hidden" name="cart[option_id][9]" value="<?php echo $rndProducts[0]['options']['options'][0]['option_id']; ?>">
                            <input type="hidden" name="action" value="add">
                            <a href="<?php echo $this->createUrl('site/product', ['alias' => $rndProducts[0]['product']['product_alias']]);?>" class="link-blue" style="border: 0; cursor: pointer; background-color: white"> <?= Lang::t('layout.add.bucket') ?> +</a>
                        </form>
                    </div>
                    <div class="text-al-div">
                        <a class="link-blue" href="<?= $this->createUrl('site/product', ['alias' => $rndProducts[0]['product']['product_alias']]) ?>?action=subscription"><?= Lang::t('layout.subscribe') ?></a>
                    </div>
                </div>
                <?php } ?>
                <?php if (!empty($rndProducts[1]) && !empty($rndProducts[1]['product'])) { ?>
                <div class="" style="text-align: center; height: 540px;">
                    <h4 class="product-also-h4"><?= Lang::t('layout.see.another.product2') ?></h4>
                    <?php $this->renderPartial('productsListSub', array('products' => [$rndProducts[1]['product']])); ?>
                    <h4 style="font-size: 22px; line-height: 20px; max-height: 40px;"><?=CHtml::encode($rndProducts[1]['product']['product_title'])?></h4>
                    <div class="text-al-div">
                        <form action="<?=$this->createUrl('site/subCart')?>" method="post">
                            <div class="product__buy-content product__buy-content--radio" style="margin-top: -20px; margin-bottom: 20px;">
                                <div class="product-div-radio">
                                    <?php
                                    $in_stock = false;
                                    $property_title = '';
                                    $variants = [];

                                    $productVariants = $rndProducts[1]['product_variants'];

                                    foreach ($productVariants['variants'] as $variant) {
                                        $variant_id = $variant['variant_id'];
                                        $values = array_values($variant['values']);

                                        $value_id = !empty($values) ? $values[0] : 0;
                                        $value_title = '';

                                        foreach ($productVariants['properties'] as $property) {
                                            foreach ($property['values'] as $value_index => $value) {
                                                if ($value_index == $value_id) {
                                                    $property_title = $property['property_title'];
                                                    $value_title = $value['value_title'];
                                                }
                                            }
                                        }

                                        $variants[] = [
                                            'id' => $variant_id,
                                            'title' => $value_title,
                                            'price' => $variant['variant_price'],
                                            'price_old' => $variant['variant_price_old'],
                                            'in_stock' => ($variant['variant_instock'] == 'in_stock') ? true : false,
                                        ];
                                    }
                                    $count = 0;
                                    ?>
                                    <?php foreach ($variants as $variant) {  if ($count == 0) {?>
                                        <label for="value-<?=$variant['id']?>" class="product__radio <?php if (!$variant['in_stock']) { ?> product-card__radio--disabled<?php } ?>" style="width: 100px; height: 25px;">
                                            <input type="radio" style="visibility:hidden;" id="value-<?=$variant['id']?>" name="cart[variant_id]" value="<?=$variant['id']?>"<?php if ($variant['in_stock'] && !$in_stock) { $in_stock = true; ?> checked<?php } elseif (!$variant['in_stock']) { ?> disabled<?php } ?>>
                                            <span style="width: 138px; "><?=CHtml::encode($variant['title'])?>/<?php echo number_format($variant['price'], 0, '.', ' ');?>грн</span>
                                        </label>
                                    <?php } $count++;} ?>

                                </div>
                            </div>
                            <input type="hidden" name="cart[product_id]" value="<?php echo $rndProducts[1]['product']['product_id'];?>">
                            <input type="hidden" name="cart[qty]" value="1">
                            <input type="hidden" name="cart[option_id][9]" value="<?php echo $rndProducts[1]['options']['options'][0]['option_id']; ?>">
                            <input type="hidden" name="action" value="add">
                            <a href="<?php echo $this->createUrl('site/product', ['alias' => $rndProducts[1]['product']['product_alias']]);?>" class="link-blue" style="border: 0; cursor: pointer; background-color: white"> <?= Lang::t('layout.add.bucket') ?> +</a>
                        </form>
                    </div>
                    <div class="text-al-div"><a class="link-blue" href="<?= $this->createUrl('site/product', ['alias' => $rndProducts[1]['product']['product_alias']]) ?>?action=subscription"><?= Lang::t('layout.subscribe') ?></a></div>
                </div>
                <?php } ?>
                <?php if (!empty($rndProducts[2]) && !empty($rndProducts[2]['product'])) { ?>
                <div class="" style="text-align: center; height: 540px;">
                    <h4 class="product-also-h4"><?= Lang::t('layout.see.another.product3') ?></h4>
                    <?php $this->renderPartial('productsListSub', array('products' => [$rndProducts[2]['product']], 'new' => true)); ?>
                    <h4 style="font-size: 22px; line-height: 20px; max-height: 40px;"><?=CHtml::encode($rndProducts[2]['product']['product_title'])?></h4>
                    <div class="text-al-div">
                        <form action="<?=$this->createUrl('site/subCart')?>" method="post">
                            <div class="product__buy-content product__buy-content--radio" style="margin-top: -20px; margin-bottom: 20px;">
                                <div class="product-div-radio">
                                    <?php
                                    $in_stock = false;
                                    $property_title = '';
                                    $variants = [];

                                    $productVariants = $rndProducts[2]['product_variants'];

                                    foreach ($productVariants['variants'] as $variant) {
                                        $variant_id = $variant['variant_id'];
                                        $values = array_values($variant['values']);

                                        $value_id = !empty($values) ? $values[0] : 0;
                                        $value_title = '';

                                        foreach ($productVariants['properties'] as $property) {
                                            foreach ($property['values'] as $value_index => $value) {
                                                if ($value_index == $value_id) {
                                                    $property_title = $property['property_title'];
                                                    $value_title = $value['value_title'];
                                                }
                                            }
                                        }

                                        $variants[] = [
                                            'id' => $variant_id,
                                            'title' => $value_title,
                                            'price' => $variant['variant_price'],
                                            'price_old' => $variant['variant_price_old'],
                                            'in_stock' => ($variant['variant_instock'] == 'in_stock') ? true : false,
                                        ];
                                    }
                                    $count = 0;
                                    ?>
                                    <?php foreach ($variants as $variant) {  if ($count == 0) {?>
                                        <label for="value-<?=$variant['id']?>" class="product__radio <?php if (!$variant['in_stock']) { ?> product-card__radio--disabled<?php } ?>" style="width: 100px; height: 25px;">
                                            <input type="radio" style="visibility:hidden;" id="value-<?=$variant['id']?>" name="cart[variant_id]" value="<?=$variant['id']?>"<?php if ($variant['in_stock'] && !$in_stock) { $in_stock = true; ?> checked<?php } elseif (!$variant['in_stock']) { ?> disabled<?php } ?>>
                                            <span style="width: 138px; "><?=CHtml::encode($variant['title'])?>/<?php echo number_format($variant['price'], 0, '.', ' ');?>грн</span>
                                        </label>
                                    <?php } $count++;} ?>

                                </div>
                            </div>
                            <input type="hidden" name="cart[product_id]" value="<?php echo $rndProducts[2]['product']['product_id'];?>">
                            <input type="hidden" name="cart[qty]" value="1">
                            <input type="hidden" name="cart[option_id][9]" value="<?php echo $rndProducts[2]['options']['options'][0]['option_id']; ?>">
                            <input type="hidden" name="action" value="add">
                            <a href="<?php echo $this->createUrl('site/product', ['alias' => $rndProducts[2]['product']['product_alias']]);?>" class="link-blue" style="border: 0; cursor: pointer; background-color: white"> <?= Lang::t('layout.add.bucket') ?> +</a>
                        </form>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <?php if ($sub) { ?>
            <div class="div-with-btn">
                <button class="product__btn btn" id="modal-btn-pause"><?= Lang::t('layout.suspend.subscription') ?></button>
                <button class="product__btn btn" id="modal-btn-remove"><?= Lang::t('layout.stop.subscription') ?></button>
            </div>
            <hr style="border-bottom: 2px solid black; width: 100%; margin-top: 100px; margin-bottom: 250px;">
        <?php } ?>
    </div>
</div>

<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>
        <h2 class="title-modal" id="modal-title"></h2>
        <p id="modal-text"></p>
        <br>
        <div id="modal-body"></div>
    </div>

</div>

<?php if (count($_SESSION['cart']['items']) > 0) {?>
    <script>
        setTimeout(function () {
            if (!is_sm_screen.matches) {
                toggleScrollLock();
            } else if (cart.hasClass('cart--hidden')) {
                scroll_top = $(window).scrollTop();
            }

            cart.toggleClass('cart--hidden');
            c.toggleClass('is-cart-open');

            if (is_sm_screen.matches && cart.hasClass('cart--hidden')) {
                $(window).scrollTop(scroll_top);
            }
        }, 1000);
    </script>
<?php } ?>

<script>

    let pause = 0;
    let questions;
    let answers;
    let language = {};
    let questionCount;
    let currentAnswer = {};
    let quizNumber = 0;
    const results = [];
    let calendarData = {};

    window.onload = function () {
        const modal = document.getElementById("myModal");
        const btnPause = document.getElementById("modal-btn-pause");
        const btnRemove = document.getElementById("modal-btn-remove");
        const span = document.getElementsByClassName("close")[0];

        btnPause.onclick = function() {
            createModalPause();
            modal.style.display = "block";
        }

        btnRemove.onclick = function() {
            createModalRemove();
            modal.style.display = "block";
        }

        span.onclick = function() {
            modal.style.display = "none";
        }

        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }

        getQuestions();
        getAnswers();
        getCurrentLanguage();

        const changeBtn = document.getElementById('change-qty-btn');

        if (changeBtn) {
          changeBtn.addEventListener('click', function () {
            changeBlock();
          });
        }

        const changeBtnSave = document.getElementById('change-qty-save');

       if (changeBtnSave) {
         changeBtnSave.addEventListener('click', function () {
           changeSubQtySave(this);
         });
       }
    }

    function modalClose() {
        const modal = document.getElementById("myModal");
        const btnClose = document.getElementById("modal-btn-close");

        btnClose.onclick = function() {
            modal.style.display = "none";
        };
    }

    function setCheck(spanId, divId, value) {
        const span = document.getElementById(spanId);
        const div = document.getElementById(divId);

        span.style.display = 'block';
        div.classList.add('checked-answer');

        pause = value;
        removeChecked(span, div);
    }

    function removeChecked(checkedSpan, checkedDiv) {
        const allSpans = document.getElementsByTagName('span');
        const allDiv = document.getElementsByClassName('quiz-answer');

        for (let i = 0; i < allSpans.length; i++) {
            if (allSpans[i].id !== checkedSpan.id) {
                if (allSpans[i].classList.contains('check')) {
                    allSpans[i].style.display = 'none';
                }
            }
        }

        for (let i = 0; i < allDiv.length; i++) {
            if (allDiv[i].id !== checkedDiv.id) {
                allDiv[i].classList.remove('checked-answer');
            }
        }
    }

    async function pauseSub() {
        if (pause > 0) {
            let data = {};
            data.pause = pause;
            try {
                const response = await fetch('/subscription/pause', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                });

                const dataResponse = await response.json();
                if (dataResponse.message === 'ok') {
                    window.location.reload();
                }
            } catch (e) {
                console.log(e);
            }
        }
    }

    async function removeSub() {
        let data = {};

        data.text =  document.getElementById('modal-textarea').value;
        try {
            const response = await fetch('/subscription/remove', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            });

            const dataResponse = await response.json();

            if (dataResponse.message === 'ok') {
                window.location.reload();
            }
        } catch (e) {
            console.log(e);
        }
    }

    function clearModal() {
        const modalBody = document.getElementById('modal-body');

        modalBody.innerHTML = "";
    }

    function createModalRemove() {
        clearModal();

        document.getElementById('modal-text').textContent = '';

        const modalBody = document.getElementById('modal-body');
        const textArea = document.createElement('textarea');
        const btnClose = document.createElement('button');
        const btnSend = document.createElement('button');

        btnClose.className = 'product__btn btn btn-white';
        btnClose.id = 'modal-btn-close';
        btnClose.textContent = 'Закрити';

        btnSend.className = 'product__btn btn';
        btnSend.style.width = '250px';
        btnSend.style.marginLeft = '10px';
        btnSend.onclick = () => { removeSub(); }

        textArea.style.width = '100%';
        textArea.style.height = '200px';
        textArea.style.marginBottom = '20px';
        textArea.id = 'modal-textarea';

        if (language.lang === 'ru') {
            document.getElementById('modal-title').textContent = 'Отмена подписки';
            btnClose.textContent = 'Закрыть';
            btnSend.textContent = 'Отменить подписку';
            textArea.placeholder = 'Напишите причину';
        } else if (language.lang === 'uk') {
            document.getElementById('modal-title').textContent = 'Відміна підписки';
            btnClose.textContent = 'Закрити';
            btnSend.textContent = 'Відмінити підписку';
            textArea.placeholder = 'Напишіть причину';
        } else if (language.lang === 'en') {
            document.getElementById('modal-title').textContent = 'Unsubscribe';
            btnClose.textContent = 'Close';
            btnSend.textContent = 'Unsubscribe';
            textArea.placeholder = 'Write a reason';
        } else {
            console.log('language error');
        }

        modalBody.appendChild(textArea);
        modalBody.appendChild(document.createElement('br'));
        modalBody.appendChild(btnClose);
        modalBody.appendChild(btnSend);

        modalClose();
    }

    function createModalPause() {
        clearModal();

        const modalBody = document.getElementById('modal-body');
        const divColumn = document.createElement('div');
        const btnClose = document.createElement('button');
        const btnSend = document.createElement('button');

        divColumn.style.columnCount = 2;

        btnClose.className = 'product__btn btn btn-white';
        btnClose.id = 'modal-btn-close';

        btnSend.className = 'product__btn btn';
        btnSend.style.width = '250px';
        btnSend.style.marginLeft = '10px';
        btnSend.onclick = () => { pauseSub(); }

        if (language.lang === 'ru') {
            btnSend.textContent = 'Приостановить подписку';
            btnClose.textContent = 'Закрыть';

            document.getElementById('modal-title').textContent = 'Временное остановление подписки';
            document.getElementById('modal-text').textContent = 'Вы отрпавляетесь в отпуск? Слишком много кофе и нужно сделать перерыв?\n' +
                'Независимо от причины, вы можете легко приостановить вашу подписку.\n' +
                'Просто выберете период приостановления:';

        } else if (language.lang === 'uk') {
            btnSend.textContent = 'Призупинити підписку';
            btnClose.textContent = 'Закрити';

            document.getElementById('modal-title').textContent = 'Тимчасове зупинення підписки';
            document.getElementById('modal-text').textContent = 'Ви відправляєтесь у відпустку? Занадто багато кави і потрібно зробити перерву?\n' +
                'Незалежно від причини, ви можете легко призупинити вашу підписку.\n' +
                'Просто виберете період призупинення:';
        } else if (language.lang === 'en') {
            btnSend.textContent = 'Suspend subscription';
            btnClose.textContent = 'Close';

            document.getElementById('modal-title').textContent = 'Temporary suspension of subscription';
            document.getElementById('modal-text').textContent = 'Are you going on vacation? Too much coffee and need to take a break?\n' +
                'Whatever the reason, you can easily suspend your subscription.\n' +
                'Just select the pause period:';
        } else {
            console.log('language error');
        }

        for (let i = 1; i < 5; i++) {
            const modalValue = document.createElement('div');
            const p = document.createElement('p');
            const span = document.createElement('span');
            const spanCheck = document.createElement('span');

            modalValue.id = 'div-' + i;
            modalValue.className = 'modal-value';
            modalValue.onclick = () => { setCheck('span-' + i, 'div-' + i, i); }

            p.className = 'modal-p';

            span.className = 'modal-span';

            if (language.lang === 'ru') {
                if (i === 1) {
                    span.textContent = 'На ' + i + ' неделю';
                } else {
                    span.textContent = 'На ' + i + ' недели'
                }
            } else if (language.lang === 'uk') {
                if (i === 1) {
                    span.textContent = 'На ' + i + ' неділю';
                } else {
                    span.textContent = 'На ' + i + ' неділі'
                }
            } else if (language.lang === 'en') {
                if (i === 1) {
                    span.textContent = 'For ' + i + ' week';
                } else {
                    span.textContent = 'For ' + i + ' weeks'
                }
            } else {
                console.log('language error');
            }

            spanCheck.className = 'check';
            spanCheck.id = 'span-' + i;
            spanCheck.style.display = 'none';

            modalValue.appendChild(p);
            p.appendChild(span);
            p.appendChild(spanCheck);
            divColumn.appendChild(modalValue);
        }

        modalBody.appendChild(divColumn);
        modalBody.appendChild(document.createElement('br'));
        modalBody.appendChild(btnClose);
        modalBody.appendChild(btnSend);

        modalClose();
    }

    function goodMarkModal()
    {
        clearModal();

        const modal = document.getElementById("myModal");

        document.getElementById('modal-title').textContent = '';
        document.getElementById('modal-text').textContent = '';

        const modalBody = document.getElementById('modal-body');
        const title = document.createElement('h4');
        const text = document.createElement('p');

        if (language.lang === 'ru') {
            title.textContent = 'Спасибо! Ваш отзив принят!';
            text.textContent = 'Мы свяжемся с вами в ближайшее время';
        } else if (language.lang === 'uk') {
            title.textContent = 'Дякую! Ваш отзив прийнятий!';
            text.textContent = 'Ми зв\'яжемося з вами найближчим часом';
        } else if (language.lang === 'en') {
            title.textContent = 'Thank! Your feedback is accepted!';
            text.textContent = 'We will contact you shortly';
        } else {
            console.log('language error');
        }

        title.className = 'title-modal';
        title.style.fontSize = '25px';
        title.style.marginLeft = '150px';

        text.style.color = 'grey';
        text.style.marginLeft = '175px';
        text.style.marginTop = '-23px';

        modalBody.style.height = '150px';

        modalBody.appendChild(title);
        modalBody.appendChild(text);

        modal.style.display = 'block';
    }

    async function goodMark(data = null) {
        if (data) {
            const response = await fetch('/sub/mark', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({mark: 'bad', data: data})
            });
        } else  {
            const response = await fetch('/sub/mark', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({mark: 'good'})
            });
        }

        if (!data) {
            goodMarkModal();
        }

        setTimeout(function(){ window.location.reload(); }, 2000);
    }

    function badMark() {
        clearModal();

        const modal = document.getElementById("myModal");
        const modalBody = document.getElementById('modal-body');

        document.getElementById('modal-title').textContent = '';
        document.getElementById('modal-text').textContent = '';

        const title = document.createElement('h4');
        const text = document.createElement('p');

        if (language.lang === 'ru') {
            title.textContent = 'Дякую за оцінку!';
            text.textContent = 'Ответьте на несколько вопросов, которые помогут подобрать ваш идеальный кофе.';
        } else if (language.lang === 'uk') {
            title.textContent = 'Дякую за оцінку!';
            text.textContent = 'Дайте відповідь на кілька питань, які допоможуть підібрати вашу ідеальну каву';
        } else if (language.lang === 'en') {
            title.textContent = 'Thanks for rating!';
            text.textContent = 'Answer a few questions to help you choose your perfect coffee.';
        } else {
            console.log('language error');
        }

        title.className = 'title-modal';
        title.style.fontSize = '25px';
        title.style.marginTop = '-33px';

        text.style.color = 'grey';
        text.style.marginTop = '-23px';

        modalBody.appendChild(title);
        modalBody.appendChild(text);

        modal.style.display = 'block';
        createMark();
    }

    function createMark()
    {
        createQuestion(questions[quizNumber]);
        createAnswers(questions[quizNumber].id)
    }

    function createQuestion(data)
    {
        const modalBody = document.getElementById('modal-body');
        const question = document.createElement('h4');

        if (language.lang === 'ru') {
            question.textContent = data.title_ru;
        } else if (language.lang === 'uk') {
            question.textContent = data.title_uk;
        } else if (language.lang === 'en') {
            question.textContent = data.title_en;
        } else {
            console.log('language error');
        }

        question.className = 'title-modal';
        question.style.fontSize = '25px';

        modalBody.appendChild(question);
    }

    function createAnswers(questionId)
    {
        const modalBody = document.getElementById('modal-body');

        answers.forEach(function (item) {
            if (item.question_id === questionId) {
                const radio = document.createElement('input');
                const label = document.createElement('label');
                const title = document.createElement('span');
                const text = document.createElement('p');

                radio.type = 'radio';
                radio.value = item.id;
                radio.dataset.quest = questionId;
                radio.name = 'radio';
                radio.onclick = () => { setAnswer(item) };

                if (language.lang === 'ru') {
                    title.textContent = item.title_ru;
                    text.textContent = item.text_ru;
                } else if (language.lang === 'uk') {
                    title.textContent = item.title_uk;
                    text.textContent = item.text_ua;
                } else if (language.lang === 'en') {
                    title.textContent = item.title_en;
                    text.textContent = item.text_en;
                } else {
                    console.log('language error');
                }

                title.style.fontSize = '22px';
                title.style.marginLeft = '20px';

                title.appendChild(text);

                text.style.marginLeft = '34px';
                text.style.fontSize = '15px';

                label.appendChild(radio);
                label.appendChild(title);

                modalBody.appendChild(label);
            }
        })
        const textArea = document.createElement('textarea');
        const title = document.createElement('h4');

        if (language.lang === 'ru') {
            title.textContent = 'Ваш вариант ответа';
            textArea.placeholder = 'Напишите причину';
        } else if (language.lang === 'uk') {
            title.textContent = 'Ваш варіант відповіді';
            textArea.placeholder = 'Напишіть причину';
        } else if (language.lang === 'en') {
            title.textContent = 'Your answer';
            textArea.placeholder = 'Write a reason';
        } else {
            console.log('language error');
        }

        title.style.fontSize = '20px';

        textArea.style.width = '100%';
        textArea.style.height = '200px';
        textArea.style.marginBottom = '20px';
        textArea.id = 'modal-textarea';

        modalBody.appendChild(title);
        modalBody.appendChild(textArea);

        createButtons();
    }

    function next() {
        // if (Object.entries(currentAnswer).length !== 0) {
        //
        // }
        const textArea = document.getElementById('modal-textarea');

        results.push({
            result: currentAnswer.id,
            question: currentAnswer.question_id,
            text: textArea.value,
        });

        if (quizNumber + 1 < questions.length) {
            quizNumber++;
            badMark();
        } else {
            modalPresent();
        }
    }

    function modalPresent() {
        clearModal();

        const modalBody = document.getElementById('modal-body');

        document.getElementById('modal-title').textContent = '';
        document.getElementById('modal-text').textContent = '';

        const title = document.createElement('h4');
        const text = document.createElement('p');

        const btnClose = document.createElement('button');
        const btnSend = document.createElement('button');

        if (language.lang === 'ru') {
            title.textContent = 'Можем ли мы вам перезвонить и подарить подарок?';
            text.textContent = '';
            btnClose.textContent = 'Назад';
            btnSend.textContent = 'Да';
        } else if (language.lang === 'uk') {
            title.textContent = 'Чи можемо ми вам передзвонити і подарувати подарунок?';
            text.textContent = '';
            btnClose.textContent = 'Назад';
            btnSend.textContent = 'Так';
        } else if (language.lang === 'en') {
            title.textContent = 'Can we call you back and give a gift?';
            text.textContent = '';
            btnClose.textContent = 'Back';
            btnSend.textContent = 'Yes';
        } else {
            console.log('language error');
        }


        title.className = 'title-modal';
        title.style.fontSize = '35px';
        title.style.marginTop = '-33px';

        text.style.color = 'grey';
        text.style.marginTop = '-23px';

        modalBody.appendChild(title);
        modalBody.appendChild(text);

        btnClose.className = 'product__btn btn btn-white';
        btnClose.onclick = () => { back(); };
        btnClose.style.marginTop = '20px';

        btnSend.className = 'product__btn btn';
        btnSend.style.width = '250px';
        btnSend.style.marginLeft = '10px';
        btnSend.onclick = () => { goodMark(results); };
        btnSend.style.marginTop = '20px';

        modalBody.appendChild(btnClose);
        modalBody.appendChild(btnSend);
    }

    function back() {
        if (quizNumber !== 0) {
            quizNumber--;
            results.splice(quizNumber, 1);
            badMark();
        } else {
            const modal = document.getElementById("myModal");
            modal.style.display = 'none';
        }
    }

    function setAnswer(answer) {
        currentAnswer = answer;
    }

    function createButtons() {
        const quizBody = document.getElementById('modal-body');
        const btnClose = document.createElement('button');
        const btnSend = document.createElement('button');

        btnClose.className = 'product__btn btn btn-white';
        btnClose.onclick = () => { back(); };

        btnSend.className = 'product__btn btn';
        btnSend.style.width = '250px';
        btnSend.style.marginLeft = '10px';
        btnSend.onclick = () => { next(); };

        if (quizNumber !== 0) {
            if (language.lang === 'ru') {
                btnClose.textContent = 'Назад';
                btnSend.textContent = 'Дальше';
            } else if (language.lang === 'uk') {
                btnClose.textContent = 'Назад';
                btnSend.textContent = 'Далі';
            } else if (language.lang === 'en') {
                btnClose.textContent = 'Back';
                btnSend.textContent = 'Next';
            } else {
                console.log('language error');
            }
        } else {
            if (language.lang === 'ru') {
                btnClose.textContent = 'Отмена';
                btnSend.textContent = 'Дальше';
            } else if (language.lang === 'uk') {
                btnClose.textContent = 'Відміна';
                btnSend.textContent = 'Далі';
            } else if (language.lang === 'en') {
                btnClose.textContent = 'Cancel';
                btnSend.textContent = 'Next';
            } else {
                console.log('language error');
            }
        }

        quizBody.appendChild(btnClose);
        quizBody.appendChild(btnSend);
    }

    async function getCurrentLanguage()
    {
        let currentUrl = window.location.href;
        let url = new URL(currentUrl);
        let requestUrl = '';

        if (url.pathname.includes('en')) {
            requestUrl = new URL('/en/language/current', url);
        } else if(url.pathname.includes('ru')) {
            requestUrl = new URL('/ru/language/current', url);
        } else  {
            requestUrl = new URL('/language/current', url);
        }

        try {
            const response = await fetch(requestUrl, {method: 'GET'});
            language = await response.json();
        } catch (e) {
            console.log(e);
        }
    }

    async function getQuestions() {
        try {
            const response = await fetch('/sub/questions', {method: 'GET'});
            questions = await response.json();
            questionCount = questions.length;
        } catch (e) {
            console.log(e);
        }
    }

    async function getAnswers() {
        try {
            const response = await fetch('/sub/answers', {method: 'GET'});
            answers = await response.json();
        } catch (e) {
            console.log(e);
        }
    }

    async function getNextPayment() {
        try {
            const response = await fetch('/next-payment', {method: 'GET'});
            let data = await response.json();

            if (data.message === 'ok') {
                let next = new Date(data.next);
                createChangeModal(next.getDate(), next.getMonth() + 1);
            } else  {
                console.log(data.message);
            }

        } catch (e) {
            console.log(e);
        }
    }

    async function changeSub() {
        try {
            const response = await fetch('/subscription/change', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(calendarData)
            });

            const dataResponse = await response.json();

            if (dataResponse.message === 'ok') {
                window.location.reload();
            }
        } catch (e) {
            console.log(e);
        }
    }

    function createChangeModal(day, month) {
        clearModal();
        const modal = document.getElementById("myModal");

        const modalBody = document.getElementById('modal-body');
        const divColumn = document.createElement('div');
        const btnClose = document.createElement('button');
        const btnSend = document.createElement('button');

        divColumn.style.columnCount = 2;

        btnClose.className = 'product__btn btn btn-white';
        btnClose.id = 'modal-btn-close';
        btnClose.style.marginTop = '20px';
        btnClose.style.marginLeft = '22px';

        btnSend.className = 'product__btn btn';
        btnSend.style.width = '250px';
        btnSend.style.marginLeft = '22px';
        btnSend.onclick = () => { changeSub(); }
        btnSend.style.marginTop = '20px';

        if (language.lang === 'ru') {
            btnSend.textContent = 'Изменить';
            btnClose.textContent = 'Закрыть';

            document.getElementById('modal-title').textContent = 'Смена графика поставок';
            document.getElementById('modal-text').textContent = 'Выберите удобный для вас график поставок, который подойдет к вашему расписанию';

        } else if (language.lang === 'uk') {
            btnSend.textContent = 'Змінити';
            btnClose.textContent = 'Закрити';

            document.getElementById('modal-title').textContent = 'Зміна графіку поставок';
            document.getElementById('modal-text').textContent = 'Оберіть зручний для вас графік поставок, який підійде до вашого розкладу';
        } else if (language.lang === 'en') {
            btnSend.textContent = 'Change';
            btnClose.textContent = 'Close';

            document.getElementById('modal-title').textContent = 'Change delivery days';
            document.getElementById('modal-text').textContent = 'Choose a convenient delivery schedule that suits your schedule';
        } else {
            console.log('language error');
        }

        calendar(divColumn, day, month);

        modalBody.appendChild(divColumn);
        modalBody.appendChild(btnClose);
        modalBody.appendChild(btnSend);

        modal.style.display = 'block';

        modalClose();
    }

    function getMissingAmount(cell, monthDays) {
        return cell - monthDays;
    }

    function calendar(divColumn, dayPayment, monthPayment) {
        const cell = 42;
        let nextDayPayment;
        const today = new Date();
        const currentJstMonth = today.getMonth();
        const currentMonth = today.getMonth() + 1;
        const currentYear = today.getFullYear();

        const nextJsMonth = currentJstMonth + 1;
        const nextMonth = currentMonth + 1;

        calendarData.day = dayPayment;
        calendarData.month = monthPayment;

        // console.log('----------------------------------');
        // console.log('Current Month: ' + currentMonth + ' Current Js Month: ' + currentJstMonth);
        // console.log('Next Month: ' + nextMonth + ' Next Js Month: ' + nextJsMonth);
        // console.log('----------------------------------');
        // console.log('Pay Day: ' + dayPayment + ' Pay Month: ' + monthPayment);
        // console.log('----------------------------------');

        nextDayPayment = renderCalendar(today, currentJstMonth, currentYear, dayPayment, monthPayment, currentMonth, getMissingAmount(cell, daysInMonth(currentJstMonth, currentYear)), divColumn, true);

        if (nextMonth === monthPayment) {
            renderCalendar(today, nextJsMonth, currentYear, nextDayPayment, monthPayment, nextMonth, getMissingAmount(cell, daysInMonth(nextJsMonth, currentYear)), divColumn, true);
        } else {
            renderCalendar(today, nextJsMonth, currentYear, nextDayPayment, monthPayment + 1, nextMonth, getMissingAmount(cell, daysInMonth(nextJsMonth, currentYear)), divColumn);
        }

    }
    
    function renderCalendar(today, currentJstMonth, currentYear, dayPayment, monthPayment, currentMonth, missingDaysInMonth, divColumn, first = false) {
        let firstDay = (new Date(currentYear, currentJstMonth)).getDay();
        const currentDaysInMonth = daysInMonth(currentJstMonth, currentYear);

        let missingCount = 0;
        let date = 1;
        let daysName = [];
        let monthName = [];

        dayPayment = parseInt(dayPayment);

        if (language.lang === 'ru') {
            daysName = ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ'];
        } else if (language.lang === 'uk') {
            daysName = ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'НД'];
            monthName = ['Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень'];
        } else if (language.lang === 'en') {
            daysName = ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ'];
        }

        const divMonth = document.createElement('div');
        const divTitle = document.createElement('div');
        const divCalendar = document.createElement('div');

        divCalendar.className = 'calendar';
        divTitle.className = 'calendar-title';
        divMonth.className = 'month';
        divMonth.style.marginLeft = '25px';

        for (let i = 0; i < daysName.length; i++) {
            const dayTitle = document.createElement('div');
            const span = document.createElement('span');
            span.textContent = daysName[i];
            dayTitle.className = 'day-title';

            dayTitle.appendChild(span);
            divCalendar.appendChild(dayTitle);
        }

        if (dayPayment > 31 || dayPayment > currentDaysInMonth) {
            dayPayment -= 31;
        }

        for (let i = 0; i < 6; i++) {
            for (let j = 1; j < 7; j++) {
                const dayButton = document.createElement('div');
                const span = document.createElement('span');

                dayButton.className = 'day-button';

                if (i === 0 && j < firstDay) {
                    span.textContent = '';
                    missingCount++;
                } else if (date > daysInMonth(currentJstMonth, currentYear)) {
                    break;
                } else {
                    span.textContent = date.toString();

                    if (date === today.getDate() && currentYear === today.getFullYear() && currentJstMonth === today.getMonth()) {
                        if (date === dayPayment) {
                            dayButton.style.backgroundColor = '#59BEFC';
                            dayPayment += 7;
                        } else {
                            dayButton.style.backgroundColor = 'green';
                        }
                    } else if (date === dayPayment && currentYear === today.getFullYear()  && monthPayment === currentMonth) {
                        if (first === true) {
                            dayButton.style.backgroundColor = '#59BEFC';
                            first = false;
                        } else {
                            dayButton.style.backgroundColor = 'rgba(89, 190, 252, 0.3)';
                        }
                        dayButton.style.cursor = 'pointer';
                        dayButton.setAttribute('date', date);
                        dayButton.onclick = () => {  createChangeModal(dayButton.getAttribute('date'), currentMonth); };

                        dayPayment += 7;
                    } else if (date < today.getDate() && currentYear === today.getFullYear() && currentJstMonth === today.getMonth()) {
                        dayButton.style.color = 'grey';
                    } else {
                        let checkDate = new Date();

                        checkDate.setFullYear(currentYear);
                        checkDate.setMonth(currentJstMonth);
                        checkDate.setDate(date);

                        if (checkDate.getDay() == 6 || checkDate.getDay() == 0) {
                            dayButton.style.color = 'grey';
                        } else {
                            dayButton.setAttribute('date', date);
                            dayButton.onclick = () => { createChangeModal(dayButton.getAttribute('date'), currentMonth); };
                            dayButton.style.cursor = 'pointer';
                        }
                    }
                    date++;
                }

                dayButton.appendChild(span);
                divCalendar.appendChild(dayButton);
            }
        }

        for (let i = 1; i <= missingDaysInMonth - missingCount; i++) {
            const dayButton = document.createElement('div');
            const span = document.createElement('span');

            dayButton.className = 'day-button';
            span.textContent = '';

            dayButton.appendChild(span);
            divCalendar.appendChild(dayButton);
        }

        divTitle.textContent = monthName[currentJstMonth];
        divMonth.appendChild(divTitle);
        divMonth.appendChild(divCalendar);
        divColumn.appendChild(divMonth);

        return dayPayment;
    }
    
    function daysInMonth(iMonth, iYear) {
        return 32 - new Date(iYear, iMonth, 32).getDate();
    }

    function changeBlock() {
      const changeBlock = document.getElementById('change-block');

      if (changeBlock) {
        if (changeBlock.style.display === 'none') {
          changeBlock.style.display = 'flex';
        } else {
          changeBlock.style.display = 'none';
        }
      }
    }

    async function changeSubQtySave(btn) {
      if (btn && btn.dataset.url !== '') {
        const response = await fetch(btn.dataset.url, {
          method: 'POST',
          body: JSON.stringify({value: document.getElementById('change-qty').value}),
          headers: {
            'Content-Type': 'application/json'
          }
        });

        const result = await response.json();

        if (result) {
          const subMessageBlock = document.getElementById('change-sub-message');
          subMessageBlock.style.display = 'block';
          subMessageBlock.textContent = result.message;

          if (result.value) {
            document.getElementById('qty-value').textContent = result.value;
          }

          setTimeout(function () {
            subMessageBlock.style.display = 'none';
            changeBlock();
          }, 3000);
        }
      }
    }
</script>