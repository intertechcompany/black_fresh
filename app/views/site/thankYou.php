<?php /* @var $this SiteController */
if (isset($_SESSION['thankYou'])) {
    $price = $_SESSION['thankYou'];
    $pids = $_SESSION['thankYouProducts'] ?? [];
} else {
    $price = $order['price'];
    $pids = $order['order_products'];
}
?>
<main class="thank-you wrap">
	<div class="thank-you__container">
		<?php if (isset($order_status)) { ?>
			<?php if ($order_status == 'paid') { ?>
			<h1 class="thank-you__title"><?=Lang::t('thankYou.tip.paymentSuccess')?></h1>
			<?php } elseif ($order_status == 'processing') { ?>
			<h1 class="thank-you__title"><?=Lang::t('thankYou.tip.paymentPending')?></h1>
			<?php } else { ?>
			<h1 class="thank-you__title"><?=Lang::t('thankYou.tip.paymentFailure')?></h1>
			<?php } ?>
		<?php } else { ?>
		<h1 class="thank-you__title"><?=Lang::t('thankYou.tip.newOrder')?></h1>
		<?php } ?>

		<div class="thank-you__tip">
			<?php if (isset($order_status) && $order_status == 'payment_error') { ?>
			<b><?=Lang::t('thankYou.tip.paymentSystemStatus')?> <?=CHtml::encode($payment_message)?></b><br><br>
			<?php } ?>
			<?=Lang::t('thankYou.tip.orderNumber')?> <b id="ty-order"><?=$thank_you['order_id']?></b><br><br>
			<span><?=Lang::t('thankYou.tip.price.message')?> <b><?php echo $price; ?></b><b> грн</b></span>
			<br><br>
			<?=str_replace(["\r", "\n"], ["", "<br>\n"], Lang::t('thankYou.tip.manager', ['{phone}' => preg_replace('#[^\d]#', '', CHtml::encode(Yii::app()->params->settings['phone'])), '{phone_formatted}' => CHtml::encode(Yii::app()->params->settings['phone'])]))?>
		</div>
		<div class="thank-you__back"><a href="<?=$this->createUrl('site/index')?>" class="btn"><?=Lang::t('thankYou.btn.backToHome')?></a></div>
	</div>

	<form id="subscribe" class="subscribe" action="<?=$this->createUrl('ajax/subscribe')?>" method="post" novalidate>
		<div class="subscribe__title"><?=Lang::t('home.tip.subscribeTitle')?></div>
		<div class="subscribe__content">
			<ul class="subscribe__special list-unstyled">
				<li><?=Lang::t('home.tip.followInsta')?> <a href="<?=CHtml::encode(Yii::app()->params->settings['instagram'])?>" target="_blank" rel="nofollow">@fresh.black.okay</a></li>
				<li><?=Lang::t('home.tip.followFacebook')?> <a href="<?=CHtml::encode(Yii::app()->params->settings['facebook'])?>" target="_blank" rel="nofollow">fresh.black.okay</a></li>
				<li><?=Lang::t('home.tip.subscribe')?></li>
			</ul>
			<div class="subscribe__group">
				<input type="email" class="subscribe__input" name="subscribe[email]" placeholder="Email" required>
				<button class="subscribe__btn">&gt;</button>
			</div>
		</div>
	</form>
	<!-- /.subscribe -->

    <p><?=@$_GET['utm_campaign']?></p>
</main>

<?php if(isset($pids)) { ?>
<script>
    const dataLayer = window.dataLayer || [];
    dataLayer.push({
        'event': 'purchase',
        'value': '<?php echo $price ?>',
        'campaign': '<?=@$_GET['utm_campaign']?>',
        'items':[
            <?php foreach($pids as $id) { ?>
            {
                'id': '<?php echo $id ?>',
                'google_business_vertical': 'retail'
            },
            <?php } ?>
        ]
    });
</script>
<?php } ?>
