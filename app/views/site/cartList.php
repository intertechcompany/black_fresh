<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<?php foreach ($cart as $cart_index => $cart_item) { ?>
<?php
	$product = $cart_item['data'];
	$variant = $cart_item['variant'];
	$options = $cart_item['options'];

	$frequency = DeliveryFrequency::model()->getDeliveryByFrequency($cart_item['frequency']);

	if ($frequency) {
        switch (Yii::app()->language) {
            case 'uk': {
                $frequencyTitle = $frequency['title_uk'];
                break;
            }
            case 'en': {
                $frequencyTitle = $frequency['title_en'];
                break;
            }
            case 'ru': {
                $frequencyTitle = $frequency['title_ru'];
                break;
            }
            default: {
                $frequencyTitle = $frequency['title_uk'];
                break;
            }
        }
	}

	$product_id = $product['product_id'];
	$product_url_params = array('alias' => $product['product_alias']);

	if (!empty($variant)) {
		$variant_id = $variant['variant_id'];
		$product_url_params['vid'] = $variant['variant_id'];
	} else {
		$variant_id = 0;
	}

	if (!empty($options)) {
		$option_id = json_encode(array_keys($options));
		$product_url_params['options'] = array();

		foreach ($options as $option) {
			$product_url_params['options'][] = $option['value_id'];
		}
	} else {
		$option_id = '';
	}

	$product_url = Yii::app()->createUrl('site/product', $product_url_params);

	if (!empty($variant['photo_path'])) {
		$product_image = json_decode($variant['photo_path'], true);
		$product_image_size = json_decode($variant['photo_size'], true);
		$product_image_size = $product_image_size['catalog'];
		$product_image_1x = $assetsUrl . '/product/' . $product_id . '/variant/' . $variant_id . '/' . $product_image['catalog'];
	} elseif (!empty($product['product_photo'])) {
		$product_image = json_decode($product['product_photo'], true);
		$product_image_size = $product_image['size']['catalog'];
		$product_image_1x = $assetsUrl . '/product/' . $product_id . '/' . $product_image['path']['catalog']['1x'];
		$product_image_2x = $assetsUrl . '/product/' . $product_id . '/' . $product_image['path']['catalog']['2x'];
	} else {
		$product_image_1x = '';
	}

	$product_sku = !empty($variant) ? $variant['variant_sku'] : $product['product_sku'];
	$price_type = !empty($variant) ? $variant['variant_price_type'] : $product['product_price_type'];
	$product_title = CHtml::encode($product['product_title']);

	$regular_price = !empty($variant) ? $variant['variant_price'] : $product['product_price'];

	if (!empty($options)) {
		foreach ($options as $option) {
			$regular_price += (float) $option['option_price'];
		}
	}

	$has_discount = false;

	$discount_price = 0; // Product::getDiscountPrice($regular_price, $product['category_id'], $product['brand_id']);
	$cart_price_qty = $cart_item['price'];

	if (!empty($discount_price['discount'])) {
		$has_discount = true;
	}

	// $discount_price['price'] = number_format((float) $discount_price['price'], 2, '.', '');
	$regular_price = number_format((float) $regular_price, 2, '.', '');

	$qty_in_stock = !empty($variant) ? $variant['variant_stock_qty'] : $product['product_stock_qty'];
	$cart_item_disabled = (Yii::app()->params->settings['stock'] != 'none' && !$qty_in_stock) ? true : false;

?>
<div class="cart-product<?php if ($cart_item_disabled) { ?> cart-product--disabled<?php } ?>">
	<div class="cart-product__img">
		<?php if (!empty($product_image_1x)) { ?>
		<a href="<?=$product_url?>">
			<picture>
				<?php /* <source type="image/webp" srcset="images/p2.webp, images/p2@2x.webp 2x"> */ ?>
				<source srcset="<?=$product_image_1x?>, <?=$product_image_2x?> 2x">
				<img src="<?=$product_image_1x?>" alt="<?=$product_title?>">
			</picture>
		</a>
		<?php } ?>
	</div>
	<div class="cart-product__content">
		<div class="cart-product__title"><?=$product_title?></div>
		<?php if (!empty($variant['values'])) { ?>
		<?php
			$variant_values = array();

			foreach ($variant['values'] as $value) {
				$variant_values[] = CHtml::encode($value['property_title'] . ': ' . $value['value_title']);
			}
		?>
		<div class="cart-product__params"><?=implode("<br>\n", $variant_values)?></div>
		<?php } ?>
		<?php if (!empty($options)) { ?>
		<?php
			$options_values = array();

			foreach ($options as $option) {
				$options_values[] = CHtml::encode($option['value_title']); // $option['property_title']
			}
		?>
		<div class="cart-product__params"><?=implode("<br>\n", $options_values)?></div>
		<?php } ?>
		<div class="cart-product__price">
			<div class="cart-product__row">
				<div class="cart-product__col cart-product__col--title"><?=Lang::t('cart.tip.qty')?></div>
				<div class="cart-product__col">
					<?php if (empty($is_checkout)) { ?>
					<input type="text" name="qty" class="cart-product__qty" value="<?=$cart_item['qty']?>" data-id="<?=$product_id?>" data-vid="<?=$variant_id?>" data-oid="<?=$option_id?>"<?php if ($cart_item_disabled) { ?> disabled<?php } ?>>
					<?php } else { ?>
					<?=$cart_item['qty']?>
					<?php } ?>
				</div>
			</div>
            <?php if ($frequency) { ?>
                <div class="cart-product__row">
                    <div class="cart-product__col cart-product__col--title"><?=Lang::t('cart.tip.delivery')?></div>
                    <div class="cart-product__col"><?=$frequencyTitle?></div>
                </div>
            <?php } ?>
			<div class="cart-product__row">
				<div class="cart-product__col cart-product__col--title"><?=Lang::t('cart.tip.price')?></div>
				<div class="cart-product__col"><?=number_format($cart_item['price'], 0, '.', ' ')?> <?=Lang::t('layout.tip.uah')?></div>
			</div>
		</div>
		<?php if (empty($is_checkout)) { ?>
		<button type="button" class="cart-product__remove" data-id="<?=$product_id?>" data-vid="<?=$variant_id?>" data-oid="<?=$option_id?>"><?=Lang::t('cart.btn.remove')?></button>
		<?php } ?>
	</div>
</div>
<?php } ?>

<?php if (isset($discountWeight) && !empty($discountWeight)) { ?>
    <script>
        if (document.getElementById('gram-discount-tip')) {
          if ("<?= $discountWeight['weight'] ?>" === "0") {
            document.getElementById('gram-discount-tip').textContent = '';
          } else {
            document.getElementById('gram-discount-tip').textContent = "<?= Lang::t('product.discount.weight', [
                'weight' => $discountWeight['weight'],
                'discount' => $discountWeight['discount']
            ]) ?>";
          }
        }
    </script>
<?php } ?>
