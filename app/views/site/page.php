<?php
/* @var $this SiteController */
$staticUrl = Yii::app()->assetManager->getBaseUrl() . '/static/' . Yii::app()->params->settings['rev'];
?>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<main class="page">
    <div class="wrap">
		<div class="page__wrap">
			<?php $this->widget('application.components.Breadcrumbs.Breadcrumbs', ['breadcrumbs' => $this->breadcrumbs]); ?>
			
			<h1 class="page__title"><?=CHtml::encode($page['page_title'])?></h1>
			
			<div class="page__description">
				<?=$page['page_description']?>
			</div>

            <?php if ($page['feedback_form'] == 1) { ?>
                <br>
                <form
                        id="feedback-form"
                        action="<?=$this->createUrl('feedback')?>"
                        class="course__form form"
                        method="post"
                >
                    <input type="hidden" id="recaptcha" name="feedback[google_recaptcha]" value="">
                    <div class="form__row">
                        <input type="hidden" name="feedback[page_alias]" value="<?php echo $page['page_alias'];?>">
                        <div class="form__column form__column--50">
                            <label for="registration-name" class="form__label"><?=Lang::t('feedback.label.name')?></label>
                            <input type="text" required id="registration-name" name="feedback[first_name]" class="form__input">
                        </div>
                        <div class="form__column form__column--50">
                            <label for="registration-surname" class="form__label"><?=Lang::t('feedback.label.surname')?></label>
                            <input type="text" required id="registration-surname" name="feedback[last_name]" class="form__input">
                        </div>
                    </div>
                    <div class="form__row">
                        <div class="form__column form__column--50">
                            <label for="registration-email" class="form__label"><?=Lang::t('feedback.label.email')?></label>
                            <input type="email" required id="registration-email" name="feedback[email]" class="form__input"/>
                        </div>
                        <div class="form__column form__column--50">
                            <label for="registration-phone" class="form__label"><?=Lang::t('feedback.label.phone')?></label>
                            <input type="tel" required id="registration-phone" name="feedback[phone]" class="form__input"/>
                        </div>
                    </div>
                    <div class="form__row">
                        <div class="form__column form__column--50">
                            <label for="registration-code" class="form__label"><?=Lang::t('feedback.label.message')?></label>
                            <input type="text" id="registration-code" name="feedback[comment]" class="form__input">
                        </div>
                    </div>
                    <div class="form__row">
                        <div class="form__column form__column--50">
                            <input type="submit" class="form__btn" id="feed-btn" value="<?=Lang::t('feedback.btn.send')?>">
                        </div>
                    </div>
                    <div class="g-recaptcha" style="padding-top: 10px" data-sitekey="6LenIpUeAAAAAK05zNYqDhdMWAwt5VrFCJI4N-dN"></div>
                </form>

                <script>
                  const btn = document.getElementById('feed-btn');

                  if (btn) {
                    btn.addEventListener('click', function (event) {
                      submitFeedForm(event);
                    })
                  }

                  function submitFeedForm(event) {
                    event.preventDefault();
                    var captcha = grecaptcha.getResponse();

                    if (!captcha.length) {
                      console.log('Google captcha submit error');
                    } else {
                      document.getElementById('recaptcha').value = captcha;
                      document.getElementById('feedback-form').submit();
                    }
                  }
                </script>
            <?php } ?>
		</div>
	</div>
</main>