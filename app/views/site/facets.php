<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<?php if ($sort != 'popular') { ?>
<input type="hidden" name="sort" value="<?=$sort?>">
<?php } ?>
<?php
	/*
	$price = $facets->getPriceFacet();

	if ($price['min'] != $price['max']) {
		if ($price['from'] == $price['min']) {
			$price['from'] = '';
		}

		if ($price['to'] == $price['max']) {
			$price['to'] = '';
		}
?>
<div class="f-block">
	<div class="fb-title"><a class="collapsed" href="#"><?=Lang::t('facets.tip.priceRange')?><i></i></a></div>
	<div class="fb-price hidden">
		<span>
			<label for="filter-from"><?=Lang::t('global.tip.from')?></label><input id="filter-from" type="text" name="filter[price][from]" placeholder="<?=$price['min']?>" value="<?=$price['from']?>">
		</span>
		<span>
			<label for="filter-to"><?=Lang::t('facets.tip.to')?></label><input id="filter-to" type="text" name="filter[price][to]" placeholder="<?=$price['max']?>" value="<?=$price['to']?>">
		</span>

		<div class="fb-range-slider">
			<input id="range-slider" type="hidden" name="price_hidden" data-min="<?=$price['min']?>" data-max="<?=$price['max']?>" data-from="<?=$price['from']?>" data-to="<?=$price['to']?>" disabled>
		</div>

		<div class="fb-apply hidden">
			<button class="btn"><?=Lang::t('facets.btn.apply')?></button>
		</div>
	</div>
</div>
<?php } */ ?>

<?php
	$properties = $facets->getPropertiesFacets();

	if (!empty($properties)) {
		foreach ($properties as $property_id => $property) {
?>
<?php if (!empty($property['values'])) { ?>
<div class="filter__item filter-dropdown">
	<?php 
		$total_selected = 0;

		foreach ($property['values'] as $value_id => $value) {
			if ($value['selected']) {
				$total_selected++;
			}
		}
	?>
	<a href="#" class="filter-dropdown__value filter-dropdown__value--counter"><?=CHtml::encode($property['property_title'])?><span><?=$total_selected ? '(' . $total_selected . ')' : ''?></span></a>
	<i class="filter-dropdown__arrow"></i>
	<ul class="filter-dropdown__list list-unstyled">
		<?php
			/* uasort($property['values'], function($a, $b) {
				return strnatcmp($a['value_title'], $b['value_title']);
			}); */
		?>
		<?php foreach ($property['values'] as $value_id => $value) { ?>
		<?php if ($value['total']) { ?>
		<li>
			<label for="filter-<?=$value_id?>" class="form__checkbox">
				<input id="filter-<?=$value_id?>" type="checkbox" name="filter[p][]" value="<?=$value_id?>" <?php if ($value['selected']) { ?> checked<?php } ?>>
				<span><?=mb_strtolower(CHtml::encode($value['value_title']), 'utf-8')?><?php /* &nbsp;<small>(<?=$value['total']?>)</small> */ ?></span>
			</label>
		</li>
		<?php } else { ?>
		<li>
			<label for="filter-<?=$value_id?>" class="form__checkbox form__checkbox--disabled">
				<input id="filter-<?=$value_id?>" type="checkbox" name="filter[p][]" value="<?=$value_id?>" disabled <?php if ($value['selected']) { ?> checked<?php } ?>>
				<span><?=mb_strtolower(CHtml::encode($value['value_title']), 'utf-8')?></span>
			</label>
		</li>
		<?php } ?>
		<?php } ?>
	</ul>
</div>
<?php } ?>
<?php /*
<div class="filter__group">
	<div class="filter__title"><a class="collapsed" href="#"><?=CHtml::encode($property['property_title'])?><i></i></a></div>
	<div class="filter__options hidden">
		<?php if (!empty($property['values'])) { ?>
		<?php
			uasort($property['values'], function($a, $b) {
				return strnatcmp($a['value_title'], $b['value_title']);
			});
		?>
		<ul class="filter__list<?php if ($property['property_size']) { ?> filter__list--size<?php } ?> list-unstyled">
			<?php foreach ($property['values'] as $value_id => $value) { ?>
			<?php if ($value['total']) { ?>
			<li><label for="option-<?=$value_id?>"><input id="option-<?=$value_id?>" type="checkbox" name="filter[p][]" value="<?=$value_id?>" <?php if ($value['selected']) { ?> checked<?php } ?>><span><?=CHtml::encode($value['value_title'])?>&nbsp;<small>(<?=$value['total']?>)</small></span></label></li>
			<?php } else { ?>
			<li><label class="disabled" for="option-<?=$value_id?>"><input id="option-<?=$value_id?>" type="checkbox" name="filter[p][]" value="<?=$value_id?>" disabled<?php if ($value['selected']) { ?> checked<?php } ?>><span><?=CHtml::encode($value['value_title'])?></span></label></li>
			<?php } ?>
			<?php } ?>
		</ul>
		<?php } else { ?>
		<p>Нет доступных опций</p>
		<?php } ?>
	</div>
</div> */ ?>
<?php } } ?>

<?php /*
	$length = $facets->getLengthFacet();

	if ($length['min'] != $length['max']) {
		if ($length['from'] == $length['min']) {
			$length['from'] = '';
		}

		if ($length['to'] == $length['max']) {
			$length['to'] = '';
		}
?>
<div class="f-block">
	<div class="fb-title"><a class="collapsed" href="#"><?=Lang::t('facets.tip.length')?><i></i></a></div>
	<div class="fb-price hidden">
		<span>
			<label for="filter-from-length"><?=Lang::t('global.tip.from')?></label><input id="filter-from-length" type="text" name="filter[length][from]" placeholder="<?=$length['min']?>" value="<?=$length['from']?>">
		</span>
		<span>
			<label for="filter-to-length"><?=Lang::t('facets.tip.to')?></label><input id="filter-to-length" type="text" name="filter[length][to]" placeholder="<?=$length['max']?>" value="<?=$length['to']?>">
		</span>

		<div class="fb-range-slider">
			<input id="range-slider-length" type="hidden" name="length_hidden" data-min="<?=$length['min']?>" data-max="<?=$length['max']?>" data-from="<?=$length['from']?>" data-to="<?=$length['to']?>" disabled>
		</div>

		<div class="fb-apply hidden">
			<button class="btn"><?=Lang::t('facets.btn.apply')?></button>
		</div>
	</div>
</div>
<?php } ?>

<?php
	$width = $facets->getWidthFacet();

	if ($width['min'] != $width['max']) {
		if ($width['from'] == $width['min']) {
			$width['from'] = '';
		}

		if ($width['to'] == $width['max']) {
			$width['to'] = '';
		}
?>
<div class="f-block">
	<div class="fb-title"><a class="collapsed" href="#"><?=Lang::t('facets.tip.width')?><i></i></a></div>
	<div class="fb-price hidden">
		<span>
			<label for="filter-from-width"><?=Lang::t('global.tip.from')?></label><input id="filter-from-width" type="text" name="filter[width][from]" placeholder="<?=$width['min']?>" value="<?=$width['from']?>">
		</span>
		<span>
			<label for="filter-to-width"><?=Lang::t('facets.tip.to')?></label><input id="filter-to-width" type="text" name="filter[width][to]" placeholder="<?=$width['max']?>" value="<?=$width['to']?>">
		</span>

		<div class="fb-range-slider">
			<input id="range-slider-width" type="hidden" name="width_hidden" data-min="<?=$width['min']?>" data-max="<?=$width['max']?>" data-from="<?=$width['from']?>" data-to="<?=$width['to']?>" disabled>
		</div>

		<div class="fb-apply hidden">
			<button class="btn"><?=Lang::t('facets.btn.apply')?></button>
		</div>
	</div>
</div>
<?php } */ ?>