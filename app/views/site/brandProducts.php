<?php
    /* @var $this SiteController */
    $assetsUrl = Yii::app()->assetManager->getBaseUrl();
    $staticUrl = Yii::app()->assetManager->getBaseUrl() . '/static/' . Yii::app()->params->settings['rev'];
?>
<div class="wrap">
    <div class="content-divider content-divider--top"></div>

    <!-- <div class="breadcrumbs">
        <a href="<?=Yii::app()->homeUrl?>">Главная</a> 
        / 
        <a href="<?=$this->createUrl('brands')?>">Бренды</a>
        /
        <?=CHtml::encode($brand['brand_name'])?>
    </div> -->

    <div class="category">
        <div class="category__side">
            <div class="category__side-block side-block">
                <div class="side-block__title">Бренды</div>
                <ul class="side-block__categories list-unstyled">
                    <?php /*
                    <li><a href="<?=$this->createUrl('newest')?>">Новинки</a></li>
                    <li><a href="<?=$this->createUrl('brands')?>">Бренды</a></li>
                    */ ?>
                    <?php foreach ($brands as $brand_item) { ?>
                    <li><a<?php if ($brand['brand_id'] == $brand_item['brand_id']) { ?> class="active"<?php } ?> href="<?=$this->createUrl('brand', ['alias' => $brand_item['brand_alias']])?>"><?=CHtml::encode($brand_item['brand_name'])?></a></li>
                    <?php } ?>
                </ul>
            </div>

            <?php /* <form id="filter" class="filter" action="<?=$this->createUrl('category', array('alias' => $category['category_alias']))?>" method="get">
			    <?php $this->renderPartial('facets', array('facets' => $facets, 'sort' => $sort)); ?>
            </form> */ ?>
        </div>
        <div class="category__content">
            <div class="category__head">
                <div class="category__main">
                    <h1 class="category__title"><?=CHtml::encode($brand['brand_name'])?></h1>
                    
                    <?php if (!empty($brand['brand_logo'])) { $brand_logo = json_decode($brand['brand_logo'], true); ?>
                    <div class="category__logo"><img src="<?=$assetsUrl . '/brand/' . $brand['brand_id'] . '/' . $brand_logo['original']['path']?>" alt="<?=CHtml::encode($brand['brand_name'])?>"></div>
                    <?php } ?>

                    <?php if (!empty($brand['brand_description'])) { ?>
                    <div class="category__description">
                        <?=$brand['brand_description']?>
                    </div>
                    <?php } ?>
                </div>
                <?php if (!empty($brand['brand_photo'])) { $brand_photo = json_decode($brand['brand_photo'], true); ?>
                <div class="category__photo">
                    <img src="<?=$assetsUrl . '/brand/' . $brand['brand_id'] . '/' . $brand_photo['small']['path']?>" alt="<?=CHtml::encode($brand['brand_name'])?>">
                </div>
                <?php } ?>
            </div>

            <?php if (!empty($products)) { ?>
			<?php
				$sort_params = array(
					'popular' => array(
						'title' => 'по новизне',
						'url' => null, // default value
					),
					'price-asc' => array(
						'title' => 'сначала дешевые',
						'url' => 'price-asc',
					),
					'price-desc' => array(
						'title' => 'сначала дорогие',
						'url' => 'price-desc',
					),
				);
			?>
			<ul class="category__sort">
				<li class="category__sort-title">Сортировать:</li>
				<?php foreach ($sort_params as $sort_index => $sort_param) { ?>
				<?php if ($sort_index == $sort) { ?>
				<li><span class="active"><?=CHtml::encode($sort_param['title'])?></span></li>
				<?php } else { ?>
				<?php 
					$sort_url_params = array('alias' => $brand['brand_alias']);

					if (!empty($sort_param['url'])) {
						$sort_url_params = array('alias' => $brand['brand_alias'], 'sort' => $sort_param['url']);
					}

					if (!empty($filters)) {
						$sort_url_params['filter'] = $filters;
					}

					$sort_url = $this->createUrl('brand', $sort_url_params);
				?>
				<li><a href="<?=$sort_url?>"><?=CHtml::encode($sort_param['title'])?></a></li>
				<?php } ?>
				<?php } ?>
            </ul>
            <div class="catalog">
                <?php $this->renderPartial('productsList', array('products' => $products)); ?>
            </div>
            <?php if (!$show_all && $pages->getPageCount() > 1) { ?>
            <div class="pagination">
                <?php
                    $this->widget('LinkPager', array(
                        'pages' => $pages,
                        'maxButtonCount' => 7,
                        'htmlOptions' => array(
                            'class' => 'list-inline',
                        ),
                    ));
                ?>
            </div>
            <div class="show-all">
                <a<?php if ($show_all) { ?> class="active"<?php } ?> href="<?=$this->createUrl('brand', array_merge(['show_all' => 1], $pages->params))?>">Показать все</a>
            </div>
            <?php } ?>
            <?php } else { ?>
            <p style="font-size: 14px">Товары не найдено.</p>
            <?php } ?>
        </div>
    </div>
</div>