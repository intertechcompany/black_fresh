<style >
    #af-number {
        width: 270px;
        height: 40px;
    }
    .width-field-number {
        width: 36%;
    }
    .width-field-date {
        width: 23%
    }
    .width-field-cvv {
        width: 12%
    }
    #af-cvv {
        width: 66px;
        height: 40px;
    }
    #af-date {
        width: 166px;
        height: 40px;
    }
    .width-field-checkbox {
        width: 100%;
        margin-bottom: 21px;
        margin-top: 21px;
    }


</style>
<div class="width-field-number"><label for="af-number"><?=Lang::t('account.card.label.card.number')?></label>
    <input id="af-number" type="text" name="card[number]" value="<?= isset($card['number']) ? $card['number'] : '' ?>"></div>

<div class="width-field-date"><label for="af-date"><?=Lang::t('account.card.label.date')?></label>
    <input id="af-date" type="date" name="card[date]" value="<?= isset($card['date']) ? $card['date'] : '' ?>"></div>

<div class="width-field-cvv"><label for="af-cvv"><?=Lang::t('account.card.label.cvv')?></label>
    <input id="af-cvv" type="password" name="card[cvv]" value="<?= isset($card['cvv']) ? $card['cvv'] : '' ?>"></div>

<div class="width-field-checkbox">

    <input type="hidden" name="card[is_main]" value="0">
    <input id="af-is_main" type="checkbox" name="card[is_main]" value="1"<?= isset($card['is_main']) && $card['is_main'] == 1 ? ' checked' : '' ?>>

    <input type="hidden" name="action" value="personal">
    <?php if (isset($card) && !empty($card['id'])) : ?>
        <input type="hidden" name="card[id]" value="<?= $card['id'] ?>">
    <?php endif; ?> <label for="af-is_main"><?=Lang::t('account.card.label.is.main')?></label> </div>
<div><button class="btn"><?=Lang::t('account.btn.create')?></button></div>
