<?php
/* @var $this SiteController */
$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>

<style>

    @media only screen and (max-width: 2200px) {

    }

    @media only screen and (max-width: 1900px) {

    }
    @media only screen and (max-width: 1600px) {

    }
    @media only screen and (max-width: 1199px) {

    }
    @media only screen and (max-width: 990px) {

    }
    @media only screen and (max-width: 767px) {

    }
    @media only screen and (max-width: 600px) {

    }

    @media only screen and (max-width: 500px) {

    }

    @media only screen and (max-width: 400px) {

    }

    .title {
        font-family: "Apercu Pro", serif;
        font-style: normal;
        font-weight: normal;
        word-wrap: break-word;
        /*line-height: 50px;*/
        text-transform: uppercase;
    }

    .title .title-page {
        font-size: 40px;
    }

    .title .quiz-question {
        font-size: 34px;
    }

    .question {
        width: 785px;
        cursor: pointer;
    }

    .quiz-body {
        display: inline-block;
        margin-left: 100px;
        margin-top: 20px;
        width: 68%;
    }

    .span-toggle {
        font-size: 34px;
        margin-top: -40px;
        margin-right: 20px;
        float: right;
    }

    .toggled {
        transform: rotate(180deg);
    }

    hr {
        border-bottom: 2px solid black;
        width: 100%;
        margin-top: 40px;
    }

    #quiz-edit-body a {
        color: #0098FF;
    }
</style>

<main class="home">
    <section class="banner" style="background-image: none;">
        <div class="wrap" id="wrap">
            <div style="margin-top: 10px;">
                <?php $this->widget('application.components.Breadcrumbs.Breadcrumbs', ['breadcrumbs' => $this->breadcrumbs]); ?>
            </div>

            <h1 class="page__title"><?= Lang::t('faq.title.main') ?></h1>

            <?php $this->renderPartial('faqMenu', ['faqGroups' => $faqGroups]); ?>

            <div id="quiz-edit-body" class="quiz-body">
                <?php
                    $iter = 0;
                    foreach ($faqGroups as $faqGroup) {

                ?>
                    <h1 class="title title-page" id="group-<?= $faqGroup['faq_id'] ?>" style="<?php if ($iter > 0) { echo 'margin-top: 150px;'; }?>"><?php echo $faqGroup['faq_title']; ?></h1>
                    <?php
                        foreach ($faqQuest as $quest) {
                        if ($quest['group_id'] == $faqGroup['faq_id']) {
                    ?>
                        <div class="question" onclick="toggle('div-<?= $iter ?>', 'span-<?= $iter ?>')">
                            <hr class="hr">
                            <h2><?php echo $quest['quest_title']; ?></h2>
                            <span class="span-toggle toggled" id="span-<?= $iter ?>">^</span>
                        </div>
                        <div class="question" id="div-<?= $iter ?>" style="display: none;">
                            <div style="margin-top: 30px;">
                                <?php echo $quest['quest_description']; ?>
                            </div>
                        </div>
                    <?php $iter++;} } ?>
                        <div class="question"><hr class="hr"></div>
                <?php } ?>
            </div>
        </div>
    </section>
</main>
<script>
    function toggle(divId, spanId) {
        const x = document.getElementById(divId);
        const toggle = document.getElementById(spanId);

        console.log('toggle');

        if (x.style.display === "none") {
            x.style.display = "block";
            toggle.classList.remove('toggled');
        } else {
            x.style.display = "none";
            toggle.classList.add('toggled');
        }
    }
</script>
