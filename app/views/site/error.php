<?php
	/* @var $this SiteController */
	/* @var $error array */

	$this->pageTitle = ($code == 404) ? Lang::t('error404.h1.notFound') : Lang::t('error.h1.errorTitle', array('{code}' => $code));
	$this->pageDescription = '';
	$this->pageKeywords = '';
?>
<?php $assetsUrl = Yii::app()->assetManager->getBaseUrl(); ?>

<div class="wrap">
	<div class="page">
		<h1 class="page__title"><?=CHtml::encode($this->pageTitle)?></h1>

		<div class="page__description">
			<p><?php echo CHtml::encode($message); ?></p>
		</div>
	</div>
</div>
<div style="display: none;">
    <?php echo @$file; ?><br><br>
    <?php echo @$line; ?><br><br>
    <?php echo @$trace; ?>
</div>
