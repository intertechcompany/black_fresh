<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<div class="breadcrumbs">
	<a href="<?=Yii::app()->homeUrl?>"><?=Lang::t('layout.link.breadcrumbsHome')?></a> 
	<span class="b-sep">|</span> 
	<?=Lang::t('blog.title.blogTitle')?>
</div>

<div class="page">
	<h1><?=Lang::t('blog.title.blogTitle')?></h1>
	
	<?php if (!empty($blogs)) { ?>
	<div class="blog-list">
		<?php
			$this->renderPartial('blogsList', array(
				'blogs' => $blogs,
			));
		?>
	</div>
	<?php } else { ?>
	<p><?=Lang::t('blog.tip.noArticlesFound')?></p>
	<?php } ?>
</div>