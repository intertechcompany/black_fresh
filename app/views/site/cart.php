<?php
    /* @var $this SiteController */
    $assetsUrl = Yii::app()->assetManager->getBaseUrl();
    $staticUrl = Yii::app()->assetManager->getBaseUrl() . '/static/' . Yii::app()->params->settings['rev'];

    $countries_ru = array(
        'au' => 'Австралия',
        'at' => 'Австрия',
        'az' => 'Азербайджан',
        'ax' => 'Аландские о-ва',
        'al' => 'Албания',
        'dz' => 'Алжир',
        'as' => 'Американское Самоа',
        'ai' => 'Ангилья',
        'ao' => 'Ангола',
        'ad' => 'Андорра',
        'aq' => 'Антарктида',
        'ag' => 'Антигуа и Барбуда',
        'ar' => 'Аргентина',
        'am' => 'Армения',
        'aw' => 'Аруба',
        'af' => 'Афганистан',
        'bs' => 'Багамские о-ва',
        'bd' => 'Бангладеш',
        'bb' => 'Барбадос',
        'bh' => 'Бахрейн',
        'by' => 'Беларусь',
        'bz' => 'Белиз',
        'be' => 'Бельгия',
        'bj' => 'Бенин',
        'ci' => 'Берег Слоновой Кости',
        'bm' => 'Бермудские о-ва',
        'bg' => 'Болгария',
        'bo' => 'Боливия',
        'bq' => 'Бонэйр, Синт-Эстатиус и Саба',
        'ba' => 'Босния и Герцеговина',
        'bw' => 'Ботсвана',
        'br' => 'Бразилия',
        'io' => 'Британская территория в Индийском океане',
        'vg' => 'Британские Виргинские о-ва',
        'bn' => 'Бруней-Даруссалам',
        'bf' => 'Буркина Фасо',
        'bi' => 'Бурунди',
        'bt' => 'Бутан',
        'vu' => 'Вануату',
        'va' => 'Ватикан',
        'gb' => 'Великобритания',
        'hu' => 'Венгрия',
        've' => 'Венесуэла',
        'vi' => 'Виргинские о-ва (США)',
        'um' => 'Внешние малые о-ва (США)',
        'tl' => 'Восточный Тимор',
        'vn' => 'Вьетнам',
        'ga' => 'Габон',
        'ht' => 'Гаити',
        'gy' => 'Гайана',
        'gm' => 'Гамбия',
        'gh' => 'Гана',
        'gp' => 'Гваделупа',
        'gt' => 'Гватемала',
        'gn' => 'Гвинея',
        'gw' => 'Гвинея-Бисау',
        'de' => 'Германия',
        'gg' => 'Гернси',
        'gi' => 'Гибралтар',
        'hn' => 'Гондурас',
        'hk' => 'Гонконг',
        'gd' => 'Гренада',
        'gl' => 'Гренландия',
        'gr' => 'Греция',
        'ge' => 'Грузия',
        'gu' => 'Гуам',
        'dk' => 'Дания',
        'je' => 'Джерси',
        'dj' => 'Джибути',
        'dm' => 'Доминика',
        'do' => 'Доминиканская Республика',
        'eg' => 'Египет',
        'zm' => 'Замбия',
        'eh' => 'Западная Сахара',
        'zw' => 'Зимбабве',
        'il' => 'Израиль',
        'in' => 'Индия',
        'id' => 'Индонезия',
        'jo' => 'Иордания',
        'iq' => 'Ирак',
        'ir' => 'Иран',
        'ie' => 'Ирландия',
        'is' => 'Исландия',
        'es' => 'Испания',
        'it' => 'Италия',
        'ye' => 'Йемен',
        'cv' => 'Кабо-Верде',
        'kz' => 'Казахстан',
        'ky' => 'Каймановы о-ва',
        'kh' => 'Камбоджа',
        'cm' => 'Камерун',
        'ca' => 'Канада',
        'qa' => 'Катар',
        'ke' => 'Кения',
        'cy' => 'Кипр',
        'kg' => 'Киргизия',
        'ki' => 'Кирибати',
        'cn' => 'Китай',
        'cc' => 'Кокосовые о-ва',
        'co' => 'Колумбия',
        'km' => 'Коморские о-ва',
        'cg' => 'Конго',
        'cd' => 'Конго (ДРК)',
        'cr' => 'Коста-Рика',
        'cu' => 'Куба',
        'kw' => 'Кувейт',
        'cw' => 'Кюрасао',
        'la' => 'Лаос',
        'lv' => 'Латвия',
        'ls' => 'Лесото',
        'lr' => 'Либерия',
        'lb' => 'Ливан',
        'ly' => 'Ливия',
        'lt' => 'Литва',
        'li' => 'Лихтенштейн',
        'lu' => 'Люксембург',
        'mu' => 'Маврикий',
        'mr' => 'Мавритания',
        'mg' => 'Мадагаскар',
        'yt' => 'Майотта',
        'mo' => 'Макао',
        'mk' => 'Македония',
        'mw' => 'Малави',
        'my' => 'Малайзия',
        'ml' => 'Мали',
        'mv' => 'Мальдивские о-ва',
        'mt' => 'Мальта',
        'ma' => 'Марокко',
        'mq' => 'Мартиника',
        'mh' => 'Маршалловы о-ва',
        'mx' => 'Мексика',
        'mz' => 'Мозамбик',
        'md' => 'Молдова',
        'mc' => 'Монако',
        'mn' => 'Монголия',
        'ms' => 'Монтсеррат',
        'mm' => 'Мьянма [Бирма]',
        'na' => 'Намибия',
        'nr' => 'Науру',
        'np' => 'Непал',
        'ne' => 'Нигер',
        'ng' => 'Нигерия',
        'nl' => 'Нидерланды',
        'ni' => 'Никарагуа',
        'nu' => 'Ниуе',
        'nz' => 'Новая Зеландия',
        'nc' => 'Новая Каледония',
        'no' => 'Норвегия',
        'bv' => 'О-в Буве',
        'im' => 'О-в Мэн',
        'nf' => 'О-в Норфолк',
        'cx' => 'О-в Рождества',
        'bl' => 'О-в Св. Бартоломея',
        'sh' => 'О-в Св. Елены',
        'mf' => 'О-в Святого Мартина',
        'ck' => 'О-ва Кука',
        'tc' => 'О-ва Тёркс и Кайкос',
        'hm' => 'О-ва Херд и Макдональд',
        'ae' => 'ОАЭ',
        'om' => 'Оман',
        'pk' => 'Пакистан',
        'pw' => 'Палау',
        'ps' => 'Палестина',
        'pa' => 'Панама',
        'pg' => 'Папуа – Новая Гвинея',
        'py' => 'Парагвай',
        'pe' => 'Перу',
        'pn' => 'Питкэрн',
        'pl' => 'Польша',
        'pt' => 'Португалия',
        'pr' => 'Пуэрто-Рико',
        'kr' => 'Республика Корея',
        're' => 'Реюньон',
        'ru' => 'Россия',
        'rw' => 'Руанда',
        'ro' => 'Румыния',
        'us' => 'США',
        'sv' => 'Сальвадор',
        'ws' => 'Самоа',
        'sm' => 'Сан-Марино',
        'st' => 'Сан-Томе и Принсипи',
        'sa' => 'Саудовская Аравия',
        'sz' => 'Свазиленд',
        'sj' => 'Свальбард и Ян-Майен',
        'IE' => 'Северная Ирландия',
        'kp' => 'Северная Корея',
        'mp' => 'Северные Марианские о-ва',
        'sc' => 'Сейшельские о-ва',
        'pm' => 'Сен-Пьер и Микелон',
        'sn' => 'Сенегал',
        'vc' => 'Сент-Винсент и Гренадины',
        'kn' => 'Сент-Китс и Невис',
        'lc' => 'Сент-Люсия',
        'rs' => 'Сербия',
        'sg' => 'Сингапур',
        'sx' => 'Синт-Мартен',
        'sy' => 'Сирия',
        'sk' => 'Словакия',
        'si' => 'Словения',
        'sb' => 'Соломоновы о-ва',
        'so' => 'Сомали',
        'sd' => 'Судан',
        'sr' => 'Суринам',
        'sl' => 'Сьерра-Леоне',
        'tj' => 'Таджикистан',
        'th' => 'Таиланд',
        'tw' => 'Тайвань',
        'tz' => 'Танзания',
        'tg' => 'Того',
        'tk' => 'Токелау',
        'to' => 'Тонга',
        'tt' => 'Тринидад и Тобаго',
        'tv' => 'Тувалу',
        'tn' => 'Тунис',
        'tm' => 'Туркменистан',
        'tr' => 'Турция',
        'ug' => 'Уганда',
        'uz' => 'Узбекистан',
        'ua' => 'Украина',
        'wf' => 'Уоллис и Футуна',
        'uy' => 'Уругвай',
        'fo' => 'Фарерские о-ва',
        'fm' => 'Федеративные Штаты Микронезии',
        'fj' => 'Фиджи',
        'ph' => 'Филиппины',
        'fi' => 'Финляндия',
        'fk' => 'Фолклендские (Мальвинские) о-ва',
        'fr' => 'Франция',
        'gf' => 'Французская Гвиана',
        'pf' => 'Французская Полинезия',
        'tf' => 'Французские Южные Территории',
        'hr' => 'Хорватия',
        'cf' => 'ЦАР',
        'td' => 'Чад',
        'me' => 'Черногория',
        'cz' => 'Чехия',
        'cl' => 'Чили',
        'ch' => 'Швейцария',
        'se' => 'Швеция',
        'lk' => 'Шри-Ланка',
        'ec' => 'Эквадор',
        'gq' => 'Экваториальная Гвинея',
        'er' => 'Эритрея',
        'ee' => 'Эстония',
        'et' => 'Эфиопия',
        'za' => 'ЮАР',
        'gs' => 'Южн. Джорджия и Южн. Сандвичевы о-ва',
        'ss' => 'Южный Судан',
        'jm' => 'Ямайка',
        'jp' => 'Япония',
    );
?>
<div class="wrap">
    <div class="content-divider content-divider--top"></div>

    <div id="cart" class="cart">
        <h1 class="cart__title">Корзина</h1>
        <?php if (empty($cart)) { ?>
        <p class="text-center">Ваша корзина пуста!</p>
        <?php } else { ?>
        <div class="cart__header">
            <div class="cart__header-product">Товар</div>
            <div class="cart__header-product-spacer">&nbsp;</div>
            <div class="cart__header-quantity">Количество</div>
            <div class="cart__header-price">Цена</div>
            <div class="cart__header-remove-spacer">&nbsp;</div>
        </div>
        <div class="cart__content">
            <form action="<?=$this->createUrl('cart')?>" class="cart__items" method="post">
                <?php $this->renderPartial('cartList', array('cart' => $cart)); ?>
            </form>
        </div>
        <div class="cart__bottom">
            <form class="cart__discount discount" method="post">
                <input type="text" class="discount__input" placeholder="Скидочный код" style="opacity: 0; pointer-events: none">
            </form>
            <div class="cart__total">
                <span class="cart__total-tip">Всего:</span>
                <span class="cart__total-amount"><?=Currency::format($price, false)?></span>
            </div>
        </div>
        <?php } ?>
    </div>

    <?php if (!empty($cart)) { ?>
    <div class="content-divider content-divider--cart"></div>
    <form id="checkout" class="checkout" action="<?=$this->createUrl('checkout')?>" method="post" novalidate>
        <div class="checkout__column">
            <div id="cart-login-form" class="ajax-login-form" data-action="<?=$this->createUrl('site/login')?>">
                <div class="error-msg hidden"></div>
                <div class="checkout__subtitle">1. Авторизация</div>
                <div class="checkout__group">
                    <div class="checkout__row">
                        <label for="checkout-login" class="checkout__label">Логин</label>
                        <input id="checkout-login" type="text" class="checkout__input" name="login[login]" value="">
                        <div class="error-msg hidden"></div>
                    </div>
                    <div class="checkout__row">
                        <label for="checkout-password" class="checkout__label">Пароль</label>
                        <a href="<?=$this->createUrl('site/reset')?>" class="forgot-link"><?=Lang::t('login.link.forgotPassword')?></a>
                        <input id="checkout-password" type="password" class="checkout__input" name="login[password]" value="">
                        <div class="error-msg hidden"></div>
                    </div>
                    <div class="form-row form-row-btn clearfix">
                        <div class="form-col form-col-100">
                            <button class="checkout__button"><?=Lang::t('login.btn.logIn')?></button>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="_crsf" value="<?=Yii::app()->request->getCsrfToken()?>">
            </div>
            <div class="checkout__subtitle">2. ЛИЧНЫЕ ДАННЫЕ</div>
            <div class="checkout__group">
                <div class="checkout__row">
                    <label for="checkout-first_name" class="checkout__label">Имя</label>
                    <input id="checkout-first_name" type="text" class="checkout__input" name="order[first_name]" value="<?=CHtml::encode($customer['first_name'])?>">
                </div>
                <div class="checkout__row">
                    <label for="checkout-last_name" class="checkout__label">Фамилия</label>
                    <input id="checkout-last_name" type="text" class="checkout__input" name="order[last_name]" value="<?=CHtml::encode($customer['last_name'])?>">
                </div>
                <div class="checkout__row">
                    <label for="checkout-email" class="checkout__label">Email</label>
                    <input id="checkout-email" type="email" class="checkout__input" name="order[email]" value="<?=CHtml::encode($customer['email'])?>">
                </div>
                <div class="checkout__row">
                    <label for="checkout-phone" class="checkout__label">Телефон</label>
                    <input id="checkout-phone" type="tel" class="checkout__input" name="order[phone]" value="<?=CHtml::encode($customer['phone'])?>">
                </div>
            </div>
        </div>
        <div class="checkout__column">
            <div class="checkout__subtitle">3. СПОСОБ ДОСТАВКИ</div>
            <div class="checkout__group">
                <div class="checkout__row checkout__radio">
                    <label for="delivery-1"><input id="delivery-1" type="radio" name="order[delivery]" value="1"<?php if (!$delivery || $delivery == 1) { ?> checked<?php } ?>><span>Новой Почтой <small style="font-size: 80%">(адресная доставка)</small> <small style="color: #f09981"><?=Yii::app()->params->settings['np_address']?> грн.</small></span></label>
                </div>
                <div class="checkout__row checkout__radio">
                    <label for="delivery-2"><input id="delivery-2" type="radio" name="order[delivery]" value="2"<?php if ($delivery == 2) { ?> checked<?php } ?>><span>Новой Почтой <small style="font-size: 80%">(на отделение)</small> <small style="color: #f09981"><?=Yii::app()->params->settings['np_department']?> грн.</small></span></label>
                </div>
                <div class="checkout__row checkout__radio">
                    <label for="delivery-3"><input id="delivery-3" type="radio" name="order[delivery]" value="3"<?php if ($delivery == 3) { ?> checked<?php } ?>><span>Доставка по миру <small style="color: #f09981"><?=Yii::app()->params->settings['delivery_world']?>€</small></span></label>
                </div>
            </div>
            <div id="checkout-np" class="checkout__group<?php if ($delivery == 3) { ?> checkout__group--hidden<?php } ?>">
                <div class="checkout__row">
                    <label for="checkout-np-city" class="checkout__label">Город</label>
                    <select id="checkout-np-city" name="order[np_city]" class="checkout__input">
                        <option value="">Выберите город...</option>
                        <?php if (!empty($np_cities)) { ?>
                        <?php foreach ($np_cities as $np_city) { ?>
                        <option value="<?=CHtml::encode($np_city['city_full_name_ru'])?>"<?php if ($customer['np_city'] == $np_city['city_full_name_ru']) { ?> selected<?php } ?>><?=CHtml::encode($np_city['city_full_name_ru'])?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="checkout__row<?php if (!$delivery || $delivery == 1) { ?> checkout__row--hidden<?php } ?>">
                    <label for="checkout-np-department" class="checkout__label">Отделение</label>
                    <select id="checkout-np-department" name="order[np_department]" class="checkout__input"<?php if (empty($np_departments)) { ?> disabled<?php } ?>>
                        <option value="">Выберите отделение...</option>
                        <?php if (!empty($np_departments)) { ?>
                        <?php foreach ($np_departments as $np_department) { ?>
                        <option value="<?=CHtml::encode($np_department['department_name_ru'])?>"<?php if ($customer['np_department'] == $np_city['department_name_ru']) { ?> selected<?php } ?>><?=CHtml::encode($np_department['department_name_ru'])?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="checkout__row<?php if ($delivery == 2) { ?> checkout__row--hidden<?php } ?>">
                    <label for="checkout-np-address" class="checkout__label">Адрес</label>
                    <input id="checkout-np-address" type="text" class="checkout__input" name="order[np_address]" value="<?=CHtml::encode($customer['np_address'])?>">
                </div>
                <div class="checkout__row checkout__row--merged<?php if ($delivery == 2) { ?> checkout__row--hidden<?php } ?>">
                    <div class="checkout__col">
                        <label for="checkout-np-building" class="checkout__label">Дом</label>
                        <input id="checkout-np-building" type="text" class="checkout__input" name="order[np_building]" value="<?=CHtml::encode($customer['np_building'])?>">
                    </div>
                    <div class="checkout__col">
                        <label for="checkout-np-apartment" class="checkout__label">Квартира/офис</label>
                        <input id="checkout-np-apartment" type="text" class="checkout__input" name="order[np_apartment]" value="<?=CHtml::encode($customer['np_apartment'])?>">
                    </div>
                </div>
            </div>
            <div id="checkout-worldwide" class="checkout__group<?php if ($delivery != 3) { ?> checkout__group--hidden<?php } ?>">
                <div class="checkout__row">
                    <label for="checkout-country" class="checkout__label">Страна</label>
                    <select id="checkout-country" name="order[country]" class="checkout__input">
                        <option value="">Выберите страну...</option>
                        <?php foreach ($countries_ru as $country) { ?>
                        <option value="<?=CHtml::encode($country)?>"<?php if ($customer['np_apartment'] == $country) { ?> selected<?php } ?>><?=CHtml::encode($country)?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="checkout__row">
                    <label for="checkout-zip" class="checkout__label">Индекс</label>
                    <input id="checkout-zip" type="text" class="checkout__input" name="order[zip]" value="<?=CHtml::encode($customer['zip'])?>">
                </div>
                <div class="checkout__row">
                    <label for="checkout-region" class="checkout__label">Регион/штат/провинция</label>
                    <input id="checkout-region" type="text" class="checkout__input" name="order[region]" value="<?=CHtml::encode($customer['region'])?>">
                </div>
                <div class="checkout__row">
                    <label for="checkout-city" class="checkout__label">Город</label>
                    <input id="checkout-city" type="text" class="checkout__input" name="order[city]" value="<?=CHtml::encode($customer['city'])?>">
                </div>
                <div class="checkout__row checkout__label">
                    <label for="checkout-address" class="checkout__label">Полный адрес</label>
                    <textarea id="checkout-address" class="checkout__input" name="order[address]" rows="2"><?=CHtml::encode($customer['address'])?></textarea>
                </div>
            </div>
        </div>
        <div class="checkout__column">
            <div class="checkout__subtitle">4. СПОСОБ ОПЛАТЫ</div>
            <div class="checkout__group">
                <div class="checkout__row checkout__radio">
                    <label for="payment-1"><input id="payment-1" type="radio" name="order[payment]" value="1"<?php if (!$payment || $payment == 1) { ?> checked<?php } ?>><span>Visa/MasterCard</span></label>
                </div>
                <div class="checkout__row checkout__radio">
                    <label for="payment-2"><input id="payment-2" type="radio" name="order[payment]" value="2"<?php if ($payment == 2) { ?> checked<?php } ?>><span>Наличными</span></label>
                </div>
            </div>
            <div class="checkout__subtitle">5. КОММЕНТАРИИ <small>(не обязательно)</small></div>
            <div class="checkout__group">
                <div class="checkout__row">
                    <textarea id="checkout-comment" class="checkout__input" name="order[comment]" rows="5"><?=CHtml::encode($customer['comment'])?></textarea>
                </div>
                <div class="checkout__row">
                    <button class="checkout__button">Заказать</button>
                    <?php if ($has_unavailable) { ?>
                    <div class="checkout__error">У вас в корзине товары, которых нет в наличии. Для оформления заказа удалите недоступный товар.</div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </form>
    <?php } ?>
</div>