<style>
    #account-details {
        width: 100%;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-direction: row;
        flex-direction: row;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
        -ms-flex-line-pack: stretch;
        align-content: stretch;
        -webkit-box-align: start;
        -ms-flex-align: start;
        align-items: flex-start;
    }
    .width-div {
        width: 39%;
        margin-bottom: 20px;
    }
    #af-user_first_name {
        width: 270px;
        height: 40px;
        padding: 8px;
    }
    #af-user_last_name {
        width: 270px;
        height: 40px;
        padding: 8px;
    }
    #af-user_email {
        width: 270px;
        height: 40px;
        padding: 8px;
    }
    #af-user_phone {
        width: 270px;
        height: 40px;
        padding: 8px;
    }
    .mt-btn {
        margin-top: 10px;
    }
    @media (max-width: 1200px) {
        .width-div {
            width: 45%;
        }
    }
    @media (max-width: 1000px) {
        .account-info {
            margin-left: 33px;
        }
        .width-div {
            width: 50%;
        }
        #af-user_first_name {
            width: 240px;
        }
        #af-user_last_name {
            width: 240px;
        }
        #af-user_email {
            width: 240px;
        }
        #af-user_phone {
            width: 240px;
        }
    }
    @media (max-width: 700px) {
        .account-info {
            margin-left: 25px;
        }
        .width-div {
            width: 47%;
        }
        #af-user_first_name {
            width: 200px;
            font-size: 12px;
            height: 35px;
        }
        #af-user_last_name {
            width: 200px;
            font-size: 12px;
            height: 35px;
        }
        #af-user_email {
            width: 200px;
            font-size: 12px;
            height: 35px;
        }
        #af-user_phone {
            width: 200px;
            font-size: 12px;
            height: 35px;

        }
    }
    @media (max-width: 620px) {
        .width-div {
            width: 51%;
        }
        .width-btn {
            width: 100%;
        }
        #af-user_first_name {
            width: 240px;

        }
        #af-user_last_name {
            width: 240px;

        }
        #af-user_email {
            width: 240px;

        }
        #af-user_phone {
            width: 240px;
        }

    }
    @media (max-width: 550px) {
        .account-info {
            margin-left: 5px;
            padding-left: 50px;
        }
        .width-div {
            width: 70%;
        }
    } 
    @media (max-width: 460px) {
        .width-div {
            width: 90%
        } 
    }
        @media (max-width: 400px) {
        .account-info {
            padding-left: 0;
            margin-left: 0;
        }
    }
    
    
</style>
<div class="width-div"><label for="af-user_first_name"><?=Lang::t('account.label.firstName')?></label>
    <input id="af-user_first_name" type="text" name="user[user_first_name]" value="<?=CHtml::encode($user['user_first_name'])?>"></div>
<div class="width-div"><label for="af-user_last_name"><?=Lang::t('account.label.lastName')?></label>
    <input id="af-user_last_name" type="text" name="user[user_last_name]" value="<?=CHtml::encode($user['user_last_name'])?>"></div>
<div class="width-div"><label for="af-user_email"><?=Lang::t('account.label.email')?></label>
    <input id="af-user_email" type="email" name="user[user_email]" value="<?=CHtml::encode($user['user_email'])?>"></div>
<div class="width-div"><label for="af-user_phone"><?=Lang::t('account.label.phone')?></label>
    <input id="af-user_phone" type="tel" name="user[user_phone]" value="<?=CHtml::encode($user['user_phone'])?>"></div>
<div class="width-btn">
        <input type="hidden" name="action" value="personal">
        <button class="btn mt-btn"><?=Lang::t('account.btn.save')?></button>
</div>

<!--<div class="form-row clearfix">-->
<!--	<div class="form-col form-col-100">-->
<!--		<label for="af-user_zip">--><?//=Lang::t('checkout.label.zip')?><!--</label>-->
<!--		<input id="af-user_zip" type="text" name="user[user_zip]" value="--><?//=CHtml::encode($user['user_zip'])?><!--">-->
<!--	</div>-->
<!--</div>-->
<!--<div class="form-row clearfix">-->
<!--	<div class="form-col form-col-100">-->
<!--		<label for="af-user_city">--><?//=Lang::t('checkout.label.city')?><!--</label>-->
<!--		<input id="af-user_city" type="text" name="user[user_city]" value="--><?//=CHtml::encode($user['user_city'])?><!--">-->
<!--	</div>-->
<!--</div>-->
<!--<div class="form-row clearfix">-->
<!--	<div class="form-col form-col-100">-->
<!--		<label for="af-user_address">--><?//=Lang::t('checkout.label.address1')?><!--</label>-->
<!--		<input id="af-user_address" type="text" name="user[user_address]" value="--><?//=CHtml::encode($user['user_address'])?><!--">-->
<!--	</div>-->
<!--</div>-->
<!--<div class="form-row clearfix">-->
<!--	<div class="form-col form-col-100">-->
<!--		<label for="af-user_address_2">--><?//=Lang::t('checkout.label.address2')?><!--</label>-->
<!--		<input id="af-user_address_2" type="text" name="user[user_address_2]" value="--><?//=CHtml::encode($user['user_address_2'])?><!--">-->
<!--	</div>-->
<!--</div>-->