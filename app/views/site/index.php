<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
    $staticUrl = $assetsUrl . '/static/' . Yii::app()->params->settings['rev'];
?>
<main class="home">
    <?php if (!empty($banners)) { ?>
        <?php if (count($banners) > 1) { ?>
            <section id="banners" class="banners">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <?php foreach ($banners as $banner) { ?>
                            <?php
                            $banner_image = null;

                            if (!empty($banner['banner_logo'])) {
                                $banner_logo = json_decode($banner['banner_logo'], true);
                                $banner_image = $assetsUrl . '/banner/' . $banner_logo['file'];
                            }
                            ?>
                            <div class="banner swiper-slide"<?php if (!empty($banner_image)) { ?> style="background-image: url(<?=$banner_image?>)"<?php } ?>>
                                <div class="wrap">
                                    <div class="banner__content">
                                        <div class="banner__title"><?=CHtml::encode($banner['banner_name'])?></div>
                                        <?php
                                        if (strpos($banner['banner_url'], '/') === 0) {
                                            $banner['banner_url'] = preg_replace('#^/#ui', Yii::app()->request->getBaseUrl() . '/', $banner['banner_url']);
                                        }
                                        ?>
                                        <?php if (isset($banner['banner_text'])  && $banner['banner_text'] != '') { ?>
                                            <div class="banner__title"><?=CHtml::encode($banner['banner_text'])?></div>
                                        <?php } ?>
                                        <div class="banner__btn"><a href="<?=CHtml::encode($banner['banner_url'])?>" class="btn"<?php if ($banner['banner_url_blank']) { ?> target="_blank"<?php } ?>><?=CHtml::encode($banner['banner_button'])?></a></div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="wrap">
                        <div class="banners__nav">
                            <div class="swiper-pagination"></div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                    </div>
                </div>
            </section>
        <?php } else { ?>
            <section class="banner">
                <div class="wrap">
                    <div class="banner__content">
                        <div class="banner__title"><?=CHtml::encode($banners[0]['banner_name'])?></div>
                        <?php
                        if (strpos($banners[0]['banner_url'], '/') === 0) {
                            $banners[0]['banner_url'] = preg_replace('#^/#ui', Yii::app()->request->getBaseUrl() . '/', $banners[0]['banner_url']);
                        }
                        ?>
                        <div class="banner__btn"><a href="<?=CHtml::encode($banners[0]['banner_url'])?>" class="btn"<?php if ($banners[0]['banner_url_blank']) { ?> target="_blank"<?php } ?>><?=CHtml::encode($banners[0]['banner_button'])?></a></div>
                    </div>
                </div>
            </section>
            <!-- /.banner -->
        <?php } ?>
    <?php } ?>

    <div class="wrap">

        <?php
        $has_categories = false;
        $this->beginClip('home_categories');
        ?>
        <section class="special">
            <div class="special__title"><?=Lang::t('home.tip.shop')?></div>
            <div class="products-list">
                <?php foreach ($categories as $index => $category) { ?>
                    <?php
                    if (!$category['category_home']) {
                        continue;
                    }

                    $has_categories = true;
                    ?>
                    <div class="product-card">
                        <?php if (!empty($category['category_photo'])) { ?>
                            <div class="product-card__img">
                                <a href="<?=$this->createUrl('site/category', ['alias' => $category['category_alias']])?>">
                                    <?php
                                    $category_photo = json_decode($category['category_photo'], true);
                                    $category_photo_1x = $assetsUrl . '/category/' . $category['category_id'] . '/' . $category_photo['1x']['path'];
                                    $category_photo_2x = '';

                                    if (isset($category_photo['2x'])) {
                                        $category_photo_2x = $assetsUrl . '/category/' . $category['category_id'] . '/' . $category_photo['2x']['path'];
                                    }
                                    ?>
                                    <picture>
                                        <?php /* <source type="image/webp" srcset="images/menu_section.webp, images/menu_section@2x.webp 2x"> */ ?>
                                        <?php if (!empty($category_photo_2x)) { ?>
                                            <source srcset="<?=$category_photo_1x?>, <?=$category_photo_2x?> 2x">
                                        <?php } ?>
                                        <img src="<?=$category_photo_1x?>" alt="<?=CHtml::encode($category['category_name'])?>">
                                    </picture>
                                </a>
                            </div>
                        <?php } ?>
                        <div class="product-card__title"><?=CHtml::encode($category['category_name'])?></div>
                        <div class="product-card__more">
                            <a href="<?=$this->createUrl('site/category', ['alias' => $category['category_alias']])?>" class="product-card__btn"><?=Lang::t('home.link.category')?> &gt;</a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </section>
        <!-- /.special -->
        <?php
        $this->endClip();

        if ($has_categories) {
            echo $this->clips['home_categories'];
        }
        ?>

        <!-- /.benefits -->

        <section class="mission">
            <div class="mission__title"><?=Lang::t('home.tip.missionTitle')?></div>
            <div class="mission__text"><?=Lang::t('home.tip.missionText')?></div>
        </section>
        <!-- /.mission -->

        <?php if (!empty($products)) { ?>
        <section class="special special--products">
            <div class="special__title"><?=Lang::t('home.tip.hits')?></div>
            <div class="products-list">
                <?php $this->renderPartial('productsList', array('products' => $products)); ?>
            </div>
            <?php foreach ($categories as $index => $category) { ?>
            <?php
                if (!$category['category_top']) {
                    continue;
                }
            ?>
            <div class="special__more">
                <a href="<?=$this->createUrl('site/category', ['alias' => $category['category_alias']])?>" class="btn"><?=Lang::t('home.btn.showMore')?></a>
            </div>
            <?php } ?>
        </section>
        <!-- /.special -->
        <?php } ?>


        <section class="benefits">
            <div class="benefits__cell"><div class="benefits__item"><?=nl2br(Lang::t('home.tip.block1'))?></div></div>
            <div class="benefits__cell"><div class="benefits__item"><?=nl2br(Lang::t('home.tip.block2'))?></div></div>
            <div class="benefits__cell"><div class="benefits__item"><?=nl2br(Lang::t('home.tip.block3'))?></div></div>
            <div class="benefits__cell"><div class="benefits__item"><?=nl2br(Lang::t('home.tip.block4'))?></div></div>
        </section>


        <section class="special special--coffee">
            <div class="special__title"><?=Lang::t('home.tip.weLiveCoffee')?></div>
            <div class="info-block info-block--school">
                <div class="info-block__img">
                    <picture>
                        <source srcset="<?php echo $staticUrl?>/images/blck_school.jpg">
                        <img src="<?=$staticUrl?>/images/school_fresh_black.jpg" alt="">
                    </picture>
                </div>
                <div class="info-block__content">
                    <div class="info-block__title"><?=Lang::t('home.tip.school')?></div>
                    <div class="info-block__tip"><?=Lang::t('home.tip.schoolText')?></div>
                    <div class="info-block__btn"><a href="<?php echo $this->createUrl('site/school')?>" class="btn"><?=Lang::t('home.btn.schedule')?></a></div>
                </div>
            </div>
            <?php /* <div class="info-block info-block--knowledge-base">
                <div class="info-block__img">
                    <picture>
                        <source type="image/webp" srcset="<?=$staticUrl?>/images/knowledge_base.webp, images/knowledge_base@2x.webp 2x">
                        <source srcset="<?=$staticUrl?>/images/knowledge_base.jpg, images/knowledge_base@2x.jpg 2x">
                        <img src="<?=$staticUrl?>/images/knowledge_base.jpg" alt="">
                    </picture>
                </div>
                <div class="info-block__content">
                    <div class="info-block__title"><?=Lang::t('home.tip.base')?></div>
                    <div class="info-block__tip"><?=Lang::t('home.tip.baseText')?></div>
                    <ul class="info-block__list list-unstyled">
                        <?php foreach ($base_categories as $category) { ?>
                        <li><a href="<?=Yii::app()->createUrl('site/basecategory', ['alias' => $category['category_alias']])?>"><?=CHtml::encode($category['category_name'])?></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div> */ ?>
    </section>
        <!-- /.special -->

        <form id="subscribe" class="subscribe" action="<?=$this->createUrl('ajax/subscribe')?>" method="post" novalidate>
            <div class="subscribe__title"><?=Lang::t('home.tip.subscribeTitle')?></div>
            <div class="subscribe__content">
                <ul class="subscribe__special list-unstyled">
                    <li><?=Lang::t('home.tip.followInsta')?> <a href="<?=CHtml::encode(Yii::app()->params->settings['instagram'])?>" target="_blank" rel="nofollow">@fresh.black.okay</a></li>
                    <li><?=Lang::t('home.tip.followFacebook')?> <a href="<?=CHtml::encode(Yii::app()->params->settings['facebook'])?>" target="_blank" rel="nofollow">fresh.black.okay</a></li>
                    <li><?=Lang::t('home.tip.subscribe')?></li>
                </ul>
                <div class="subscribe__group">
                    <input type="email" class="subscribe__input" name="subscribe[email]" placeholder="Email" required>
                    <button class="subscribe__btn">&gt;</button>
                </div>
            </div>
        </form>
        <!-- /.subscribe -->
    </div>

    <section class="instagram">
        <div id="instagram-slider" class="instagram__slider swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <a href="<?=CHtml::encode(Yii::app()->params->settings['instagram'])?>" target="_blank">
                        <picture>
                            <source type="image/webp" srcset="<?=$staticUrl?>/images/insta1.webp, <?=$staticUrl?>/images/insta1@2x.webp 2x">
                            <source srcset="<?=$staticUrl?>/images/insta1.jpg, <?=$staticUrl?>/images/insta1@2x.jpg 2x">
                            <img src="<?=$staticUrl?>/images/insta1.jpg" alt="">
                        </picture>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="<?=CHtml::encode(Yii::app()->params->settings['instagram'])?>" target="_blank">
                        <picture>
                            <source type="image/webp" srcset="<?=$staticUrl?>/images/insta2.webp, <?=$staticUrl?>/images/insta2@2x.webp 2x">
                            <source srcset="<?=$staticUrl?>/images/insta2.jpg, <?=$staticUrl?>/images/insta2@2x.jpg 2x">
                            <img src="<?=$staticUrl?>/images/insta2.jpg" alt="">
                        </picture>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="<?=CHtml::encode(Yii::app()->params->settings['instagram'])?>" target="_blank">
                        <picture>
                            <source type="image/webp" srcset="<?=$staticUrl?>/images/insta3.webp, <?=$staticUrl?>/images/insta3@2x.webp 2x">
                            <source srcset="<?=$staticUrl?>/images/insta3.jpg, <?=$staticUrl?>/images/insta3@2x.jpg 2x">
                            <img src="<?=$staticUrl?>/images/insta3.jpg" alt="">
                        </picture>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="<?=CHtml::encode(Yii::app()->params->settings['instagram'])?>" target="_blank">
                        <picture>
                            <source type="image/webp" srcset="<?=$staticUrl?>/images/insta4.webp, <?=$staticUrl?>/images/insta4@2x.webp 2x">
                            <source srcset="<?=$staticUrl?>/images/insta4.jpg, <?=$staticUrl?>/images/insta4@2x.jpg 2x">
                            <img src="<?=$staticUrl?>/images/insta4.jpg" alt="">
                        </picture>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="<?=CHtml::encode(Yii::app()->params->settings['instagram'])?>" target="_blank">
                        <picture>
                            <source type="image/webp" srcset="<?=$staticUrl?>/images/insta1.webp, <?=$staticUrl?>/images/insta1@2x.webp 2x">
                            <source srcset="<?=$staticUrl?>/images/insta1.jpg, <?=$staticUrl?>/images/insta1@2x.jpg 2x">
                            <img src="<?=$staticUrl?>/images/insta1.jpg" alt="">
                        </picture>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="<?=CHtml::encode(Yii::app()->params->settings['instagram'])?>" target="_blank">
                        <picture>
                            <source type="image/webp" srcset="<?=$staticUrl?>/images/insta2.webp, <?=$staticUrl?>/images/insta2@2x.webp 2x">
                            <source srcset="<?=$staticUrl?>/images/insta2.jpg, <?=$staticUrl?>/images/insta2@2x.jpg 2x">
                            <img src="<?=$staticUrl?>/images/insta2.jpg" alt="">
                        </picture>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="<?=CHtml::encode(Yii::app()->params->settings['instagram'])?>" target="_blank">
                        <picture>
                            <source type="image/webp" srcset="<?=$staticUrl?>/images/insta3.webp, <?=$staticUrl?>/images/insta3@2x.webp 2x">
                            <source srcset="<?=$staticUrl?>/images/insta3.jpg, <?=$staticUrl?>/images/insta3@2x.jpg 2x">
                            <img src="<?=$staticUrl?>/images/insta3.jpg" alt="">
                        </picture>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="<?=CHtml::encode(Yii::app()->params->settings['instagram'])?>" target="_blank">
                        <picture>
                            <source type="image/webp" srcset="<?=$staticUrl?>/images/insta4.webp, <?=$staticUrl?>/images/insta4@2x.webp 2x">
                            <source srcset="<?=$staticUrl?>/images/insta4.jpg, <?=$staticUrl?>/images/insta4@2x.jpg 2x">
                            <img src="<?=$staticUrl?>/images/insta4.jpg" alt="">
                        </picture>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- /.instagram -->
</main>

<style>

    @media only screen and (max-width: 2400px) {
        .modal-content {
            width: 476px;
            height: 201px;
        }

        .modal {
            top: 64%;
            left: 68%;
            width: 75vh;
        }
    }

    @media only screen and (max-width: 2200px) {
        .modal-content {
            width: 476px;
            height: 201px;
        }

        .modal {
            top: 64%;
            left: 66%;
            width: 75vh;
        }
    }

    @media only screen and (max-width: 1900px) {
        .modal-content {
            width: 476px;
            height: 201px;
        }

        .modal {
            top: 64%;
            left: 65%;
            width: 75vh;
        }
    }

    @media only screen and (max-width: 1600px) {
        .modal-content {
            width: 476px;
            height: 201px;
        }

        .modal {
            top: 57%;
            left: 57%;
            width: 43%;
        }
    }

    @media only screen and (max-width: 1199px) {
        .modal-content {
            width: 476px;
            height: 212px;
        }

        .modal {
            top: 68%;
            left: 50%;
            width: 50%;
        }
    }

    @media only screen and (max-width: 990px) {
        .modal-content {
            width: 476px;
            height: 220px;
        }

        .modal {
            top: 69%;
            left: 36%;
            width: 62%;
        }
    }

    @media only screen and (max-width: 767px) {
        .modal-content {
            width: 476px;
            height: 220px;
        }

        .modal {
            top: 44%;
            left: 23%;
            width: 76%;
        }
    }

    @media only screen and (max-width: 600px) {
        .modal-content {
            width: 476px;
            height: 220px;
        }

        .modal {
            left: 6%;
            width: 91%;
        }
    }

    @media only screen and (max-width: 500px) {
        .modal-content {
            width: 404px;
            height: 241px;
        }

        .modal {
            top: 48%;
            left: 0;
            width: 100%;
        }
    }

    @media only screen and (max-width: 400px) {

        .modal-content {
            width: 313px;
            height: 274px;
        }

        .modal {
            left: 0;
            width: 100%;
        }
    }

    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        height: 59%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,-1); /* Black w/ opacity */
    }

    /* Modal Content/Box */
    .modal-content {
        background-color: #fefefe;
        margin: 15% auto; /* 15% from the top and centered */
        padding: 20px;
        border: 1px solid #888;
    }

    /* The Close Button */
    .close {
        color: black;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: black;
        text-decoration: none;
        cursor: pointer;
    }

    /* modal content */

    .modal-value:hover {
        border: 1px solid #0098FF;
        cursor: pointer;
    }

    .modal-value {
        border: 1px solid #000000;
        margin-top: 20px;
        display: inline-block;
        height: 60px;
        width: 320px;
    }

    .modal-p {
        margin-left: 10px;
    }

    .modal-span {
        margin-left: 10px;
    }

    .title-modal {
        font-family: "Apercu Pro", serif;
        font-style: normal;
        font-weight: normal;
        word-wrap: break-word;
        font-size: 30px;
        line-height: 32px;
    }
    .special {
        padding-bottom: 30px;
    }
</style>

<div id="myModal" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>
        <h2 class="title-modal" id="modal-title"><?=Lang::t('home.modal.text')?></h2>
        <a class="product__btn btn" href="<?=$this->createUrl('quiz')?>" style="width: 60%; height: 57px;"><?=Lang::t('home.modal.btn')?></a>
    </div>

</div>

<?php
    $user = Yii::app()->user->id;
    $userSub = Subscription::model()->getSubByUser($user);
?>

<?php //if (!$userSub) { ?>
<!--<script>-->
<!--    window.onload = function () {-->
<!--        console.log($(window).width());-->
<!--        const modal = document.getElementById("myModal");-->
<!--        const span = document.getElementsByClassName("close")[0];-->
<!--        modal.style.display = "block";-->
<!--        console.log('show');-->
<!---->
<!--        span.onclick = function() {-->
<!--            modal.style.display = "none";-->
<!--        }-->
<!---->
<!--        window.onclick = function(event) {-->
<!--            if (event.target == modal) {-->
<!--                modal.style.display = "none";-->
<!--            }-->
<!--        }-->
<!--    }-->
<!--</script>-->
<?php //} ?>
