<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<?php foreach ($blogs as $blog) { ?>
<?php
	$blog_url = $this->createUrl('newsarticle', ['category_alias' => $blog['category_alias'], 'alias' => $blog['blog_alias']]);
	$blog_image = '';
	$blog_image_2x = '';

	if (!empty($blog['blog_photo'])) {
		$blog_image = json_decode($blog['blog_photo'], true);
		$blog_image_size = $blog_image['list']['1x']['size'];
		$blog_image = $assetsUrl . '/blog/' . $blog['blog_id'] . '/' . $blog_image['list']['1x']['path'];
		
		if (!empty($blog_image['list']['2x'])) {
			$blog_image_2x = $assetsUrl . '/blog/' . $blog['blog_id'] . '/' . $blog_image['list']['2x']['path'];
		}
	}
?>
<div class="news-card">
	<?php if (!empty($blog_image)) { ?>
	<div class="news-card__img">
		<a href="<?=$blog_url?>">
			<picture>
				<!-- <source type="image/webp" srcset="images/news_thumb.webp, images/news_thumb@2x.webp 2x"> -->
				<?php if (!empty($blog_image_2x)) { ?>
				<source srcset="<?=$blog_image?>, <?=$blog_image_2x?> 2x">
				<?php } ?>
				<img src="<?=$blog_image?>" alt="">
			</picture>
		</a>
	</div>
	<?php } ?>
	<div class="news-card__title"><a href="<?=$blog_url?>"><?=CHtml::encode($blog['blog_title'])?></a></div>
	<div class="news-card__tag"><?=CHtml::encode($blog['category_name'])?></div>
</div>
<?php } ?>