<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<div class="wrap">
	<div class="content-divider content-divider--top"></div>

	<!-- <div class="breadcrumbs">
		<a href="<?=Yii::app()->homeUrl?>">Главная</a> 
		/ 
		<?=CHtml::encode($this->pageTitle)?>
	</div> -->

	<div class="category">
		<div class="category__side">
			<div class="category__side-block side-block">
				<div class="side-block__title">Категории</div>
				<ul class="side-block__categories list-unstyled">
					<li><a href="<?=$this->createUrl('newest')?>">Новинки</a></li>
					<li><a href="<?=$this->createUrl('brands')?>">Бренды</a></li>
					<?=Category::buildCategoriesTree($this, $categories_tree, 0)?>
				</ul>
			</div>

			<?php /* <form id="filter" class="filter" action="<?=$this->createUrl('category', array('alias' => $category['category_alias']))?>" method="get">
				<?php $this->renderPartial('facets', array('facets' => $facets, 'sort' => $sort)); ?>
			</form> */ ?>
		</div>
		<div class="category__content">
			<h1 class="category__title"><?=CHtml::encode($this->pageTitle)?></h1>
			<?php if (!empty($products)) { ?>
			<?php
				$sort_params = array(
					'popular' => array(
						'title' => 'по новизне',
						'url' => null, // default value
					),
					'price-asc' => array(
						'title' => 'сначала дешевые',
						'url' => 'price-asc',
					),
					'price-desc' => array(
						'title' => 'сначала дорогие',
						'url' => 'price-desc',
					),
				);
			?>
			<ul class="category__sort">
				<li class="category__sort-title">Сортировать:</li>
				<?php foreach ($sort_params as $sort_index => $sort_param) { ?>
				<?php if ($sort_index == $sort) { ?>
				<li><span class="active"><?=CHtml::encode($sort_param['title'])?></span></li>
				<?php } else { ?>
				<?php 
					if (!empty($sort_param['url'])) {
						$sort_url = $this->createUrl('search', array('keyword' => $keyword, 'sort' => $sort_param['url']));
					} else {
						$sort_url = $this->createUrl('search', array('keyword' => $keyword));
					}
				?>
				<li><a href="<?=$sort_url?>"><?=CHtml::encode($sort_param['title'])?></a></li>
				<?php } ?>
				<?php } ?>
			</ul>
			<?php } ?>
			
			<?php if (!empty($errors)) { ?>
			<p style="font-size: 14px"><?=$errors?></p>
			<?php } else { ?>
			<?php if (!empty($products)) { ?>
			<div class="catalog">
				<?php $this->renderPartial('productsList', array('products' => $products)); ?>
			</div>
			<?php } else { ?>
			<p style="font-size: 14px">Товары не найдено.</p>
			<?php } ?>
			<?php } ?>
		</div>
	</div>
</div>