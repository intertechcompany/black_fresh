<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();

	switch ($order['status']) {
		case 'processing':
			$status_class = 'status-warning';
			$status = Lang::t('account.tip.orderStatusInProcessing');
			break;
		case 'paid':
			$status_class = 'status-success';
			$status = Lang::t('account.tip.orderStatusPaid');
			break;
		case 'completed':
			$status_class = 'status-success';
			$status = Lang::t('account.tip.orderStatusCompleted');
			break;
		case 'cancelled':
			$status_class = 'status-cancel';
			$status = Lang::t('account.tip.orderStatusCancelled');
			break;
		case 'payment_error':
			$status_class = 'status-error';
			$status = Lang::t('account.tip.orderStatusPaymentError');
			break;
		default:
			$status = Lang::t('account.tip.orderStatusNew');
			$status_class = '';
	}
?>
<div class="wrap">
    <div class="content-divider"></div>
    <div class="breadcrumbs">
        <a href="<?=Yii::app()->homeUrl?>"><?=Lang::t('layout.link.breadcrumbsHome')?></a>
        <span class="b-sep">|</span>
        <a href="<?=$this->createUrl('site/account')?>"><?=Lang::t('layout.link.account')?></a>
        <span class="b-sep">|</span>
        <a href="<?=$this->createUrl('site/orders')?>"><?=Lang::t('account.title.ordersHistory')?></a>
        <span class="b-sep">|</span>
        <?=CHtml::encode($this->pageTitle)?>
    </div>
    <?php $this->renderPartial('accountMenu'); ?>
    <div class="account-wrap clearfix">
        <div class="account-personal account-order-summary">
            <h1 class="account-title"><?=Lang::t('accountOrder.title.order')?></h1>

            <div class="order-summary">
                <div class="account-details">
                    <div class="account-details-row">
                        <div class="account-details-tip"><?=Lang::t('accountOrder.tip.fullName')?></div>
                        <div class="account-details-value"><?=CHtml::encode($order['first_name'] . ' ' . $order['last_name'])?></div>
                    </div>
                    <?php $order_date = new DateTime($order['created'], new DateTimeZone(Yii::app()->timeZone)); ?>
                    <div class="account-details-row">
                        <div class="account-details-tip"><?=Lang::t('accountOrder.tip.orderDate')?></div>
                        <div class="account-details-value"><?=$order_date->format('d.m.Y H:i')?></div>
                    </div>
                    <div class="account-details-row">
                        <div class="account-details-tip"><?=Lang::t('accountOrder.tip.status')?></div>
                        <?php if (!empty($status_class)) { ?>
                        <div class="account-details-value"><span class="<?=$status_class?>"><?=CHtml::encode($status)?></span></div>
                        <?php } else { ?>
                        <div class="account-details-value"><?=CHtml::encode($status)?></div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="account-orders account-order-details">
            <h2 class="account-title"><?=Lang::t('accountOrder.title.orderDetails')?></h2>

            <div class="account-order-products">
                <?php if (!empty($products)) { ?>
                <table>
                    <thead>
                        <tr>
                            <th><?=Lang::t('accountOrder.th.photo')?></th>
                            <th><?=Lang::t('accountOrder.th.title')?></th>
                            <th><?=Lang::t('accountOrder.th.price')?></th>
                            <th><?=Lang::t('accountOrder.th.qty')?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($products as $product) { ?>
                        <?php
                            $product_image = '';
                            $variants = '';
                            $options = '';
                            $properties = '';

                            if (!empty($product['product_alias'])) {
                                $product_url = $this->createUrl('site/product', array('alias' => $product['product_alias']));

                                if (!empty($product['product_photo'])) {
                                    $product_image = json_decode($product['product_photo'], true);
                                    $product_image_size = $product_image['size']['catalog'];
                                    $product_image = $assetsUrl . '/product/' . $product['product_id'] . '/' . $product_image['path']['catalog']['1x'];
                                    $product_image = '<a href="' . $product_url . '"><img src="' . $product_image . '"></a>';
                                }

                                $product_title = '<a href="' . $product_url . '">' . CHtml::encode($product['product_title']) . '</a>';

                                if (!empty($product['properties'])) {
                                    $product_properties = array();

                                    foreach ($product['properties'] as $property) {
                                        $product_properties[] = CHtml::encode($property['property_title'] . ': ' . $property['value_title']);
                                    }

                                    $properties = '<br><small>' . implode("<br>\n", $product_properties) . '</small>';
                                }

                                if (!empty($product['variant'])) {
                                    $product['product_sku'] = $product['variant']['variant_sku'];

                                    if (!empty($product['variant']['values'])) {
                                        $variant_values = array();

                                        foreach ($product['variant']['values'] as $value) {
                                            $variant_values[] = CHtml::encode($value['property_title'] . ': ' . $value['value_title']);
                                        }

                                        $variants = '<br><small>' . implode("<br>\n", $variant_values) . '</small>';
                                    }
                                }

                                if (!empty($product['options'])) {
                                    $options_values = array();

                                    foreach ($product['options'] as $option) {
                                        if (!empty($option['value_title'])) {
                                            $options_values[] = CHtml::encode($option['property_title'] . ': ' . $option['value_title']);
                                        } else {
                                            $options_values[] = CHtml::encode($option['option_title']);
                                        }
                                    }

                                    $options = '<br><span style="font-size: 10px;">' . implode("<br>\n", $options_values) . '</span>';
                                }
                            } else {
                                $product_title = CHtml::encode($product['title']);

                                if (!empty($product['variant_title'])) {
                                    $variants = '<br><small>' . str_replace("\n", "<br>\n", CHtml::encode($product['variant_title'])) . '</small>';
                                }

                                if (!empty($product['options_title'])) {
                                    $options = '<br><span style="color: #aaa">' . str_replace("\n", "<br>\n", CHtml::encode($product['options_title'])) . '</span>';
                                }
                            }
                        ?>
                        <tr>
                            <td><?=$product_image?></td>
                            <td>
                                <?=$product_title?>
                                <?php if (!empty($product['product_sku'])) { ?>
                                <br><?=Lang::t('accountOrder.tip.productCode')?> <?=$product['product_sku']?>
                                <?php } ?>
                                <?=$variants?>
                                <?=$options?>
                                <?=$properties?>
                            </td>
                            <td>
                                <?=$product['price']?> грн.
                            </td>
                            <td><?=$product['quantity']?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php } else { ?>
                <p><?=Lang::t('accountOrder.tip.noRecordsFound')?></p>
                <?php } ?>
            </div>
        </div>
    </div>
</div>