<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<div class="breadcrumbs">
	<a href="<?=Yii::app()->homeUrl?>"><?=Lang::t('layout.link.breadcrumbsHome')?></a> 
	<span class="b-sep">|</span> 
	<?=Category::renderBreadrumbs($this, $categories, $categories[$category['category_id']]['parents'], $category['category_id'])?>
</div>

<div class="catalog-category clearfix">
	<?php if ($facets->hasFacets()) { ?>
	<div class="cc-side">
		<a id="filter-mobile" class="filter-mobile" href="#"><?=Lang::t('category.title.filter')?> <i class="icon icon-filter"></i></a>
		<form id="filter" class="filter mobile-hidden" method="get">
			<div class="f-title"><?=Lang::t('category.title.filter')?></div>
			<?php $this->renderPartial('facets', array('facets' => $facets, 'sort' => $sort)); ?>
		</form>
	</div>
	<div class="cc-content">
	<?php } ?>
		<div class="catalog-head clearfix">
			<h1 class="ch-title"><?=CHtml::encode($category['category_name'])?></h1>
			<?php if (false && !empty($collections)) { ?>
			<?php
				$sort_params = array(
					'popular' => array(
						'title' => Lang::t('category.link.sortPopularity'),
						'url' => null, // default value
					),
					'price-asc' => array(
						'title' => Lang::t('category.link.sortPriceAsc'),
						'url' => 'price-asc',
					),
					'price-desc' => array(
						'title' => Lang::t('category.link.sortPriceDesc'),
						'url' => 'price-desc',
					),
				);
			?>
			<ul class="ch-sort list-inline">
				<li class="chs-title"><?=Lang::t('category.title.sortBy')?></li>
				<?php foreach ($sort_params as $sort_index => $sort_param) { ?>
				<?php if ($sort_index == $sort) { ?>
				<li><span class="active"><?=CHtml::encode($sort_param['title'])?></span></li>
				<?php } else { ?>
				<?php 
					$sort_url_params = array('alias' => $category['category_alias']);

					if (!empty($sort_param['url'])) {
						$sort_url_params = array('alias' => $category['category_alias'], 'sort' => $sort_param['url']);
					}

					if (!empty($filters)) {
						$sort_url_params['filter'] = $filters;
					}

					$sort_url = $this->createUrl('category', $sort_url_params);
				?>
				<li><a href="<?=$sort_url?>"><?=CHtml::encode($sort_param['title'])?></a></li>
				<?php } ?>
				<?php } ?>
			</ul>
			<?php } ?>
		</div>

		<?php if ($facets->hasFilter()) { ?>
		<?php $this->renderPartial('facetsReset', array('facets' => $facets, 'category' => $category, 'sort' => $sort)); ?>
		<?php } ?>

		<?php if (!empty($collections)) { ?>
		<div id="catalog-list" class="catalog-list">
			<?php $this->renderPartial('collectionsList', array('collections' => $collections, 'category' => $category)); ?>
		</div>
		<?php if ($pages->getPageCount() > 1) { ?>
		<div class="catalog-pagination text-center">
		<?php
			$this->widget('LinkPager', array(
				'pages' => $pages,
				'maxButtonCount' => 7,
				'htmlOptions' => array(
					'class' => 'list-inline',
				),
			));
		?>
		</div>
		<?php } ?>
		<?php } else { ?>
		<p><?=Lang::t('category.tip.noCollections')?></p>
		<?php } ?>
	<?php if ($facets->hasFacets()) { ?>
	</div>
	<?php } ?>
</div>