<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
	$staticUrl = $assetsUrl . '/static/' . Yii::app()->params->settings['rev'];
?>
<main class="school">
	<div class="wrap">
		<?php $this->widget('application.components.Breadcrumbs.Breadcrumbs', ['breadcrumbs' => $this->breadcrumbs]); ?>

		<h1 class="school__title"><?=$this->pageTitle?></h1>

		<div class="school__img">
			<picture>
				<!-- <source type="image/webp" srcset="<?=$staticUrl?>/images/school_promo.webp, <?=$staticUrl?>/images/school_promo@2x.webp 2x"> -->
				<source srcset="<?=$staticUrl?>/images/blck_school.jpg, <?=$staticUrl?>/images/blck_school@2x.jpg 2x">
				<img src="<?=$staticUrl?>/images/blck_school.jpg" alt="">
			</picture>
		</div>

		<div class="school__text">
			<div class="shool__tip"><?=Lang::t('school.tip.introTitle')?></div>
			<div class="school__description">
				<?=str_replace(["\r", "\n"], ["", "<br>\n"], Lang::t('school.tip.introText'))?>
			</div>
		</div>

		<?php if (!empty($events)) { ?>
		<section class="master-classes">
			<div class="master-classes__title"><?=Lang::t('school.tip.masterClassesTitle')?></div>

			<div class="master-classes__list">
				<?php foreach ($events as $event) { ?>
				<div class="master-class-card">
					<div class="master-class-card__title"><?=CHtml::encode($event['course_name'])?></div>
					<div class="master-class-card__details"><?//=Lang::t('school.tip.hours', $event['course_duration'])?> <!-- / --> <?=round($event['course_price'])?> <?=Lang::t('layout.tip.uah')?></div>
					<div class="master-class-card__more"><a href="<?=$this->createUrl('course', ['alias' => $event['course_alias']])?>"><?=Lang::t('school.link.more')?> &gt;</a></div>
				</div>
				<?php } ?>
			</div>
		</section>
		<!-- /.master-classes -->
		<?php } ?>

		<?php if (!empty($courses)) { ?>
		<section class="courses">
			<div class="courses__title"><?=Lang::t('school.tip.coursesTitle')?></div>

			<div class="courses__list">
				<?php foreach ($courses as $course) { ?>
				<div class="course-card">
					<div class="course-card__title"><?=CHtml::encode($course['course_name'])?></div>
					<div class="course-card__description"><?=CHtml::encode($course['course_tip'])?></div>
					<div class="course-card__details">
						<div><?=round($course['course_price'])?> <?=Lang::t('layout.tip.uah')?><?php /* / міс */ ?></div>
						<?php /* <div><?=Lang::t('course.tip.monthes', $course['course_duration'])?></div>
						<div><?=Lang::t('course.tip.perWeek', $course['course_duration_week'])?></div> */ ?>
					</div>
					<div class="course-card__extras">
						<div class="course-card__author"><?=implode(', ', $course['authors'])?></div>
						<div class="course-card__start"><?=Lang::t('course.tip.start')?> <?=Yii::app()->dateFormatter->format('d MMMM yyyy', strtotime($course['course_date']))?></div>
						<div class="course-card__more"><a href="<?=$this->createUrl('course', ['alias' => $course['course_alias']])?>"><?=str_replace(' ', '&nbsp;', Lang::t('school.link.more'))?>&nbsp;&gt;</a></div>
					</div>
				</div>
				<?php } ?>
			</div>
		</section>
		<!-- /.courses -->
		<?php } ?>

		<div class="school__extra">
			<div class="school__extra-title"><?=Lang::t('school.tip.education')?></div>
			<div class="school__extra-description">
				<?=Lang::t('school.html.education')?>
			</div>
		</div>

		<?php if (!empty($authors)) { ?>
		<div class="lectors">
			<div class="lectors__title"><?=Lang::t('school.tip.lectors')?></div>
			
			<div class="lectors__list">
				<?php foreach ($authors as $author) { ?>
				<div class="lector-card">
					<div class="lector-card__photo">
						<?php if (!empty($author['author_image'])) { ?>
						<?php
							$author_image = '';
							$author_image_2x = '';
						
							if (!empty($author['author_image'])) {
								$author_image = json_decode($author['author_image'], true);
								$author_image = $assetsUrl . '/author/' . $author['author_id'] . '/' . $author_image['1x']['path'];
								
								if (!empty($author_image['2x'])) {
									$author_image_2x = $assetsUrl . '/author/' . $author['author_id'] . '/' . $author_image['2x']['path'];
								}
							}
						?>
						<picture>
							<?php /* <source type="image/webp" srcset="images/lector_1.webp, images/lector_1@2x.webp 2x"> */ ?>
							<?php if (!empty($author_image_2x)) { ?>
							<source srcset="<?=$author_image?>, <?=$author_image_2x?> 2x">
							<?php } ?>
							<img src="<?=$author_image?>" alt="">
						</picture>
						<?php } ?>
					</div>
					<div class="lector-card__title"><?=CHtml::encode($author['author_name'])?></div>
					<div class="lector-card__bio"><?=$author['author_description']?></div>
				</div>
				<?php } ?>
			</div>
		</div>
		<?php } ?>
	</div>
</main>
