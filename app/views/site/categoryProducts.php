<?php
    /* @var $this SiteController */
    $assetsUrl = Yii::app()->assetManager->getBaseUrl();
    $staticUrl = Yii::app()->assetManager->getBaseUrl() . '/static/' . Yii::app()->params->settings['rev'];
?>
<main class="catalog">
    <div class="wrap">
        <?php $this->widget('application.components.Breadcrumbs.Breadcrumbs', ['breadcrumbs' => $this->breadcrumbs]); ?>

        <h1 class="catalog__title"><?=CHtml::encode($category['category_name'])?></h1>

        <?php if (!empty($subcategories)) { ?>
        <ul class="catalog__subcategories list-unstyled">
            <?php foreach ($subcategories as $subcategory) { ?>
            <li><a href="<?=Yii::app()->createUrl('site/category', ['alias' => $subcategory['category_alias']])?>" class="subcategory<?php if ($subcategory['category_id'] == $category['category_id']) { ?> subcategory--active<?php } ?>"><?=$subcategory['category_name']?> (<?=$subcategory['total']?>)</a></li>
            <?php } ?>
        </ul>
        <?php } ?>

        <?php if ($facets->hasFacets()) { $selected_facets = $facets->getSelectedFacets(); ?>
        <form id="filter" class="filter" action="<?=$this->createUrl('category', ['alias' => $category['category_alias']])?>" method="get">
            <a href="#" class="filter-toggle" data-hide-text="<?=Lang::t('catalog.btn.hideFilter')?>">
                <span class="filter-toggle__text"><?=Lang::t('catalog.btn.showFilter')?></span>    
                <span class="filter-toggle__counter<?php if (empty($selected_facets)) { ?> filter-toggle__counter--hidden<?php } ?>">(<?=count($selected_facets)?>)</span>    
            </a>
            <div class="filter__wrap">
                <?php $this->renderPartial('facets', array('facets' => $facets, 'sort' => $sort)); ?>
                <a href="<?=$this->createUrl('category', ['alias' => $category['category_alias']])?>" class="filter__reset<?php if (empty($selected_facets)) { ?> filter__reset--hidden<?php } ?>"><?=Lang::t('catalog.btn.reset')?><span class="filter__reset-counter">(<?=count($selected_facets)?>)</span></a>
            </div>
        </form>
        <?php } ?>

        <?php if (!empty($products)) { ?>
        <div class="products-list">
            <?php $this->renderPartial('productsList', ['products' => $products]); ?>
        </div>
        <?php } else { ?>
        <p><?=Lang::t('catalog.tip.productsNotFound')?></p>
        <?php } ?>

        <section class="questions">
            <div class="questions__title"><?=Lang::t('layout.tip.questions')?></div>
            <div class="questions__tip">
                <ul class="questions__contacts list-unstyled">
                    <li><span><?=Lang::t('about.tip.call')?>:</span> <a href="tel:+<?=preg_replace('#[^\d]#', '', CHtml::encode(Yii::app()->params->settings['phone']))?>"><?=CHtml::encode(Yii::app()->params->settings['phone'])?></a></li>
                    <li><span><?=Lang::t('about.tip.write')?>:</span> <a href="mailto:<?=CHtml::encode(Yii::app()->params->settings['mail'])?>"><?=CHtml::encode(Yii::app()->params->settings['mail'])?></a></li>
                    <li><?=Lang::t('layout.tip.glad')?></li>
                </ul>
            </div>
        </section>
    </div>
</main>

<?php /*
<div class="wrap">
    <div class="content-divider content-divider--top"></div>

    <!-- <div class="breadcrumbs">
        <a href="<?=Yii::app()->homeUrl?>">Главная</a> 
        / 
        <?=Category::renderBreadrumbs($this, $categories, $categories[$category['category_id']]['parents'], $category['category_id'])?>
    </div> -->

    <div class="category">
        <div class="category__side">
            <div class="category__side-block side-block">
                <div class="side-block__title">Категории</div>
                <ul class="side-block__categories list-unstyled">
                    <li><a href="<?=$this->createUrl('newest')?>">Новинки</a></li>
                    <li><a href="<?=$this->createUrl('brands')?>">Бренды</a></li>
                    <?=Category::buildCategoriesTree($this, $categories_tree, $category['category_id'])?>
                    <?php if (Yii::app()->params->has_sale) { ?>
                    <li><a href="<?=$this->createUrl('sale')?>" style="color:#ff3700">SALE</a></li>
                    <?php } ?>
                </ul>
            </div>

            <?php if ($facets->hasFacets()) { ?>
            <form id="filter" class="filter" action="<?=$this->createUrl('category', array('alias' => $category['category_alias']))?>" method="get">
			    <?php $this->renderPartial('facets', array('facets' => $facets, 'sort' => $sort)); ?>
            </form>
            <?php } ?>
        </div>
        <div class="category__content">
            <h1 class="category__title"><?=CHtml::encode($category['category_name'])?></h1>
            <?php if ($facets->hasFilter()) { ?>
            <?php $this->renderPartial('facetsReset', array('facets' => $facets, 'category' => $category, 'sort' => $sort)); ?>
            <?php } ?>
            <?php if (!empty($products)) { ?>
			<?php
				$sort_params = array(
					'popular' => array(
						'title' => 'по новизне',
						'url' => null, // default value
					),
					'price-asc' => array(
						'title' => 'сначала дешевые',
						'url' => 'price-asc',
					),
					'price-desc' => array(
						'title' => 'сначала дорогие',
						'url' => 'price-desc',
					),
				);
			?>
			<ul class="category__sort">
				<li class="category__sort-title">Сортировать:</li>
				<?php foreach ($sort_params as $sort_index => $sort_param) { ?>
				<?php if ($sort_index == $sort) { ?>
				<li><span class="active"><?=CHtml::encode($sort_param['title'])?></span></li>
				<?php } else { ?>
				<?php 
					$sort_url_params = array('alias' => $category['category_alias']);

					if (!empty($sort_param['url'])) {
						$sort_url_params = array('alias' => $category['category_alias'], 'sort' => $sort_param['url']);
					}

					if (!empty($filters)) {
						$sort_url_params['filter'] = $filters;
					}

					$sort_url = $this->createUrl('category', $sort_url_params);
				?>
				<li><a href="<?=$sort_url?>"><?=CHtml::encode($sort_param['title'])?></a></li>
				<?php } ?>
				<?php } ?>
            </ul>
            <div class="catalog">
                <?php $this->renderPartial('productsList', array('products' => $products)); ?>
            </div>
            <?php if (!$show_all && $pages->getPageCount() > 1) { ?>
            <div class="pagination">
                <?php
                    $this->widget('LinkPager', array(
                        'pages' => $pages,
                        'maxButtonCount' => 7,
                        'htmlOptions' => array(
                            'class' => 'list-inline',
                        ),
                    ));
                ?>
            </div>
            <div class="show-all">
                <a<?php if ($show_all) { ?> class="active"<?php } ?> href="<?=$this->createUrl('category', array_merge(['show_all' => 1], $pages->params))?>">Показать все</a>
            </div>
            <?php } ?>
            <?php } else { ?>
            <p style="font-size: 14px">Товари не знайдено.</p>
            <?php } ?>
        </div>
    </div>
</div>
*/ ?>