<?php
/* @var $this SiteController */
$assetsUrl = Yii::app()->assetManager->getBaseUrl();
$staticUrl = $assetsUrl . '/static/' . Yii::app()->params->settings['rev'];

$product_sku = $product['product_sku'];

$typeCook = Yii::app()->request->getQuery('type');

// variants
$is_variants_type = ($product['product_price_type'] == 'variants') ? true : false;

// options
$options = array();
$selected_options = array();
$options_price = 0;
$options_regular_price = 0;
$delivery_option = [];

$user = Yii::app()->user->id;

if (!empty($user)) {
    $sub = Subscription::model()->getSubByUser(Yii::app()->user->id);
}

if (!empty($product_options['options'])) {
    // prepare options lists
    foreach ($product_options['options'] as $product_option) {
        $value_id = $product_option['value_id'];

        $options[$value_id] = array(
            'option_id' => $product_option['option_id'],
            'price' => (float) $product_option['option_price'],
            'regular_price' => (float) $product_option['option_price'],
        );

        if (!empty($option_ids) && in_array($value_id, $option_ids)) {
            $selected_options[$value_id] = array(
                'option_id' => $product_option['option_id'],
            );
        }
    }
}

// setup product price and other attributes
if (empty($selected_variant)) {
    $product_price = $product['product_price'];
    $discount_price = 0;
    $price_type = $product['product_price_type'];
} else {
    $variant_id = $selected_variant['variant_id'];
    $price_type = $selected_variant['price_data']['price_type'];

    $discount_price = 0;
    $product_price = $selected_variant['price_data']['regular_price'];
}

$properties = [];
$product_subtitle = [
    'icon' => '',
    'special' => '',
];

foreach ($product_properties as $property) {
    if ($property['property_special']) {
        $product_subtitle['special'] = CHtml::encode($property['value_title']);
    }

    if ($property['property_icon']) {
        $product_subtitle['icon'] = CHtml::encode($property['value_title']);
    }

    if ($property['property_hide'] || $property['property_special'] || $property['property_icon']) {
        continue;
    }

    if ($property['is_delicious']) {
        $images = '';

        foreach ($property['delicious'] as $item) {
            $images = $images . '<img src="'. $item['delicious_icon'] . '" alt="">';
        }

        $properties[] = '<div class="product__additional-param">
			<div class="product__additional-param-title">' . CHtml::encode($property['property_title']) . '</div>
			<div class="product__additional-param-value">'. $images . '</div>
		</div>';
    }

    if (count($product_options['properties']) == 0) {
        $properties[] = '<div class="product__additional-param">
			<div class="product__additional-param-title">' . CHtml::encode($property['property_title']) . '</div>
			<div class="product__additional-param-value">' . mb_strtoupper(CHtml::encode($property['value_title']), 'utf-8') . '</div>
		</div>';
    }
}
?>

<style>
    /* Style the tab */
    .tab {
        overflow: hidden;
    }

    /* Style the buttons inside the tab */
    .tab button {
        width: 137px;
        height: 32px;

        background-color: white;
        float: left;
        outline: none;
        cursor: pointer;
        transition: 0.3s;
        font-size: 17px;
        text-align: center;

        border: 1px solid #000000;
        box-sizing: border-box;
        border-radius: 100px;

        margin-top: 15px;
    }

    .tab button:hover {
        background-color: #000000;
        color: white;
    }

    .tab button.active {
        background-color: #000000;
        color: white;
    }

    .tabcontent {
        display: none;
        /*padding: 6px 12px;*/
        /*border: 1px solid #ccc;*/
        /*border-top: none;*/
    }
    .product_qty_front {
        background: #F7F7F7;
        border: 1px solid rgba(0, 0, 0, 0.3);
        box-sizing: border-box;
        border-radius: 16px;
    }
    .btn-input {
        display: none
    }
    .qty-btn {
        margin-left: 0;
        width: 33.33%;
    }
    .btn-input:checked + .qty-btn {
        background: #FFFFFF;
        border: 1px solid #000000;
        box-sizing: border-box;
        border-radius: 16px;
    }
    input:checked + .qty-btn .qty-btn__icon .icon-package {
        background-image: url(/assets/static/v1.0.26/images/package.svg) !important;
    }
    .qty-btn .qty-btn__icon .icon-package {
        background-image: url(/assets/quiz/product_grey.svg);
    }

    .qty-btn__title {
        color: black;
        opacity: 0.6;
    }
    .btn-input:checked + .qty-btn .qty-btn__title {
        opacity: unset;
    }
    .radio_front_new {
        background: #F6F6F6;
        border: 1px solid rgba(0, 0, 0, 0.3);
        box-sizing: border-box;
        border-radius: 100px;
    }
    .product__radio {
        margin-left: 0;
        margin-top: 0;
        width: 50%;
    }
    .product__radio span {
        border: none;
    }
    .product-card__radio input:checked+span, .product__radio input:checked+span {
        background: white;
        color: black;
        border: 1px solid #000000;
        box-sizing: border-box;
        border-radius: 100px;
    }
    .btn-input:checked + .check_radio {
        background: #FFFFFF;
        border: 1px solid #000000;
        box-sizing: border-box;
        border-radius: 16px;
    }
    .product__choose-tip {
        font-size: 12px;
    }
    .product-card__info-params {
        margin-top: 0;
    }
    #gram-discount-tip {
        font-weight: 800;
        font-size: 14px;
    }
</style>

<main id="product" class="product" data-url="<?=$this->canonicalUrl?>" data-id="<?=$product['product_id']?>" data-sku="<?=$product['product_sku']?>">
    <div class="wrap">
        <?php $this->widget('application.components.Breadcrumbs.Breadcrumbs', ['breadcrumbs' => $this->breadcrumbs]); ?>

        <section class="product__section product__section--main">
            <?php if (!empty($product_photos)) { ?>
                <div id="product-gallery" class="product__gallery">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <?php foreach ($product_photos as $photo) { ?>
                                <?php
                                $photo_path = json_decode($photo['photo_path'], true);
                                $photo_size = json_decode($photo['photo_size'], true);
                                ?>
                                <div class="swiper-slide">
                                    <picture>
                                        <?php /* <source type="image/webp" srcset="images/product.webp, images/product@2x.webp 2x"> */ ?>
                                        <?php if (!empty($photo_path['large']['2x'])) { ?>
                                            <source srcset="<?=$assetsUrl?>/product/<?=$product['product_id']?>/<?=$photo_path['large']['1x']?>, <?=$assetsUrl?>/product/<?=$product['product_id']?>/<?=$photo_path['large']['2x']?> 2x">
                                        <?php } ?>
                                        <img src="<?=$assetsUrl?>/product/<?=$product['product_id']?>/<?=$photo_path['large']['1x']?>" alt="<?=CHtml::encode($product['product_title'])?>">
                                    </picture>
                                </div>
                            <?php } ?>
                        </div>

                        <?php if (count($product_photos) > 1) { ?>
                            <div class="swiper-pagination"></div>

                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>

            <div class="product__info">
                <h1 class="product__title"><?=CHtml::encode($product['product_title'])?></h1>
                <?php if (!empty($product_subtitle['icon']) || !empty($product_subtitle['special'])) { ?>
                    <div class="product__type"><?=implode(' / ', $product_subtitle)?></div>
                <?php } ?>

                <div class="tab">
                    <?php if (isset($_GET['action']) && $_GET['action'] == 'subscription') { ?>
                        <button data-test="2" class="tablinks" id="tab-btn-buy" onclick="openTab('tab-btn-buy', 'tab-buy')"><?=Lang::t('product.tab.buy')?></button>
                        <?php if ($options && $product['is_sub'] == true && !$product['is_for_parents']) { ?>
                            <button class="tablinks active" id="tab-btn-sub" style="margin-left: 5px;" onclick="openTab('tab-btn-sub', 'tab-sub')"><?=Lang::t('product.tab.sub')?></button>
                        <?php } ?>
                    <?php } else { ?>
                        <button data-test="<?= $product['is_sub'] ?>" class="tablinks active" id="tab-btn-buy" onclick="openTab('tab-btn-buy', 'tab-buy')" <?php if ($product['is_sub'] == false || count($options) == 0): ?>style="display:none"<?php endif; ?>><?=Lang::t('product.tab.buy')?></button>
                        <?php if ($options && $product['is_sub'] == true && !$product['is_for_parents']) { ?>
                            <button class="tablinks" id="tab-btn-sub" style="margin-left: 5px;" onclick="openTab('tab-btn-sub', 'tab-sub')"><?=Lang::t('product.tab.sub')?></button>
                        <?php } ?>
                    <?php } ?>
                </div>

                <?php if (isset($_GET['action']) && $_GET['action'] == 'subscription') { ?>
                <div id="tab-buy" class="tabcontent" style="display: none;">
                    <?php } else { ?>
                    <div id="tab-buy" class="tabcontent" style="display: block;">
                        <?php } ?>
                        <form action="<?=$this->createUrl('cart')?>" class="product__buy" method="post">
                            <input type="hidden" id="product-id" value="<?= $product['product_id'] ?>">
                            <?php if ($product['product_instock'] == 'out_of_stock') { ?>
                                <div class="product__total product__total--out-of-stock">
                                    <div class="product__price"><span data-price="<?=round($product['product_price'])?>"><?=number_format($product['product_price'], 0, '.', ' ')?></span> <?=Lang::t('layout.tip.uah')?></div>
                                    <div class="product__add">
                                        <?=Lang::t('product.tip.outOfStock')?>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <?php if (!empty($options)) { ?>
                                    <?php foreach ($product_options['properties'] as $property_id => $property) { ?>
                                        <div class="product__buy-section product__buy-section--option">
                                            <div class="product__group"><?=CHtml::encode($property['property_title'])?>:</div>
                                            <?php if (count($property['values']) > 2) { ?>
                                                <div class="product__buy-content">
                                                    <div class="product__option">
                                                        <div class="product__option-value"></div>
                                                        <select id="product-option" name="cart[option_id][<?=$property_id?>]" class="product__select">
                                                            <?php foreach ($property['values'] as $value) { ?>
                                                                <?php
                                                                if (!isset($options[$value['value_id']])) {
                                                                    continue;
                                                                }

                                                                $option_id = $options[$value['value_id']]['option_id'];
                                                                $option_price = $options[$value['value_id']]['price'];
                                                                $option_title = CHtml::encode($value['value_title']);

                                                                if ($option_price) {
                                                                    $option_title .= ' (+' . number_format($option_price, 0, '.', ' ') . ' ' . Lang::t('layout.tip.uah') . ')';
                                                                }
                                                                ?>
                                                                <option value="<?=$option_id?>"><?=CHtml::encode($option_title)?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <div class="product__buy-content product__buy-content--radio product_qty_front">
                                                    <?php foreach ($property['values'] as $value) { ?>
                                                        <?php
                                                        if (!isset($options[$value['value_id']])) {
                                                            continue;
                                                        }

                                                        $option_id = $options[$value['value_id']]['option_id'];
                                                        $option_price = $options[$value['value_id']]['price'];
                                                        $option_title = CHtml::encode($value['value_title']);

                                                        if ($option_price) {
                                                            $option_title .= ' (+' . number_format($option_price, 0, '.', ' ') . ' ' . Lang::t('layout.tip.uah') . ')';
                                                        }
                                                        ?>

                                                        <label for="option-<?=$option_id?>" class="product__radio test check_radio">
                                                            <input class="btn-input" id="option-<?=$option_id?>" type="radio" name="cart[option_id][<?=$property_id?>]" value="<?=$option_id?>">
                                                            <span><?=CHtml::encode($option_title)?></span>
                                                        </label>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                <?php } ?>

                                <?php if ($product['product_price_type'] == 'variants') { ?>
                                    <?php
                                    $product_price = false;
                                    $product_price_old = false;
                                    ?>
                                    <?php if (!empty($product_variants['variants'])) { ?>
                                        <?php
                                        $in_stock = false;
                                        $property_title = '';
                                        $variants = [];

                                        foreach ($product_variants['variants'] as $variant) {
                                            $variant_id = $variant['variant_id'];
                                            $values = array_values($variant['values']);

                                            $value_id = !empty($values) ? $values[0] : 0;
                                            $value_title = '';

                                            foreach ($product_variants['properties'] as $property) {
                                                foreach ($property['values'] as $value_index => $value) {
                                                    if ($value_index == $value_id) {
                                                        $property_title = $property['property_title'];
                                                        $value_title = $value['value_title'];
                                                    }
                                                }
                                            }

                                            $variants[] = [
                                                'id' => $variant_id,
                                                'title' => $value_title,
                                                'price' => $variant['variant_price'],
                                                'price_old' => $variant['variant_price_old'],
                                                'in_stock' => ($variant['variant_instock'] == 'in_stock') ? true : false,
                                            ];

                                            if ($variant['variant_instock'] == 'in_stock' && $product_price === false) {
                                                $product_price = (float) $variant['variant_price'];
                                                $product_price_old = (float) $variant['variant_price_old'];
                                            }
                                        }
                                        ?>
                                        <div id="product-variant" class="product__buy-section product__buy-section--option">
                                            <div class="product__group"><?=CHtml::encode($property_title)?>:</div>
                                            <div class="product__buy-content product__buy-content--radio radio_front_new">
                                                <?php foreach ($variants as $variant) { ?>
                                                    <label for="value-<?=$variant['id']?>" class="product__radio<?php if (!$variant['in_stock']) { ?> product-card__radio--disabled<?php } ?>" data-price="<?=number_format($variant['price'], 0, '.', ' ')?>" data-price-old="<?=number_format($variant['price_old'], 0, '.', ' ')?>">
                                                        <input id="value-<?=$variant['id']?>" type="radio" name="cart[variant_id]" value="<?=$variant['id']?>"<?php if ($variant['in_stock'] && !$in_stock) { $in_stock = true; ?> checked<?php } elseif (!$variant['in_stock']) { ?> disabled<?php } ?>>
                                                        <span><?=CHtml::encode($variant['title'])?></span>
                                                    </label>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } else { ?>
                                    <?php
                                    $product_price = (float) $product['product_price'];
                                    $product_price_old = (float) $product['product_price_old'];
                                    ?>
                                <?php } ?>

                                <div class="product__buy-section">
                                    <div class="product__group"><?=Lang::t('product.tip.qty')?>:</div>
                                    <div class="product__buy-content">
                                        <?php if (!empty($options)) { ?>
                                            <div class="product__qty-btns product_qty_front">
                                                <input class="btn-input" id="qty-1" type="radio" name="qty" value="1" checked>
                                                <label for="qty-1" class="qty-btn" data-target="buy" data-value="1">

                                                <span class="qty-btn__icon">
                                            <i class="icon-inline icon-package"></i>
                                        </span>
                                                    <span class="qty-btn__title">1</span>
                                                    <span class="qty-btn__tip"></span>
                                                </label>
                                                <input class="btn-input" id="qty-2" type="radio" name="qty" value="2">
                                                <label for="qty-2" class="qty-btn" data-target="buy" data-value="2">

                                                <span class="qty-btn__icon">
                                            <i class="icon-inline icon-package"></i>
                                            <i class="icon-inline icon-package"></i>
                                        </span>
                                                    <span class="qty-btn__title">2</span>
                                                    <!-- <span class="qty-btn__tip">Бесплатная доставка</span> -->
                                                </label>
                                                <input class="btn-input" id="qty-3" type="radio" name="qty" value="3">
                                                <label for="qty-3" class="qty-btn" data-target="buy" data-value="3">

                                                <span class="qty-btn__icon">
                                            <i class="icon-inline icon-package"></i>
                                            <i class="icon-inline icon-package"></i>
                                            <i class="icon-inline icon-package"></i>
                                        </span>
                                                    <span class="qty-btn__title">3</span>
                                                    <!-- <span class="qty-btn__tip">Бесплатная доставка<br> +15-20%</span> -->
                                                </label>
                                            </div>
                                            <?php if(empty($product['is_for_parents'])) { ?>
                                                <div class="product__qty-custom">
<!--                                                    <span>--><?//=Lang::t('product.tip.ownQuantity')?><!--</span>-->
                                                    <input type="text" name="qty" class="product__input buy" value="">
                                                </div>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <?php if(empty($product['is_for_parents'])) { ?>
                                                <div class="product__qty-custom">
                                                    <input type="text" name="qty" class="product__input" value="1">
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                        <input id="product-qty" type="hidden" name="cart[qty]" value="1">
                                    </div>
                                </div>

                                <div class="product__total">
                                    <div class="product__price">
                                        <s class="product__price-old"<?php if (empty($product_price_old)) { ?> style="display:none"<?php } ?>><span><?=number_format($product_price_old, 0, '.', ' ')?></span> <?=Lang::t('layout.tip.uah')?></s>
                                        <span id="buy-price-span" data-price="<?=number_format($product_price, 0, '.', '')?>"><?=number_format($product_price, 0, '.', ' ')?></span> <?=Lang::t('layout.tip.uah')?>
                                    </div>
                                    <div class="product__add">
                                        <input type="hidden" name="action" value="add">
                                        <input type="hidden" name="cart[product_id]" value="<?=$product['product_id']?>">

                                        <button class="product__btn btn"><?=Lang::t('product.btn.buy')?></button>
                                    </div>
                                    <?php if (!empty($options)) { ?>
                                        <div class="product__choose-tip"><?=Lang::t('product.tip.chooseToAdd')?></div>
                                    <?php } ?>
                                    <div class="product__choose-tip" id="gram-discount-tip"></div>
                                </div>
                            <?php } ?>
                        </form>
                    </div>

                    <?php if ($options && $product['is_sub'] == true) { ?>

                    <?php if (isset($_GET['action']) && $_GET['action'] == 'subscription') { ?>
                    <div id="tab-sub" class="tabcontent" style="display: block;">
                        <?php } else { ?>
                        <div id="tab-sub" class="tabcontent">
                            <?php } ?>
                            <form action="<?=$this->createUrl('site/sub')?>" name="test" class="product__buy" method="post">
                                <?php if ($product['product_instock'] == 'out_of_stock') { ?>
                                    <div class="product__total product__total--out-of-stock">
                                        <div class="product__price"><span data-price="<?=round($product['product_price'])?>"><?=number_format($product['product_price'], 0, '.', ' ')?></span> <?=Lang::t('layout.tip.uah')?></div>
                                        <div class="product__add">
                                            <?=Lang::t('product.tip.outOfStock')?>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <?php if (!empty($options)) { ?>
                                        <?php foreach ($product_options['properties'] as $property_id => $property) { ?>
                                            <div class="product__buy-section product__buy-section--option">
                                                <div class="product__group"><?=CHtml::encode($property['property_title'])?>:</div>
                                                <?php if (count($property['values']) > 2) { ?>
                                                    <div class="product__buy-content">
                                                        <div class="product__option">
                                                            <div class="product__option-value"></div>
                                                            <select name="cart[option_id][<?=$property_id?>]" class="product__select">
                                                                <?php foreach ($property['values'] as $value) { ?>
                                                                    <?php
                                                                    if (!isset($options[$value['value_id']])) {
                                                                        continue;
                                                                    }

                                                                    $option_id = $options[$value['value_id']]['option_id'];
                                                                    $option_price = $options[$value['value_id']]['price'];
                                                                    $option_title = CHtml::encode($value['value_title']);

                                                                    if ($option_price) {
                                                                        $option_title .= ' (+' . number_format($option_price, 0, '.', ' ') . ' ' . Lang::t('layout.tip.uah') . ')';
                                                                    }
                                                                    if ($typeCook == $option_title) {
                                                                        ?>
                                                                        <option value="<?=$option_id?>"><?=CHtml::encode($option_title)?></option>
                                                                    <?php }} ?>
                                                                <?php foreach ($property['values'] as $value) { ?>
                                                                    <?php
                                                                    if (!isset($options[$value['value_id']])) {
                                                                        continue;
                                                                    }

                                                                    $option_id = $options[$value['value_id']]['option_id'];
                                                                    $option_price = $options[$value['value_id']]['price'];
                                                                    $option_title = CHtml::encode($value['value_title']);

                                                                    if ($option_price) {
                                                                        $option_title .= ' (+' . number_format($option_price, 0, '.', ' ') . ' ' . Lang::t('layout.tip.uah') . ')';
                                                                    }
                                                                    if ($option_title != $typeCook) {
                                                                        ?>
                                                                        <option value="<?=$option_id?>"><?=CHtml::encode($option_title)?></option>
                                                                    <?php }} ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="product__buy-content product__buy-content--radio product_qty_front">
                                                        <?php foreach ($property['values'] as $value) { ?>
                                                            <?php
                                                            if (!isset($options[$value['value_id']])) {
                                                                continue;
                                                            }

                                                            $option_id = $options[$value['value_id']]['option_id'];
                                                            $option_price = $options[$value['value_id']]['price'];
                                                            $option_title = CHtml::encode($value['value_title']);

                                                            if ($option_price) {
                                                                $option_title .= ' (+' . number_format($option_price, 0, '.', ' ') . ' ' . Lang::t('layout.tip.uah') . ')';
                                                            }
                                                            ?>

                                                            <label for="option-<?=$option_id?>-sub" class="product__radio check_radio">
                                                                <input class="btn-input" id="option-<?=$option_id?>-sub" type="radio" name="cart[option_id][<?=$property_id?>]" value="<?=$option_id?>">
                                                                <span><?=CHtml::encode($option_title)?></span>
                                                            </label>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>

                                    <?php if ($product['product_price_type'] == 'variants') { ?>
                                        <?php
                                        $product_price = false;
                                        $product_price_old = false;
                                        ?>
                                        <?php if (!empty($product_variants['variants'])) { ?>
                                            <?php
                                            $in_stock = false;
                                            $property_title = '';
                                            $variants = [];

                                            foreach ($product_variants['variants'] as $variant) {
                                                $variant_id = $variant['variant_id'];
                                                $values = array_values($variant['values']);

                                                $value_id = !empty($values) ? $values[0] : 0;
                                                $value_title = '';

                                                foreach ($product_variants['properties'] as $property) {
                                                    foreach ($property['values'] as $value_index => $value) {
                                                        if ($value_index == $value_id) {
                                                            $property_title = $property['property_title'];
                                                            $value_title = $value['value_title'];
                                                        }
                                                    }
                                                }

                                                $variants[] = [
                                                    'id' => $variant_id,
                                                    'title' => $value_title,
                                                    'price' => $variant['variant_price'],
                                                    'price_old' => $variant['variant_price_old'],
                                                    'in_stock' => ($variant['variant_instock'] == 'in_stock') ? true : false,
                                                ];

                                                if ($variant['variant_instock'] == 'in_stock' && $product_price === false) {
                                                    $product_price = (float) $variant['variant_price'];
                                                    $product_price_old = (float) $variant['variant_price_old'];
                                                }
                                            }
                                            $count = 0;
                                            ?>
                                            <div id="product-variant" class="product__buy-section product__buy-section--option">
                                                <div class="product__group"><?=CHtml::encode($property_title)?>:</div>
                                                <div class="product__buy-content product__buy-content--radio">
                                                    <?php foreach ($variants as $variant) {  if ($count == 0) {?>
                                                        <label for="value-<?=$variant['id']?>-sub" class="product__radio<?php if (!$variant['in_stock']) { ?> product-card__radio--disabled<?php } ?>" data-price="<?=number_format($variant['price'], 0, '.', ' ')?>" data-price-old="<?=number_format($variant['price_old'], 0, '.', ' ')?>">
                                                            <input id="value-<?=$variant['id']?>-sub" type="radio" name="cart[variant_id]" value="<?=$variant['id']?>"<?php if ($variant['in_stock'] && !$in_stock) { $in_stock = true; ?> checked<?php } elseif (!$variant['in_stock']) { ?> disabled<?php } ?>>
                                                            <span><?=CHtml::encode($variant['title'])?></span>
                                                        </label>
                                                    <?php } $count++; } ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <?php
                                        $product_price = (float) $product['product_price'];
                                        $product_price_old = (float) $product['product_price_old'];
                                        ?>
                                    <?php } ?>

                                    <div class="product__buy-section">
                                        <div class="product__group"><?=Lang::t('product.tip.qty')?>:</div>
                                        <div class="product__buy-content">
                                            <?php if (!empty($options)) { ?>
                                                <div class="product__qty-btns product_qty_front">
                                                    <input class="btn-input" id="qty-1-sub" class="test" type="radio" name="qty-sub" value="1">
                                                    <label for="qty-1-sub" class="qty-btn" data-target="sub" data-value="1">

                                                <span class="qty-btn__icon">
                                            <i class="icon-inline icon-package"></i>
                                        </span>
                                                        <span class="qty-btn__title">1</span>
                                                        <span class="qty-btn__tip"></span>
                                                    </label>
                                                    <input class="btn-input" id="qty-2-sub" class="test" type="radio" name="qty-sub" value="2">
                                                    <label for="qty-2-sub" class="qty-btn" data-target="sub" data-value="2">

                                                <span class="qty-btn__icon">
                                            <i class="icon-inline icon-package"></i>
                                            <i class="icon-inline icon-package"></i>
                                        </span>
                                                        <span class="qty-btn__title">2</span>
                                                        <!-- <span class="qty-btn__tip">Бесплатная доставка</span> -->
                                                    </label>
                                                    <input class="btn-input" id="qty-3-sub" class="test" type="radio" name="qty-sub" value="3">
                                                    <label for="qty-3-sub" class="qty-btn" data-target="sub" data-value="3">

                                                <span class="qty-btn__icon">
                                            <i class="icon-inline icon-package"></i>
                                            <i class="icon-inline icon-package"></i>
                                            <i class="icon-inline icon-package"></i>
                                        </span>
                                                        <span class="qty-btn__title">3</span>
                                                        <!-- <span class="qty-btn__tip">Бесплатная доставка<br> +15-20%</span> -->
                                                    </label>
                                                </div>
                                                <div class="product__qty-custom">
<!--                                                    <span>--><?//=Lang::t('product.tip.ownQuantity')?><!--</span>-->
                                                    <input type="text" name="qty-sub-custom" class="product__input sub" value="">
                                                </div>
                                            <?php } else { ?>
                                                <div class="product__qty-custom">
                                                    <input type="text" name="qty-sub-custom" class="product__input-sub" value="1">
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <?php
                                    foreach ($deliveryFrequency as $item) {
                                        switch (Yii::app()->language) {
                                            case 'en': {
                                                $delivery_option[] = '<option value="' . $item['frequency'] . '">' . $item['title_en'] . '</option>';
                                                break;
                                            }
                                            case 'uk': {
                                                $delivery_option[] = '<option value="' . $item['frequency'] . '">' . $item['title_uk'] . '</option>';
                                                break;
                                            }
                                            case 'ru': {
                                                $delivery_option[] = '<option value="' . $item['frequency'] . '">' . $item['title_ru'] . '</option>';
                                                break;
                                            }
                                            default: {
                                                $delivery_option[] = '<option value="' . $item['frequency'] . '">' . $item['title_uk'] . '</option>';
                                                break;
                                            }
                                        }
                                    }
                                    ?>
                                    <div class="product__buy-section product__buy-section--option">
                                        <div class="product__group">Частота</div>
                                        <div class="product__buy-content">
                                            <div class="product__option">
                                                <div class="product__option-value">раз в тиждень</div>
                                                <select name="cart[frequency]" class="product__select">
                                                    <?=implode("\n", $delivery_option)?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="product__total">
                                        <div class="product__price">
                                            <!--                                    <div style="width: 250px; line-height: 15px;">-->
                                            <!--                                        <small style="font-size: 15px; color: #0098FF;">--><?//=Lang::t('product.textunder.price')?><!--</small>-->
                                            <!--                                    </div>-->
                                            <s class="product__price-old"<?php if (empty($product_price_old)) { ?> style="display:none"<?php } ?>><span><?=number_format($product_price_old, 0, '.', ' ')?></span> <?=Lang::t('layout.tip.uah')?></s>
                                            <?php
                                            $price = 0;
                                            if ($sub) {
                                                $price = number_format($product_price, 0, '.', ' ');
                                            } else {
                                                $price =  number_format($product_price - ($product_price * 0.2), 0, '.', ' ');
                                            }

                                            ?>
                                            <span id="sub-price-span" data-sub="<?php echo $sub ? '1' : '0'; ?>" data-price-old="<?php echo $product_price_old; ?>" data-price2="<?php echo $product_price; ?>" data-price="<?php echo $price; ?>"><?php echo $price; ?></span> <?=Lang::t('layout.tip.uah')?>
                                        </div>
                                        <div class="product__add">
                                            <input type="hidden" name="action" value="add">
                                            <input type="hidden" name="cart[product_id]" value="<?=$product['product_id']?>">

                                            <?php if (empty($sub)) { ?>
                                                <button type="submit" class="product__btn btn"><?=Lang::t('product.btn.sub')?></button>
                                            <?php } else { ?>
                                                <a href="<?php echo $this->createUrl('site/accountSubscription', ['sub' => 'exist']); ?>" class="product__btn btn"><?=Lang::t('product.btn.sub')?></a>
                                            <?php } ?>
                                        </div>
                                        <?php if (!empty($options)) { ?>
                                            <div class="product__choose-tip"><?=Lang::t('product.tip.chooseToAdd')?></div>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </form>
                        </div>
                        <?php } ?>
                        <?php if (!empty($product['product_tip'])) { ?>
                            <div class="product__tip"><?=str_replace(["\r", "\n"], ["", "<br>\n"], $product['product_tip'])?></div>
                        <?php } ?>
                    </div>
        </section>

        <section class="product__section">
            <div style="display: none;" class="product__subtitle"><?=Lang::t('product.tip.about')?></div>

            <div class="product__content">
                <div class="product__description">
                    <?=$product['product_description']?>
                </div>
                <?php if (!empty($product['product_special_title']) || !empty($properties) || !empty($product['product_tags'])) { ?>
                    <div class="product__additional">
                        <?php if (!empty($product['product_special_title'])) { ?>
                            <div class="product__additional-head">
                                <?=str_replace("\n", "<br>\n", $product['product_special_title'])?>
                            </div>
                        <?php } ?>
                        <?php if (!empty($properties) || !empty($product['product_tags'])) { ?>
                            <div class="product-card__info-params">
                                <?php if (!empty($properties)) { ?>
                                    <?=implode("\n", $properties)?>
                                <?php } ?>
                                <div id="product-card__info-params">

                                </div>
                                <?php if (!empty($product['product_tags']) && count($product_options['properties']) == 0) { $tags = explode("\n", str_replace(["\r", "#"], '', $product['product_tags'])); ?>
                                    <div class="product__additional-param product__additional-param--taste">
                                        <div class="product__additional-param-title"><?=Lang::t('catalog.tip.taste')?></div>
                                        <div class="product__additional-param-value"><?=CHtml::encode(implode(', ', $tags))?></div>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <?php /* if (!empty($product['product_tags'])) { $tags = explode("\n", $product['product_tags']); ?>
					<div class="product__additional-tags">
						<?php foreach ($tags as $tag) { ?>
						<span><?=CHtml::encode($tag)?></span>
						<?php } ?>
					</ul>
					<?php } */ ?>
                    </div>
                <?php } ?>
            </div>
        </section>

        <section class="product__section">
            <div class="product__subtitle"><?=Lang::t('product.tip.delivery')?></div>

            <ol class="product__delivery list-unstyled">
                <li>
                    <span class="product__delivery-title"><?=Lang::t('product.tip.pickupTitle')?></span>
                    <span class="product__delivery-tip"><?=Lang::t('product.tip.pickup')?></span>
                </li>
                <li>
                    <span class="product__delivery-title"><?=Lang::t('product.tip.courierTitle')?> <?php if (!empty(Yii::app()->params->settings['courier'])) { ?><small>(<?=Yii::app()->params->settings['courier']?> <?=Lang::t('layout.tip.uah')?>)</small><?php } ?></span>
                    <span class="product__delivery-tip"><?=Lang::t('product.tip.courier')?></span>
                    <strong><?=Lang::t('quiz.tip.sub.delivery')?></strong>
                </li>
                <li>
                    <span class="product__delivery-title"><?=Lang::t('product.tip.npTitle')?></span>
                    <span class="product__delivery-tip"><?=Lang::t('product.tip.np')?></span>
                    <strong><?=Lang::t('quiz.tip.sub.delivery')?></strong>
                </li>
            </ol>
        </section>

        <?php if (!empty($related_products)) { ?>
            <section class="product__section product__section--related">
                <div class="product__subtitle"><?=Lang::t('product.tip.related')?></div>

                <div class="products-list">
                    <?php $this->renderPartial('productsList', array('products' => $related_products)); ?>
                </div>
            </section>
        <?php } ?>

        <section class="questions">
            <div class="questions__title"><?=Lang::t('layout.tip.questions')?></div>
            <div class="questions__tip">
                <ul class="questions__contacts list-unstyled">
                    <li><span><?=Lang::t('about.tip.call')?>:</span> <a href="tel:+<?=preg_replace('#[^\d]#', '', CHtml::encode(Yii::app()->params->settings['phone']))?>"><?=CHtml::encode(Yii::app()->params->settings['phone'])?></a></li>
                    <li><span><?=Lang::t('about.tip.write')?>:</span> <a href="mailto:<?=CHtml::encode(Yii::app()->params->settings['mail'])?>"><?=CHtml::encode(Yii::app()->params->settings['mail'])?></a></li>
                    <li><?=Lang::t('layout.tip.glad')?></li>
                </ul>
            </div>
        </section>
    </div>
</main>

<script src="https://code.jquery.com/jquery-3.5.1.slim.js" integrity="sha256-DrT5NfxfbHvMHux31Lkhxg42LY6of8TaYyK50jnxRnM=" crossorigin="anonymous"></script>
<script>
    const dataLayer = window.dataLayer || [];
    dataLayer.push({
        'event': 'view_item',
        'value': '<?php echo round($product['product_price']) ?>',
        'items':[{
            'id': '<?php echo $product['product_id'] ?>',
            'google_business_vertical': 'retail'
        }]
    });

    function openTab(btn, tab) {
        const tabContent = document.getElementsByClassName("tabcontent");

        for (let i = 0; i < tabContent.length; i++) {
            tabContent[i].style.display = "none";
        }
        const tabLinks = document.getElementsByClassName("tablinks");

        for (let i = 0; i < tabLinks.length; i++) {
            tabLinks[i].className = tabLinks[i].className.replace(" active", "");
        }

        document.getElementById(tab).style.display = "block";
        document.getElementById(btn).className += " active";

        if(tab === 'tab-sub') {
            document.querySelector('label[for="qty-1-sub"]').click()
        }
    }

    window.onload = function () {
        getProductProperties($('#product-option').val());
    }

    $('#product-option').on('change', function() {
        getProductProperties(this.value);
    });

    function getProductProperties(option) {
        let currentUrl = window.location.href;
        let url = new URL(currentUrl);

        let requestUrl = '';

        if (url.pathname.includes('en/')) {
            requestUrl = new URL('/en/product-properties', url);
        } else if(url.pathname.includes('ru/')) {
            requestUrl = new URL('/ru/product-properties', url);
        } else  {
            requestUrl = new URL('/product-properties', url);
        }
        $.post(requestUrl, {data: {option: option, product: $("#product-id").val()}}, function (response) {
            const result = JSON.parse(response);

            clearProductProperties();

            if (result.data.length > 0) {
                createProductProperties(result.data);
            }
        });
    }

    function clearProductProperties()
    {
        const body = document.getElementById('product-card__info-params');
        body.innerHTML = '';
    }

    function createProductProperties(data) {
        const body = document.getElementById('product-card__info-params');

        let titleNotes = '';
        let valueNotes = '';

        for (let i = 0; i < data.length; i++) {

            if (data[i].title === 'Вкусовые ноты' || data[i].title === 'Flavor notes' || data[i].title === 'Смакові ноти') {
                titleNotes = data[i].title;
                if (valueNotes === '') {
                    valueNotes = data[i].value;
                } else {
                    valueNotes = valueNotes + ', ' + data[i].value;
                }
            } else {
                const div = document.createElement('div');
                div.className = 'product__additional-param';

                const title = document.createElement('div');
                title.className = 'product__additional-param-title';
                title.textContent = data[i].title;

                const value = document.createElement('div');
                value.className = 'product__additional-param-value';
                value.textContent = data[i].value;

                body.appendChild(div);
                div.appendChild(title);
                div.appendChild(value);
            }
        }

        const div = document.createElement('div');
        div.className = 'product__additional-param';

        const title = document.createElement('div');
        title.className = 'product__additional-param-title';
        title.textContent = titleNotes;

        const value = document.createElement('div');
        value.className = 'product__additional-param-value';
        value.textContent = valueNotes;

        body.appendChild(div);
        div.appendChild(title);
        div.appendChild(value);
    }

    let price = 0;

    $(".qty-btn").click(function () {
        const element = $(this);
        if (element.data('target') === 'buy') {
            if (price === 0) {
                $("#buy-price-span").text($("#buy-price-span").data('price') * element.data('value'));
            } else {
                if (isNaN(price) && price.includes(' ')) {
                    price = price.replace(/\s/g, "");
                }

                $("#buy-price-span").text(price * element.data('value'));
            }
        } else if (element.data('target') === 'sub') {
            if($("#sub-price-span").data('price-old') != '0') {
                $('#tab-sub .product__price-old').hide();
                $("#sub-price-span").text($("#sub-price-span").data('price2'));
            } else {
                $("#sub-price-span").text($("#sub-price-span").data('price') * element.data('value'));
            }
        }
    });

    $(".buy").on("keyup", function() {
        const element = $(this);
        if ($.isNumeric(element.val())) {
            if (price === 0) {
                $("#buy-price-span").text($("#buy-price-span").data('price') * element.val());
            } else {
                $("#buy-price-span").text(price * element.val());
            }
        }
    });

    $(".sub").on("keyup", function() {
        const element = $(this);
        if ($.isNumeric(element.val())) {
            $("#sub-price-span").text($("#sub-price-span").data('price') * element.val());
        }
    });

    $('.product__radio').change(function() {
        if ($(this).data('price')) {
            price = $(this).data('price');
            setTimeout(function () {
                if (price === 0) {
                    $("#buy-price-span").text($("#buy-price-span").data('price') * 1);
                    // $('.product__price-old').show();
                } else {
                    if (isNaN(price) && price.includes(' ')) {
                        price = price.replace(/\s/g, "");
                    }
                    $("#buy-price-span").text(price);
                    // $('.product__price-old').show();
                }
                $("#sub-price-span").text($("#sub-price-span").data('price'));
                // $('.product__price-old').hide();
            }, 200);
        }
    });

    $('.product__add').click(function (event) {
        const id = $(this).find('input[name="cart[product_id]"]').val();
        const value = $('#buy-price-span').attr('data-price');
        const dataLayer = window.dataLayer || [];
        dataLayer.push({
            'event': 'add_to_cart',
            'value': `${value}`,
            'items':[{
                'id': `${id}`,
                'google_business_vertical': 'retail'
            }]
        });
    });

</script>
