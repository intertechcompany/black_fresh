<?php
/* @var $this SiteController */
$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<?php foreach ($products as $product) { ?>
    <div style="width: 250px;display: table; margin: 0 auto;">
        <?php
        $product_url = $this->createUrl('site/product', ['alias' => $product['product_alias']]);

        if (!empty($product['product_photo'])) {
            $product_image = json_decode($product['product_photo'], true);
            $product_image_size_2x = $product_image['size']['catalog']['2x'];
            $product_image_2x = $assetsUrl . '/product/' . $product['product_id'] . '/' . $product_image['path']['catalog']['2x'];
            $product_image_size_1x = $product_image['size']['catalog']['1x'];
            $product_image_1x = $assetsUrl . '/product/' . $product['product_id'] . '/' . $product_image['path']['catalog']['1x'];
        } else {
            $product_image_2x = '';
            $product_image_1x = '';
        }

        $product_price = (float) $product['product_price'];
        $product_price_old = (float) $product['product_price_old'];
        // $discount_price = Product::getDiscountPrice($product_price, $product['category_id'], $product['brand_id']);

        $product_special = '';
        $product_icon = '';
        ?>
        <div class="product-card__img">
            <a href="<?=$product_url?>">
                <?php if (!empty($product_image_1x)) { ?>
                    <picture>
                        <?php /* <source type="image/webp" srcset="images/p1.webp, images/p1@2x.webp 2x"> */ ?>
                        <source srcset="<?=$product_image_1x?>, <?=$product_image_2x?> 2x">
                        <img style="z-index: 1;" src="<?=$product_image_1x?>" alt="<?=CHtml::encode($product['product_title'])?>">
                        <?php if (isset($new)){ ?> <div style="position: absolute; top: 8px; right: 11px; z-index: 2; background-color: white; border-radius: 25px; width: 82px; border: 1px solid var(--black-color); height: 27px;"><?= Lang::t('layout.see.product.new') ?></div> <?php } ?>
                    </picture>
                <?php } ?>
            </a>
            <?php
            $properties = [];

            if (!empty($product['props'])) {
                foreach ($product['props'] as $property) {
                    if ($property['property_special']) {
                        $product_special = CHtml::encode($property['value_title']);
                    }

                    if ($property['property_icon']) {
                        $product_icon = $property['value_icon'];
                    }

                    if ($property['property_hide'] || $property['property_special'] || $property['property_icon']) {
                        continue;
                    }

                    $properties[] = '<div class="product-card__info-param">
						<div class="product-card__info-param-title">' . CHtml::encode($property['property_title']) . '</div>
						<div class="product-card__info-param-value">' . mb_strtoupper(CHtml::encode($property['value_title']), 'utf-8') . '</div>
					</div>';
                }
            }
            ?>
            <?php if ($product['product_extended']) { ?>
                <div class="product-card__info">
                    <div class="product-card__info-head">
                        <?php if (!empty($product['product_special_title'])) { ?>
                            <div class="product-card__info-title">
                                <?=str_replace("\n", "<br>\n", $product['product_special_title'])?>
                            </div>
                        <?php } ?>
                        <?php if (!empty($product['product_special_tip'])) { ?>
                            <div class="product-card__info-tip">
                                <?=str_replace("\n", "<br>\n", $product['product_special_tip'])?>
                            </div>
                        <?php } ?>
                    </div>
                    <?php if (!empty($product['props']) || !empty($product['product_tags'])) { ?>
                        <div class="product-card__info-params">
                            <?php if (!empty($product['props'])) { ?>
                                <?=implode("\n", $properties)?>
                            <?php } ?>
                            <?php if (!empty($product['product_tags'])) { $tags = explode("\n", str_replace(["\r", "#"], '', $product['product_tags'])); ?>
                                <div class="product-card__info-param product-card__info-param--taste">
                                    <div class="product-card__info-param-title"><?=Lang::t('catalog.tip.taste')?></div>
                                    <div class="product-card__info-param-value"><?=CHtml::encode(implode(', ', $tags))?></div>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <div class="product-card__info-bottom">
                        <?php if (!empty($product_icon)) { ?>
                            <?php if ($product_icon == 'espresso') { ?>
                                <div class="product-card__info-param">
                                    <div class="product-card__info-param-title">Roasted for</div>
                                    <div class="product-card__info-param-value">Espresso</div>
                                </div>
                                <div class="product-card__info-roasted">
                                    <i class="icon-inline icon-espresso"></i>
                                </div>
                            <?php } elseif ($product_icon == 'filter') { ?>
                                <div class="product-card__info-param">
                                    <div class="product-card__info-param-title">Roasted for</div>
                                    <div class="product-card__info-param-value">Filter</div>
                                </div>
                                <div class="product-card__info-roasted">
                                    <i class="icon-inline icon-filter"></i>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
            <?php if ($this->route != 'site/index' && !empty($product_special)) { ?>
                <div class="product-card__type"><?=mb_strtolower(CHtml::encode($product_special), 'utf-8')?></div>
            <?php } ?>
        </div>
        <?php if ($this->route == 'site/index' && !empty($product_special)) { ?>
            <div class="product-card__category"><?=CHtml::encode($product_special)?></div>
        <?php } ?>
    </div>



<?php } ?>