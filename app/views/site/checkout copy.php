<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
	$staticUrl = Yii::app()->assetManager->getBaseUrl() . '/static/' . Yii::app()->params->settings['rev'];

	if (!Yii::app()->user->isGuest) {
		$user_data = Yii::app()->user->user_data;
		
		$user = array(
			'first_name' => $user_data['user_first_name'],
			'last_name' => $user_data['user_last_name'],
			'phone' => $user_data['user_phone'],
			'email' => $user_data['user_email'],
			'zip' => $user_data['user_zip'],
			'city' => $user_data['user_city'],
			'address' => $user_data['user_address'],
			'address_2' => $user_data['user_address_2'],
		);
	}
?>
<div class="breadcrumbs">
	<a href="<?=Yii::app()->homeUrl?>"><?=Lang::t('layout.link.breadcrumbsHome')?></a> 
	<span class="b-sep">|</span> 
	<a href="<?=$this->createUrl('cart')?>"><?=Lang::t('checkout.title.cart')?></a> 
	<span class="b-sep">|</span> 
	<?=CHtml::encode($this->pageTitle)?>
</div>

<div class="order-nav list-inline">
	<li><a href="<?=$this->createUrl('site/cart')?>"><?=Lang::t('checkout.tip.navCart')?></a></li><!--
	--><li class="active"><span><?=Lang::t('checkout.tip.navCheckout')?></span></li><!--
	--><li><span><?=Lang::t('checkout.tip.navDone')?></span></li>
</div>

<div class="checkout-wrap clearfix">
	<div id="checkout" class="checkout">
		<h1><?=Lang::t('checkout.title.checkout')?></h1>

		<div class="checkout-step step-1">
			<div class="cs-head"><?=Lang::t('checkout.title.personalInformation')?></div>

			<div class="cs-content">
				<form action="<?=$this->createUrl('checkout')?>" method="post" novalidate>
					<input type="hidden" name="action" value="personal">
					
					<?php if (!empty($user)) { ?>
					<div class="form-row clearfix">
						<div class="form-col form-col-100">
							<div class="rotate-tiles">
								<label for="cs-seller-data"><input id="cs-seller-data" type="checkbox" name="order[seller_data]" value="1"<?php if (!empty($customer['seller_data'])) { ?> checked<?php } ?>><span><?=Lang::t('checkout.label.userCustomerData')?></span></label>
							</div>
						</div>
					</div>
					<?php } ?>

					<div class="form-row clearfix">
						<div class="form-col form-col-50">
							<label for="cs-name"><?=Lang::t('checkout.label.firstName')?></label>
							<input id="cs-name" type="text" name="order[first_name]" value="<?=CHtml::encode($customer['first_name'])?>"<?php if (!empty($user)) { ?> data-seller="<?=CHtml::encode($user['first_name'])?>"<?php } ?>>
						</div>
						<div class="form-col form-col-50">
							<label for="cs-lastname"><?=Lang::t('checkout.label.lastName')?></label>
							<input id="cs-lastname" type="text" name="order[last_name]" value="<?=CHtml::encode($customer['last_name'])?>"<?php if (!empty($user)) { ?> data-seller="<?=CHtml::encode($user['last_name'])?>"<?php } ?>>
						</div>
					</div>
					<div class="form-row clearfix">
						<div class="form-col form-col-50">
							<label for="cs-email"><?=Lang::t('checkout.label.email')?></label>
							<input id="cs-email" type="email" name="order[email]" value="<?=CHtml::encode($customer['email'])?>"<?php if (!empty($user)) { ?> data-seller="<?=CHtml::encode($user['email'])?>"<?php } ?>>
						</div>
						<div class="form-col form-col-50">
							<label for="cs-phone"><?=Lang::t('checkout.label.telephone')?></label>
							<input id="cs-phone" type="tel" name="order[phone]" value="<?=CHtml::encode($customer['phone'])?>"<?php if (!empty($user)) { ?> data-seller="<?=CHtml::encode($user['phone'])?>"<?php } ?>>
						</div>
					</div>
					<div class="form-row clearfix">
						<div class="form-col form-col-50">
							<label for="cs-zip"><?=Lang::t('checkout.label.zip')?></label>
							<input id="cs-zip" type="text" name="order[zip]" value="<?=CHtml::encode($customer['zip'])?>"<?php if (!empty($user)) { ?> data-seller="<?=CHtml::encode($user['zip'])?>"<?php } ?>>
						</div>
						<div class="form-col form-col-50">
							<label for="cs-city"><?=Lang::t('checkout.label.city')?></label>
							<input id="cs-city" type="text" name="order[city]" value="<?=CHtml::encode($customer['city'])?>"<?php if (!empty($user)) { ?> data-seller="<?=CHtml::encode($user['city'])?>"<?php } ?>>
						</div>
					</div>
					<div class="form-row clearfix">
						<div class="form-col form-col-50">
							<label for="cs-address-1"><?=Lang::t('checkout.label.address1')?></label>
							<input id="cs-address-1" type="text" name="order[address]" value="<?=CHtml::encode($customer['address'])?>"<?php if (!empty($user)) { ?> data-seller="<?=CHtml::encode($user['address'])?>"<?php } ?>>
						</div>
						<div class="form-col form-col-50">
							<label for="cs-address-2"><?=Lang::t('checkout.label.address2')?> <small><?=Lang::t('checkout.tip.optional')?></small></label>
							<input id="cs-address-2" type="text" name="order[address_2]" value="<?=CHtml::encode($customer['address_2'])?>"<?php if (!empty($user)) { ?> data-seller="<?=CHtml::encode($user['address_2'])?>"<?php } ?>>
						</div>
					</div>
					
					<div class="form-row clearfix">
						<div class="form-col form-col-100">
							<div class="rotate-tiles">
								<label for="cs-send-email"><input id="cs-send-email" type="checkbox" name="order[send_email]" value="1"<?php if (!empty($customer['send_email'])) { ?> checked<?php } ?>><span><?=Lang::t('checkout.label.sendEmail')?></span></label>
							</div>
						</div>
					</div>

					<div class="form-row clearfix">
						<div class="form-col form-col-100">
							<div class="rotate-tiles">
								<label for="cs-delivery-to-address"><input id="cs-delivery-to-address" type="checkbox" name="order[delivery_to_address]" value="1"<?php if (!empty($customer['delivery_to_address'])) { ?> checked<?php } ?>><span><?=Lang::t('checkout.label.deliveryToAddress')?></span></label>
							</div>
						</div>
					</div>

					<div id="cs-delivery-group" class="cs-delivery-group hidden">
						<div class="form-row clearfix">
							<div class="form-col form-col-50">
								<label for="cs-delivery-name"><?=Lang::t('checkout.label.firstName')?></label>
								<input id="cs-delivery-name" type="text" name="order[delivery_first_name]" value="<?=CHtml::encode($customer['delivery_first_name'])?>">
							</div>
							<div class="form-col form-col-50">
								<label for="cs-delivery-lastname"><?=Lang::t('checkout.label.lastName')?></label>
								<input id="cs-delivery-lastname" type="text" name="order[delivery_last_name]" value="<?=CHtml::encode($customer['delivery_last_name'])?>">
							</div>
						</div>
						<div class="form-row clearfix">
							<div class="form-col form-col-50">
								<label for="cs-delivery-email"><?=Lang::t('checkout.label.email')?></label>
								<input id="cs-delivery-email" type="email" name="order[delivery_email]" value="<?=CHtml::encode($customer['delivery_email'])?>">
							</div>
							<div class="form-col form-col-50">
								<label for="cs-delivery-phone"><?=Lang::t('checkout.label.telephone')?></label>
								<input id="cs-delivery-phone" type="tel" name="order[delivery_phone]" value="<?=CHtml::encode($customer['delivery_phone'])?>">
							</div>
						</div>
						<div class="form-row clearfix">
							<div class="form-col form-col-50">
								<label for="cs-delivery-zip"><?=Lang::t('checkout.label.zip')?></label>
								<input id="cs-delivery-zip" type="text" name="order[delivery_zip]" value="<?=CHtml::encode($customer['delivery_zip'])?>">
							</div>
							<div class="form-col form-col-50">
								<label for="cs-delivery-city"><?=Lang::t('checkout.label.city')?></label>
								<input id="cs-delivery-city" type="text" name="order[delivery_city]" value="<?=CHtml::encode($customer['delivery_city'])?>">
							</div>
						</div>
						<div class="form-row clearfix">
							<div class="form-col form-col-50">
								<label for="cs-delivery-address-1"><?=Lang::t('checkout.label.address1')?></label>
								<input id="cs-delivery-address-1" type="text" name="order[delivery_address]" value="<?=CHtml::encode($customer['delivery_address'])?>">
							</div>
							<div class="form-col form-col-50">
								<label for="cs-delivery-address-2"><?=Lang::t('checkout.label.address2')?> <small><?=Lang::t('checkout.tip.optional')?></small></label>
								<input id="cs-delivery-address-2" type="text" name="order[delivery_address_2]" value="<?=CHtml::encode($customer['delivery_address_2'])?>">
							</div>
						</div>
					</div>

					<div class="form-row form-row-btn clearfix">
						<div class="form-col form-col-50 text-left">
							<button id="checkout-offer" type="button" class="btn"><?=Lang::t('checkout.btn.offer')?></button>
						</div>
						<div class="form-col form-col-50 text-right">
							<button class="btn"><?=Lang::t('checkout.btn.nextStep')?></button>
						</div>
					</div>
				</form>
			</div>
		</div>

		<div class="checkout-step step-2<?php if (empty($delivery)) { ?> disabled<?php } ?>">
			<div class="cs-head"><?=Lang::t('checkout.title.selectDeliveryMethod')?></div>

			<?php if (!empty($delivery)) { ?>
			<div class="cs-content">
				<?php $this->renderPartial('checkoutStep2', array('delivery' => $delivery)); ?>
			</div>
			<?php } ?>
		</div>

		<div class="checkout-step step-3<?php if (empty($payment)) { ?> disabled<?php } ?>">
			<div class="cs-head"><?=Lang::t('checkout.title.selectPaymentMethod')?></div>

			<?php if (!empty($payment)) { ?>
			<div class="cs-content">
				<?php $this->renderPartial('checkoutStep3', array('payment' => $payment)); ?>
			</div>
			<?php } ?>
		</div>

		<div class="checkout-step step-4<?php if (empty($payment)) { ?> disabled<?php } ?>">
			<div class="cs-head"><?=Lang::t('checkout.title.checkYourOrder')?></div>

			<?php if (!empty($payment)) { ?>
			<div class="cs-content">
				<?php $this->renderPartial('checkoutStep4', array('cart' => $cart, 'price' => $price, 'agree' => $agree)); ?>
			</div>
			<?php } ?>
		</div>
	</div>
	
	<div class="payments">
		<div class="payments-title"><?=Lang::t('cart.title.paymentsGuide')?></div>

		<div class="payments-blocks">
			<div class="payments-block">
				<div class="pb-title"><?=Lang::t('cart.title.onlineBanking')?></div>
				<div class="pb-tip"><?=Lang::t('cart.tip.onlineBanking')?></div>
				<ul class="list-inline">
					<li><img src="<?=$staticUrl?>/images/payments/nordea-text.png" width="50" height="50" alt=""></li>
					<li><img src="<?=$staticUrl?>/images/payments/osuuspankki.png" width="50" height="50" alt=""></li>
					<li><img src="<?=$staticUrl?>/images/payments/spmaksunappi.png" width="50" height="48" alt=""></li>
					<li><img src="<?=$staticUrl?>/images/payments/omasp_painike.png" width="50" height="50" alt=""></li>
					<li><img src="<?=$staticUrl?>/images/payments/pop_maksunappi.png" width="50" height="49" alt=""></li>
					<li><img src="<?=$staticUrl?>/images/payments/aktiamaksunappi.png" width="50" height="50" alt=""></li>
					<li><img src="<?=$staticUrl?>/images/payments/danskebank.png" width="40" height="40" alt=""></li>
					<li><img src="<?=$staticUrl?>/images/payments/handelsbanken.gif" width="120" height="17" alt=""></li>
					<li><img src="<?=$staticUrl?>/images/payments/s_pankki_green.gif" width="78" height="16" alt=""></li>
					<li><img src="<?=$staticUrl?>/images/payments/alandsbanken.gif" width="49" height="46" alt=""></li>
				</ul>
			</div>
			<div class="payments-block">
				<div class="pb-title"><?=Lang::t('cart.title.cardPayments')?></div>
				<div class="pb-tip"><?=Lang::t('cart.tip.cardPayments')?></div>
				<ul class="list-inline">
					<li><img src="<?=$staticUrl?>/images/payments/visa.svg" alt=""></li>
					<li><img src="<?=$staticUrl?>/images/payments/mc.svg" alt=""></li>
				</ul>
			</div>
			<div class="payments-block">
				<div class="pb-title"><?=Lang::t('cart.title.creditPayments')?></div>
				<div class="pb-tip"><?=Lang::t('cart.tip.creditPayments')?></div>
				<ul class="list-inline">
					<li><img src="<?=$staticUrl?>/images/payments/jousto.png" alt=""></li>
					<li><img src="<?=$staticUrl?>/images/payments/collector-bank.svg" alt=""></li>
					<li><img src="<?=$staticUrl?>/images/payments/euroloan.png" alt=""></li>
				</ul>
			</div>
			<div class="payments-block">
				<div class="pb-title"><?=Lang::t('cart.title.invoicePayments')?></div>
				<div class="pb-tip"><?=Lang::t('cart.tip.invoicePayments')?></div>
				<ul class="list-inline">
					<li><img src="<?=$staticUrl?>/images/payments/invoice.png" alt=""></li>
				</ul>
			</div>
		</div>
	</div>
</div>