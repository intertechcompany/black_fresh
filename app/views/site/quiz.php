<style>
    @media only screen and (max-width: 2200px) {
        .quiz-answer {
            height: 110px;
            width: 540px;
        }

        /*.result-product-img {*/
        /*    width: 367px;*/
        /*    height: 440px;*/
        /*}*/

        /*.result-product-div {*/
        /*    width: 600px;*/
        /*    height: 600px;*/
        /*}*/

        .check {
            margin: 5px 25px 3px;
        }

        .result-product-img {
            width: 551px;
            height: 661px;
        }

        .product-description {
            width: 460px;
        }

        .product-description p {
            width: 460px;
        }

        .div-desc {
            margin-left: 50px;
            width: 400px;
        }

        #quiz-answers {
            column-count: 2;
        }

        .p-icon {
            margin-top: 35px;
        }
    }

    @media only screen and (max-width: 1900px) {
        .quiz-answer {
            height: 110px;
            width: 540px;
        }

        /*.result-product-img {*/
        /*    width: 367px;*/
        /*    height: 440px;*/
        /*}*/

        /*.result-product-div {*/
        /*    width: 600px;*/
        /*    height: 600px;*/
        /*}*/

        .check {
            margin: 5px 25px 3px;
        }

        .result-product-img {
            width: 551px;
            height: 661px;
        }

        .product-description {
            width: 460px;
        }

        .product-description p {
            width: 460px;
        }

        .div-desc {
            margin-left: 50px;
            width: 400px;
        }

        #quiz-answers {
            column-count: 2;
        }

        .p-icon {
            margin-top: 35px;
        }
    }
    @media only screen and (max-width: 1600px) {
        .quiz-answer {
            width: 540px;
            height: 110px;
        }

        /*.result-product-img {*/
        /*    width: 367px;*/
        /*    height: 440px;*/
        /*}*/

        /*.result-product-div {*/
        /*    width: 600px;*/
        /*    height: 600px;*/
        /*}*/

        .check {
            margin: 5px 25px 3px;
        }

        .result-product-img {
            width: 551px;
            height: 561px;
        }

        .product-description {
            width: 460px;
        }

        .product-description p {
            width: 460px;
        }

        .div-desc {
            margin-left: 50px;
            width: 400px;
        }

        #quiz-answers {
            column-count: 2;
        }

        .p-icon {
            margin-top: 35px;
        }
    }
    @media only screen and (max-width: 1199px) {
        .quiz-answer {
            width: 455px;
            height: 110px;
        }

        /*.result-product-img {*/
        /*    width: 350px;*/
        /*    height: 440px;*/
        /*}*/

        /*.result-product-div {*/
        /*    width: 600px;*/
        /*    height: 600px;*/
        /*}*/

        .check {
            margin: 5px 25px 3px;
        }

        .result-product-img {
            width: 450px;
            height: 535px
        }

        .product-description {
            width: 460px;
        }

        .product-description p {
            width: 460px;
        }

        .div-desc {
            margin-left: 50px;
            width: 400px;
        }

        #quiz-answers {
            column-count: 2;
        }

        .p-icon {
            margin-top: 35px;
        }
        .banner:before {
            display: none;
        }
        .banner:after {
            display: none;
        }
    }
    @media only screen and (max-width: 990px) {
        .quiz-answer {
            width: 340px;
            height: 110px;
        }

        /*.result-product-img {*/
        /*    width: 335px;*/
        /*    height: 440px;*/
        /*}*/

        /*.result-product-div {*/
        /*    width: 375px;*/
        /*    height: 380px;*/
        /*}*/

        .check {
            margin: 5px 25px 3px;
        }

        #quiz-body {
            display: block;
        }

        .result-product-img {
            width: 450px;
            height: 535px;
            margin-left: 110px;
        }

        .product-description {
            width: 460px;
        }

        .product-description p {
            width: 460px;
        }

        .div-desc {
            margin-left: 50px;
            width: 400px;
        }

        #quiz-answers {
            column-count: 2;
        }

        .p-icon {
            margin-top: 35px;
        }
        .banner:before {
            display: none;
        }
        .banner:after {
            display: none;
        }
    }
    @media only screen and (max-width: 767px) {
        .quiz-answer {
            width: 300px;
            height: 115px;
        }

        /*.result-product-img {*/
        /*    width: 385px;*/
        /*    height: 440px;*/
        /*    margin-left: 170px;*/
        /*}*/

        /*.result-product-div {*/
        /*    margin-left: 140px;*/
        /*    width: 400px;*/
        /*    height: 380px;*/
        /*}*/

        .check {
            margin: 5px 25px 3px;
        }

        .result-product-img {
            width: 400px;
            height: 430px;
            margin-left: 120px;
        }

        .product-description {
            width: 400px;
        }

        .product-description p {
            width: 400px;
        }

        .div-desc {
            margin-left: 50px;
        }

        #quiz-answers {
            column-count: 2;
        }

        .p-icon {
            margin-top: 35px;
        }
    }
    @media only screen and (max-width: 600px) {
        .quiz-answer {
            width: 100%;
            height: 150px;
        }

        /*.result-product-img {*/
        /*    width: 320px;*/
        /*    height: 340px;*/
        /*    margin-left: 80px;*/
        /*    margin-top: 60px;*/
        /*}*/

        /*.result-product-div {*/
        /*    margin-left: 46px;*/
        /*    width: 400px;*/
        /*    height: 380px;*/
        /*}*/

        .check {
            margin: 5px 25px 3px;
        }

        .product-description {
            width: 340px;
        }

        .product-description p {
            width: 340px;
        }

        .result-product-img {
            width: 360px;
            height: 360px;
            margin-left: 60px;
        }

        .div-desc {
            margin-left: 0;
            width: 400px;
        }

        #quiz-answers {
            column-count: 2;
        }

        .p-icon {
            margin-top: 45px;
        }
    }

    .product-description p {
        margin-left: 0px;
    }

    @media only screen and (max-width: 500px) {
        .quiz-answer {
            width: 100%;
            height: 190px;
        }

        /*.result-product-img {*/
        /*    width: 286px;*/
        /*    height: 320px;*/
        /*    margin-top: 55px;*/
        /*}*/

        /*.result-product-div {*/
        /*    margin-left: 40px;*/
        /*    width: 360px;*/
        /*    height: 380px;*/
        /*    margin-top: -60px;*/
        /*}*/

        .check {
            margin: 5px 25px 3px;
        }

        .result-product-img {
            width: 241px;
            height: 268px;
            margin-left: 50px;

        }

        .div-desc {
            margin-left: 0;
            width: auto;
        }

        #quiz-answers {
            column-count: 1;
        }

        .p-icon {
            margin-top: 80px;
        }
    }

    @media only screen and (max-width: 400px) {
        .quiz-answer {
            width: 100%;
            height: 200px;
        }

        /*.result-product-img {*/
        /*    width: 286px;*/
        /*    height: 320px;*/
        /*    margin-top: 55px;*/
        /*    margin-left: 10px;*/
        /*}*/

        /*.result-product-div {*/
        /*    width: 342px;*/
        /*    height: 380px;*/
        /*    margin-top: -60px;*/
        /*    margin-left: -30px;*/
        /*}*/

        .check {
            margin: -2px 12px 3px;
        }

        .div-desc {
            margin-left: 0;
            width: auto;

        }

        .result-product-img {
            width: 241px;
            height: 268px;
            margin-left: 20px;

        }

        #quiz-answers {
            column-count: 1;
        }

        .p-icon {
            margin-top: 80px;
        }
    }

    .quiz-answer:hover {
        border: 1px solid #000000;
        cursor: pointer;
    }

    .quiz-answer {
        border: 1px solid #b3b3b3;
        margin-top: 20px;
        display: inline-block;
    }

    .quiz-question {
        font-family: "Apercu Pro", serif;
        font-style: normal;
        font-weight: normal;
        word-wrap: break-word;
        font-size: 34px;
        /*line-height: 50px;*/
        text-transform: uppercase;
    }

    .result-header {
        font-family: "Apercu Pro", serif;
        font-style: normal;
        font-weight: normal;
        word-wrap: break-word;
        font-size: 25px;
        /*line-height: 50px;*/
        text-transform: uppercase;
        margin-bottom: -40px;
        /*margin-bottom: 25px;*/
    }

    .result-product-name {
        font-family: "Apercu Pro", serif;
        font-style: normal;
        font-weight: normal;
        word-wrap: break-word;
        font-size: 42px;
        /*line-height: 50px;*/
        text-transform: uppercase;
        margin-bottom: 25px;
        /*margin-left: 40px;*/
    }

    /*.result-product-btn {*/
    /*    width: 241px;*/
    /*    margin-top: 25px;*/
    /*    margin-left: 40px;*/
    /*}*/

    /*.result-product-div {*/
    /*    display: inline-block;*/
    /*}*/

    p {
        margin-left: 40px;
        margin-bottom: 10px;
    }

    .check {
        display: inline-block;
        width: 10px;
        height: 20px;
        /*margin: 5px 25px 3px;*/
        border: solid #0098FF;
        border-width: 0 3px 3px 0;
        transform: rotate( 45deg);
        float: right;
    }

    .checked-answer {
        border: 1px solid #0098FF;
    }

    .slider {
        -webkit-appearance: none;
        width: 100%;
        height: 15px;
        border-radius: 100px;
        background: white;
        border: 1px solid #000000;
        box-sizing: border-box;
        outline: none;
        opacity: 0.7;
        -webkit-transition: .2s;
        transition: opacity .2s;
    }

    .slider::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        width: 23px;
        height: 23px;
        border-radius: 50%;
        background: black;
        cursor: pointer;
    }

    .slider::-moz-range-thumb {
        width: 23px;
        height: 23px;
        border-radius: 50%;
        background: black;
        cursor: pointer;
        overflow: hidden;
    }

    .ticks {
        display: flex;
        justify-content: space-between;
        padding: 5px 5px;
    }

    .quiz-answer-text {
        margin-left: 20px;
        color: grey;
        margin-right: 20px;
    }

    .span-toggle {
        font-size: 34px;
        margin-top: -40px;
        margin-right: 20px;
        float: right;
    }

    .toggled {
        transform: rotate(180deg);
    }

    hr {
        border-bottom: 2px solid black;
        width: 100%;
    }

    .product-div {
        display: inline-block;
    }

    .result-radio {
        width: 100px;
        height: 25px;
    }

    .result-span {
        width: 190px;
    }

    .product-tip {
        margin: 20px 0 0;
    }



</style>
<main class="home">
    <section class="banner" style="background-image: none;">
        <div class="wrap">
            <div id="quiz-body"></div>
            <div id="quiz-tip" style="display: none;" data-text="<?=CHtml::encode(Lang::t('quiz.tip.not.found'))?>">
                <hr class="hr">
                <section class="product__section">
                    <div class="product__subtitle"><?=Lang::t('quiz.tip.title')?></div>

                    <ol class="product__delivery list-unstyled">
                        <li>
                            <span class="product__delivery-title"><?=Lang::t('quiz.tip.first.title')?></span>
                            <span class="product__delivery-tip"><?=Lang::t('quiz.tip.first.text')?></span>
                        </li>
                        <li>
                            <span class="product__delivery-title"><?=Lang::t('quiz.tip.second.title')?></span>
                            <span class="product__delivery-tip"><?=Lang::t('quiz.tip.second.text')?></span>
                        </li>
                        <li>
                            <span class="product__delivery-title"><?=Lang::t('quiz.tip.third.title')?></span>
                            <span class="product__delivery-tip"><?=Lang::t('quiz.tip.third.text')?></span>
                        </li>
                    </ol>
                </section>
            </div>
            <!--            <button class="product__btn btn" id="quiz-button" disabled onclick="next()" style="width: 241px; margin-top: 35px;">--><?//=Lang::t('quiz.btn.next')?><!--</button>-->
            <div style="margin-top: 200px;">
                <h5 id="under-quiz-title" class="quiz-question" style="font-size: 24px;"></h5>
                <p id="under-quiz-text" style="margin-left: 0"></p>
            </div>
        </div>
    </section>
</main>

<script>
    let quizNumber = 0;
    let questions;
    let answers;
    let language = {};
    let currentAnswer = {};
    let questionCount = 0;
    let slider = false;

    const results = [];

    function toggle(divId, spanId) {
        const x = document.getElementById(divId);
        const toggle = document.getElementById(spanId);

        console.log('toggle');

        if (x.style.display === "none") {
            x.style.display = "block";
            toggle.classList.remove('toggled');
        } else {
            x.style.display = "none";
            toggle.classList.add('toggled');
        }
    }

    window.onload = async function() {
        const quizBody = document.getElementById('quiz-body');
        if (quizBody) {
            await getCurrentLanguage();
            await getAnswers();
            await getQuestions();
        }
    }

    async function getCurrentLanguage()
    {
        let currentUrl = window.location.href;
        let url = new URL(currentUrl);
        let requestUrl = new URL('language/current', url);

        try {
            const response = await fetch(requestUrl, {method: 'GET'});
            language = await response.json();
        } catch (e) {
            console.log(e);
        }
    }

    async function getQuestions() {
        try {
            const response = await fetch('/questions', {method: 'GET'});
            questions = await response.json();
            questionCount = questions.length;
            createQuiz();
        } catch (e) {
            console.log(e);
        }
    }

    async function getAnswers() {
        try {
            const response = await fetch('/answers', {method: 'GET'});
            answers = await response.json();
        } catch (e) {
            console.log(e);
        }
    }

    function createQuiz() {
        if (quizNumber !== 0) {
            createBackButton();
        }

        createQuizStatus();
        createQuestion(questions[quizNumber]);

        if (questions[quizNumber].type === 'slider') {
            createSlider(questions[quizNumber].id);
            slider = true;
            document.getElementById('quiz-button').disabled = false;
        } else {
            createAnswers(questions[quizNumber].id);
        }
    }

    function createSlider(questionId) {
        let count = -1;
        let sliderAnswers = [];
        const quizBody = document.getElementById('quiz-body');
        const sliderDiv = document.createElement('div');
        const slider = document.createElement('input');
        const sliderTicks = document.createElement('div');

        slider.type = 'range';
        slider.min = 0;
        slider.value = 0;
        slider.step = 1;
        slider.className = 'slider';
        slider.id = 'slider';
        slider.setAttribute('question', questionId);

        sliderTicks.className = 'ticks';

        sliderDiv.appendChild(slider);

        answers.forEach(function (item) {
            if (item.question_id === questionId) {
                const span = document.createElement('span');

                if (language.lang === 'ru') {
                    span.textContent = item.title_ru;
                } else if (language.lang === 'uk') {
                    span.textContent = item.title_uk;
                } else if (language.lang === 'en') {
                    span.textContent = item.title_en;
                }

                sliderTicks.appendChild(span);
                sliderAnswers[count + 1] = item;
                count++;
            }
        })
        slider.max = count;
        sliderDiv.appendChild(sliderTicks);

        quizBody.appendChild(sliderDiv);
        sliderFill(count, sliderAnswers) ;
    }

    function sliderFill(countMax, sliderAnswers) {
        const slider = document.getElementById("slider");
        currentAnswer = sliderAnswers[slider.value];

        slider.oninput = function() {
            let max = 100 / countMax;
            let value = this.value * max;
            this.style.background = 'linear-gradient(to right, #000000 0%, #000000 ' + value + '%, #fff ' + value + '%, white 100%)'

            currentAnswer = sliderAnswers[slider.value];
        };
    }

    function createBackButton() {
        const quizBody = document.getElementById('quiz-body');
        const button = document.createElement('a');

        if (language.lang === 'ru') {
            button.textContent = '< Назад';
        } else if (language.lang === 'uk') {
            button.textContent = '< Назад';
        } else if (language.lang === 'en') {
            button.textContent = '< Back';
        }

        button.style.color = '#0098FF';
        button.onclick = () => {back();}
        button.href = '#';
        button.style.textDecoration = 'none';

        quizBody.appendChild(button);
    }

    function createQuizStatus() {
        const quizBody = document.getElementById('quiz-body');
        const questionDiv = document.createElement('div');
        const span = document.createElement('span');

        if (language.lang === 'ru') {
            span.textContent = quizNumber + 1 + ' из ' + questionCount;
        } else if (language.lang === 'uk') {
            span.textContent = quizNumber + 1 + ' із ' + questionCount;
        } else if (language.lang === 'en') {
            span.textContent = quizNumber + 1 + ' of ' + questionCount;
        }

        questionDiv.id = 'question-body';
        questionDiv.appendChild(span);
        questionDiv.style.marginTop = '15px';
        quizBody.style.marginTop = '20px';

        quizBody.appendChild(questionDiv);
    }

    function createQuestion(data) {
        const quizBody = document.getElementById('quiz-body');
        const question = document.createElement('h1');
        const questionDiv = document.getElementById('question-body');

        question.className = 'quiz-question';
        question.setAttribute('value', data.id);
        console.log(data);
        if (language.lang === 'ru') {
            question.textContent = data.title_ru;
            document.getElementById('under-quiz-title').textContent = data.title_under_ru;
            document.getElementById('under-quiz-text').textContent = data.text_under_ru;
        } else if (language.lang === 'uk') {
            question.textContent = data.title_uk;
            document.getElementById('under-quiz-title').textContent = data.title_under_uk;
            document.getElementById('under-quiz-text').textContent = data.text_under_uk;
        } else if (language.lang === 'en') {
            question.textContent = data.title_en;
            document.getElementById('under-quiz-title').textContent = data.title_under_en;
            document.getElementById('under-quiz-text').textContent = data.text_under_en;
        } else {
            console.log('language error');
        }

        questionDiv.appendChild(question);
        quizBody.appendChild(questionDiv);
    }

    function createAnswers(questionId) {
        const quizBody = document.getElementById('quiz-body');
        const quizAnswerBody = document.createElement('div');

        quizAnswerBody.id = 'quiz-answers';

        quizBody.appendChild(quizAnswerBody);

        answers.forEach(function (item) {
            if (item.question_id === questionId) {
                const answerDiv = document.createElement('div');
                const answerP = document.createElement('p');
                const answerSpan = document.createElement('span');
                const answerCheck = document.createElement('span');
                const answerText = document.createElement('p');

                answerDiv.className = 'quiz-answer';
                answerDiv.id = 'div-' + item.id;
                answerDiv.setAttribute('value', item.id);

                answerP.style.marginLeft = '10px';

                answerCheck.className = 'check';
                answerCheck.id = 'span-' + item.id;
                answerCheck.style.display = 'none';

                answerSpan.style.marginLeft = '10px';

                if (language.lang === 'ru') {
                    answerSpan.textContent = item.title_ru;
                    answerText.textContent = item.text_ru;
                } else if (language.lang === 'uk') {
                    answerSpan.textContent = item.title_uk;
                    answerText.textContent = item.text_ua;
                } else if (language.lang === 'en') {
                    answerSpan.textContent = item.title_en;
                    answerText.textContent = item.text_en;
                } else {
                    console.log('language error');
                }

                answerText.className = 'quiz-answer-text';

                if (item.icon) {
                    const icon = document.createElement('img');

                    let currentUrl = window.location.href;
                    let url = new URL(currentUrl);

                    icon.src = url.origin + '/' + item.icon;

                    icon.style.width = '40px';
                    icon.style.height = '35px';

                    answerP.appendChild(icon);
                }
                answerP.appendChild(answerSpan);
                answerP.appendChild(answerCheck);

                answerDiv.onclick = () => {setAnswer(item, answerCheck, answerDiv);}
                answerDiv.appendChild(answerP);

                if (item.text_ua) {
                    answerDiv.appendChild(answerText);
                } else {
                    answerP.className = 'p-icon';
                }

                quizAnswerBody.appendChild(answerDiv);
            }
        })
    }

    let type = '';

    function setAnswer(answer, span, div) {
        span.style.display = 'block';
        div.classList.add('checked-answer');

        // document.getElementById('quiz-button').disabled = false;

        if (answer.question_id === "2") {
            if (language.lang === 'ru') {
                type = answer.title_ru;
            } else if (language.lang === 'uk') {
                type = answer.title_uk;
            } else if (language.lang === 'en') {
                type = answer.title_en;
            } else {
                console.log('language error');
            }
        }

        currentAnswer = answer;
        removeChecked(span, div);
        next();
    }

    function next() {
        if (Object.entries(currentAnswer).length !== 0) {
            results.push({
                result: currentAnswer.id,
                question: currentAnswer.question_id,
            });

            clearQuiz();

            // if (quizNumber + 2 === questions.length) {
            //     if (language.lang === 'ru') {
            //         document.getElementById('quiz-button').textContent = 'Узнать результат';
            //     } else if (language.lang === 'uk') {
            //         document.getElementById('quiz-button').textContent = 'Дізнатись результат';
            //     } else if (language.lang === 'en') {
            //         document.getElementById('quiz-button').textContent = 'Get Result';
            //     }
            // }

            if (quizNumber + 1 < questions.length) {
                quizNumber++;

                if (questions[quizNumber].type === 'slider') {
                    slider = true;
                    // document.getElementById('quiz-button').disabled = true;
                    createQuiz();
                } else {
                    createQuiz();
                }
            } else  {
                sendAnswers();
            }
        }
    }

    function back() {
        quizNumber--;
        results.splice(quizNumber, 1);
        clearQuiz();
        createQuiz();
    }

    function removeChecked(checkedSpan, checkedDiv) {
        const allSpans = document.getElementsByTagName('span');
        const allDiv = document.getElementsByClassName('quiz-answer');

        for (let i = 0; i < allSpans.length; i++) {
            if (allSpans[i].id !== checkedSpan.id) {
                if (allSpans[i].classList.contains('check')) {
                    allSpans[i].style.display = 'none';
                }
            }
        }

        for (let i = 0; i < allDiv.length; i++) {
            if (allDiv[i].id !== checkedDiv.id) {
                allDiv[i].classList.remove('checked-answer');
            }
        }
    }

    function clearQuiz() {
        // document.getElementById('quiz-button').disabled = true;
        const quizBody = document.getElementById('quiz-body');
        quizBody.innerHTML = '';
    }

    async function sendAnswers() {
        try {
            const response = await fetch('/answers/new', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(results)
            });

            const data = await response.json();

            console.log(data);

            if (data.message === 'success') {
                createResult(data);
            } else if (data.message === 'empty') {
                createEmptyResult();
            }

        } catch (e) {
            console.log(e);
        }
    }

    function createEmptyResult() {
        const quizBody = document.getElementById('quiz-body');

        const header = document.createElement('h5');
        header.className = 'result-header';

        if (language.lang === 'ru') {
            header.innerText = document.getElementById('quiz-tip').dataset.text;
        } else if (language.lang === 'uk') {
            header.innerText = document.getElementById('quiz-tip').dataset.text;
        } else if (language.lang === 'en') {
            header.innerText = document.getElementById('quiz-tip').dataset.text;
        }

        quizBody.appendChild(header);
    }

    let resultView = false;

    function changeView()
    {
        if (window.screen.width > 990) {
            if (resultView) {
                document.getElementById('quiz-body').style.display = 'inline-flex';
            }
        } else {
            document.getElementById('quiz-body').style.display = 'block';
        }
    }

    window.addEventListener('resize', function(event){
        changeView();
    });

    function createResult(product) {
        clearQuiz();
        document.getElementById('under-quiz-title').textContent = '';
        document.getElementById('under-quiz-text').textContent = '';

        const quizBody = document.getElementById('quiz-body');

        let imageJson = JSON.parse(product.product_photo);

        // Part 1
        const divProductImg = document.createElement('div');
        const productImg = document.createElement('img');

        divProductImg.className = 'product-div';

        productImg.src = product.assetUrl + '/product/' + product.product_id + '/' + imageJson.path.original['1x'];
        productImg.className = 'result-product-img';


        // Part 2
        const divProduct = document.createElement('div');
        const header = document.createElement('h5');
        const productTitle = document.createElement('h4');
        const productTip = document.createElement('p');

        // product reason
        const divReasonHeader = document.createElement('div');
        const reasonToggle = document.createElement('span');
        const reasonHeader = document.createElement('h4');
        const divReasonBody = document.createElement('div');
        const reasonText = document.createElement('p');

        //product description
        const divProductHeader = document.createElement('div');
        const productToggle = document.createElement('span');
        const productHeader = document.createElement('h4');
        const divProductBody = document.createElement('div');
        const productText = document.createElement('p');

        divProduct.className = 'product-div div-desc';

        header.className = 'result-header';

        productTitle.textContent = product.product_brand;
        productTitle.className = 'result-product-name';

        productTip.className = 'product-tip';

        // product reason
        divReasonHeader.style.cursor = 'pointer';
        divReasonHeader.onclick = () => { toggle('div-1', 'span-1') };

        reasonToggle.className = 'span-toggle toggled';
        reasonToggle.id = 'span-1';
        reasonToggle.textContent = '^';
        reasonToggle.style.marginTop = '-50px';

        divReasonBody.style.display = 'none';
        divReasonBody.id = 'div-1';

        reasonText.style.marginLeft = 0;

        divReasonHeader.appendChild(reasonHeader);
        divReasonHeader.appendChild(reasonToggle);

        divReasonBody.appendChild(reasonText);

        //product description
        divProductHeader.style.cursor = 'pointer';
        divProductHeader.onclick = () => { toggle('div-2', 'span-2') };

        productToggle.className = 'span-toggle toggled';
        productToggle.id = 'span-2';
        productToggle.textContent = '^';
        productToggle.style.marginTop = '-50px';

        divProductBody.style.display = 'none';
        divProductBody.id = 'div-2';

        productText.innerHTML = product.product_tip;
        productText.style.marginLeft = 0;
        productText.className = 'product-description';

        divProductHeader.appendChild(productHeader);
        divProductHeader.appendChild(productToggle);

        divProductBody.appendChild(productText);

        let currentUrl = window.location.href;
        let url = new URL(currentUrl);
        let requestUrl = new URL('product/' + product.product_alias + '?action=subscription&type=' + type, url);

        const productButton = document.createElement('a');
        productButton.href = requestUrl.href;
        productButton.className = 'product__btn btn result-product-btn';
        productButton.style.width = '50%';

        if (language.lang === 'ru') {
            header.textContent = 'Твой кофе:';

            productTip.innerHTML = 'Получи скидку 20% на свою первую подписку:'
                + '<br>' + '- Подписывайтесь и оценивайте кофе'
                + '<br>' + '- Никогда не платите за доставку подписки'
                + '<br>' + '- Выберите удобный для вас график поставок'
                + '<br>' + '- Свежая обжарки и доставка прямо к вам'
            ;

            reasonHeader.textContent = 'УЗНАЙ БОЛЬШЕ ПРО СВОЮ ПОДПИСКУ:';
            productHeader.textContent = 'БОЛЬШЕ ОБ ЭТОМ СОРТЕ';
            productButton.textContent = 'Оформить подписку';
            reasonText.textContent = product.text.text_ru;
        } else if (language.lang === 'uk') {
            header.textContent = 'Твоя кава:';

            productTip.innerHTML = 'Отримай знижку 20% на свою першу підписку:'
                + '<br>' + '- Підписуйтесь та оцінюйте каву'
                + '<br>' + '- Ніколи не платіть за доставку підписки'
                + '<br>' + '- Оберіть зручний для вас графік доставок'
                + '<br>' + '- Свіже обсмаження та доставка прямо до вас'
            ;

            reasonHeader.textContent = 'ДІЗНАЙСЯ БІЛЬШЕ ПРО СВОЮ ПІДПИСКУ:';
            productHeader.textContent = 'БІЛЬШЕ ПРО ЦЕЙ СОРТ';
            productButton.textContent = 'Оформити підписку';
            reasonText.textContent = product.text.text_uk;
        } else if (language.lang === 'en') {
            header.textContent = 'Your coffee:';

            productTip.innerHTML = 'Get a 20% discount on your first subscription:'
                + '<br>' + '- Subscribe and rate coffee'
                + '<br>' + '- Never pay for a subscription delivery'
                + '<br>' + '- Choose a convenient delivery schedule for you'
                + '<br>' + '- Fresh roasted and delivery directly to you'
            ;

            reasonHeader.textContent = 'WHY DO YOU LIKE THIS COFFEE?';
            productHeader.textContent = 'LEARN MORE ABOUT YOUR SUBSCRIPTION:';
            productButton.textContent = 'Subscribe';
            reasonText.textContent = product.text.text_en;
        }

        quizBody.appendChild(divProductImg);
        quizBody.appendChild(divProduct);

        document.getElementById('quiz-tip').style.display = 'block';

        // Part 1
        divProductImg.appendChild(productImg);

        //Part 2
        divProduct.appendChild(header);
        divProduct.appendChild(productTitle);
        productVariants(divProduct, product);
        divProduct.appendChild(productTip);

        if (product.text.text_uk) {
            divProduct.appendChild(document.createElement('hr'));
            divProduct.appendChild(divReasonHeader);
            divProduct.appendChild(divReasonBody);
        }

        divProduct.appendChild(document.createElement('hr'));
        divProduct.appendChild(divProductHeader);
        divProduct.appendChild(divProductBody);
        divProduct.appendChild(productButton);

        setData(product);
        resultView = true;
        document.getElementById('quiz-body').style.display = 'inline-flex';
        changeView();
    }

    function productVariants(divProduct, product) {
        // product variants
        for (let i = 0; i < 2; i++) {
            const label = document.createElement('label');
            const input = document.createElement('a');
            const span = document.createElement('span');

            label.className = 'product__radio result-radio';

            if (i === 0) {
                label.style.marginLeft = '0';
                label.style.marginTop = '0';
            } else {
                label.style.marginLeft = '60px';
                label.style.marginTop = '0';
            }

            let currentUrl = window.location.href;
            let url = new URL(currentUrl);

            input.href = new URL('product/' + product.product_alias + '?action=subscription&type=' + type, url);
            input.id = 'value-' + i;
            input.style.textDecoration = 'none';

            span.id = 'span-radio-' + i;
            span.className = 'result-span';

            input.appendChild(span);
            label.appendChild(input);

            if (i > 0) {
                label.style.display = 'none';
            }

            // label.appendChild(span);
            divProduct.appendChild(label);
        }
    }

    function setData(product) {
        const productVariants = product.variants.variants;
        let count = 0;

        Object.keys(productVariants).forEach(function (key){
           let text = '';
           let currency = '';
           if (count === 0) {
               if (language.lang === 'ru') {
                   text = '200 г / ';
                   currency = 'грн';
               } else if (language.lang === 'uk') {
                   text = '200 г / ';
                   currency = 'грн';
               } else if (language.lang === 'en') {
                   text = '200 g / ';
                   currency = 'uah';
               }
           } else {
               if (language.lang === 'ru') {
                   text = '1 кг / ';
                   currency = 'грн';
               } else if (language.lang === 'uk') {
                   text = '1 кг / ';
                   currency = 'грн';
               } else if (language.lang === 'en') {
                   text = '1 kg / ';
                   currency = 'uah';
               }
           }
           let price = productVariants[key].variant_price - (productVariants[key].variant_price * 0.2);
           let htmlData = text + "<small style='text-decoration: line-through; color: #1f78d8; font-size: 15px;'>" + (productVariants[key].variant_price).replace('.00', '') + "</small>" + ' ' + Math.round(price) + " " + currency;
           document.getElementById('span-radio-' + count).innerHTML = htmlData;
           count++;
        });
    }
</script>