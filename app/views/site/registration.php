<?php
/* @var $this SiteController */
?>
<style>
    .main-wrap {
        margin-top: 54px;
    }
    .h1-registration{
        font-family: Apercu Pro;
        font-style: normal;
        font-weight: normal;
        font-size: 50px;
        line-height: 58px;
        margin-bottom: 3px;
    }
    .flex-for-colomn {
        display: flex;
    }
    .have-account{
        margin-bottom: 19px;
        margin-top: 0;
    }
    .for-after:after {
        content: "";
        color: #333;
        border: 1px solid #302f31;
        height: 275px;
        position: absolute;
        left: 53%;
        /* margin-left: -20px; */
        top: 198px;
    }
    #registration {
        width: 57%;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-direction: row;
        flex-direction: row;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
        -ms-flex-line-pack: stretch;
        align-content: stretch;
        -webkit-box-align: start;
        -ms-flex-align: start;
        align-items: flex-start;
    }
    .width-field {
        width: 44%;
    }
    .form__input {
        width: 270px;
        height: 40px;
        padding: 0 20px 6px 20px;
    }
    .link-reg-login {
        color: #0098ff;
        width: 100%;
    }
    .mb-login {
        margin-bottom: 20px;
    }
    .btn-reg {
        padding: 0 13px;
        width: 241px;
    }
    .div-socials-flex {
        width: 35%;
    }
    .socials-div-enter-facebook {
        background-color: #3D5D9C;
        color: #fff;
        text-align: center;
        padding: 16px 39px 16px 22px;
        border-radius: 100px;
        font-size: 15px;
        margin-bottom: 10px;
    }
    .socials-div-enter-google {
        background-color: #DC4D28;
        color: #fff;
        text-align: center;
        padding: 16px 39px 16px 22px;
        border-radius: 100px;
        font-size: 15px;
    }
    .socials-div-enter-google a {
        color: #fff;
        text-decoration: none;
    }
    .socials-div-enter-facebook a {
        color: #fff;
        text-decoration: none;
    }
    .login-socials {
        font-family: inherit;
        font-weight: 400;
        line-height: 1.1;
        color: #000000;
        margin-top: -14px;
        line-height: 22px;
        font-size: 15px;
        margin-bottom: 11px;
    }
    @media (max-width: 1200px) {
        .for-after:after{
            left: 52%;
            height: 320px;
            top: 170px;
        }
        .form__input {
            width: 226px;
        }
    }
    @media (max-width: 1200px) {
        .form__input {
            width: 165px;
        }
        .div-socials-flex {
            width: 42%;
        }
    }
    @media (max-width: 720px)  {
        .form__input {
            width: 155px;
        }
        .for-after:after {
            left: 52%;
            top: 165px;
        }
    }
    @media (max-width: 690px)  {
        .form__input {
            width: 247px;
        }
        .width-field {
            width: 100%;
        }
        .for-after:after {
            display: none;
        }
        #registration {
            width: 52%;
        }
    }
    @media (max-width: 580px) {
        .flex-for-colomn {
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-pack: start;
            -ms-flex-pack: start;
            justify-content: flex-start;
            -ms-flex-line-pack: stretch;
            align-content: stretch;
            -webkit-box-align: start;
            -ms-flex-align: start;
            align-items: flex-start;
        }
        .width-field {
            width: 48%;
        }
        #registration {
            width: 100%;
        }
        .div-socials-flex {
            width: 56%;
        }
        .form__input {
            width: 222px;
        }
    }
    @media (max-width: 580px) {
        .form__input {
            width: 200px;
        }
    }
    @media (max-width: 480px) {
        .width-field {
            width: 78%;
        }
        .div-socials-flex {
            width: 100%;
        }
        .form__input {
            width: 300px;
        }
    }
    @media (max-width: 370px) {
        .form__input {
            width: 290px;
        }
    }

</style>
<div class="wrap main-wrap">
    <div class="content-divider"></div>

    <div class="auth-form">
        <h1 class="h1-registration" ><?= CHtml::encode($this->pageTitle) ?></h1>
        <h4 class="have-account"><?=Lang::t('account.title.withaccount')?><a class="link-reg-login" href="/login"> <?=Lang::t('account.title.log')?></a> </h4>

        <?php if (Yii::app()->user->hasFlash('registration')) { ?>
            <div class="success-msg"><?= Yii::app()->user->getFlash('registration') ?></div>
        <?php } else { ?>
        <div class="flex-for-colomn">
            <form class="for-after" id="registration" action="<?= $this->createUrl('site/registration') ?>"
                  class="registration-form" method="post">
                <?php if (!empty($result['errorCode'])) { ?>
                    <div class="error-msg"><?= $result['errorCode'] ?></div>
                <?php } elseif (Yii::app()->user->hasFlash('registration_error')) { ?>
                    <div class="error-msg"><?= Yii::app()->user->getFlash('registration_error') ?></div>
                <?php } ?>
                <div class="form-row clearfix width-field">
                    <div class="form-col form-col-100 mb-login">
                        <label for="registration-email"><?= Lang::t('registration.label.email') ?></label>
                        <input class="form__input" id="registration-email" <?php if (isset($result['errorFields']['email'])) { ?> class="error-field"<?php } ?>
                               type="email" name="registration[email]" required
                               value="<?php if (!empty($data['email']) && empty($result['errorFields']['email'])) { ?><?=$data['email']?><?php } ?>">
                        <?php if (!empty($result['errorFields']['email'][0])) { ?>
                            <div class="error-msg"><?= implode('<br>',
                            $result['errorFields']['email']) ?></div><?php } ?>
                    </div>
                </div>
                <div class="form-row clearfix width-field">
                    <div class="form-col form-col-100 mb-login">
                        <label for="registration-phone"><?= Lang::t('registration.label.phone') ?></label>
                        <input class="form__input" id="registration-phone" <?php if (isset($result['errorFields']['phone'])) { ?> class="error-field"<?php } ?>
                               type="text" name="registration[phone]" required
                               value="<?php if (!empty($data['phone']) && empty($result['errorFields']['phone'])) { ?><?=$data['phone']?><?php } ?>">
                        <?php if (!empty($result['errorFields']['phone'][0])) { ?>
                            <div class="error-msg"><?= implode('<br>',
                            $result['errorFields']['phone']) ?></div><?php } ?>
                    </div>
                </div>
                <div class="form-row clearfix width-field">
                    <div class="form-col form-col-100 mb-login">
                        <label for="registration-first_name"><?= Lang::t('registration.label.firstName') ?></label>
                        <input class="form__input" id="registration-first_name" <?php if (isset($result['errorFields']['first_name'])) { ?> class="error-field"<?php } ?>
                               type="text" name="registration[first_name]" required
                               value="<?php if (!empty($data['first_name']) && empty($result['errorFields']['first_name'])) { ?><?=$data['first_name']?><?php } ?>">
                        <?php if (!empty($result['errorFields']['first_name'][0])) { ?>
                            <div class="error-msg"><?= implode('<br>',
                            $result['errorFields']['first_name']) ?></div><?php } ?>
                    </div>
                </div>
                <div class="form-row clearfix width-field">
                    <div class="form-col form-col-100 mb-login">
                        <label for="registration-last_name"><?= Lang::t('registration.label.lastName') ?></label>
                        <input class="form__input" id="registration-last_name" <?php if (isset($result['errorFields']['last_name'])) { ?> class="error-field"<?php } ?>
                               type="text" name="registration[last_name]" required
                               value="<?php if (!empty($data['last_name']) && empty($result['errorFields']['last_name'])) { ?><?=$data['last_name']?><?php } ?>">
                        <?php if (!empty($result['errorFields']['last_name'][0])) { ?>
                            <div class="error-msg"><?= implode('<br>',
                            $result['errorFields']['last_name']) ?></div><?php } ?>
                    </div>
                </div>
                <div class="form-row clearfix width-field">
                    <div class="form-col form-col-100 mb-login">
                        <label for="registration-password"><?= Lang::t('registration.label.password') ?></label>
                        <input class="form__input" id="registration-password" <?php if (isset($result['errorFields']['password'])) { ?> class="error-field"<?php } ?>
                               type="password" name="registration[password]" required>
                        <?php if (!empty($result['errorFields']['password'][0])) { ?>
                            <div class="error-msg"><?= implode('<br>',
                            $result['errorFields']['password']) ?></div><?php } ?>
                    </div>
                </div>
                <div class="form-row clearfix width-field">
                    <div class="form-col form-col-100 mb-login">
                        <label for="registration-password_confirm"><?= Lang::t('registration.label.passwordConfirm') ?></label>
                        <input class="form__input" id="registration-password_confirm"
                            <?php if (isset($result['errorFields']['password_confirm'])) { ?> class="error-field"<?php } ?>
                               type="password" name="registration[password_confirm]" required>
                        <?php if (!empty($result['errorFields']['password_confirm'][0])) { ?>
                            <div class="error-msg"><?= implode('<br>',
                            $result['errorFields']['password_confirm']) ?></div><?php } ?>
                    </div>
                </div>
                <div class="form-row form-row-btn clearfix">
                    <div class="form-col form-col-100 mb-login">
                        <button class="btn btn-reg"><?= Lang::t('registration.btn.submit') ?></button>
                    </div>
                </div>
            </form>
            <?php } ?>

            <div class="div-socials-flex">
                <h4 class="login-socials"><?= Lang::t('login.social.title') ?></h4>
                <div class="socials-div-enter-facebook"><a href="<?=$this->createUrl('auth/facebook')?>"> <?= Lang::t('login.social.facebook') ?></a></div>
                <div class="socials-div-enter-google"><a href="<?=$this->createUrl('auth/google')?>"><?= Lang::t('login.social.google') ?></a></div>
            </div>
        </div>
    </div>
</div>