<?php
	/* @var $this SiteController */
	$staticUrl = Yii::app()->assetManager->getBaseUrl() . '/static/' . Yii::app()->params->settings['rev'];
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
	$blockPhotosUrl = Yii::app()->assetManager->getBaseUrl() . '/page/' . $page['page_id'];
?>
<main class="about">
    <div class="wrap">
		<?php $this->widget('application.components.Breadcrumbs.Breadcrumbs', ['breadcrumbs' => $this->breadcrumbs]); ?>
		
		<h1 class="about__title"><?=CHtml::encode($page['page_title'])?></h1>
		
		<?php if (!empty($page['page_content'])) { ?>
		<section class="about__blocks">
			<?php 
				$page_content = json_decode($page['page_content'], true);
				$page_content_lang = json_decode($page['page_content_lang'], true);
			?>
			<?php foreach ($page_content as $index => $block) { ?>
			<div class="about-block">
				<div class="about-block__img">
					<?php if (!empty($block['photo'])) { ?>
					<picture>
						<?php if (!empty($block['photo']['2x'])) { ?>
						<source srcset="<?=$blockPhotosUrl . '/' . $block['photo']['1x']['file']?>, <?=$blockPhotosUrl . '/' . $block['photo']['2x']['file']?> 2x">
						<?php } ?>
						<img src="<?=$blockPhotosUrl . '/' . $block['photo']['1x']['file']?>" alt="">
					</picture>
					<?php } ?>
				</div>
				<div class="about-block__content">
					<div class="about-block__title"><?=CHtml::encode($page_content_lang[$index]['title'])?></div>
					<div class="about-block__description"><?=str_replace(["\n", "\r"], ["<br>\n", ""], $page_content_lang[$index]['text'])?></div>
				</div>
			</div>
			<?php } ?>
		</section>
		<?php } ?>

		<?php if ($page['page_alias'] == 'about-us') { ?>
		<?php if (!empty($page['page_vacancy'])) { ?>
		<section class="about-vacancies">
			<div class="about-vacancies__title"><?=Lang::t('about.tip.joinTeam')?></div>
			<div class="about-vacancies__subtitle"><?=Lang::t('about.tip.looking')?></div>
			<ul class="about-vacancies__list list-unstyled">
				<?php $vacancies = explode("\n", $page['page_vacancy']); ?>
				<?php foreach ($vacancies as $vacancy) { ?>
				<li><?=CHtml::encode($vacancy)?></li>
				<?php } ?>
			</ul>
			<div class="about-vacancies__contact"><?=str_replace(["\r", "\n"], ["", "<br>\n"], Lang::t('about.tip.contact'))?></div>
		</section>
		<?php } ?>

		<section class="about__bottom">
			<div class="lectors lectors--about">
				<div class="lectors__title" style="width: 100%;  margin-top: 29px;"><?//=Lang::t('about.tip.creators')?></div>
				
				<?php if (!empty($members)) { ?>
				<div class="lectors__list">
					<?php foreach ($members as $member) { ?>
					<div class="lector-card">
						<div class="lector-card__photo">
							<?php if (!empty($member['member_image'])) { ?>
							<?php
								$member_image = '';
								$member_image_2x = '';
							
								if (!empty($member['member_image'])) {
									$member_image = json_decode($member['member_image'], true);
									$member_image = $assetsUrl . '/member/' . $member['member_id'] . '/' . $member_image['1x']['path'];
									
									if (!empty($member_image['2x'])) {
										$member_image_2x = $assetsUrl . '/member/' . $member['member_id'] . '/' . $member_image['2x']['path'];
									}
								}
							?>
							<picture>
								<?php /* <source type="image/webp" srcset="images/lector_1.webp, images/lector_1@2x.webp 2x"> */ ?>
								<?php if (!empty($member_image_2x)) { ?>
								<source srcset="<?=$member_image?>, <?=$member_image_2x?> 2x">
								<?php } ?>
								<img src="<?=$member_image?>" alt="">
							</picture>
							<?php } ?>
						</div>
						<div class="lector-card__title"><?=CHtml::encode($member['member_name'])?></div>
						<div class="lector-card__bio"><?=$member['member_description']?></div>
						<?php if (!empty($member['member_facebook'])) { ?>
						<div class="lector-card__social"><a href="<?=CHtml::encode($member['member_facebook'])?>" rel="nofollow" target="_blank">Facebook</a></div>
						<?php } ?>
					</div>
					<?php } ?>
				</div>
				<?php } ?>
			</div>

			<div class="about-contacts">
				<div class="about-contacts__title"><?=Lang::t('about.tip.contacts')?></div>
				<div class="about-contacts__list">
					<?php if (!empty(Yii::app()->params->settings['phone'])) { ?>
					<div class="about-contacts__line">
						<div class="about-contacts__tip"><?=Lang::t('about.tip.call')?></div>
						<div class="about-contacts__value"><a href="tel:+<?=preg_replace('#[^\d]#', '', CHtml::encode(Yii::app()->params->settings['phone']))?>"><?=CHtml::encode(Yii::app()->params->settings['phone'])?></a></div>
					</div>
					<?php } ?>
					<?php if (!empty(Yii::app()->params->settings['mail'])) { ?>
					<div class="about-contacts__line">
						<div class="about-contacts__tip"><?=Lang::t('about.tip.write')?></div>
						<div class="about-contacts__value"><a href="mailto:<?=CHtml::encode(Yii::app()->params->settings['mail'])?>"><?=CHtml::encode(Yii::app()->params->settings['mail'])?></a></div>
					</div>
					<?php } ?>
					<?php if (!empty(Yii::app()->params->settings['lat']) && !empty(Yii::app()->params->settings['long'])) { ?>
					<div class="about-contacts__line">
						<div class="about-contacts__tip"><?=Lang::t('about.tip.find')?></div>
						<div class="about-contacts__value"><a href="https://www.google.com/maps/@<?=Yii::app()->params->settings['lat']?>,<?=Yii::app()->params->settings['long']?>,17z" rel="nofollow" target="_blank"><?=Lang::t('layout.tip.footerAddress')?></a></div>
					</div>
					<?php } ?>
					<?php if (!empty(Yii::app()->params->settings['facebook']) || !empty(Yii::app()->params->settings['instagram'])) { ?>
					<div class="about-contacts__line">
						<div class="about-contacts__tip"><?=Lang::t('about.tip.beWithUs')?></div>
						<div class="about-contacts__value">
							<?php if (!empty(Yii::app()->params->settings['facebook'])) { ?>
							<a href="<?=CHtml::encode(Yii::app()->params->settings['facebook'])?>" rel="nofollow" target="_blank">Facebook</a>
							<?php } ?>
							<?php if (!empty(Yii::app()->params->settings['instagram'])) { ?>
							<a href="<?=CHtml::encode(Yii::app()->params->settings['instagram'])?>" rel="nofollow" target="_blank">Instagram</a>
							<?php } ?>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</section>
		<?php } ?>
        <?php if ($page['feedback_form'] == 0) { ?>
            <br>
            <form action="<?=$this->createUrl('feedback')?>" class="course__form form" method="post">
                <h5><?=Lang::t('feedback.form.title')?></h5>
                <div class="form__row">
                    <input type="hidden" name="feedback[page_alias]" value="<?php echo $page['page_alias'];?>">
                    <div class="form__column form__column--50">
                        <label for="registration-name" class="form__label"><?=Lang::t('feedback.label.name')?></label>
                        <input type="text" required id="registration-name" name="feedback[first_name]" class="form__input">
                    </div>
                    <div class="form__column form__column--50">
                        <label for="registration-surname" class="form__label"><?=Lang::t('feedback.label.surname')?></label>
                        <input type="text" required id="registration-surname" name="feedback[last_name]" class="form__input">
                    </div>
                </div>
                <div class="form__row">
                    <div class="form__column form__column--50">
                        <label for="registration-email" class="form__label"><?=Lang::t('feedback.label.email')?></label>
                        <input type="email" required id="registration-email" name="feedback[email]" class="form__input">
                    </div>
                    <div class="form__column form__column--50">
                        <label for="registration-phone" class="form__label"><?=Lang::t('feedback.label.phone')?></label>
                        <input type="tel" required id="registration-phone" name="feedback[phone]" class="form__input">
                    </div>
                </div>
                <div class="form__row">
                    <div class="form__column form__column--50">
                        <label for="registration-code" class="form__label"><?=Lang::t('feedback.label.message')?></label>
                        <input type="text" id="registration-code" name="feedback[comment]" class="form__input">
                    </div>
                </div>
                <div class="form__row">
                    <div class="form__column form__column--50">
                        <button class="form__btn"><?=Lang::t('feedback.btn.send')?></button>
                    </div>
                </div>
            </form>
        <?php } ?>
	</div>
</main>
