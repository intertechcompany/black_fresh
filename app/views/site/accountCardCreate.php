<style>
    .link-blue {
        color: #0098FF;
    }
    #account-personal {
        display: inline-block;
        margin-left: 45px;
        margin-top: 30px;
        width: 70%;
    }
    #account-addresses {
        width: 100%;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-direction: row;
        flex-direction: row;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
        -ms-flex-line-pack: stretch;
        align-content: stretch;
        -webkit-box-align: start;
        -ms-flex-align: start;
        align-items: flex-start;
    }
</style>
<?php
/* @var $this SiteController */
$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<div class="wrap account-edit">
    <div class="content-divider"></div>
    <?php $this->renderPartial('accountMenu'); ?>
    <div class="account-wrap clearfix">
        <div id="account-personal" class="account-personal">
            <a class="link-blue"
               href="<?=$this->createUrl('/accountedit')?>"
               style="
                        text-decoration: none;
                    "
            >
                <- <?=Lang::t('account.title.userAddressesBack')?>
            </a>
            <h1 class="account-title"><?=Lang::t('account.title.userCard')?></h1>

            <div class="account-personal-data width-flex">



                <form id="account-addresses" action="<?=$this->createUrl('/accountCardCreate')?>" class="account-form hidden" method="post" novalidate>
                    <?php $this->renderPartial('accountUserCardForm', array('user' => $user)); ?>
                </form>
            </div>
        </div>
    </div>
</div>