<style>
    .account-menu-box {
        margin-top: 30px;
        float: left;
        display: inline-block;
        width: 21%;
    }
    h1 {
        margin-top: 25px;
        margin-bottom: 25px;
    }
    .box-content ul {
        list-style: none;
        padding-left: 0;

    }
    .personal-cabinet-title {
        font-size: 19px;
        line-height: 22px;
        margin-bottom: 20px;
    }
    .box-content li {
        margin-bottom: 16px;
        font-size: 18px;
        line-height: 22px;
    }
    .box-content li a {
        text-decoration: none;
    }
    @media (max-width: 1000px) {
        .personal-cabinet-title {
            font-size: 14px;
        }
        .box-content li {
            margin-bottom: 10px;
            font-size: 13px;
            line-height: 17px;
        }
    }
    @media (max-width: 400px) {
        .account-menu-box {
            width: 100%;
        }
    }

</style>
<div class="account-menu-box">
    <div class="box-heading personal-cabinet-title"><?= Lang::t('layout.title.account') ?></div>
    <div class="box-content">
        <ul>
            <li><a href="<?= $this->createUrl('site/accountedit') ?>"><?= Lang::t('layout.title.accountEdit') ?></a></li>

            <?php /* ?><li><a href="<?= $this->createUrl('site/orders') ?>"><?= Lang::t('layout.title.ordersHistory') ?></a></li>
            <?php */ ?>
            <li><a href="<?= $this->createUrl('site/accountSubscription') ?>"><?= Lang::t('layout.title.editSubscription') ?></a></li>
            <li><a href="<?= $this->createUrl('site/accountPromo') ?>"><?= Lang::t('layout.title.promoCode') ?></a></li>
            <li><a href="<?= $this->createUrl('/user-feedback') ?>"><?= Lang::t('layout.title.feedback') ?></a></li>
            <li><a href="<?= $this->createUrl('site/logout') ?>"><?= Lang::t('layout.title.logout') ?></a></li>
        </ul>
    </div>
</div>