<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<main class="course">
    <div class="wrap">
        <?php $this->widget('application.components.Breadcrumbs.Breadcrumbs', ['breadcrumbs' => $this->breadcrumbs]); ?>

        <h1 class="course__title"><?=CHtml::encode($course['course_name'])?></h1>

        <div class="course__head">
            <div class="course__photo">
                <?php if (!empty($course['course_image'])) { ?>
                <?php
                    $course_image = '';
                    $course_image_2x = '';
                
                    if (!empty($course['course_image'])) {
                        $course_image = json_decode($course['course_image'], true);
                        $course_image = $assetsUrl . '/course/' . $course['course_id'] . '/' . $course_image['details']['1x']['path'];
                        
                        if (!empty($course_image['2x'])) {
                            $course_image_2x = $assetsUrl . '/course/' . $course['course_id'] . '/' . $course_image['details']['2x']['path'];
                        }
                    }
                ?>
                <picture>
                    <?php /* <source type="image/webp" srcset="images/lector_1.webp, images/lector_1@2x.webp 2x"> */ ?>
                    <?php if (!empty($course_image_2x)) { ?>
                    <source srcset="<?=$course_image?>, <?=$course_image_2x?> 2x">
                    <?php } ?>
                    <img src="<?=$course_image?>" alt="">
                </picture>
                <?php } ?>
            </div>
            <div class="course__general">
                <?php $course_date = new DateTime($course['course_date'], new DateTimeZone(Yii::app()->timeZone)); ?>
                <?php if ($course['course_type'] == 'event') { ?>
                <div class="course__info">
                   <!--  <div class="course__info-title"><//?=Lang::t('course.tip.dateTime')?></div> -->
                    <?php
                        $date_time = $course_date->format('d.m.Y');
                        $date_time .= ' (' . mb_strtolower(Yii::app()->dateFormatter->format('EEEE', strtotime($course['course_date'])), 'utf-8') . '), ';
                        $date_time .= $course_date->format('H:i');
                    ?>
                    <!-- <div class="course__info-tip"><//?=$date_time?></div> -->
                </div>
                <div class="course__info">
                    <div class="course__info-title"><?=Lang::t('course.tip.price')?></div>
                    <div class="course__info-tip"><?=round($course['course_price'])?> <?=Lang::t('layout.tip.uah')?></div>
                </div>
                <?php } else { ?>
                <div class="course__info">
                    <div class="course__info-title"><?=Lang::t('course.tip.start')?></div>
                    <div class="course__info-tip"><?=$course_date->format('d.m.Y')?></div>
                </div>
                <?php /* <div class="course__info">
                    <div class="course__info-title"><?=Lang::t('course.tip.schedule')?></div>
                    <div class="course__info-tip"><?=Lang::t('course.tip.monthes', $course['course_duration'])?>, <?=Lang::t('course.tip.perWeek', $course['course_duration_week'])?></div>
                </div> */ ?>
                <div class="course__info">
                    <div class="course__info-title"><?=Lang::t('course.tip.price')?></div>
                    <div class="course__info-tip"><?=round($course['course_price'])?> <?=Lang::t('layout.tip.uah')?><?php /* / міс */ ?></div>
                </div>
                <?php } ?>
                <?php if (!empty($course['course_place'])) { ?>
                <div class="course__info">
                    <div class="course__info-title"><?=Lang::t('course.tip.address')?></div>
                    <div class="course__info-tip"><?=CHtml::encode($course['course_place'])?></div>
                </div>
                <?php } ?>
                <?php if (strtotime($course['course_date']) > time()) { ?>
                <div class="course__registration">
                    <a href="#"><?=Lang::t('course.btn.registration')?></a>
                </div>
                <?php } ?>
            </div>
        </div>

        <?php if (!empty($course['course_content_lang'])) { ?>
        <?php $course_content_lang = json_decode($course['course_content_lang'], true); ?>
        <?php foreach ($course_content_lang as $block) { ?>
        <div class="course__extra">
            <div class="course__extra-title"><?=CHtml::encode($block['title'])?></div>
            <div class="course__extra-description">
                <?=$block['text']?>
            </div>
        </div>
        <?php } ?>
        <?php } ?>

        <?php if (!empty($course['course_place'])) { ?>
        <div class="course__extra">
            <div class="course__extra-title"><?=Lang::t('course.tip.place')?></div>
            <div class="course__extra-description">
                <p><?=CHtml::encode($course['course_place'])?></p>
            </div>
        </div>
        <?php } ?>

        <div id="registration" class="course__extra">
            <div class="course__extra-title"><?=Lang::t('course.tip.registration')?></div>
            <div class="course__extra-description">
                <?php if (strtotime($course['course_date']) <= time()) { ?>
                <?php if ($course['course_type'] == 'event') { ?>
                <p><?=Lang::t('course.tip.masterClassStarted')?></p>
                <?php } else { ?>
                <p><?=Lang::t('course.tip.courseStarted')?></p>
                <?php } ?>
                <?php } else { ?>
                <form action="<?=$this->createUrl('ajax/registration')?>" class="course__form form" method="post" novalidate>
                    <div class="form__row">
                        <input type="hidden" name="registration[course_id]" value="<?=$course['course_id']?>">
                        <div class="form__column form__column--50">
                            <label for="registration-name" class="form__label"><?=Lang::t('course.label.name')?></label>
                            <input type="text" id="registration-name" name="registration[first_name]" class="form__input">
                        </div>
                        <div class="form__column form__column--50">
                            <label for="registration-surname" class="form__label"><?=Lang::t('course.label.surname')?></label>
                            <input type="text" id="registration-surname" name="registration[last_name]" class="form__input">
                        </div>
                    </div>
                    <div class="form__row">
                        <div class="form__column form__column--50">
                            <label for="registration-email" class="form__label"><?=Lang::t('course.label.email')?></label>
                            <input type="email" id="registration-email" name="registration[email]" class="form__input">
                        </div>
                        <div class="form__column form__column--50">
                            <label for="registration-phone" class="form__label"><?=Lang::t('course.label.phone')?></label>
                            <input type="tel" id="registration-phone" name="registration[phone]" class="form__input">
                        </div>
                    </div>
                    <div class="form__row">
                        <div class="form__column form__column--50">
                            <label for="registration-code" class="form__label"><?=Lang::t('course.label.promocode')?></label>
                            <input type="text" id="registration-code" name="registration[code]" class="form__input">
                        </div>
                    </div>
                    <div class="form__row">
                        <div class="form__column form__column--50">
                            <button class="form__btn"><?=Lang::t('course.btn.registration')?></button>
                        </div>
                    </div>
                </form>
                <?php } ?>
            </div>
        </div>
    </div>
</main>