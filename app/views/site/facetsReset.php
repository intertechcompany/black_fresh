<?php
	$facet_url = array(
		'alias' => $category['category_alias'],
	);

	if ($sort != 'popular') {
		$facet_url['sort'] = $sort;
	}

	/* $price_facet = $facets->getPriceFacet();

	if ($price_facet['min'] != $price_facet['from'] || $price_facet['max'] != $price_facet['to']) {
		$facet_url['filter']['price'] = array(
			'from' => $price_facet['from'],
			'to' => $price_facet['to'],
		);
	}

	$length_facet = $facets->getLengthFacet();

	if ($length_facet['min'] != $length_facet['from'] || $length_facet['max'] != $length_facet['to']) {
		$facet_url['filter']['length'] = array(
			'from' => $length_facet['from'],
			'to' => $length_facet['to'],
		);
	}

	$width_facet = $facets->getWidthFacet();

	if ($width_facet['min'] != $width_facet['from'] || $width_facet['max'] != $width_facet['to']) {
		$facet_url['filter']['width'] = array(
			'from' => $width_facet['from'],
			'to' => $width_facet['to'],
		);
	} */

	$selected_facets = $facets->getSelectedFacets();
	$values = array_column($selected_facets, 'value_id');
?>
<div class="category__reset">
	<span>Фильтры:</span>

	<?php if (isset($facet_url['filter']['price'])) { ?>
	<?php 
		$current_facet_url = $facet_url;
		unset($current_facet_url['filter']['price']);

		if (!empty($values)) {
			$current_facet_url['filter']['p'] = $values;
		}

		$facet_reset_url = $this->createUrl('category', $current_facet_url);
	?>
	<a href="<?=$facet_reset_url?>"><?=CHtml::encode(Lang::t('facets.tip.filterPrice', array('{from}' => $facet_url['filter']['price']['from'], '{to}' => $facet_url['filter']['price']['to'])))?><i class="icon icon-reset"></i></a>
	<?php } ?>
	
	<?php foreach ($selected_facets as $facet) { ?>
	<?php
		$current_facet_url = $facet_url;
		$values_tmp = $values;

		$value_position = array_search($facet['value_id'], $values_tmp);
		unset($values_tmp[$value_position]);

		if (!empty($values_tmp)) {
			$current_facet_url['filter']['p'] = $values_tmp;
		}

		$facet_reset_url = $this->createUrl('category', $current_facet_url);
	?>
	<a href="<?=$facet_reset_url?>"><?=CHtml::encode($facet['value_title'])?> &times;</a>	
	<?php } ?>

	<?php if (isset($facet_url['filter']['length'])) { ?>
	<?php 
		$current_facet_url = $facet_url;
		unset($current_facet_url['filter']['length']);

		if (!empty($values)) {
			$current_facet_url['filter']['p'] = $values;
		}

		$facet_reset_url = $this->createUrl('category', $current_facet_url);
	?>
	<a href="<?=$facet_reset_url?>"><?=CHtml::encode(Lang::t('facets.tip.filterLength', array('{from}' => $facet_url['filter']['length']['from'], '{to}' => $facet_url['filter']['length']['to'])))?><i class="icon icon-reset"></i></a>
	<?php } ?>

	<?php if (isset($facet_url['filter']['width'])) { ?>
	<?php 
		$current_facet_url = $facet_url;
		unset($current_facet_url['filter']['width']);

		if (!empty($values)) {
			$current_facet_url['filter']['p'] = $values;
		}

		$facet_reset_url = $this->createUrl('category', $current_facet_url);
	?>
	<a href="<?=$facet_reset_url?>">
	<?=CHtml::encode(Lang::t('facets.tip.filterWidth', array('{from}' => $facet_url['filter']['width']['from'], '{to}' => $facet_url['filter']['width']['to'])))?><i class="icon icon-reset"></i></a>
	<?php } ?>

	<a href="<?=$this->createUrl('category', $facet_url)?>" class="category__reset-all">сбросить все &times;</a>
</div>