<div class="wrapper">
    <div class="section section-form">
        <div class="container">
            <div class="section__half">
                <div class="container half"><button class="btn btn__arrow"></button>
                    <div class="section-form__title"><?=Lang::t('festivals.block5.title')?></div>
                    <p class="section-form__description"><?=Lang::t('festivals.block5.description')?></p>
                    <div class="section-form__info info">
                        <div class="info__items"><span class="info__item">💸 <?=Lang::t('festivals.block5.text1')?></span><span class="info__item">☕️ <?=Lang::t('festivals.block5.text2')?></span><span class="info__item">🫂 <?=Lang::t('festivals.block5.text3')?></span></div>
                        <div class="info__items"><span class="info__item">😎 <?=Lang::t('festivals.block5.text4')?></span><span class="info__item">📌️ <?=Lang::t('festivals.block5.text5')?></span><span class="info__item">🇺🇦 <?=Lang::t('festivals.block5.text6')?></span></div>
                    </div>
                </div>
            </div>
            <div class="section__half form" data-form-wrapper>
                <div class="container half">
                    <form action="/festivals-feedback" method="post" data-type="form" id="form">
                        <div class="form__types"><button type="button" class="btn btn__arrow"></button><label class="radio"><input class="radio__input" type="radio" name="type" value="personal" autocomplete="off">
                                <div class="radio__text"><?=Lang::t('festivals.form.personal')?></div>
                            </label><label class="radio"><input class="radio__input" type="radio" name="type" value="business" checked="checked" autocomplete="off">
                                <div class="radio__text"><?=Lang::t('festivals.form.business')?></div>
                            </label></div>
                        <div class="form__items"><input class="input" name="user_name" placeholder="<?=Lang::t('festivals.form.name')?>" data-type="form-item" autocomplete="off"><input data-type="form-item" class="input" type="email" name="email" placeholder="<?=Lang::t('festivals.form.email')?>" autocomplete="off"><input data-type="form-item" data-phone-mask class="input" type="tel" name="phone" placeholder="<?=Lang::t('festivals.form.phone')?>" autocomplete="off"><label class="check" data-type="form-item"><input data-type="form-item" class="check__input" type="checkbox" name="terms" autocomplete="off"><span class="check__box"></span>
                                <div class="check__text"><span><?=Lang::t('festivals.form.text1')?></span> <a href="<?php echo Yii::app()->params->settings['festivals_page1']; ?>" target="_blank"><?=Lang::t('festivals.form.page1')?></a> <span><?=Lang::t('festivals.form.text2')?></span> <a href="<?php echo Yii::app()->params->settings['festivals_page2']; ?>" target="_blank"><?=Lang::t('festivals.form.page2')?>.</a></div>
                            </label><button class="btn btn__submit"><?=Lang::t('festivals.form.join')?></button></div>
                    </form>
                    <div class="form__success form-success"><img class="form-success__icon" src="/festival-assets/images/icons/ok.svg" alt="ok"><span class="form-success__title"><?=Lang::t('festivals.form.success.title')?></span><span class="form-success__description"><?=Lang::t('festivals.form.success.description')?></span><img class="form-success__logo" src="/festival-assets/images/icons/logo-small.svg" alt="fresh black"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="section section__logo section-logo">
        <div class="container section-logo__container">
            <picture data-bi>
                <source srcset="/festival-assets/images/mockups/main-background-mob.webp" media="(max-width: 550px)">
                <img src="/festival-assets/images/mockups/main-background.webp" alt="main">
            </picture>
            <div class="background-green section-logo__logo"><img src="/festival-assets/images/logo.svg" alt="logo"></div>
            <div class="section-logo__label first"><?=Lang::t('festivals.block1.word1')?></div>
            <div class="section-logo__label second"><?=Lang::t('festivals.block1.word2')?></div>
            <div class="section-logo__label third"><?=Lang::t('festivals.block1.word3')?></div>
            <div class="section-logo__description"><?=Lang::t('festivals.block1.text1')?></div><button data-scroll-to="#form" class="btn btn__arrow main"></button>
        </div>
    </div>
    <div class="logos">
        <div class="container" data-type="fill-wrapper"><img data-type="fill-item" src="/festival-assets/images/icons/logo-ok.svg" alt="fresh-black"></div>
    </div>
    <div class="section section-command">
        <div class="container">
            <h1 class="section-command__title"><?=Lang::t('festivals.block2.title')?></h1>
            <div class="section-command__description">
                <p><?=Lang::t('festivals.block2.description')?></p><button data-scroll-to="#form" class="btn btn__arrow"></button>
            </div>
            <div class="commands">
                <div class="swiper commands-swiper">
                    <div class="swiper-wrapper" data-commands>
                        <img class="swiper-slide commands__item" src="/festival-assets/images/mockups/commands/1.png" alt="">
                        <img class="swiper-slide commands__item" src="/festival-assets/images/mockups/commands/2.png" alt="">
                        <img class="swiper-slide commands__item" src="/festival-assets/images/mockups/commands/3.png" alt="">
                        <img class="swiper-slide commands__item" src="/festival-assets/images/mockups/commands/4.png" alt="">
                        <img class="swiper-slide commands__item" src="/festival-assets/images/mockups/commands/5.png" alt="">
                        <img class="swiper-slide commands__item" src="/festival-assets/images/mockups/commands/6.png" alt="">
                        <img class="swiper-slide commands__item" src="/festival-assets/images/mockups/commands/7.png" alt="">
                        <img class="swiper-slide commands__item" src="/festival-assets/images/mockups/commands/8.png" alt="">
                        <img class="swiper-slide commands__item" src="/festival-assets/images/mockups/commands/9.png" alt="">
                        <img class="swiper-slide commands__item" src="/festival-assets/images/mockups/commands/10.png" alt="">
                        <img class="swiper-slide commands__item" src="/festival-assets/images/mockups/commands/11.png" alt="">
                        <img class="swiper-slide commands__item" src="/festival-assets/images/mockups/commands/12.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section section-faq">
        <div class="container">
            <div class="section__half"><button data-scroll-to="#form" class="btn btn__arrow"></button>
                <p><?=Lang::t('festivals.block3.description1')?></p>
            </div>
            <div class="section__half"><a href="https://chat.whatsapp.com/J84xLKyWJLBKSngojcinM8" target="_blank" class="btn btn__phone"></a>
                <p><?=Lang::t('festivals.block3.description2')?></p>
            </div>
        </div>
    </div>
    <div class="section section-description">
        <div class="container">
            <div class="text-block white section-description__logo"><img src="/festival-assets/images/icons/logo-big.svg" alt="fresh-black"></div>
            <div class="section-description__text"><span>
                    <?=Lang::t('festivals.block4.description')?>
                </span></div>
        </div>
    </div>
    <div class="logos">
        <div class="container" data-type="fill-wrapper"><img data-type="fill-item" src="/festival-assets/images/icons/logo-ok.svg" alt="fresh-black"></div>
    </div>
    
    <footer class="section footer">
        <div class="container">
            <div class="pay">
            <div style="color: #555555;font-size: 17px;padding-bottom: 10px;text-align: center;line-height: 24px;"><?=Lang::t('festivals.payment.text')?></div>
<script async
  src="https://js.stripe.com/v3/buy-button.js">
</script>

<stripe-buy-button
  buy-button-id="buy_btn_1NLPZ0AIfrnLqqFeL57sYRsq"
  publishable-key="pk_live_51NIrVZAIfrnLqqFe0AlIjuiYIufibUMIMALwNpCAGrGCKuWn5O9tJ8pUM47aj8emnSeKDYa6pVzDfwEcz3rCRDCx002HVOk73A"
>
</stripe-buy-button>
            </div>
            <div class="footer-links">
                <?php if(!empty(Yii::app()->params->settings['festivals_connect'])): ?>
                  
                <a class="footer-links__item" href="<?php echo Yii::app()->params->settings['festivals_connect']; ?>" target="_blank"><?=Lang::t('festivals.connect')?></a>
                <?php endif; ?>
                <?php if(!empty(Yii::app()->params->settings['festivals_instagram'])): ?>
                <a class="footer-links__item" href="<?php echo Yii::app()->params->settings['festivals_instagram']; ?>" target="_blank"><?=Lang::t('festivals.instagram')?></a>
                <?php endif; ?>
                <a class="footer-links__item" href="https://www.facebook.com/Fresh.Black.Okay/" target="_blank"><?=Lang::t('festivals.facebook')?></a>
            </div>
        </div>
    </footer>
</div>

<!-- <script type="text/javascript" id="widget-wfp-script"
        src="https://secure.wayforpay.com/server/pay-widget.js?ref=button"></script>

<script type="text/javascript">
    function runWfpWdgt(url) {
        var wayforpay = new Wayforpay();
        wayforpay.invoice(url);
    }
</script> -->
