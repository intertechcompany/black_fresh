<?php
/* @var $this SiteController */
?>

<style>
    .input {
        width: 420px;
        height: 40px;

        margin-top: 10px;
    }

    @media (min-width: 320px) and (max-width: 1000px){
        .input {
            width: 100%;
        }
    }
</style>

<div class="wrap">
    <div class="content-divider"></div>

    <div class="auth-form">
        <h1><?=CHtml::encode($this->pageTitle)?></h1>
        <form id="reset-form" method="post" data-type="newpassword" novalidate>
            <?php if (!empty($result['errorCode'])) { ?>
                <div class="error-msg"><?=$result['errorCode']?></div>
            <?php } elseif (Yii::app()->user->hasFlash('reset_error')) { ?>
                <div class="error-msg"><?=Yii::app()->user->getFlash('reset_error')?></div>
            <?php } ?>
            <input type="hidden" name="newpassword[user_id]" value="<?=$result['uid']?>">
            <input type="hidden" name="newpassword[token]" value="<?=$result['token']?>">
            <div class="form-row clearfix">
                <div class="" style="display: block; width: 100%;">
                    <label for="reset-password" style="width: auto"><?=Lang::t('newPassword.label.newPassword')?></label>
                    <br>
                    <input id="reset-password" class="input" <?php if (isset($result['errorFields']['password'])) { ?> class="error-field"<?php } ?> type="password" name="newpassword[password]" value="">
                    <?php if (!empty($result['errorFields']['password'][0])) { ?><div class="error-msg"><?=implode('<br>', $result['errorFields']['password'])?></div><?php } ?>
                </div>
            </div>
            <div class="form-row clearfix">
                <div class="form-col form-col-100">
                    <label for="reset-password_confirm"><?=Lang::t('newPassword.label.confirmNewPassword')?></label>
                    <br>
                    <input id="reset-password_confirm" class="input" <?php if (isset($result['errorFields']['password_confirm'])) { ?> class="error-field"<?php } ?> type="password" name="newpassword[password_confirm]" value="">
                    <?php if (!empty($result['errorFields']['password_confirm'][0])) { ?><div class="error-msg"><?=implode('<br>', $result['errorFields']['password_confirm'])?></div><?php } ?>
                </div>
            </div>
<!--            <div style="margin-top: 10px;">-->
<!--                <a href="--><?php //echo $this->createUrl('site/login')?><!--">--><?//=Lang::t('newPassword.link.loginToAccount')?><!--</a>-->
<!--            </div>-->
            <div class="form-row form-row-btn clearfix">
                <div class="form-col form-col-100" style="margin-top: 20px;">
                    <button class="btn"><?=Lang::t('newPassword.btn.changePassword')?></button>
                </div>
            </div>
        </form>
    </div>
</div>