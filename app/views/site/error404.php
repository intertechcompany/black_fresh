<?php
	/* @var $this SiteController */
	/* @var $error array */

	$this->pageTitle = Lang::t('error404.h1.notFound');
	$this->pageDescription = '';
	$this->pageKeywords = '';
?>
<div class="wrap">
	<div class="page">
		<h1 class="page__title"><?=Lang::t('error404.h1.notFound')?></h1>

		<div class="page__description">
			<?=Lang::t('error404.tip.notFoundText', ['{home_url}' => Yii::app()->homeUrl])?>
		</div>
	</div>
</div>