<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<main class="article">
	<div class="wrap">
		<?php $this->widget('application.components.Breadcrumbs.Breadcrumbs', ['breadcrumbs' => $this->breadcrumbs]); ?>

		<h1 class="article__title"><?=CHtml::encode($blog['blog_title'])?></h1>
		
		<?php if (!empty($blog['blog_photo'])) { ?>
		<?php
			$blog_image = '';
			$blog_image_2x = '';
		
			if (!empty($blog['blog_photo'])) {
				$blog_image = json_decode($blog['blog_photo'], true);
				$blog_image_size = $blog_image['details']['1x']['size'];
				$blog_image = $assetsUrl . '/blog/' . $blog['blog_id'] . '/' . $blog_image['details']['1x']['path'];
				
				if (!empty($blog_image['details']['2x'])) {
					$blog_image_2x = $assetsUrl . '/blog/' . $blog['blog_id'] . '/' . $blog_image['details']['2x']['path'];
				}
			}
		?>
		<div class="article__img">
			<picture>
				<!-- <source type="image/webp" srcset="images/news.webp, images/news@2x.webp 2x"> -->
				<?php if (!empty($blog_image_2x)) { ?>
				<source srcset="<?=$blog_image?>, <?=$blog_image_2x?> 2x">
				<?php } ?>
				<img src="<?=$blog_image?>" alt="">
			</picture>
		</div>
		<?php } ?>

		<div class="article__content">
			<?=$blog['blog_description']?>
		</div>
	</div>
</main>
