<style>
    body {
        background: #6CC0F7;
    }
    .parents * {
        color: #322419;
        font-size: 20px;
        line-height: 20px;
        font-style: normal;
        font-weight: 400;
        font-family: 'Proxima Nova Cn Rg',Arial,Helvetica,sans-serif;
    }
    .parents a {
        text-decoration: none;
    }
    /*.parents h1, .parents h2, .parents h3, .parents h4 {*/
    /*    font-family: 'Proxima Nova Cn Rg',Arial,Helvetica,sans-serif;*/
    /*}*/
    .container.parents {
        background: #6CC0F7;
        display: flex;
        flex-direction: column;
        max-width: 1440px;
        margin: auto;
    }
    .parents section, .parents .section {
        padding: 0px 51px 120px 51px;
    }
    .parents .screen3 {
        padding: 0px 140px 120px 139px;
    }
    .parents section:nth-of-type(1) {
        padding-top: 65px;
    }
    .screen1 h1 {
        font-size: 90px;
        line-height: 80px;
        text-transform: uppercase;
        margin: 0;
        padding: 0;
        margin-bottom: 38px;
        font-weight: 700;
    }
    .screen2 h2 {
        font-size: 70px;
        line-height: 63px;
        font-weight: 700;
        text-transform: uppercase;
        font-weight: 700;
        margin: 0;
        padding: 0;
        margin-bottom: 65px
    }
    .screen3 h3 {
        font-size: 64px;
        line-height: 57px;
        margin: 0;
        padding: 0;
        font-weight: 700;
        text-transform: uppercase;
        color: #FFFFFF;
        font-weight: 700;
        margin-bottom: 40px;
    }
    .screen4 h4 {
        font-weight: 700;
        font-size: 123px;
        line-height: 110px;
        text-align: center;
        text-transform: uppercase;
        margin: 0;
        padding: 0;
        margin-bottom: 22px;
        font-weight: 700;
    }
    .parents p {
        margin: 0;
        padding: 0;
        font-family: 'KharkivTone',Arial,Helvetica,sans-serif;
    }
    .screen3 span, .screen3 p {
        font-weight: 400;
        font-size: 18px;
        line-height: 18px;
        color: #FFFFFF;
        margin-left: 2px;
    }
    .absolute {
        position: absolute;
    }
    .start-end {
        justify-content: flex-start;
        align-items: end;
    }
    .screen2 .wrapper {
        background-color: #FFE7D4;
        border-radius: 91px;
    }
    .screen4 .wrapper {
        padding: 66px 78px 43px 77px;
    }
    .screen4 .right-block {
        margin-left: 60px;
    }
    .screen4 p {
        font-style: normal;
        font-weight: 400;
        font-size: 18px;
        line-height: 18px;
        text-transform: uppercase;
        text-align: center;
    }

    .screen3 a {
        font-weight: 700;
        font-size: 50px;
        line-height: 48px;
        text-transform: uppercase;
        color: #FFFFFF;
        margin-bottom:5px;
        margin-left: 2px;
        overflow-wrap: break-word;
    }
    .screen3 a:not(.btn-primary) {
        min-height: 93px;
        display: block;
    }
    .screen3 img {
        margin-bottom: 32px;
    }
    .screen4 a {
        top: 30%;
        left: 35%;
    }
    .screen5 a img {
        width: 46px;
        height: 51px;
    }
    .screen1 .left-block {
        padding: 99px 0px 99px 88px;
        max-width: 48%;
        z-index: 1;
    }
    .screen2 .left-block {
        padding: 98px 0px 80px 88px;
        max-width: 573px;
    }
    .parents .size-100 {
        font-size: 103px;
        line-height: 103px;
        max-width: 320px;
    }
    .parents .footer-parents h2 {
        font-size: 80px;
        max-width: 631px;
        line-height: 80px;
    }
    .parents .price {
        font-weight: 700;
        font-size: 36px;
        line-height: 44px;
        color: #FFFFFF;
        display: none;
    }
    .parents .price-block {
        align-items: center;
        justify-content: start;
        margin-top: 20px;
    }
    .parents .mb-60 {
        margin-bottom: 60px;
    }
    .parents .mb-40 {
        margin-bottom: 40px;
    }
    .parents .d-flex {
        display: flex;
    }
    .parents .row {
        flex-direction: row;
    }
    .parents .col {
        flex-direction: column;
    }
    .parents .col-6 {
        width: 50%;
    }
    .parents .center-between {
        justify-content: space-between;
        align-items: center;
    }
    .parents .center-center {
        justify-content: center;
        align-items: center;
    }
    .parents .btn-primary {
        display: flex;
        flex-direction: row;
        justify-content: center;
        align-items: center;
        width: 100%;
        max-width: 1161px;
        height: 67px;
        line-height: 35px;
        font-size: 29px;
        background: none;
        font-weight: 700;
        border: 4px solid #322419;
        border-radius: 100px;
        text-decoration: none;
        padding: 16px 20px;
        gap: 17.28px;
        margin: 0 auto;
        margin-top: 40px;
        text-transform: none;
    }

    .parents .btn-primary:hover {
        border: none;
        background: #FFE7D4;
        border-radius: 100px;
        color: #322419;
    }

    .parents .btn--add {
        width: 131px;
        height: 36px;
        padding: 6px 24px;
        font-weight: 700;
        font-size: 20px;
        line-height: 24px;
        color: #000000;
        overflow: hidden;
        background: #FFFFFF;
        border-radius: 100px;
        border: none;
        margin: 0px;
        /*margin-left: 31px;*/
    }

    .parents .image3, .parents .image4 {
        height: 100%;
        width: 100%;

    }
    .parents .image3 img {
        width: 772px;
        height: 772px;
    }

    .parents .image4 img {
        width: 524px;
        height: 524px;
    }

    .parents .image4 {
        display: none;
        margin-top: -100px;
    }

    .parents .image1 {
        height: 100%;
        width: 100%;
    }
    .parents .image1 img {
        width: 786.78px;
        height: 636.16px;
    }
    .parents .image1--desktop {
        display: block;
        /*width: 820px;*/
        position: absolute;
        right: 0;
        max-height: 100%;
    }
    .image3--desktop {
        display: block;
    }
    .image3--mobile {
        display: none;
    }
    .parents .image2--desktop {
        display: block;
        right: 0;
        position: absolute;
        height: 100%;
    }

    .parents .image1--mobile {
        display: none;
    }
    .parents .image2--mobile {
        display: none;
    }
    .parents .image2 {
        margin-top: 10px;
        margin-bottom: 0px;
    }

    .parents .image2 img {
        width: 100%;
        height: 100%;
        border: 1px solid #FFFFFF;
    }

    .parents .image2--mobile {
        display: none;
    }

    .parents .card-list {
        display: flex;
        align-items: baseline;
        justify-content: flex-start;
        flex-wrap: wrap;
    }

    .parents .card {
        width: 32%;
        margin-bottom: 40px;
        overflow-wrap: break-word;
        position: relative;
    }

    .parents .card:first-child {
        margin-right: 20px;
    }
    .parents .card:nth-child(2n) {
        margin-right: 20px;
    }
    .parents .card:last-child {
        margin-right: 0px;
    }

    .parents .card img {
        width: 100%;
        height: 434px;
        border: 4px solid #FFFFFF;
        border-radius: 20px;
    }

    .parents .bordered {
        border: 4px solid #322419;
        border-radius: 77px;
    }

    .parents .relative {
        position: relative;
    }

    .footer-parents svg {
        cursor: pointer;
    }


    .footer-parents svg:nth-child(2) {
        margin: 0px 24px;
    }
    .footer-parents span {
        margin-left: 15px;
        font-size: 60px;
    }
    .screen5 {
        padding-bottom: 84px;
    }

    .screen5 .wrapper {
        background: #FFE7D5;
        border-radius: 91px;
        border-bottom-right-radius: 120px;
        border-bottom-left-radius: 120px;
    }

    .screen5 .main-block {
        padding: 117px 94px 36px 91px
    }

    .image3--desktop {
        width: 100%;
    }

    .screen5 h5 {
        font-weight: 700;
        font-size: 112px;
        line-height: 100px;
        text-transform: uppercase;
        margin: 0;
        padding: 0;
        margin-bottom: 34px;
    }

    .screen5 p {
        font-weight: 400;
        font-size: 18px;
        line-height: 18px;
    }

    .screen5 .card-list {
        display: flex;
        align-items: center;
        justify-content: space-between;
        flex-wrap: wrap;
        margin-top: 82px;
    }

    .screen5 .card-video {
        position: relative;
    }

    .screen5 .card-video a {
        position: absolute;
        top: 45%;
        left: 43%;
    }

    .screen5 .card-video img {
        width: 100%;
        max-width: 345px;
    }

    .screen5 .max-width {
        max-width: 626px;
    }

    .parents section.footer-parents {
        padding: 0px 140px 60px 139px;
    }

    .footer-parents h6 {
        font-weight: 700;
        font-size: 54px;
        line-height: 48px;
        text-transform: uppercase;
        color: #FFFFFF;
        max-width: 487px;
        margin: 0;
        padding: 0;
    }


    .parents .product-card__variants {
        background: #F6F6F6;
        border: 1px solid rgba(0, 0, 0, 0.3);
        box-sizing: border-box;
        border-radius: 18px;
        max-width: 369px;
        max-height: 111px;
        width: max-content;
        margin-bottom: 0px;
    }
    .parents .product-card__variants span, .parents .product-card__variants * {
        color: #322419;
        font-size: 14px;
        line-height: 30px;
    }
    .parents .product-card__radio {
        margin-right: 0;
        margin-left: -2px;
    }
    .parents .product-card__radio span {
        border: none;
    }
    .parents .product-card__radio input:checked+span, .parents .product__radio input:checked+span {
        background-color: #000;
        background-color: var(--white-color);
        color: #fff;
        color: var(--black-color);
        border: 1px solid #000;
        border-radius: 15px;
    }
    .parents .product-card__variants {
         background: #F6F6F6;
         border: 1px solid rgba(0, 0, 0, 0.3);
         box-sizing: border-box;
         border-radius: 18px;
         max-width: 369px;
         max-height: 111px;
         width: max-content;
         margin-bottom: 0px;
     }
    .parents .product-card__variants span, .parents .product-card__variants * {
        color: #322419;
        font-size: 14px;
        line-height: 30px;
    }
    .parents .product-card__radio {
        margin-right: 0;
    }
    .parents .product-card__radio span {
        border: none;
    }
    .parents .product-card__radio input:checked+span, .parents .product__radio input:checked+span {
        background-color: #000;
        background-color: var(--white-color);
        color: #fff;
        color: var(--black-color);
        border: 1px solid #000;
        border-radius: 15px;
    }
    .parents .mobile {
        display: none !important;
    }
    .parents .icons {
        max-width: fit-content;
        width: 100%;
    }
    .product-mob-icon {
        display: none;
    }

    @media screen and (max-width: 1530px) {
        .parents .card {
            width: 31%;
        }
        .parents .card img {
            height: 100%;
        }
    }

    @media screen and (max-width: 1430px) {
        .screen1 .left-block {
            padding: 59px 0px 59px 48px;
        }
        .screen2 .left-block {
            padding: 58px 0px 40px 48px;
            max-width: 513px;
        }
        .screen4 .right-block {
            margin-left: 40px;
        }
        .screen4 .wrapper {
            padding: 20px 58px 23px 57px;
        }
        .screen5 .main-block{
            padding: 97px 74px 16px 71px;
        }
        .screen4 h4 {
            font-size: 103px;
            line-height: 90px;
        }
        .parents .image1--desktop {
            width: 720px;
        }
        .screen3 a {
            font-size: 40px;
            line-height: 38px;
        }
    }

    @media screen and (max-width: 1330px) {
        .parents .image1--desktop {
            width: 620px;
        }
        .card-video {
            width: 30%;
        }
        .screen1 h1 {
            font-size: 70px;
            line-height: 75px;
        }
    }

    @media screen and (max-width: 1230px) {
        .parents .image1--desktop {
            width: 610px;
        }
        /*.parents .image2--desktop {
            width: 520px;
        }*/
        .screen1 h1 {
            font-size: 60px;
            line-height: 57px;
        }
        .screen2 h2 {
            font-size: 50px;
            line-height: 43px;
        }
        .screen3 a {
            font-size: 35px;
            line-height: 33px;
        }
        .parents .price {
            font-size: 30px;
            line-height: 28px;
        }
        .screen4 h4 {
            font-size: 83px;
            line-height: 70px;
        }
        .screen4 .image3--desktop {
            width: 419px;
        }
        .screen4 a.mob-hidden {
            top: 100px;
            left: 140px;
        }
        .screen4 a.mob-hidden img {
            width: 58px;
        }
    }

    @media screen and (max-width: 1130px) {
        /*.parents .image2--desktop {
            width: 490px;
        }*/
        .screen2 h2 {
            font-size: 45px;
            line-height: 38px;
        }
        .parents .card img {
            height: 100%;
        }
        .parents .product-card__variants * {
            font-size: 10px;
        }
        .screen4 h4 {
            font-size: 63px;
            line-height: 60px;
        }
        .screen4 .image3--desktop {
            width: 379px;
        }
        .screen4 a.mob-hidden {
            top: 90px;
            left: 130px;
        }
        .screen4 a.mob-hidden img {
            width: 48px;
        }

        .parents .footer-parents svg:nth-child(1) {
            width: 62px;
            height: 62px;
        }
        .parents .footer-parents svg:nth-child(3) {
            width: 82px;
            height: 55px;
        }
        .parents .footer-parents svg:nth-child(2) {
            width: 67px;
            height: 67px;
            margin: 0px 10px;
        }
    }

    @media screen and (max-width: 1030px) {
        .screen1 h1 {
            font-size: 50px;
            line-height: 53px;
        }
        .screen1 .left-block{
           padding: 49px 0px 49px 38px; 
        }
        
        .screen2 h2 {
            font-size: 40px;
            line-height: 33px;
        }
        .screen2 .left-block {
            max-width: 463px;
        }
        .screen5 h5 {
            font-size: 102px;
            line-height: 90px;
        }
        .parents .image1--desktop {
            width: 550px;
        }
        /*.parents .image2--desktop {
            width: 420px;
        }*/
        .parents .card {
            width: 46%;
        }
        .parents .card:first-child {
            margin-right: 0px;
        }
        .parents .card:nth-child(2n) {
            margin-left: 20px;
            margin-right: 0px;
        }
        .parents .card:last-child {
            margin-right: 0px;
        }
        .screen3 a {
            font-size: 30px;
            line-height: 28px;
        }
        .parents .price {
            font-size: 22px;
            line-height: 20px;
        }
        .parents .footer-parents svg:nth-child(1) {
            width: 52px;
            height: 52px;
        }
        .parents .footer-parents svg:nth-child(3) {
            width: 72px;
            height: 45px;
        }
        .parents .footer-parents svg:nth-child(2) {
            width: 57px;
            height: 57px;
            margin: 0px 5px;
        }
        .footer-parents h6 {
            font-size: 46px;
        }
    }

    @media screen and (max-width: 930px) {
        .screen1 h1 {
            font-size: 40px;
            line-height: 41px;
        }
        .screen2 h2 {
            font-size: 30px;
            line-height: 23px;
        }
        .screen2 .left-block {
            max-width: 333px;
        }
        .screen5 .main-block{
            padding: 87px 64px 6px 61px;
        }
        .screen5 h5 {
            font-size: 92px;
            line-height: 80px;
        }
        .parents .image1--desktop {
            width: 470px;
        }
        .parents .image2--desktop {
            width: 380px;
        }
        .parents .card {
            width: 48%;
        }
        .screen4 h4 {
            font-size: 45px;
            line-height: 45px;
        }
        .screen4 .image3--desktop {
            width: 300px;
        }
        .screen4 a.mob-hidden {
            top: 70px;
            left: 110px;
        }
        .screen4 a.mob-hidden img {
            width: 38px;
        }
        .footer-parents h6 {
            font-size: 38px;
        }
    }

    @media screen and (max-width: 799px) {
        .screen5 .card-video {
            text-align: center;
        }
        /*.screen5 .card-video a {
            left: 47%;
        }*/
        .screen5 a img {
            height: 100%;
        }
        .parents * {
            font-size: 16px;
            line-height: 16px;
            font-weight: 400;
        }

        .parents section {
            padding: 0px 12px 100px 12px;
        }

        .parents .screen3 {
            padding: 0px 12px 20px 12px;
        }

        .parents section:nth-of-type(1) {
            padding-top: 10px;
        }

        .parents .col-mob {
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }

        .screen1 .bordered {
            border-radius: 42px;
            border: 2px solid #322419;
        }

        .screen1 h1 {
            font-size: 62px;
            line-height: 55px;
            margin-bottom: 20px;
            overflow-wrap: break-word;
        }

        .screen2 h2 {
            font-size: 50px;
            line-height: 44px;
            margin-bottom: 30px;
            overflow-wrap: break-word;
        }

        .screen1 .left-block {
            padding: 30px 19px 27px 18px;
            max-width: 100%;
        }

        .screen1 .btn-primary {
            margin-top: 20px;
        }

        .screen2 .left-block {
            padding: 50px 8px 50px 18px;
            max-width: 100%;
        }

        .screen2 .wrapper {
            border-radius: 45px;
        }

        .screen4 .bordered {
            border: none;
            padding-left: 30px;
            padding-right: 30px;
        }

        .screen4 h4 {
            font-size: 50px;
            line-height: 57px;
            text-align: center;
            margin-top: 28px;
            margin-bottom: 19px;
        }

        .screen4 .right-block {
            margin-left: 0px;
        }

        .screen4 p {
            font-size: 11px;
            line-height: 11px;
            text-transform: uppercase;
            font-weight: 700;
        }

        .screen5 .main-block {
            padding: 50px 29px 22px 30px;
        }

        .screen5 h5 {
            font-size: 92px;
            line-height: 80px;
            margin-bottom: 30px;
        }

        .screen5 p {
            font-size: 18px;
            line-height: 18px;
        }

        .parents .screen5 {
            padding: 0px;
        }

        .screen5 .wrapper {
            border-radius: 0px;
        }

        .parents .image1--desktop {
            display: none;
        }

        .parents .image2--desktop {
            display: none;
        }

        .image3--desktop {
            display: none;
        }

        .image3--mobile {
            display: block;
        }

        .image3--mobile img {
            width: 100%;
        }

        .parents .image1--mobile {
            display: block;
            width: 100%;
        }

        .parents .image2--mobile {
            display: block;
            width: 100%;
        }

        .parents .card-list {
            flex-direction: column;
        }

        .screen3 .card-list {
            align-items: center;
            flex-wrap: nowrap;
        }

        .parents .card {
            margin-left: 0px !important;
            margin-right: 0px !important;
            margin-bottom: 80px;
            width: 96%;
        }

        .parents .card img {
            height: 100%;
        }

        .screen3 a {
            font-size: 49px;
            line-height: 47px;
        }

        .screen3 .price {
            font-size: 33px;
            line-height: 43px;
        }

        .screen3 p {
            font-size: 18px;
            line-height: 18px;
        }

        .screen3 .btn--add {
            font-size: 20px;
            line-height: 24px;
        }

        .mob-hidden {
            display: none;
        }

        .card-video {
            width: 90%;
            margin: 0;
            margin-bottom: 40px;
        }

        .screen5 .max-width {
            max-width: 100%;
            overflow-wrap: break-word;
        }

        .parents .desktop {
            display: none !important;
        }

        .parents .mobile {
            display: flex !important;
        }

        .screen5 .card-list {
            margin-top: 0px;
        }

        .screen5 .image3--mobile {
            width: 100%;
        }

        .footer-parents h6 {
            font-size: 49px;
            line-height: 57px;
            max-width: 100%;
            margin-bottom: 52px;
        }

        .parents section.footer-parents {
            padding: 120px 32px 71px 32px;
        }

        .parents .footer-parents svg:nth-child(1) {
            width: 62px;
            height: 62px;
        }

        .parents .footer-parents svg:nth-child(3) {
            width: 82px;
            height: 55px;
        }

        .parents .footer-parents svg.screen4 .wrapper:nth-child(2) {
            width: 67px;
            height: 67px;
            margin: 0px;
            margin-left: 22px;
            margin-right: 12px;
        }

        .screen3 h3 {
            margin-bottom: 20px;
        }

        .product-mob-icon {
            display: block;
            position: absolute;
            right: 0;
            margin: 20px;
            cursor: pointer;
        }
    }

    @media screen and (max-width: 550px) {
        .parents .card img {
            max-height: 100%;
        }
    }

</style>

<script type="module">
    import { Fancybox } from "https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.esm.js";
    window.Fancybox = Fancybox;
</script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css" />

<?php
$lang = Yii::app()->language;
$assetsUrl = Yii::app()->assetManager->getBaseUrl();
$staticUrl = Yii::app()->assetManager->getBaseUrl() . '/static/' . Yii::app()->params->settings['rev'].'/images';
?>

<div class="container parents">
    <section class="screen1">
        <div class="wrapper bordered">
            <div class="d-flex start-end relative col-mob">
                <div class="d-flex col left-block">
                    <h1>
                        <?=Lang::t('parents.block1.title')?>
                    </h1>
                    <p>
                        <?=Lang::t('parents.block1.description')?>
                    </p>
                </div>
                <img class="image1--desktop" src="<?=$staticUrl?>/parents/screen1.png" />
                <img class="image1--mobile" src="<?=$staticUrl?>/parents/screen1_mob.png" />
            </div>
        </div>

        <a href="#products" class="btn-primary"><?=Lang::t('parents.block1.button')?></a>
    </section>

    <section class="screen2">
        <div class="wrapper">
            <div class="d-flex center-between relative col-mob">
                <div class="d-flex col left-block">
                    <h2>
                        <?=Lang::t('parents.block2.title')?>
                    </h2>
                    <p>
                        <?=Lang::t('parents.block2.description')?>
                    </p>
                </div>
                <img class="absolute image2--desktop" src="<?=$staticUrl?>/parents/screen2_2.png" />
                <img class="image2--mobile" src="<?=$staticUrl?>/parents/screen2_mob.png" />
            </div>
        </div>
    </section>
    <div id="products" ></div>
    <section class="screen3" >
        <h3>
            <?=Lang::t('parents.block3.title')?>
        </h3>
        <div class="card-list">
            <?php foreach ($products as $k => $product) { ?>
                <?php
                    $product_url = $this->createUrl('site/product', array('alias' => $product['product_alias']));

                    if (!empty($product['product_photo'])) {
                        $product_image = json_decode($product['product_photo'], true);
                        $product_image_size = $product_image['size']['catalog']['2x'];
                        $product_image = $assetsUrl . '/product/' . $product['product_id'] . '/' . $product_image['path']['catalog']['2x'];
                    } else {
                        $product_image = '';
                    }

                    $product_price = (float) $product['product_price'];
                ?>

            <div class="card" data-url="<?=$this->canonicalUrl?>" data-id="<?=$product['product_id']?>" data-sku="<?=$product['product_sku']?>">
                <svg class="product-mob-icon" onclick="window.open('<?=$product_url?>', '_self')" height="32px" version="1.1" viewBox="0 0 32 32" width="32px" xmlns="http://www.w3.org/2000/svg" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns" xmlns:xlink="http://www.w3.org/1999/xlink"><title/><desc/><defs/><g fill="none" fill-rule="evenodd" id="Page-1" stroke="none" stroke-width="1"><g fill="#000000" id="icon-27-one-finger-click"><path d="M13.1244203,0 L13.1244203,4 L14.1244203,4 L14.1244203,0 L13.1244203,0 L13.1244203,0 Z M19.5269278,2.57686389 L16.418344,5.09414546 L17.0476644,5.87129142 L20.1562482,3.35400985 L19.5269278,2.57686389 L19.5269278,2.57686389 Z M21.5318564,9.3124234 L17.6343761,8.41261918 L17.4094251,9.38698925 L21.3069053,10.2867935 L21.5318564,9.3124234 L21.5318564,9.3124234 Z M5.94193535,10.2867935 L9.83941561,9.38698925 L9.61446455,8.41261918 L5.71698429,9.3124234 L5.94193535,10.2867935 L5.94193535,10.2867935 Z M7.09259245,3.35400985 L10.2011763,5.87129142 L10.8304967,5.09414546 L7.72191284,2.57686389 L7.09259245,3.35400985 L7.09259245,3.35400985 Z M19.6231595,30.9999999 C23.7659915,31 27.1244203,27.4147752 27.1244203,23.5 C27.1244203,23.5 27.1244203,25.8132437 27.1244203,23.5 L27.1244203,19.7491622 L27.1244203,17.5016756 C27.1244203,16.6723231 26.4586231,16 25.6244203,16 C24.7959932,16 24.1244203,16.6711894 24.1244203,17.5016756 L24.1244203,18 L23.1244203,18 L23.1244203,15.5064385 C23.1244203,14.6744555 22.4586231,14 21.6244203,14 C20.7959932,14 20.1244203,14.6715406 20.1244203,15.5064385 L20.1244203,17 L19.1244203,17 L19.1244203,14.5064385 C19.1244203,13.6744555 18.4586231,13 17.6244203,13 C16.7959932,13 16.1244203,13.6715406 16.1244203,14.5064385 L16.1244203,18 L15.1244203,18 L15.1244203,7.50524116 C15.1244203,6.67391942 14.4586231,6 13.6244203,6 C12.7959932,6 12.1244203,6.66712976 12.1244203,7.50524116 L12.1244203,18.7999878 C10.0660207,16.599567 7.35605012,14.1791206 6.24545305,15.2957153 C5.15828327,16.3887562 7.95978233,19.4007216 11.8717958,25.9830936 C13.6344162,28.9488875 15.8647052,30.9995418 19.6231595,30.9999999 L19.6231595,30.9999999 Z M28.1244203,23.5 C28.1244203,28.1944206 24.3188409,32 19.6244203,32.0000003 C16.5115051,32.0000003 13.2262274,30.5474856 10.9652407,26.4282229 C7.70175208,20.4825159 3.52827319,16.5832077 5.51553361,14.5959473 C6.9371827,13.1742982 9.16926196,14.5381668 11.1244203,16.3667868 L11.1244203,16.3667868 L11.1244203,7.50840855 C11.1244203,6.11541748 12.2437085,5 13.6244203,5 C15.0147583,5 16.1244203,6.12305276 16.1244203,7.50840855 L16.1244203,12.4983653 C16.5422506,12.1853054 17.0616174,12 17.6244203,12 C18.7069384,12 19.6193054,12.6774672 19.9702378,13.6281239 C20.4110134,13.2379894 20.9901312,13 21.6244203,13 C23.0147583,13 24.1244203,14.1182256 24.1244203,15.4976267 L24.1244203,15.5110883 C24.5422506,15.1985158 25.0616174,15.014191 25.6244203,15.014191 C27.0147583,15.014191 28.1244203,16.1335355 28.1244203,17.5143168 L28.1244203,23.5 L28.1244203,23.5 Z" id="one-finger-click"/></g></g></svg>
                <img src="<?=$product_image?>" alt="<?=CHtml::encode($product['product_title'])?>" onclick="window.open('<?=$product_url?>', '_self')" />
                <a href="<?=$product_url?>"><?php echo $product['product_title']; ?></a>
                <p><?=Lang::t('parents.block3.products.desc.'.($k+1))?></p>

                <div class="d-flex price-block">
                    <?php if ($product['product_price_type'] == 'variants') { $variant_id = null; ?>
                        <?php if (!empty($product['variants'])) { $variant_id = null; $in_stock = false; ?>
                            <div class="product-card__variants">
                                <?php $i = 0; foreach ($product['variants'] as $vk => $variant) { ?>
                                    <?php
                                    $i++;
                                    $variant_id = $variant['variant_id'];
                                    $values = array_values($variant['values']);

                                    $value_id = !empty($values) ? $values[0] : 0;
                                    $value_title = '';

                                    foreach ($product['properties'] as $property) {
                                        foreach ($property['values'] as $value_index => $value) {
                                            if ($value_index == $value_id) {
                                                $value_title = $value['value_title'];
                                            }
                                        }
                                    }

                                    $variant_in_stock = ($variant['variant_instock'] == 'in_stock') ? true : false;
                                    $variant['variant_price_old'] = (float) $variant['variant_price_old'];

                                    $isDiscount = false;
                                    $discount = 0;

                                    if (isset($variant['variant_price_old']) && $variant['variant_price_old'] != '0.00') {
                                        $value = ((float) $variant['variant_price'] / (float) $variant['variant_price_old']) * 100;
                                        $discount = 100 - round($value);

                                        $isDiscount = true;
                                    }
                                    ?>
                                    <label for="value-<?=$variant_id?>" class="product-card__radio<?php if (!$variant_in_stock) { ?> product-card__radio--disabled<?php } ?>">
                                        <input id="value-<?=$variant_id?>" data-sale-tag="sale-tag-<?= $product['product_id'] ?>" data-is-sale="<?= $isDiscount ?>"
                                            <?php if ($isDiscount) { ?> data-discount="<?php echo Lang::t('layout.see.product.discount.sale') . ' - ' . round($discount) . '% '; ?>" <?php } ?>
                                               type="radio" name="cart[variant_id<?=$product['product_id']?>]"
                                               value="<?=$variant_id?>"<?php if ($i == 1) { $in_stock = true; ?> checked<?php } elseif (!$variant_in_stock) { ?> disabled<?php } ?>
                                        >
                                        <span>
					<?=preg_replace('#([a-zа-я]+)#ui', '<small>$1</small>', CHtml::encode($value_title))?> /
					<?php if (!empty($variant['variant_price_old'])) { ?><s style="color: #0098ff;color: var(--blue-color);"><?=number_format($variant['variant_price_old'], 0, '.', ' ')?></s><?php } ?>
                                            <?=number_format($variant['variant_price'], 0, '.', ' ')?> <small><?=Lang::t('layout.tip.uah')?></small>
				</span>
                                    </label>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="d-flex price-block">
                    <div class="price">
                        <?php if (!empty($product_price_old)) { ?><?=number_format($product_price_old, 0, '.', ' ')?><?php } ?>
                        <?=number_format($product_price, 0, '.', ' ')?> <?=Lang::t('layout.tip.uah')?>
                    </div>

                    <?php if(empty($product['variants'])) { ?>
                    <form action="<?=$this->createUrl('cart')?>" method="post">
                        <input type="hidden" name="cart[qty]" value="1">
                        <input type="hidden" name="action" value="add">
                        <input type="hidden" name="cart[product_id]" value="<?= $product['product_id'] ?>">

                        <button class="btn-primary btn--add"><?=Lang::t('parents.block3.add')?></button>
                    </form>
                    <?php } else { ?>
                        <a class="btn-primary btn--add" href="<?=$product_url?>"><?=Lang::t('parents.block3.add')?></a>
                    <?php } ?>
                </div>
            </div>
            <?php } ?>
        </div>
    </section>

    <section class="screen4">
        <div class="wrapper bordered">
            <div class="d-flex center-between relative col-mob">
                <div class="relative">
                    <a class="absolute mob-hidden" href="https://www.youtube.com/watch?v=m9INZ3yVRnw"  data-fancybox="" data-type="iframe" data-preload="false">
                        <img src="<?=$staticUrl?>/parents/play.png" />
                    </a>
                    <img class="image3--desktop" src="<?=$staticUrl?>/parents/screen4.png" />
                    <a class="image3--mobile" href="https://www.youtube.com/watch?v=m9INZ3yVRnw" data-fancybox="" data-type="iframe" data-preload="false">
                        <img src="<?=$staticUrl?>/parents/screen4_mob.png" />
                    </a>
                </div>
                <div class="d-flex col right-block">
                    <h4>
                        <?=Lang::t('parents.block4.title')?>
                    </h4>
                    <p>
                        <?=Lang::t('parents.block4.description')?>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="screen5">
        <div class="wrapper">
            <div class="d-flex col main-block">
                <div class="d-flex center-between">
                    <div class="d-flex col max-width">
                        <h5>
                            <?=Lang::t('parents.block5.title')?>
                        </h5>
                        <p>
                            <?=Lang::t('parents.block5.description')?>
                        </p>
                    </div>
                    <img class="mob-hidden" src="<?=$staticUrl?>/parents/screen5_hand.svg" />
                </div>
                <div class="card-list desktop">
                    <div class="card-video">
                            <a class="absolute" href="https://www.youtube.com/watch?v=ps9-arXjWj0" data-fancybox="" data-type="iframe" data-preload="false">
                                <img src="<?=$staticUrl?>/parents/play.png" />
                            </a>
                        <?php if($lang == 'en') { ?>
                            <img src="<?=$staticUrl?>/parents/1eng.png" />
                        <?php } else { ?>
                            <img src="<?=$staticUrl?>/parents/screen5_1.png" />
                        <?php } ?>
                    </div>
                    <div class="card-video">
                            <a class="absolute" href="https://www.youtube.com/watch?v=qFdrtLaGj-E" data-fancybox="" data-type="iframe" data-preload="false">
                                <img src="<?=$staticUrl?>/parents/play.png" />
                            </a>
                        <?php if($lang == 'en') { ?>
                            <img src="<?=$staticUrl?>/parents/2eng.png" />
                        <?php } else { ?>
                            <img src="<?=$staticUrl?>/parents/screen5_2.png" />
                        <?php } ?>
                    </div>
                    <div class="card-video">
                            <a class="absolute" href="https://www.youtube.com/watch?v=hl7V3SS3EGw" data-fancybox="" data-type="iframe" data-preload="false">
                                <img src="<?=$staticUrl?>/parents/play.png" />
                            </a>
                        <?php if($lang == 'en') { ?>
                            <img src="<?=$staticUrl?>/parents/3eng.png" />
                        <?php } else { ?>
                            <img src="<?=$staticUrl?>/parents/screen5_3.png" />
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="card-list mobile">
                <div class="card-video">
                    <a class="absolute" href="https://www.youtube.com/watch?v=ps9-arXjWj0" data-fancybox="" data-type="iframe" data-preload="false">
                        <img src="<?=$staticUrl?>/parents/play.png" />
                    </a>
                    <?php if($lang == 'en') { ?>
                        <img src="<?=$staticUrl?>/parents/1eng.png" />
                    <?php } else { ?>
                        <img src="<?=$staticUrl?>/parents/screen5_1.png" />
                    <?php } ?>
                </div>
                <div class="card-video">
                    <a class="absolute" href="https://www.youtube.com/watch?v=qFdrtLaGj-E" data-fancybox="" data-type="iframe" data-preload="false">
                        <img src="<?=$staticUrl?>/parents/play.png" />
                    </a>
                    <?php if($lang == 'en') { ?>
                        <img src="<?=$staticUrl?>/parents/2eng.png" />
                    <?php } else { ?>
                        <img src="<?=$staticUrl?>/parents/screen5_2.png" />
                    <?php } ?>
                </div>
                <div class="card-video">
                    <a class="absolute" href="https://www.youtube.com/watch?v=hl7V3SS3EGw" data-fancybox="" data-type="iframe" data-preload="false">
                        <img src="<?=$staticUrl?>/parents/play.png" />
                    </a>
                    <?php if($lang == 'en') { ?>
                        <img src="<?=$staticUrl?>/parents/3eng.png" />
                    <?php } else { ?>
                        <img src="<?=$staticUrl?>/parents/screen5_3.png" />
                    <?php } ?>
                </div>
            </div>
            <img class="image3--desktop" src="<?=$staticUrl?>/parents/screen5.png" />
            <img class="image3--mobile" src="<?=$staticUrl?>/parents/screen5_mob.png" />
        </div>
    </section>
    <section class="footer-parents">
        <div class="d-flex col-mob center-between">
            <h6><?=Lang::t('parents.block6.title1')?><i style="margin-left:10px;font-size: 64px;">👌</i></h6>
            <div class="icons">

                <svg
                    onclick="window.open('https://www.instagram.com/fresh.black.okay/', '_blank')"
                    width="93" height="93" viewBox="0 0 93 93" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M46.0589 28.7866C42.6428 28.7866 39.3034 29.7996 36.4631 31.6975C33.6227 33.5953 31.4089 36.2929 30.1016 39.4489C28.7943 42.605 28.4523 46.0778 29.1187 49.4283C29.7852 52.7787 31.4302 55.8563 33.8457 58.2718C36.2612 60.6874 39.3388 62.3324 42.6893 62.9988C46.0397 63.6653 49.5126 63.3232 52.6686 62.016C55.8247 60.7087 58.5222 58.4949 60.4201 55.6545C62.318 52.8141 63.331 49.4747 63.331 46.0587C63.331 41.4778 61.5112 37.0846 58.2721 33.8455C55.0329 30.6063 50.6397 28.7866 46.0589 28.7866ZM46.0589 57.5734C43.7815 57.5734 41.5553 56.898 39.6617 55.6328C37.7681 54.3675 36.2922 52.5692 35.4207 50.4651C34.5492 48.3611 34.3211 46.0459 34.7654 43.8122C35.2097 41.5786 36.3064 39.5269 37.9168 37.9165C39.5271 36.3062 41.5789 35.2095 43.8125 34.7652C46.0461 34.3209 48.3613 34.5489 50.4654 35.4204C52.5694 36.292 54.3678 37.7678 55.633 39.6614C56.8983 41.555 57.5736 43.7813 57.5736 46.0587C57.5641 49.1096 56.3479 52.0329 54.1905 54.1903C52.0332 56.3477 49.1099 57.5639 46.0589 57.5734ZM61.8916 10.0752H30.2262C24.8819 10.0752 19.7564 12.1982 15.9775 15.9772C12.1985 19.7562 10.0754 24.8816 10.0754 30.2259V61.8914C10.0754 67.2357 12.1985 72.3611 15.9775 76.1401C19.7564 79.9191 24.8819 82.0421 30.2262 82.0421H61.8916C67.2359 82.0421 72.3613 79.9191 76.1403 76.1401C79.9193 72.3611 82.0424 67.2357 82.0424 61.8914V30.2259C82.0424 24.8816 79.9193 19.7562 76.1403 15.9772C72.3613 12.1982 67.2359 10.0752 61.8916 10.0752ZM76.285 61.8914C76.285 63.7815 75.9127 65.6532 75.1894 67.3995C74.466 69.1458 73.4058 70.7325 72.0693 72.069C70.7327 73.4056 69.146 74.4658 67.3997 75.1891C65.6534 75.9125 63.7818 76.2848 61.8916 76.2848H30.2262C28.336 76.2848 26.4644 75.9125 24.7181 75.1891C22.9718 74.4658 21.3851 73.4056 20.0485 72.069C18.712 70.7325 17.6518 69.1458 16.9284 67.3995C16.2051 65.6532 15.8328 63.7815 15.8328 61.8914V30.2259C15.8328 26.4086 17.3492 22.7476 20.0485 20.0483C22.7478 17.349 26.4088 15.8325 30.2262 15.8325H61.8916C63.7818 15.8325 65.6534 16.2048 67.3997 16.9282C69.146 17.6515 70.7327 18.7117 72.0693 20.0483C73.4058 21.3848 74.466 22.9715 75.1894 24.7178C75.9127 26.4641 76.285 28.3358 76.285 30.2259V61.8914ZM69.0883 27.3473C69.0883 28.2013 68.8351 29.0361 68.3606 29.7462C67.8861 30.4563 67.2117 31.0098 66.4227 31.3366C65.6337 31.6634 64.7655 31.7489 63.9279 31.5823C63.0903 31.4157 62.3209 31.0044 61.717 30.4006C61.1131 29.7967 60.7019 29.0273 60.5352 28.1897C60.3686 27.352 60.4541 26.4838 60.781 25.6948C61.1078 24.9058 61.6612 24.2314 62.3713 23.757C63.0814 23.2825 63.9163 23.0292 64.7703 23.0292C65.9155 23.0292 67.0138 23.4842 67.8236 24.294C68.6334 25.1037 69.0883 26.202 69.0883 27.3473Z" fill="white"/>
                </svg>


                <svg
                    onclick="window.open('https://www.facebook.com/Fresh.Black.Okay/', '_blank')"
                    width="93" height="93" viewBox="0 0 93 93" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M84.5584 46.0581C84.5584 24.8711 67.3631 7.67578 46.1761 7.67578C24.989 7.67578 7.7937 24.8711 7.7937 46.0581C7.7937 64.6352 20.9972 80.1033 38.4996 83.6728V57.5728H30.8231V46.0581H38.4996V36.4625C38.4996 29.0548 44.5256 23.0287 51.9334 23.0287H61.529V34.5434H53.8525C51.7415 34.5434 50.0143 36.2706 50.0143 38.3817V46.0581H61.529V57.5728H50.0143V84.2486C69.3974 82.3295 84.5584 65.9786 84.5584 46.0581Z" fill="white"/>
                </svg>


                <svg
                    onclick="window.open('https://www.youtube.com/channel/UCt0aIl5CJyn0MH_DSNnPMTg', '_blank')"
                    width="93" height="93" viewBox="0 0 93 93" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clip-path="url(#clip0_1_48)">
                        <path d="M46.5878 11.5093H47.1002C51.8327 11.5265 75.8121 11.6993 82.2776 13.438C84.2322 13.9686 86.0133 15.003 87.4429 16.4376C88.8725 17.8723 89.9005 19.657 90.4243 21.6134C91.0058 23.8012 91.4146 26.6972 91.6909 29.6852L91.7485 30.284L91.8751 31.7809L91.9212 32.3797C92.2954 37.6419 92.3415 42.5702 92.3472 43.6468V44.0786C92.3415 45.1956 92.2897 50.4578 91.8751 55.9388L91.8291 56.5433L91.7773 57.1421C91.4894 60.4353 91.0633 63.7054 90.4243 66.112C89.9021 68.0692 88.8746 69.8548 87.4448 71.2897C86.0149 72.7246 84.233 73.7584 82.2776 74.2875C75.5991 76.0838 50.2149 76.2104 46.6972 76.2162H45.8796C44.1006 76.2162 36.7427 76.1816 29.0279 75.9168L28.0491 75.8822L27.5482 75.8592L26.5637 75.8189L25.5792 75.7786C19.1886 75.4965 13.103 75.0417 10.2992 74.2817C8.34452 73.7531 6.56306 72.7202 5.13327 71.2864C3.70349 69.8526 2.67559 68.0682 2.15255 66.112C1.51348 63.7112 1.08744 60.4353 0.799572 57.1421L0.753513 56.5375L0.707455 55.9388C0.423315 52.0376 0.265865 48.1282 0.235352 44.2168L0.235352 43.5086C0.246866 42.2708 0.292925 37.9931 0.603822 33.2721L0.644124 32.6791L0.661396 32.3797L0.707455 31.7809L0.834116 30.284L0.89169 29.6852C1.16804 26.6972 1.57681 23.7955 2.15831 21.6134C2.68046 19.6562 3.70797 17.8706 5.13781 16.4357C6.56766 15.0008 8.34959 13.967 10.305 13.438C13.1088 12.6895 19.1943 12.2289 25.585 11.9411L26.5637 11.9008L27.554 11.8662L28.0491 11.849L29.0336 11.8087C34.5129 11.6323 39.9945 11.5345 45.4766 11.515H46.5878V11.5093ZM37.0824 29.9904V57.7293L61.0157 43.8656L37.0824 29.9904Z" fill="white"/>
                    </g>
                    <defs>
                        <clipPath id="clip0_1_48">
                            <rect width="92.1176" height="92.1176" fill="white" transform="translate(0.235352)"/>
                        </clipPath>
                    </defs>
                </svg>

            </div>
        </div>
    </section>
</div>
