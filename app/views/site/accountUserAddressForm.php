<style >
    .width-field {
        width: 37%;
        margin-bottom: 20px;
    }
    .input-width {
        width: 270px;
        height: 40px;
        padding: 10px;
    }
    .width-field-checkbox {
        width: 100%;
        margin-bottom: 21px;
    }

</style>
<?php //if ($validate) { ?>
<!--    <h3 style="color: red">--><?//=Lang::t('account.address.validate')?><!--</h3>-->
<?php //} ?>
<div class="width-field" ><label for="af-first_name"><?=Lang::t('account.address.label.first.name')?></label>
    <input class="input-width" id="af-first_name" required type="text" name="address[first_name]" value="<?= isset($address['first_name']) ? $address['first_name'] : '' ?>"></div>

<div class="width-field"><label for="af-last_name"><?=Lang::t('account.address.label.last.name')?></label>
    <input class="input-width" id="af-last_name" required type="text" name="address[last_name]" value="<?= isset($address['last_name']) ? $address['last_name'] : '' ?>"></div>

<div class="width-field"><label for="af-street"><?=Lang::t('account.address.label.street')?></label>
    <input class="input-width" id="af-street" required type="text" name="address[street]" value="<?= isset($address['street']) ? $address['street'] : '' ?>"></div>

<div class="width-field"><label for="af-flat"><?=Lang::t('account.address.label.flat')?></label>
    <input class="input-width" id="af-flat" type="text" name="address[flat]" value="<?= isset($address['flat']) ? $address['flat'] : '' ?>"></div>

<div class="width-field"><label for="af-city"><?=Lang::t('account.address.label.city')?></label>
    <input class="input-width" id="af-city" required type="text" name="address[city]" value="<?= isset($address['city']) ? $address['city'] : '' ?>"></div>

<div class="width-field"><label for="af-region"><?=Lang::t('account.address.label.region')?></label>
    <input class="input-width" id="af-region" type="text" name="address[region]" value="<?= isset($address['region']) ? $address['region'] : '' ?>"></div>

<div class="width-field"><label for="af-zip_code"><?=Lang::t('account.address.label.zip.code')?></label>
    <input class="input-width" id="af-zip_code" type="text" name="address[zip_code]" value="<?= isset($address['zip_code']) ? $address['zip_code'] : '' ?>"></div>

<div class="width-field"><label for="af-country"><?=Lang::t('account.address.label.country')?></label>
    <input class="input-width" id="af-country" type="text" name="address[country]" value="<?= isset($address['country']) ? $address['country'] : '' ?>"></div>

<div class="width-field-checkbox">

    <input class="check-width" type="hidden" name="address[is_main]" value="0">
    <input class="check-width" id="af-is_main" type="checkbox" name="address[is_main]" value="1"<?= isset($address['is_main']) && $address['is_main'] == 1 ? ' checked' : '' ?>>
    <label for="af-is_main"><?=Lang::t('account.address.label.is.main')?></label>
</div>


<input type="hidden" name="action" value="personal">
<?php if (isset($address) && !empty($address['id'])) : ?>
    <input type="hidden" name="address[id]" value="<?= $address['id'] ?>">
<?php endif; ?>
<div><button class="btn"><?=Lang::t('account.btn.create')?></button></div>
