<style >
    #account-personal {
        display: inline-block;
        margin-left: 45px;
        margin-top: 30px;
        width: 70%;
    }
    #account-addresses {
        width: 100%;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-direction: row;
        flex-direction: row;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
        -ms-flex-line-pack: stretch;
        align-content: stretch;
        -webkit-box-align: start;
        -ms-flex-align: start;
        align-items: flex-start;
    }
    .link-blue {
        color: #0098FF;
    }
    @media (max-width: 1200px) {
        .width-field {
            width: 45% !important;
        }
        
    }
    @media (max-width: 1000px) {
        .input-width {
            width: 200px !important;
        }
    }
    @media (max-width: 700px) {
        .input-width {
            width: 175px !important;
        }
    }
    @media (max-width: 600px) {
        .input-width {
            width: 240px !important;
        }
        .width-field {
            width: 68% !important;
        }
        #account-personal {
            width: 66% !important;
        }
        .account-menu-box {
            width: 100% !important;
        }
        .box-content li {
            font-size: 15px !important;
            line-height: 20px !important;
        }
        #account-personal {
            margin-left: 0px;
        }
    }
    @media (max-width: 400px) {
        #account-personal{
            width: 100% !important;
            margin-left: 0 !important;
        }
    }

</style>
<?php
/* @var $this SiteController */
$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<div class="wrap account-edit">
    <div class="content-divider"></div>
    <?php $this->renderPartial('accountMenu'); ?>
    <div class="account-wrap clearfix">
        <div id="account-personal" class="account-personal">
            <a class="link-blue"
               href="<?=$this->createUrl('site/accountedit')?>"
               style="
                        text-decoration: none;
                    "
            >
                <- <?=Lang::t('account.title.userAddressesBack')?>
            </a>
            <h1 class="account-title"><?=Lang::t('account.title.userAddresses')?></h1>

            <div class="account-personal-data">



                <form id="account-addresses" action="<?=$this->createUrl('site/accountAddressCreate')?>" class="account-form hidden" method="post">
                    <?php $this->renderPartial('accountUserAddressForm', array('user' => $user)); ?>
                </form>
            </div>
        </div>
    </div>
</div>