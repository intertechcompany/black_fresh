<?php
/* @var $this SiteController */
$assetsUrl = Yii::app()->assetManager->getBaseUrl();

$reasonsList = [];
$lang = Yii::app()->language;

foreach ($reasons as $reason) {
    if ($lang == 'uk') {
        $reasonsList[] = '<option value="' . $reason['id'] . '">' . $reason['reason_uk'] . '</option>';
    } elseif ($lang == 'ru') {
        $reasonsList[] = '<option value="' . $reason['id'] . '">' . $reason['reason_ru'] . '</option>';
    } elseif ($lang == 'en') {
        $reasonsList[] = '<option value="' . $reason['id'] . '">' . $reason['reason_en'] . '</option>';
    }

}
?>
<style >
    .feedback-form-location {
        display: inline-block;
        margin-left: 120px;
        width: 68%;
        margin-top: 5px;
    }
    .title {
        font-family: "Apercu Pro", serif;
        font-style: normal;
        font-weight: normal;
        word-wrap: break-word;
        font-size: 34px;
        /*line-height: 50px;*/
        text-transform: uppercase;
        margin-bottom: 40px;
    }
    .title-form {
        font-size: 15px;
        line-height: 22px;
        margin-top: 0px;
        margin-bottom: 5px;
    }
    .product__option {
        border-radius: 0px;
        width: 357px;
        height: 40px;
        margin-bottom: 16px;
    }
    #feedback-textarea {
        padding: 10px;
    }
    .download-file {
        /*visibility: hidden;*/
        border: 1px dashed #000000;
        padding-bottom: 9px;
        padding-top: 9px;
        padding-left: 130px;
        padding-right: 97px;
        /*background: url(/assets/quiz/download.svg) no-repeat 125px 10px;*/
    }
   /* .label-download {
        border: 1px dashed #000000;
        padding-bottom: 9px;
        padding-top: 9px;
        padding-left: 130px;
        padding-right: 97px;
        background: url(/assets/quiz/download.svg) no-repeat 125px 10px;
    }*/
    .form__column {
        padding: 0;
    }
    
    @media (max-width: 1200px) {
        .feedback-form-location {
            margin-left: 100px !important;
        }
    }
    @media (max-width: 1000px) {
        .feedback-form-location {
            margin-left: 50px !important;
        }
        #feedback-textarea {
            width: 490px !important;
        }
        .download-file {
            padding-left: 118px;
            padding-right: 51px;
        }
    }
    @media (max-width: 700px) {
        .feedback-form-location {
            margin-left: 37px !important;
        }
        #feedback-textarea {
            width: 440px !important;
        }
        .feedback-form-location .title {
                font-size: 22px;
        }
        .download-file {
            padding-left: 85px;
            padding-right: 32px;
        }
    }
    @media (max-width: 600px) {
        #feedback-textarea {
            width: 355px !important;
        }

    }
    @media (max-width: 580px) {
        .download-file  {
           padding-left: 21px;
            padding-right: 14px;
        }

    }
    @media (max-width: 500px) {
        #feedback-textarea {
            width: 320px !important;
        }
        .product__option {
            width: 320px !important;
        }
        .feedback-form-location {
           margin-left: 0 !important;
        }
        .download-file  {
            padding-left: 8px;
            padding-right: 14px;
            font-size: 14px;
        }
        .account-menu-box {
            width: 100% !important;
        }
    }
    @media (max-width: 450px) {
        .feedback-form-location {
            width: 100%;
        }
        .account-menu-box {
            width: 100% !important;
        }
        .download-file {
            font-size: 14px;
        }
    }
    @media (max-width: 360px) {
        .product__option {
            width: 310px !important;
        }
        #feedback-textarea {
            width: 310px !important;
        }
        .download-file {
            font-size: 13px;
        }
    }
</style>
<div class="wrap account-edit">
    <div class="content-divider"></div>
    <?php $this->renderPartial('accountMenu'); ?>
    <br>
    <form class="feedback-form-location" action="<?=$this->createUrl('userFeedback')?>" class="" method="post" enctype="multipart/form-data">
        <div class="product__options" style="margin-left: 0">
            <h4 class="title"><?= Lang::t('layout.title.feedback.page') ?></h4>
            <h5 class="title-form"><?= Lang::t('layout.feedback.title') ?></h5>
            <div class="product__option">
                <div class="product__option-value">Reasons</div>
                <select name="feedback[reason_id]" class="product__select">
                    <?=implode("\n", $reasonsList)?>
                </select>
            </div>
        </div>
        <h5 class="title-form"><?= Lang::t('layout.feedback.comments') ?></h5>
        <div>
            <textarea name="feedback[message]" id="feedback-textarea" cols="30" rows="10" style="width: 548px; height: 189px;"></textarea>
        </div>
        <br>
        <div class="form__column form__column--50">
            <input type="hidden" name="MAX_FILE_SIZE" value="30000000" />
            
            <label class="label-download"  ><input class="download-file" type="file" name="files[]" multiple ></label> 
            <br><br>
        </div>
        <div class="form__column form__column--50">
            <input type="submit" class="form__btn" style="width: 50%;" value="<?=Lang::t('feedback.btn.send')?>"/>
        </div>
    </form>
</div>