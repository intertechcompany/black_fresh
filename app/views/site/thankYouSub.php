<?php /* @var $this SiteController */
if (isset($_SESSION['thankYou'])) {
    $price = $_SESSION['thankYou'];
    $pids = $_SESSION['thankYouProducts'] ?? [];
}
?>
<main class="thank-you wrap">
	<div class="thank-you__container">
		<h1 class="thank-you__title"><?=Lang::t('thankYou.subscription.new')?></h1><br><br>
        <span><?=Lang::t('thankYou.tip.price.message')?> <b><?php if (isset($price)) { echo $price; } ?></b><b> грн</b></span>
		<div class="thank-you__back"><br><a href="<?=$this->createUrl('site/index')?>" class="btn"><?=Lang::t('thankYou.btn.backToHome')?></a></div>
	</div>

	<form id="subscribe" class="subscribe" action="<?=$this->createUrl('ajax/subscribe')?>" method="post" novalidate>
		<div class="subscribe__title"><?=Lang::t('home.tip.subscribeTitle')?></div>
		<div class="subscribe__content">
			<ul class="subscribe__special list-unstyled">
				<li><?=Lang::t('home.tip.followInsta')?> <a href="<?=CHtml::encode(Yii::app()->params->settings['instagram'])?>" target="_blank" rel="nofollow">@fresh.black.okay</a></li>
				<li><?=Lang::t('home.tip.followFacebook')?> <a href="<?=CHtml::encode(Yii::app()->params->settings['facebook'])?>" target="_blank" rel="nofollow">fresh.black.okay</a></li>
				<li><?=Lang::t('home.tip.subscribe')?></li>
			</ul>
			<div class="subscribe__group">
				<input type="email" class="subscribe__input" name="subscribe[email]" placeholder="Email" required>
				<button class="subscribe__btn">&gt;</button>
			</div>
		</div>
	</form>
	<!-- /.subscribe -->

    <p><?=@$_GET['utm_campaign']?></p>
</main>

<?php if(isset($pids)) { ?>
<script>
    const dataLayer = window.dataLayer || [];
    dataLayer.push({
        'event': 'purchase',
        'value': '<?php echo $price ?>',
        'campaign': '<?=@$_GET['utm_campaign']?>',
        'items':[
            <?php foreach($pids as $id) { ?>
            {
                'id': '<?php echo $id ?>',
                'google_business_vertical': 'retail'
            },
            <?php } ?>
        ]
    });
</script>
<?php } ?>
