<?php
    /* @var $this SiteController */
    $assetsUrl = Yii::app()->assetManager->getBaseUrl();
    $staticUrl = Yii::app()->assetManager->getBaseUrl() . '/static/' . Yii::app()->params->settings['rev'];
?>
<div class="wrap">
    <div class="content-divider content-divider--top"></div>

    <?php /* <div class="breadcrumbs">
        <a href="<?=Yii::app()->homeUrl?>">Главная</a> 
        / 
        <?=Category::renderBreadrumbs($this, $categories, $categories[$category['category_id']]['parents'], $category['category_id'])?>
    </div> */ ?>

    <div class="category">
        <div class="category__side">
            <div class="category__side-block side-block">
                <div class="side-block__title">Категории</div>
                <ul class="side-block__categories list-unstyled">
                    <li><a href="<?=$this->createUrl('newest')?>">Новинки</a></li>
                    <li><a href="<?=$this->createUrl('brands')?>">Бренды</a></li>
                    <?=Category::buildCategoriesTree($this, $categories_tree, 0)?>
                    <?php if (Yii::app()->params->has_sale) { ?>
                    <li><a class="active" href="<?=$this->createUrl('sale')?>" style="color:#ff3700">SALE</a></li>
                    <?php } ?>
                </ul>
            </div>

            <?php /* <form id="filter" class="filter" action="<?=$this->createUrl('category', array('alias' => $category['category_alias']))?>" method="get">
			    <?php $this->renderPartial('facets', array('facets' => $facets, 'sort' => $sort)); ?>
            </form> */ ?>
        </div>
        <div class="category__content">
            <h1 class="category__title">SALE</h1>
            <?php if (!empty($products)) { ?>
			<?php
				$sort_params = array(
					'popular' => array(
						'title' => 'по новизне',
						'url' => null, // default value
					),
					'price-asc' => array(
						'title' => 'сначала дешевые',
						'url' => 'price-asc',
					),
					'price-desc' => array(
						'title' => 'сначала дорогие',
						'url' => 'price-desc',
					),
				);
			?>
			<ul class="category__sort">
				<li class="category__sort-title">Сортировать:</li>
				<?php foreach ($sort_params as $sort_index => $sort_param) { ?>
				<?php if ($sort_index == $sort) { ?>
				<li><span class="active"><?=CHtml::encode($sort_param['title'])?></span></li>
				<?php } else { ?>
				<?php 
					$sort_url_params = array();

					if (!empty($sort_param['url'])) {
						$sort_url_params = array('sort' => $sort_param['url']);
					}

					if (!empty($filters)) {
						$sort_url_params['filter'] = $filters;
					}

					$sort_url = $this->createUrl('newest', $sort_url_params);
				?>
				<li><a href="<?=$sort_url?>"><?=CHtml::encode($sort_param['title'])?></a></li>
				<?php } ?>
				<?php } ?>
            </ul>
            <div class="catalog">
                <?php $this->renderPartial('productsList', array('products' => $products, 'is_home' => true)); ?>
            </div>
            <?php if ($pages->getPageCount() > 1) { ?>
            <div class="pagination">
                <?php
                    $this->widget('LinkPager', array(
                        'pages' => $pages,
                        'maxButtonCount' => 7,
                        'htmlOptions' => array(
                            'class' => 'list-inline',
                        ),
                    ));
                ?>
            </div>
            <?php } ?>
            <?php } else { ?>
            <p style="font-size: 14px">Товары не найдено.</p>
            <?php } ?>
        </div>
    </div>
</div>