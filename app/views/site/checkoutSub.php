<?php
    /* @var $this SiteController */
    $assetsUrl = Yii::app()->assetManager->getBaseUrl();
    $staticUrl = Yii::app()->assetManager->getBaseUrl() . '/static/' . Yii::app()->params->settings['rev'];

    $discount_value = 0;

    $user = Yii::app()->user->id;

    if ($user) {
        $user = User::model()->getUserById($user);
        $userAddress = UserAddress::model()->getUserAddresses($user['user_id']);
    }
?>

<style>
    .remove-discount {
        display: inline-block;
        border: 0;
        background-color: #000;
        background-color: var(--black-color);
        /* min-width: 200px; */
        padding: 0 30px;
        color: #fff;
        color: var(--white-color);
        font-family: 'Apercu Pro Mono';
        font-size: 15px;
        text-decoration: none;
        /* text-align: center; */
        cursor: pointer;
        /* vertical-align: top; */
        width: 100%;
        height: 21px;
        /* line-height: 4; */
        border-radius: 30px;
    }
</style>
<main class="checkout wrap">
	<h1 class="checkout__title"><?=Lang::t('checkout.sub.title')?></h1>
	<form action="<?=$this->createUrl('confirmSub')?>" id="checkout" class="checkout__form form" method="post" data-baseurl="<?=Yii::app()->request->getBaseUrl()?>" novalidate>
		<div class="checkout__group">
			<div class="checkout__group-title"><?=Lang::t('checkout.tip.personal')?></div>
            <div class="form__row">
                <div class="form__column form__column--50">
                    <label for="checkout-name" class="form__label"><?=Lang::t('checkout.label.name')?></label>
                    <input type="text" id="checkout-name" class="form__input" name="order[first_name]" value="<?=CHtml::encode($user['user_first_name'])?>">
                </div>
                <div class="form__column form__column--50">
                    <label for="checkout-surname" class="form__label"><?=Lang::t('checkout.label.surname')?></label>
                    <input type="text" id="checkout-surname" class="form__input" name="order[last_name]" value="<?=CHtml::encode($user['user_last_name'])?>">
                </div>
            </div>
            <div class="form__row">
                <div class="form__column form__column--50">
                    <label for="checkout-email" class="form__label"><?=Lang::t('checkout.label.email')?></label>
                    <input type="email" id="checkout-email" class="form__input" name="order[email]" value="<?=CHtml::encode($user['user_email'])?>">
                </div>
                <div class="form__column form__column--50">
                    <label for="checkout-phone" class="form__label"><?=Lang::t('checkout.label.phone')?></label>
                    <input type="tel" id="checkout-phone" class="form__input" name="order[phone]" value="<?=CHtml::encode($user['user_phone'])?>">
                </div>
            </div>
		</div>
		<div class="checkout__group">
			<div class="checkout__group-title"><?=Lang::t('checkout.tip.delivery')?></div>
<div class="form__row">
				<div class="form__column">
					<label for="delivery-2" class="form__radio">
						<input type="radio" id="delivery-2" name="order[delivery]" value="2" checked <?php if ($delivery == 2) { ?> checked<?php } ?>>
                        <span><?=Lang::t('checkout.label.courier')?> <?php if (!empty(Yii::app()->params->settings['courier'])) { ?>(<?=Yii::app()->params->settings['courier']?> <?=Lang::t('layout.tip.uah')?>)<?php } ?></span>
					</label>
					<div class="form__radio-tip"><?=Lang::t('checkout.sub.tip.courier')?> <strong><?=Lang::t('checkout.tip.sub.delivery')?></strong></div>
				</div>
			</div>
			<div class="form__row">
				<div class="form__column">
					<label for="delivery-3" class="form__radio">
						<input type="radio" id="delivery-3" name="order[delivery]" value="3"<?php if ($delivery == 3) { ?> checked<?php } ?>>
						<span><?=Lang::t('checkout.label.np')?></span>
					</label>
					<div class="form__radio-tip"><?=Lang::t('checkout.sub.tip.np')?> <strong><?=Lang::t('checkout.tip.sub.delivery')?></strong></div>
				</div>
            </div>

            <div class="form__row">
                <div class="form__column">
                    <label for="delivery-4" class="form__radio">
                        <input type="radio" id="delivery-4" name="order[delivery]" value="4"<?php if ($delivery == 4) { ?> checked<?php } ?>>
                        <span><?=Lang::t('checkout.label.np.courier')?></span>
                    </label>
                    <div class="form__radio-tip"><?=Lang::t('checkout.sub.tip.np.courier')?></div>
                </div>
            </div>
            
            <div id="checkout-courier" class="checkout__delivery-group<?php if ($delivery != 2) { ?> checkout__delivery-group--hidden<?php } ?>">
                <div class="form__row">
                    <div class="form__column">
                        <div class="form__column">
                            <?php if ($userAddress) { ?>
                                <label for="checkout-address" class="form__label"><?=Lang::t('checkout.label.addressSave')?></label>
                                <br>
                                <select id="checkout-address" name="order[address]" class="form__input">
<!--                                    <option value="">--><?//=Lang::t('checkout.tip.addressSave')?><!--</option>-->
                                    <?php foreach ($userAddress as $address) { ?>
                                        <option value="<?php echo "м " . $address['city'] . ', ' . $address['region'] . ' обл, ' . $address['street'] . ' ' . $address['flat']; ?>"><?php echo "м " . $address['city'] . ', ' . $address['region'] . ' обл, ' . $address['street'] . ' ' . $address['flat']; ?></option>
                                    <?php } ?>
                                </select>
                            <?php } else { ?>
                                <label for="checkout-address" class="form__label"><?=Lang::t('checkout.label.address')?></label>
                                <input type="text" id="checkout-address" name="order[address]" class="form__input" value="<?=CHtml::encode($customer['address'])?>" placeholder="<?=Lang::t('checkout.placeholder.address')?>">
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            
            <div id="checkout-np" class="checkout__delivery-group<?php if ($delivery != 3) { ?> checkout__delivery-group--hidden<?php } ?>">
                <div class="form__row">
                    <div class="form__column form__column--33">
                        <label for="checkout-np-city" class="form__label"><?=Lang::t('checkout.label.city')?></label>
                        <select id="checkout-np-city" name="order[np_city]" class="form__input">
                            <option value=""><?=Lang::t('checkout.tip.citySelect')?></option>
                            <?php if (!empty($np_cities)) { ?>
                            <?php $field = (Yii::app()->language == 'ru') ? 'city_name_ru' : 'city_name'; ?>
							<?php foreach ($np_cities as $np_city) { ?>
							<?php if (empty($np_city['city_name_ru'])) { $np_city['city_name_ru'] = $np_city['city_name']; } ?>
                            <option value="<?=CHtml::encode($np_city[$field])?>"<?php if ($customer['np_city'] === $np_city['city_name'] || $customer['np_city'] === $np_city['city_name_ru']) { ?> selected<?php } ?>><?=CHtml::encode($np_city[$field])?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form__column form__column--66">
                        <label for="checkout-np-department" class="form__label"><?=Lang::t('checkout.label.department')?></label>
                        <select id="checkout-np-department" name="order[np_department]" class="form__input"<?php if (empty($np_departments)) { ?> disabled<?php } ?>>
                            <option value=""><?=Lang::t('checkout.tip.departmentSelect')?></option>
                            <?php if (!empty($np_departments)) { ?>
                            <?php $field = (Yii::app()->language == 'ru') ? 'department_name_ru' : 'department_name'; ?>
							<?php foreach ($np_departments as $np_department) { ?>
							<?php if (empty($np_department['department_name_ru'])) { $np_department['department_name_ru'] = $np_department['department_name']; } ?>
                            <option value="<?=CHtml::encode($np_department[$field])?>"<?php if ($customer['np_department'] === $np_department['department_name'] || $customer['np_department'] === $np_department['department_name_ru']) { ?> selected<?php } ?>><?=CHtml::encode($np_department[$field])?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>

            <div id="checkout-np-courier" class="checkout__delivery-group<?php if ($delivery != 4) { ?> checkout__delivery-group--hidden<?php } ?>">
                <div class="form__row">
                    <div class="form__column form__column--33">
                        <label for="checkout-np-city-courier" class="form__label"><?=Lang::t('checkout.label.city')?></label>
                        <select id="checkout-np-city-courier" name="order[np_city]" class="form__input">
                            <option value=""><?=Lang::t('checkout.tip.citySelect')?></option>
                            <?php if (!empty($np_cities)) { ?>
                                <?php $field = (Yii::app()->language == 'ru') ? 'city_name_ru' : 'city_name'; ?>
                                <?php foreach ($np_cities as $np_city) { ?>
                                    <?php if (empty($np_city['city_name_ru'])) { $np_city['city_name_ru'] = $np_city['city_name']; } ?>
                                    <option value="<?=CHtml::encode($np_city[$field])?>"<?php if ($customer['np_city'] === $np_city['city_name'] || $customer['np_city'] === $np_city['city_name_ru']) { ?> selected<?php } ?>><?=CHtml::encode($np_city[$field])?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form__column form__column--33">
                        <label for="checkout-np-department-courier" class="form__label"><?=Lang::t('checkout.label.street')?></label>
                        <select id="checkout-np-department-courier" name="order[address]" class="form__input"<?php if (empty($np_departments)) { ?> disabled<?php } ?>>
                            <option value=""><?=Lang::t('checkout.tip.streetSelect')?></option>
                            <?php if (!empty($np_departments)) { ?>
                                <?php $field = (Yii::app()->language == 'ru') ? 'department_name_ru' : 'department_name'; ?>
                                <?php foreach ($np_departments as $np_department) { ?>
                                    <?php if (empty($np_department['department_name_ru'])) { $np_department['department_name_ru'] = $np_department['department_name']; } ?>
                                    <option value="<?=CHtml::encode($np_department[$field])?>"<?php if ($customer['np_department'] === $np_department['department_name'] || $customer['np_department'] === $np_department['department_name_ru']) { ?> selected<?php } ?>><?=CHtml::encode($np_department[$field])?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form__column form__column--33">
                        <label for="checkout-address" class="form__label"><?=Lang::t('checkout.label.np.house')?></label>
                        <input type="text" id="checkout-address" name="order[np_building]" class="form__input" value="" placeholder="<?=Lang::t('checkout.label.np.house.placeholder')?>">
                    </div>
                </div>
            </div>
		</div>
		<div class="checkout__group">
			<div class="checkout__group-title"><?=Lang::t('checkout.tip.payment')?></div>
			<div class="form__row">
				<div class="form__column">
					<label for="payment-2" class="form__radio"<?php if ($price >= Yii::app()->params->settings['max_order_amount']) { ?> style="opacity: .5; pointer-events: none<?php } ?>>
						<input type="radio" checked id="payment-2" name="order[payment]" value="2"<?php if ($payment == 2) { ?> checked<?php } ?>>
						<span><?=Lang::t('checkout.label.card')?></span>
                    </label>
                    <?php if ($price >= Yii::app()->params->settings['max_order_amount']) { ?>
                    <div class="form__error"><?=Lang::t('checkout.tip.maxAmount', ['{max_amount}' => Yii::app()->params->settings['max_order_amount']])?></div>
                    <?php } ?>
				</div>
			</div>
		</div>
		<div class="form__row form__row--btn">
			<label for="agree"><input id="agree" type="checkbox" value="1"> <?=Lang::t('checkout.tip.agree', ['{url}' => $this->createUrl('page', ['alias' => 'dogovir-oferti'])])?></label>
		</div>
		<div class="form__row">
			<div class="form__column"><button class="form__btn" disabled><?=Lang::t('checkout.btn.confirm')?></button></div>
		</div>
	</form>
	<aside class="checkout__cart cart">
		<div class="cart__container">
            <div class="cart__title">ПІДПИСКА</div>
			<div class="cart__products">
				<?php $this->renderPartial('cartListSub', array('cart' => $cart, 'is_checkout' => true)); ?>
			</div>
            <?php
                $cartItems = [];

                foreach ($cart as $item) {
                    $cartItems[] = [
                        'product' => $item['product_id'],
                        'qty' => $item['qty'],
                        'variant' => $item['variant']['variant_price'] ?? $item['price']
                    ];
                }

                $user = Yii::app()->user->id;

                $sub = Subscription::model()->getSubByUser($user);
                $subHistory = Subscription::model()->checkIfHaveSubHistory($user);

                $price = $price + $discount_value;

                if (!$sub && $subHistory['total'] <= 0) {
                    $price = round($price - ($price * 0.2));
                } elseif (!$sub) {
                    $subCart = Subscription::model()->getFirstCartValue($cart);
                    $percentage = 5;
                    if ($subCart['qty'] >= 2) {
                        $percentageValue = ($percentage * (int) $subCart['qty']) / 100;

                        if ($percentageValue >= 1) {
                            $percentageValue = 0.9;
                        }

                        $price = round($price - ($price * $percentageValue));
                    }
                }
            ?>
			<form id="discount" class="checkout__code form" action="<?=$this->createUrl('ajax/discount')?>" method="post" data-price="<?= $price ?>">
				<div class="form__row">
					<div class="form__column">
                        <input type="hidden" name="type" value="sub">
                        <input type="hidden" name="products" value='<?= json_encode($cartItems) ?>'>
						<input type="text" class="form__input" name="discount[code]" placeholder="<?=Lang::t('checkout.label.promocode')?>">
					</div>
				</div>
            </form>
            <?php if (!empty($discount)) { ?>
                <!--			--><?php //$data = Discount::getDiscountValue($price); ?>
                <?php $data = Discount::model()->checkDiscount($discount, 'sub', $cartItems); ?>
                <?php if (!empty($data)) { ?>
                    <div class="checkout__delivery checkout__delivery--discount" id="discount-div" data-price="<?= $price ?>">
                        <span><?=Lang::t('checkout.tip.discount')?> <b><?=$discount['discount_code']?></b></span>
                        <span><?=$data['value']?> <?=Lang::t('layout.tip.uah')?></span>
                    </div>
                    <button id="remove-discount" class="remove-discount" data-code="<?=$discount['discount_code']?>"><?= Lang::t('checkout.tip.discount.btn') ?></button>
                <?php } ?>
            <?php } ?>
			<div class="checkout__total">
				<span><?=Lang::t('checkout.tip.total')?></span>
				<span id="total-price">
                    <?php if (!empty($data)) { ?>
                        <?=number_format($data['newPrice'], 0, '.', ' ')?> <?=Lang::t('layout.tip.uah')?>
                    <?php } else { ?>
                        <?=number_format($price, 0, '.', ' ')?> <?=Lang::t('layout.tip.uah')?>
                    <?php } ?>
                </span>
			</div>
		</div>
	</aside>
	<!-- /.cart -->
</main>
<!-- /.checkout -->

<script>
  document.body.addEventListener('click', function (evt) {
    if (evt.target.className === 'remove-discount') {
      fetch('ajax/removeDiscount?type=sub', {
        method: 'post',
      }).then(res => res.json())
        .then(function (res) {
          if(res.price) {
            const discountDiv = document.getElementById('discount-div');
            discountDiv.innerHTML = '';
            discountDiv.remove();
            evt.target.remove();

            document.getElementById('total-price').textContent = res.price + ' грн';
          }
        });
    }
  }, false);
</script>