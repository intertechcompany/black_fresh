
<?php
/* @var $this SiteController */
?>
<style>
    .main-wrap {
        margin-top: 75px;
    }
    .h1-login{
        font-family: Apercu Pro;
        font-style: normal;
        font-weight: normal;
        font-size: 50px;
        line-height: 58px;
        margin-bottom: 3px;
    }
    .link-reg-login {
        color: #0098ff;
        width: 100%;
    }
    #login-form {
        width: 41%;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-direction: row;
        flex-direction: row;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
        -ms-flex-line-pack: stretch;
        align-content: stretch;
        -webkit-box-align: start;
        -ms-flex-align: start;
        align-items: flex-start;
    }
    #login-password {
        width: 420px;
        height: 40px;
        padding: 0 20px 6px 20px;

    }
    #login-email {
        width: 420px;
        height: 40px;
        padding: 0 20px 6px 20px;
    }
    .label-login-email {
        font-family: Apercu Pro;
        font-style: normal;
        font-weight: normal;
        font-size: 15px;
        line-height: 22px;
    }
    .mb-login {
        margin-bottom: 20px;
    }
    .btn-login {
        width: 241px;
    }
    .for-after:after {
        content: "";
        color: #333;
        border: 1px solid #302f31;
        height: 352px;
        position: absolute;
        left: 44%;
        /* margin-left: -20px; */
        top: 245px;
    }
    .login-socials {
        font-family: inherit;
        font-weight: 400;
        line-height: 1.1;
        color: #000000;
        margin-top: -14px;
        line-height: 22px;
        font-size: 15px;
        margin-bottom: 11px;
    }
    .flex-for-colomn {
        display: flex;
    }
    .div-socials-flex {
        width: 35%;
        margin-left: 42px;
    }
    .socials-div-enter-facebook {
        background-color: #3D5D9C;
        color: #fff;
        text-align: center;
        padding: 16px 39px 16px 22px;
        border-radius: 100px;
        font-size: 15px;
        margin-bottom: 10px;
    }
    .socials-div-enter-google {
        background-color: #DC4D28;
        color: #fff;
        text-align: center;
        padding: 16px 39px 16px 22px;
        border-radius: 100px;
        font-size: 15px;
    }
    .socials-div-enter-google a {
        color: #fff;
        text-decoration: none;
    }
    .socials-div-enter-facebook a {
        color: #fff;
        text-decoration: none;
    }
    .have-account{
        margin-bottom: 19px;
        margin-top: 0;
    }
    @media (max-width: 1200px) {
        .for-after:after {
            left: 43%;
            height: 312px;
        }
        #login-email {
            width: 347px;
        }
        #login-password {
            width: 347px;
        }
        #login-form {
        	width: 40%;
        }
    }
    @media (max-width: 1000px) {
        .for-after:after {
            display: none;
        }
        #login-form {
            width: 100%;
        }
        .div-socials-flex {
            width: 100%;
        }
    }
    @media (max-width: 680px) {
        .flex-for-colomn {
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-pack: start;
            -ms-flex-pack: start;
            justify-content: flex-start;
            -ms-flex-line-pack: stretch;
            align-content: stretch;
            -webkit-box-align: start;
            -ms-flex-align: start;
            align-items: flex-start;
        }
        #login-form {
            width: 50%;
        }
        .div-socials-flex {
            margin-left: 2px;
            width: 100%;
            margin-top: 20px;
        }
        .socials-div-enter-facebook {
            margin-right: 20%;
        }
        .socials-div-enter-google {
            margin-right: 20%;
        }
    }
    @media (max-width: 340px) {
        #login-email {
            width: 320px
        }
        #login-password {
            width: 320px
        }
    }
</style> 
<div class="wrap main-wrap">
    <div class="content-divider"></div>

    <div class="auth-form">
        <h1 class="h1-login"><?=CHtml::encode($this->pageTitle)?></h1>

        <h4 class="have-account"><?=Lang::t('account.title.withoutaccount')?><a class="link-reg-login" href="/registration"> <?=Lang::t('account.title.registration')?></a> </h4>

        <?php if (Yii::app()->user->hasFlash('login')) { ?>

            <div class="success-msg"><?=Yii::app()->user->getFlash('login')?></div>
        <?php } else { ?>
        <div class="flex-for-colomn">
            <form class="for-after" id="login-form" method="post" data-type="login" novalidate>
                <?php if (!empty($result['errorCode'])) { ?>
                    <div class="error-msg"><?=$result['errorCode']?></div>
                <?php } elseif (Yii::app()->user->hasFlash('login_error')) { ?>
                    <div class="error-msg"><?=Yii::app()->user->getFlash('login_error')?></div>
                <?php } ?>
                <div class="form-row clearfix">
                    <div class="form-col form-col-100 mb-login">
                        <label class="label-login-email" for="login-email"><?=Lang::t('login.label.login')?></label>
                        <input class="form__input" id="login-email" <?php if (isset($result['errorFields']['login'])) { ?> class="error-field"<?php } ?> type="email" name="login[login]" value="<?=CHtml::encode($model->login)?>">
                        <?php if (!empty($result['errorFields']['login'][0])) { ?><div class="error-msg"><?=implode('<br>', $result['errorFields']['login'])?></div><?php } ?>
                    </div>
                </div>
                <div class="form-row clearfix">
                    <div class="form-col form-col-100 mb-login">
                        <label class="label-login-email" for="login-password"><?=Lang::t('login.label.password')?></label>
                        <input id="login-password" <?php if (isset($result['errorFields']['password'])) { ?> class="error-field"<?php } ?> type="password" name="login[password]" value="">
                        <?php if (!empty($result['errorFields']['password'][0])) { ?><div class="error-msg"><?=implode('<br>', $result['errorFields']['password'])?></div><?php } ?>
                    </div>
                </div>
                <div class="form-row form-row-btn clearfix">
                    <div class="form-col form-col-100 mb-login">
                        <button class="btn btn-login"><?=Lang::t('login.btn.logIn')?></button>
                    </div>
                </div>
                <a class="link-reg-login" href="<?=$this->createUrl('site/reset')?>"><?=Lang::t('login.link.forgotPassword')?></a>
            </form>
            <?php } ?>

            <div class="div-socials-flex">
                <h4 class="login-socials"><?= Lang::t('login.social.title') ?></h4>
                <div class="socials-div-enter-facebook"><a href="<?=$this->createUrl('auth/facebook')?>"> <?= Lang::t('login.social.facebook') ?></a></div>
                <div class="socials-div-enter-google"><a href="<?=$this->createUrl('auth/google')?>"><?= Lang::t('login.social.google') ?></a></div>
            </div>
        </div>
    </div>
</div>