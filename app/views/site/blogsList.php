<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<?php if (!empty($blogs)) { ?>
<?php foreach ($blogs as $blog) { ?><div class="blog-item clearfix">
	<div class="bi-content">
		<?php
			$blog_url = $this->createUrl('blogitem', array('alias' => $blog['blog_alias']));

			if (!empty($blog['blog_photo'])) {
				$blog_image = json_decode($blog['blog_photo'], true);
				$blog_image_size = $blog_image['list']['size'];
				$blog_image = $assetsUrl . '/blog/' . $blog['blog_id'] . '/' . $blog_image['list']['path'];
			} else {
				$blog_image = '';
			}
		?>
		<div class="bi-read-more"><a href="<?=$blog_url?>"><?=Lang::t('blog.link.readMore')?><i class="icon-inline icon-read-more"></i></a></div>
		<div class="bi-title"><a href="<?=$blog_url?>"><?=CHtml::encode($blog['blog_title'])?></a></div>
		<div class="bi-intro"><?=str_replace("\n", "<br>\n", CHtml::encode($blog['blog_intro']))?></div>
	</div>
	<?php if (!empty($blog_image)) { ?>
	<div class="bi-img" style="background-image: url(<?=$blog_image?>);"><img src="<?=$blog_image?>" alt="" width="<?=$blog_image_size['w']?>" height="<?=$blog_image_size['h']?>"></div>
	<?php } ?>
</div><?php } ?>
<?php } ?>