<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
    $staticUrl = $assetsUrl . '/static/' . Yii::app()->params->settings['rev'];

    $product_sku = $product['product_sku'];
	
	// variants
	$is_variants_type = ($product['product_price_type'] == 'variants') ? true : false;
	$variant_id = 0;
	$variants = array();
	$variants_lang = array();
	$selected_variant = array();
	$active_properties = array();
	$out_of_stock_values = array();
	
	// options
	$options = array();
	$selected_options = array();
	$options_price = 0;
	$options_regular_price = 0;

	echo '<pre>';
	print_r($product_variants['variants']);
	echo '</pre>';

	if ($is_variants_type && !empty($product_variants['variants'])) {
		$variants_lang = array(
			'productCode' => Lang::t('product.tip.productCode'),
			'yourPrice' => Lang::t('global.tip.yourPrice'),
			'standartPrice' => Lang::t('global.tip.standartPrice'),
			'quantity' => Lang::t('product.tip.quantity'),
			'addToCart' => Lang::t('product.btn.addToCart'),
			'selectOptionsAbove' => Lang::t('product.tip.selectOptionsAbove'),
		);
		
		// prepare selected variant
		if (!empty($vid) && isset($product_variants['variants'][$vid])) {
			$selected_variant = $product_variants['variants'][$vid];
			$product_sku = $selected_variant['variant_sku'];

			// get selected properties
			$selected_properties = $selected_variant['values'];
			$total_selected = count($selected_properties);

			// iterate each selected property value
			foreach ($selected_properties as $selected_property_id => $selected_value_id) {
				// iterate each variant
				foreach ($product_variants['variants'] as $variant_index => $variant) {
					// iterate variant values
					foreach ($variant['values'] as $property_id => $value_id) {
						// collect properties only for selected values
						if ($property_id != $selected_property_id) {
							continue;
						}

						if (!isset($active_properties[$property_id])) {
							$active_properties[$property_id] = array();
						}

						$active_properties[$property_id][$value_id] = $value_id;
					}
				}
			}
		}

		// prepare variants
		foreach ($product_variants['variants'] as $product_variant) {
			$values = $product_variant['values'];

			sort($values, SORT_NUMERIC);
			$variant_key = implode('_', $values);

			$variant_price = $product_variant['variant_price'];
			$variant_regular_price = $product_variant['variant_price'];

			$variant_photos = array();

			if (!empty($product_variant['photos'])) {
				foreach ($product_variant['photos'] as $variant_photo) {
					$variant_photo['photo_path'] = json_decode($variant_photo['photo_path'], true);
					$variant_photo['photo_size'] = json_decode($variant_photo['photo_size'], true);
					
					$variant_photos[] = $variant_photo;
				}
			}

			$variant_discount_price = 0; // Product::getDiscountPrice($variant_price, $product['category_id'], $product['brand_id']);
			$variant_price = !empty($variant_discount_price['discount']) ? $variant_discount_price['price'] : $variant_price;
			$variant_current_price = $variant_price;
			
			$variants[$variant_key] = array(
				'variant_id' => $product_variant['variant_id'],
				'sku' => $product_variant['variant_sku'],
				'current_price' => $variant_current_price,
				'price' => $variant_price,
				'regular_price' => $variant_regular_price,
				'in_stock' => $product_variant['variant_instock'],
				'price_type' => $product_variant['variant_price_type'],
				'values' => $product_variant['values'],
				'photos' => $variant_photos,
			);

			if (count($product_variant['sizes']) == 1 && $product_variant['variant_instock'] == 'out_of_stock') {
				$value_id = $product_variant['sizes'][0];
				$out_of_stock_values[$value_id] = $value_id;
			}

			if (!empty($selected_variant) && $product_variant['variant_id'] == $selected_variant['variant_id']) {
				$selected_variant['json'] = $variants[$variant_key];
				
				// set price to selected variant
				$selected_variant['price_data'] = array(
					'price' => $variant_price,
					'regular_price' => $variant_regular_price,
					'price_type' => $product_variant['variant_price_type'],
				);
			}
		}
	}

	if (!empty($product_options['options'])) {
		// prepare options lists
		foreach ($product_options['options'] as $product_option) {
			$value_id = $product_option['value_id'];
			
			$options[$value_id] = array(
				'option_id' => $product_option['option_id'],
				'price' => (float) $product_option['option_price'],
				'regular_price' => (float) $product_option['option_price'],
			);

			if (!empty($option_ids) && in_array($value_id, $option_ids)) {
				$selected_options[$value_id] = array(
					'option_id' => $product_option['option_id'],
				);
			}
		}
    }

	// setup product price and other attributes
	if (empty($selected_variant)) {
		$product_price = $product['product_price'];
		$discount_price = 0;
		$price_type = $product['product_price_type'];
	} else {		
		$variant_id = $selected_variant['variant_id'];
		$price_type = $selected_variant['price_data']['price_type'];
		
		$discount_price = 0;
		$product_price = $selected_variant['price_data']['regular_price'];
	}

	$properties = [];
	$product_subtitle = [
		'icon' => '',
		'special' => '',
	];

	foreach ($product_properties as $property) {
		if ($property['property_special']) {
			$product_subtitle['special'] = CHtml::encode($property['value_title']);
		}
		
		if ($property['property_icon']) {
			$product_subtitle['icon'] = CHtml::encode($property['value_title']);
		}
		
		if ($property['property_hide'] || $property['property_special'] || $property['property_icon']) {
			continue;
		}

		$properties[] = '<div class="product__additional-param">
			<div class="product__additional-param-title">' . CHtml::encode($property['property_title']) . '</div>
			<div class="product__additional-param-value">' . mb_strtoupper(CHtml::encode($property['value_title']), 'utf-8') . '</div>
		</div>';
	}
?>
<main id="product" class="product" data-url="<?=$this->canonicalUrl?>" data-id="<?=$product['product_id']?>" data-sku="<?=$product['product_sku']?>">
	<div class="wrap">
		<?php $this->widget('application.components.Breadcrumbs.Breadcrumbs', ['breadcrumbs' => $this->breadcrumbs]); ?>

		<section class="product__section product__section--main">
			<?php if (!empty($product_photos)) { ?>
			<div id="product-gallery" class="product__gallery">
				<div class="swiper-container">
					<div class="swiper-wrapper">
						<?php foreach ($product_photos as $photo) { ?>
						<?php
							$photo_path = json_decode($photo['photo_path'], true);
							$photo_size = json_decode($photo['photo_size'], true);
						?>
						<div class="swiper-slide">
							<picture>
								<?php /* <source type="image/webp" srcset="images/product.webp, images/product@2x.webp 2x"> */ ?>
								<?php if (!empty($photo_path['large']['2x'])) { ?>
								<source srcset="<?=$assetsUrl?>/product/<?=$product['product_id']?>/<?=$photo_path['large']['1x']?>, <?=$assetsUrl?>/product/<?=$product['product_id']?>/<?=$photo_path['large']['2x']?> 2x">
								<?php } ?>
								<img src="<?=$assetsUrl?>/product/<?=$product['product_id']?>/<?=$photo_path['large']['1x']?>" alt="<?=CHtml::encode($product['product_title'])?>">
							</picture>
						</div>
						<?php } ?>
					</div>

					<?php if (count($product_photos) > 1) { ?>
					<div class="swiper-pagination"></div>

					<div class="swiper-button-prev"></div>
					<div class="swiper-button-next"></div>
					<?php } ?>
				</div>
			</div>
			<?php } ?>

			<div class="product__info">
				<h1 class="product__title"><?=CHtml::encode($product['product_title'])?></h1>
				<?php if (!empty($product_subtitle['icon']) || !empty($product_subtitle['special'])) { ?>
				<div class="product__type"><?=implode(' / ', $product_subtitle)?></div>
				<?php } ?>
				<form action="<?=$this->createUrl('cart')?>" class="product__buy" method="post">
					<?php if ($product['product_instock'] == 'out_of_stock') { ?>
					<div class="product__total product__total--out-of-stock">
						<div class="product__price"><span data-price="<?=round($product['product_price'])?>"><?=number_format($product['product_price'], 0, '.', ' ')?></span> грн</div>
						<div class="product__add">
							Нет в наличии
						</div>
					</div>
					<?php } else { ?>
					<?php if ($product['product_price_type'] == 'variants') { ?>
					<?php if (!empty($product_variants['variants'])) { ?>
					<div id="product-variants" class="product__variants" data-variants="<?=CHtml::encode(json_encode($variants))?>" data-lang="<?=CHtml::encode(json_encode($variants_lang))?>"<?php if (!empty($selected_variant)) { ?> data-variant="<?=CHtml::encode(json_encode($selected_variant['json']))?>"<?php } ?>>
						<?php foreach ($product_variants['properties'] as $property_id => $property) { ?>
						<div id="variants-<?=$property_id?>" class="buy__option" data-property="<?=$property_id?>">
							<div class="product__group"><?=CHtml::encode($property['property_title'])?></div>
							<?php
								uasort($property['values'], function($a, $b){
									return strnatcmp($a['value_title'], $b['value_title']);
								});
							?>
							<?php foreach ($property['values'] as $value_id => $value) { ?>
							<?php
								$variant_attributes = '';

								if (!empty($selected_variant)) {
									if (!isset($active_properties[$property_id][$value_id]) || isset($out_of_stock_values[$value_id])) {
										$variant_attributes = ' class="disabled" disabled';
									} elseif (in_array($value_id, $selected_variant['values'])) {
										$variant_attributes = ' data-selected="1" checked';
									}
								}
							?>
							<?php /* <option value="<?=$value_id?>"<?=$variant_attributes?>><?=CHtml::encode($value['value_title'])?></option> */ ?>
							<label for="value-<?=$value_id?>" class="product__radio">
								<input id="value-<?=$value_id?>" type="radio"  name="variants[<?=$property_id?>]" value="<?=$value_id?>"<?=$variant_attributes?>>
								<span><?=CHtml::encode($value['value_title'])?></span>
							</label>
							<?php } ?>
						</div>
						<?php } ?>
						<?php /* <div class="buy__variants-reset<?php if (empty($selected_variant)) { ?> buy__variants-reset--hidden<?php } ?>">
							<a href="#">сбросить выбор</a>
						</div> */ ?>
					</div>
					<?php } else { ?>
					<?php /* <div class="buy__no-variants">Нет доступных вариантов</div> */ ?>
					<?php } ?>
					<?php } ?>
					<div class="product__qty">
						<div class="product__group">Кількість</div>
						<input type="text" name="cart[qty]" class="product__input" value="1">
					</div>
					<?php if (!empty($options)) { ?>
					<?php
						$options_title = '';
						$options_value_title = '';
						$options_list = [];

						foreach ($options as $value_id => $option) {
							$option_price = (float) $option['price'];
							
							foreach ($product_options['properties'] as $property) {
								foreach ($property['values'] as $value) {
									if ($value_id == $value['value_id']) {
										$option_title = CHtml::encode($value['value_title']);

										if ($option_price) {
											$option_title .= ' (+' . number_format($option_price, 0, '.', ' ') . ' грн.)';
										}

										$options_list[] = '<option value="' . $option['option_id'] . '">' . $option_title . '</option>';
										
										if (empty($options_title)) {
											$options_title = $property['property_title'];
										}
										
										if (empty($options_value_title)) {
											$options_value_title = $option_title;
										}
										
										break 2;
									}
								}
							}
						}
					?>
					<div class="product__options">
						<div class="product__group"><?=CHtml::encode($options_title)?></div>
						<div class="product__option">
							<div class="product__option-value"><?=$options_value_title?></div>
							<select name="cart[option_id][]" class="product__select">
								<?=implode("\n", $options_list)?>
							</select>
						</div>
					</div>
					<?php } ?>
					<div class="product__total">
						<div class="product__price"><span data-price="<?=round($product['product_price'])?>"><?=number_format($product['product_price'], 0, '.', ' ')?></span> грн</div>
						<div class="product__add">
							<input type="hidden" name="action" value="add">
							<input type="hidden" name="cart[product_id]" value="<?=$product['product_id']?>">
							<?php if ($is_variants_type) { ?>
							<input id="pb-variant" type="hidden" name="cart[variant_id]" value="<?=$variant_id?>">
							<?php } ?>
							
							<button class="product__btn btn" disabled>Додати у кошик</button>
						</div>
					</div>
					<?php } ?>
				</form>
				<?php if (!empty($product['product_tip'])) { ?>
				<div class="product__tip"><?=str_replace(["\r", "\n"], ["", "<br>\n"], $product['product_tip'])?></div>
				<?php } ?>
			</div>
		</section>

		<section class="product__section">
			<div class="product__subtitle">Про цей товар</div>
			
			<div class="product__content">
				<div class="product__description">
					<?=$product['product_description']?>
				</div>
				<?php if (!empty($product['product_special_title']) || !empty($properties) || !empty($product['product_tags'])) { ?>
				<div class="product__additional">
					<?php if (!empty($product['product_special_title'])) { ?>
					<div class="product__additional-head">
						<?=str_replace("\n", "<br>\n", $product['product_special_title'])?>
					</div>
					<?php } ?>
					<?php if (!empty($properties) || !empty($product['product_tags'])) { ?>
					<div class="product-card__info-params">
						<?php if (!empty($properties)) { ?>
						<?=implode("\n", $properties)?>
						<?php } ?>
						<?php if (!empty($product['product_tags'])) { $tags = explode("\n", str_replace(["\r", "#"], '', $product['product_tags'])); ?>
						<div class="product__additional-param product__additional-param--taste">
							<div class="product__additional-param-title">Смакові ноти</div>
							<div class="product__additional-param-value"><?=CHtml::encode(implode(', ', $tags))?></div>
						</div>
						<?php } ?>
					</div>
					<?php } ?>
					<?php /* if (!empty($product['product_tags'])) { $tags = explode("\n", $product['product_tags']); ?>
					<div class="product__additional-tags">
						<?php foreach ($tags as $tag) { ?>
						<span><?=CHtml::encode($tag)?></span>
						<?php } ?>
					</ul>
					<?php } */ ?>
				</div>
				<?php } ?>
			</div>
		</section>

		<section class="product__section">
			<div class="product__subtitle">Варіанти доставки</div>

			<ol class="product__delivery list-unstyled">
				<li>
					<span class="product__delivery-title">САМОВИВІЗ <small>(Безкоштовно)</small></span>
					<span class="product__delivery-tip">Самовивіз з Black Coffee Center. Здійснюється за адресою вул. Біломорська 1а. Black Сoffee Center. За умови оплати товару карткою на сайті.</span>
				</li>
				<li>
					<span class="product__delivery-title">Доставка кур'єром <small>(200 грн)</small></span>
					<span class="product__delivery-tip">Адресна доставка по м. Київ В день замовлення, або на наступний. При замовленні від 5кг доставка безкоштовна. </span>
				</li>
				<li>
					<span class="product__delivery-title">НА ВІДДІЛЕННЯ НОВОЇ ПОШТИ <small>(згідно з тарифами пошти)</small></span>
					<span class="product__delivery-tip">Для доставки по Україні за умови оплати товару карткою на сайті. Доставка за рахунок отримувача</span>
				</li>
			</ol>
		</section>

		<?php if (!empty($related_products)) { ?>
		<section class="product__section product__section--related">
			<div class="product__subtitle">Супутні товари</div>

			<div class="products-list">
				<?php $this->renderPartial('productsList', array('products' => $related_products)); ?>
			</div>
		</section>
		<?php } ?>

		<section class="questions">
            <div class="questions__title">Виникли питання?</div>
            <div class="questions__tip">
                <ul class="questions__contacts list-unstyled">
                    <li><span>Дзвоніть:</span> <a href="tel:+<?=preg_replace('#[^\d]#', '', CHtml::encode(Yii::app()->params->settings['phone']))?>"><?=CHtml::encode(Yii::app()->params->settings['phone'])?></a></li>
                    <li><span>Пишіть:</span> <a href="mailto:<?=CHtml::encode(Yii::app()->params->settings['mail'])?>"><?=CHtml::encode(Yii::app()->params->settings['mail'])?></a></li>
                    <li>З радістю підкажемо!</li>
                </ul>
            </div>
        </section>
	</div>
</main>
