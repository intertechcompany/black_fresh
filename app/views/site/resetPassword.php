<style>
    .main-div-front {
        margin-bottom: 20px;
        width: 50%;
    }
/*    .col-front {
        width: 50%;
    }*/
    #reset-email {
        width: 420px;
        height: 40px;
    }
    .title-reset-pas {
        font-size: 50px;
        line-height: 58px;
    }
    @media (max-width: 700px) {
        .title-reset-pas{
            font-size: 30px;
        }
        .main-div-front {
            width: 100%
        }
    }
    @media (max-width: 480px) {
        #reset-email {
            width: 340px;
        }
    }
   
</style>
<?php
/* @var $this SiteController */
?>
<div class="wrap">
    <div class="content-divider"></div>

    <div class="auth-form">
        <h1 class="title-reset-pas" ><?=CHtml::encode($this->pageTitle)?></h1>
        <?php if (Yii::app()->user->hasFlash('reset')) { ?>
            <div class="success-msg"><?=Yii::app()->user->getFlash('reset')?></div>
        <?php } else { ?>
            <form id="reset-form" method="post" data-type="reset" novalidate>
                <?php if (!empty($result['errorCode'])) { ?>
                    <div class="error-msg"><?=$result['errorCode']?></div>
                <?php } elseif (Yii::app()->user->hasFlash('reset_error')) { ?>
                    <div class="error-msg"><?=Yii::app()->user->getFlash('reset_error')?></div>
                <?php } ?>
                <div class="form-row clearfix main-div-front">
                    <div class="form-col form-col-100 col-front">
                        <label class="lab-rest-pas" for="reset-email"><?=Lang::t('resetPassword.label.loginOrEmail')?></label>
                        <input id="reset-email" <?php if (isset($result['errorFields']['login'])) { ?> class="error-field"<?php } ?> type="email" name="reset[login]" value="<?=CHtml::encode($model->login)?>">
                        <?php if (!empty($result['errorFields']['login'][0])) { ?><div class="error-msg"><?=implode('<br>', $result['errorFields']['login'])?></div><?php } ?>
                        <?php /* ?>
                        <a href="<?=$this->createUrl('site/login')?>"><?=Lang::t('resetPassword.link.loginToAccount')?></a>
                        <?php */ ?>
                    </div>
                </div>
                <div class="form-row form-row-btn clearfix">
                    <div class="form-col form-col-100">
                        <button class="btn"><?=Lang::t('resetPassword.btn.reset')?></button>
                    </div>
                </div>
            </form>
        <?php } ?>
    </div>
</div>