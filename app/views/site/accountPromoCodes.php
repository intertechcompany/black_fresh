<?php
/* @var $this SiteController */
$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>

<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    .table-div {
        width: 100%;
        margin-top: 20px; margin-left: 20px; margin-right: 20px; overflow-x: auto;
    }

    td, th {
        border: 1px solid black;
        padding: 10px;
        text-align: center;
    }

    tr:nth-child(even) {
        border-color: #dddddd;
    }

    tr:hover {
        cursor: pointer;
        background-color: #dddddd;
    }

    .page-style {
        display: flex;
        flex-direction: row;
    }

    .account-menu-box {
        width: 22% !important;
    }

    @media (min-width: 320px) and (max-width: 1200px) {
        .page-style {
            flex-direction: column;
        }

        caption {
            text-align: left;
        }

        .account-menu-box {
            width: 100% !important;
            text-align: center;
        }

        .table-div {
            width: 100%;
            margin-left: 0;
            margin-top: 0;
        }
    }
</style>

<!--<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">-->

<div class="wrap account-menu page-style">
    <div class="content-divider"></div>
    <?php $this->renderPartial('accountMenu'); ?>
    <div class="table-div">
        <?php if (!empty($discounts) && !empty($discounts['register'])) {?>
            <table>
                <caption><h4><?= Lang::t('layout.account.promo.register.table') ?></h4></caption>
                <tr style="pointer-events:none;">
                    <th><?= Lang::t('layout.account.promo.name') ?></th>
                    <th><?= Lang::t('layout.account.promo.value') ?></th>
                    <th><?= Lang::t('layout.account.promo.start') ?></th>
                    <th><?= Lang::t('layout.account.promo.end') ?></th>
                </tr>
                <?php foreach ($discounts['register'] as $discount) { ?>
                    <tr style="text-align: center" data-promo="<?= $discount['discount_code'] ?>">
                        <td><?= $discount['discount_code'] ?></td>
                        <td>
                            <?php if ($discount['discount_type'] == 'percentage') {
                                echo $discount['discount_value'] . '%';
                            } else {
                                echo $discount['discount_value'] . ' грн';
                            }
                            ?>
                        </td>
                        <td><?= Lang::t('layout.account.promo.date.nolimit') ?></td>
                        <td><?= Lang::t('layout.account.promo.date.nolimit') ?></td>
                    </tr>
                <?php } ?>
            </table>
            <span style="font-size: 12px;"><?= Lang::t('layout.account.promo.copy.tooltip') ?></span>
        <?php } ?>

        <?php if (!empty($discounts) && !empty($discounts['seasons'])) {?>
            <table>
                <caption><h4><?= Lang::t('layout.account.promo.season.table') ?></h4></caption>
                <tr style="pointer-events:none;">
                    <th><?= Lang::t('layout.account.promo.name') ?></th>
                    <th><?= Lang::t('layout.account.promo.value') ?></th>
                    <th><?= Lang::t('layout.account.promo.start') ?></th>
                    <th><?= Lang::t('layout.account.promo.end') ?></th>
                </tr>
                <?php foreach ($discounts['seasons'] as $discount) { ?>
                    <tr style="text-align: center" data-promo="<?= $discount['discount_code'] ?>">
                        <td><?= $discount['discount_code'] ?></td>
                        <td>
                            <?php if ($discount['discount_type'] == 'percentage') {
                                echo $discount['discount_value'] . '%';
                            } else {
                                echo $discount['discount_value'] . ' грн';
                            }
                            ?>
                        </td>
                        <?php
                            $startDate = $discount['season_date_start'];
                            $endDate = $discount['season_date_end'];
                        ?>
                        <td><?= date('d.m.Y', strtotime($startDate)) ?></td>
                        <td><?= date('d.m.Y', strtotime($endDate)) ?></td>
                    </tr>
                <?php } ?>
            </table>
            <span style="font-size: 12px;"><?= Lang::t('layout.account.promo.copy.tooltip') ?></span>
        <?php } ?>

        <?php if (!empty($discounts) && !empty($discounts['personal'])) {?>
            <table>
                <caption><h4><?= Lang::t('layout.account.promo.personal.table') ?></h4></caption>
                <tr style="pointer-events:none;">
                    <th><?= Lang::t('layout.account.promo.name') ?></th>
                    <th><?= Lang::t('layout.account.promo.value') ?></th>
                    <th><?= Lang::t('layout.account.promo.start') ?></th>
                    <th><?= Lang::t('layout.account.promo.end') ?></th>
                </tr>
                <?php foreach ($discounts['personal'] as $discount) { ?>
                    <tr style="text-align: center" data-promo="<?= $discount['discount_code'] ?>">
                        <td><?= $discount['discount_code'] ?></td>
                        <td>
                            <?php if ($discount['discount_type'] == 'percentage') {
                                echo $discount['discount_value'] . '%';
                            } else {
                                echo $discount['discount_value'] . ' грн';
                            }
                            ?>
                        </td>
                        <?php
                        $startDate = $discount['season_date_start'];
                        $endDate = $discount['season_date_end'];
                        ?>
                        <td><?= Lang::t('layout.account.promo.date.nolimit') ?></td>
                        <td><?= Lang::t('layout.account.promo.date.nolimit') ?></td>
                    </tr>
                <?php } ?>
            </table>
            <span style="font-size: 12px;"><?= Lang::t('layout.account.promo.copy.tooltip') ?></span>
        <?php } ?>
    </div>
</div>

<script>
  document.querySelectorAll('table tr')
    .forEach(e => e.addEventListener("click", function() {
      if (this.getAttribute('data-promo')) {
        const elem = document.createElement('textarea');
        elem.value = this.getAttribute('data-promo');
        document.body.appendChild(elem);
        elem.select();
        document.execCommand('copy');
        document.body.removeChild(elem);
      }
    }))
  ;

</script>