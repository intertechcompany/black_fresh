<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<div class="breadcrumbs">
	<a href="<?=Yii::app()->homeUrl?>"><?=Lang::t('layout.link.breadcrumbsHome')?></a> 
	<span class="b-sep">|</span> 
	<a href="<?=Yii::app()->createUrl('blog')?>"><?=Lang::t('blog.title.blogTitle')?></a>
	<span class="b-sep">|</span> 
	<?=CHtml::encode($blog['blog_title'])?>
</div>

<?php if (!empty($blog['blog_photo'])) { $blog_photo = json_decode($blog['blog_photo'], true); ?>
<div class="blog-img" style="background-image: url(<?=$assetsUrl . '/blog/' . $blog['blog_id'] . '/' . $blog_photo['details']['path']?>);"></div>
<?php } ?>
				
<div class="blog-article">
	<h1><?=CHtml::encode($blog['blog_title'])?></h1>
	<div class="ba-text">
		<?=$blog['blog_description']?>
	</div>
	<div class="ba-footer clearfix">
		<?php
			$date = new DateTime($blog['blog_published'], new DateTimeZone(Yii::app()->timeZone));
			$now = new DateTime('now', new DateTimeZone(Yii::app()->timeZone));

			if ($date->format('Y') < $now->format('Y')) {
				$date_format = 'd MMMM yyyy';
			} else {
				$date_format = 'd MMMM';
			}
		?>
		<div class="ba-time"><i class="icon-inline icon-clock"></i><?=Yii::app()->dateFormatter->format($date_format, $date->getTimestamp())?></div>
		<?php
			$url = $this->createAbsoluteUrl('blogitem', array('alias' => $blog['blog_alias']));

			$facebook = array(
				'u' => $url,
			);

			$twitter = array(
				'url' => $url,
				'text' => !empty($blog['post_og_title']) ? $blog['blog_og_title'] : $blog['blog_title'],
			);
		?>
		<div id="ba-share" class="ba-share">
			<ul class="list-inline">
				<li><a rel="nofollow" href="https://www.facebook.com/sharer/sharer.php?<?=http_build_query($facebook, '', '&amp;')?>"><i class="icon-inline icon-fs-facebook"></i></a></li><!--
				--><li><a rel="nofollow" href="https://twitter.com/intent/tweet?<?=http_build_query($twitter, '', '&amp;')?>"><i class="icon-inline icon-fs-twitter"></i></a></li>
			</ul>
		</div>
	</div>
</div>