<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<main class="article">
	<div class="wrap">
		<?php $this->widget('application.components.Breadcrumbs.Breadcrumbs', ['breadcrumbs' => $this->breadcrumbs]); ?>

		<h1 class="article__title"><?=CHtml::encode($base['base_title'])?></h1>
		
		<?php if (!empty($base['base_photo'])) { ?>
		<?php
			$base_image = '';
			$base_image_2x = '';
		
			if (!empty($base['base_photo'])) {
				$base_image = json_decode($base['base_photo'], true);
				$base_image_size = $base_image['details']['1x']['size'];
				$base_image = $assetsUrl . '/base/' . $base['base_id'] . '/' . $base_image['details']['1x']['path'];
				
				if (!empty($base_image['details']['2x'])) {
					$base_image_2x = $assetsUrl . '/base/' . $base['base_id'] . '/' . $base_image['details']['2x']['path'];
				}
			}
		?>
		<div class="article__img">
			<picture>
				<!-- <source type="image/webp" srcset="images/news.webp, images/news@2x.webp 2x"> -->
				<?php if (!empty($base_image_2x)) { ?>
				<source srcset="<?=$base_image?>, <?=$base_image_2x?> 2x">
				<?php } ?>
				<img src="<?=$base_image?>" alt="">
			</picture>
		</div>
		<?php } ?>

		<div class="article__content">
			<?=$base['base_description']?>
		</div>
	</div>
</main>
