<style >
    .product-card__variants {
        background: #F6F6F6;
        border: 1px solid rgba(0, 0, 0, 0.3);
        box-sizing: border-box;
        border-radius: 18px;
        max-width: 369px;
        max-height: 111px;
        width: max-content;
    }
    .product-card__radio {
        margin-right: 0;
    }
    .product-card__radio span {
        border: none;
    }
    .product-card__radio input:checked+span, .product__radio input:checked+span {
        background-color: #000;
        background-color: var(--white-color);
        color: #fff;
        color: var(--black-color);
        border: 1px solid #000;
        border-radius: 15px;
    }

    .product-tag-label {
        position: absolute;
        top: 8px;
        right: 11px;
        z-index: 2;
        background-color: black;
        border-radius: 25px;
        width: auto;
        border: 1px solid var(--black-color);
        color: white;
        height: 27px;
        text-align: center;
        padding-left: 10px;
        padding-right: 10px;
    }
</style>
<?php
/* @var $this SiteController */
$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<?php foreach ($products as $product) { ?>
    <div class="product-card">
        <?php
        $product_url = $this->createUrl('site/product', ['alias' => $product['product_alias']]);

        if (!empty($product['product_photo'])) {
            $product_image = json_decode($product['product_photo'], true);
            $product_image_size_2x = $product_image['size']['catalog']['2x'];
            $product_image_2x = $assetsUrl . '/product/' . $product['product_id'] . '/' . $product_image['path']['catalog']['2x'];
            $product_image_size_1x = $product_image['size']['catalog']['1x'];
            $product_image_1x = $assetsUrl . '/product/' . $product['product_id'] . '/' . $product_image['path']['catalog']['1x'];
        } else {
            $product_image_2x = '';
            $product_image_1x = '';
        }

        $product_price = (float) $product['product_price'];
        $product_price_old = (float) $product['product_price_old'];
        // $discount_price = Product::getDiscountPrice($product_price, $product['category_id'], $product['brand_id']);

        $product_special = '';
        $product_icon = '';
        ?>
        <div class="product-card__img">
            <a href="<?=$product_url?>">
                <?php if (!empty($product_image_1x)) { ?>
                    <picture>
                        <?php /* <source type="image/webp" srcset="images/p1.webp, images/p1@2x.webp 2x"> */ ?>
                        <source srcset="<?=$product_image_1x?>, <?=$product_image_2x?> 2x">
                        <img src="<?=$product_image_1x?>" alt="<?=CHtml::encode($product['product_title'])?>">
                        <?php
                            $isDiscount = false;

                            if (isset($product['product_price']) && $product['product_price_old'] != '0.00') {
                                $value = ((float) $product['product_price'] / (float) $product['product_price_old']) * 100;
                                $discount = 100 - round($value);

                                $isDiscount = true;
                            }
                        ?>
<!--                        <div id="sale-tag---><?php //= $product['product_id'] ?><!--" --><?php //if (!$isDiscount) { ?><!-- style="display: none;" --><?php //} ?><!-- class="product-tag-label">-->
<!--                            --><?php
//                                if ($isDiscount) {
//                                    echo Lang::t('layout.see.product.discount.sale');
//                                }
//                            ?>
<!--                        </div>-->
                    </picture>
                <?php } ?>
            </a>
            <?php
            $properties = [];

            if (isset($product['props']) && !empty($product['props'])) {
                foreach ($product['props'] as $property) {
                    if ($property['property_special']) {
                        $product_special = CHtml::encode($property['value_title']);
                    }

                    if ($property['property_icon']) {
                        $product_icon = $property['value_icon'];
                    }

                    if ($property['property_hide'] || $property['property_special'] || $property['property_icon']) {
                        continue;
                    }

                    if (!$property['is_delicious']) {
                        $properties[] = '<div class="product-card__info-param">
                                            <div class="product-card__info-param-title">' . CHtml::encode($property['property_title']) . '</div>
                                            <div class="product-card__info-param-value">' . mb_strtoupper(CHtml::encode($property['value_title']), 'utf-8') . '</div>
                                        </div>';
                    }
                }
            }
            ?>
            <?php if ($product['product_extended']) { ?>
                <div class="product-card__info">
                    <div class="product-card__info-head">
                        <?php if (!empty($product['product_special_title'])) { ?>
                            <div class="product-card__info-title">
                                <?=str_replace("\n", "<br>\n", $product['product_special_title'])?>
                            </div>
                        <?php } ?>
                        <?php if (!empty($product['product_special_tip'])) { ?>
                            <div class="product-card__info-tip">
                                <?=str_replace("\n", "<br>\n", $product['product_special_tip'])?>
                            </div>
                        <?php } ?>
                    </div>
                    <?php if (isset($product['props']) && !empty($product['props']) || !empty($product['product_tags'])) { ?>
                        <div class="product-card__info-params">
                            <?php if (isset($product['props']) && !empty($product['props'])) { ?>
                                <?=implode("\n", $properties)?>
                            <?php } ?>
                            <?php if (!empty($product['product_tags'])) { $tags = explode("\n", str_replace(["\r", "#"], '', $product['product_tags'])); ?>
                                <div class="product-card__info-param product-card__info-param--taste">
                                    <div class="product-card__info-param-title"><?=Lang::t('catalog.tip.taste')?></div>
                                    <div class="product-card__info-param-value"><?=CHtml::encode(implode(', ', $tags))?></div>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <div class="product-card__info-bottom">
                        <?php
                        $exist = false;
                        if (isset($product['props']) && !empty($product['props'])) {
                        foreach ($product['props'] as $property) {
                            if ($property['is_delicious']) {

                        ?>
                                <?php if (!$exist) { ?>
                                    <div class="product-card__info-param">
                                        <div class="product-card__info-param-title"><?=Lang::t('product.info.delicious')?></div>
                                    </div>
                                <?php } ?>
                                <div class="product-card__info-roasted">
                                    <img src="<?= $property['delicious_icon'] ?>" alt="">
                                </div>
                            <?php } ?>
                        <?php $exist = true; } } ?>
                    </div>
                </div>
            <?php } ?>
            <?php if ($this->route != 'site/index' && !empty($product_special)) { ?>
                <div class="product-card__type"><?=mb_strtolower(CHtml::encode($product_special), 'utf-8')?></div>
            <?php } ?>
        </div>
        <?php if ($this->route == 'site/index' && !empty($product_special)) { ?>
            <div class="product-card__category"><?=CHtml::encode($product_special)?></div>
        <?php } ?>
        <div class="product-card__title"><a href="<?=$product_url?>"><?=CHtml::encode($product['product_title'])?></a></div>
        <?php if (Yii::app()->params->settings['quick_buy']) { ?>
            <form action="<?=$this->createUrl('site/cart')?>" class="product-card__buy" method="post">
                <?php if ($product['product_instock'] == 'out_of_stock') { ?>
                    <?php if (!empty($product_price_old)) { ?><s style="color: #0098ff;color: var(--blue-color);"><?=number_format($product_price_old, 0, '.', ' ')?></s><?php } ?>
                    <?=number_format($product_price, 0, '.', ' ')?> <small><?=Lang::t('layout.tip.uah')?></small>
                    / <?=Lang::t('catalog.tip.outOfStock')?>
                <?php } else { ?>
                    <?php if ($product['product_price_type'] == 'variants') { ?>
                        <?php if (!empty($product['variants'])) { $in_stock = false; ?>
                            <div class="product-card__variants">
                                <?php foreach ($product['variants'] as $variant) { ?>
                                    <?php
                                    $variant_id = $variant['variant_id'];
                                    $values = array_values($variant['values']);

                                    $value_id = !empty($values) ? $values[0] : 0;
                                    $value_title = '';

                                    foreach ($product['properties'] as $property) {
                                        foreach ($property['values'] as $value_index => $value) {
                                            if ($value_index == $value_id) {
                                                $value_title = $value['value_title'];
                                            }
                                        }
                                    }

                                    $variant_in_stock = ($variant['variant_instock'] == 'in_stock') ? true : false;
                                    $variant['variant_price_old'] = (float) $variant['variant_price_old'];

                                    $isDiscount = false;
                                    $discount = 0;

                                    if (isset($variant['variant_price_old']) && $variant['variant_price_old'] != '0.00') {
                                        $value = ((float) $variant['variant_price'] / (float) $variant['variant_price_old']) * 100;
                                        $discount = 100 - round($value);

                                        $isDiscount = true;
                                    }
                                    ?>
                                    <label for="value-<?=$variant_id?>" class="product-card__radio<?php if (!$variant_in_stock) { ?> product-card__radio--disabled<?php } ?>">
                                        <input id="value-<?=$variant_id?>" data-sale-tag="sale-tag-<?= $product['product_id'] ?>" data-is-sale="<?= $isDiscount ?>"
                                               <?php if ($isDiscount) { ?> data-discount="<?php echo Lang::t('layout.see.product.discount.sale') . ' - ' . round($discount) . '% '; ?>" <?php } ?>
                                               type="radio" name="cart[variant_id]"
                                               value="<?=$variant_id?>"<?php if ($variant_in_stock && !$in_stock) { $in_stock = true; ?> checked<?php } elseif (!$variant_in_stock) { ?> disabled<?php } ?>
                                        >
                                        <span>
					<?=preg_replace('#([a-zа-я]+)#ui', '<small>$1</small>', CHtml::encode($value_title))?> /
					<?php if (!empty($variant['variant_price_old'])) { ?><s style="color: #0098ff;color: var(--blue-color);"><?=number_format($variant['variant_price_old'], 0, '.', ' ')?></s><?php } ?>
                                            <?=number_format($variant['variant_price'], 0, '.', ' ')?> <small><?=Lang::t('layout.tip.uah')?></small>
				</span>
                                    </label>
                                <?php } ?>
                            </div>
                            <?php if ($in_stock) { ?>
                                <a class="product-card__btn" href="<?php echo $this->createUrl('site/product', ['alias' => $product['product_alias']]);?>"><?=Lang::t('catalog.btn.buy')?></a>
                            <?php } ?>
                        <?php } ?>
                    <?php } else { ?>
                        <?php if (!empty($product_price_old)) { ?><s style="color: #0098ff;color: var(--blue-color);"><?=number_format($product_price_old, 0, '.', ' ')?></s><?php } ?>
                        <?=number_format($product_price, 0, '.', ' ')?> <small><?=Lang::t('layout.tip.uah')?></small> / <button class="product-card__btn"><?=Lang::t('catalog.btn.buy')?></button>
                    <?php } ?>
                <?php } ?>

                <input type="hidden" name="action" value="add">
                <input type="hidden" name="cart[product_id]" value="<?=$product['product_id']?>">
                <input type="hidden" name="cart[qty]" value="1">
            </form>
        <?php } ?>
    </div>


    <?php continue; ?>
    <div class="catalog__product product-card<?php if (!empty($is_slider)) { ?> swiper-slide<?php } ?>">
        <?php
        $product_url = $this->createUrl('site/product', array('alias' => $product['product_alias']));

        if (!empty($product['product_photo'])) {
            $product_image = json_decode($product['product_photo'], true);
            $product_image_size = $product_image['size']['catalog']['2x'];
            $product_image = $assetsUrl . '/product/' . $product['product_id'] . '/' . $product_image['path']['catalog']['2x'];
        } else {
            $product_image = '';
        }

        $product_price = (float) $product['product_price'];
        $discount_price = 0;
        // $discount_price = Product::getDiscountPrice($product_price, $product['category_id'], $product['brand_id']);
        ?>
        <div class="product-card__image">
            <?php if (!empty($product_image)) { ?>
                <a href="<?=$product_url?>"><img src="<?=$product_image?>" width="<?=$product_image_size['w']?>" height="<?=$product_image_size['h']?>" alt="<?=CHtml::encode($product['product_title'])?>"></a>
            <?php } else { ?>
                <a href="<?=$product_url?>"></a>
            <?php } ?>
            <?php if (Yii::app()->params->settings['quick_buy']) { ?>
                <form action="<?=$this->createUrl('site/cart')?>" class="product-card__quick-buy quick-buy" method="post">
                    <input type="hidden" name="action" value="add">
                    <input type="hidden" name="cart[product_id]" value="<?=$product['product_id']?>">
                    <input type="hidden" name="cart[variant_id]" value="0">
                    <input type="hidden" name="cart[qty]" value="1">

                    <?php if ($product['product_instock'] == 'out_of_stock') { ?>
                        <div class="quick-buy__not-available">Нет в наличии</div>
                    <?php } else { ?>
                        <?php if ($product['product_price_type'] == 'variants' && empty($product['sizes'])) { ?>
                            <div class="quick-buy__not-available">Нет доступных размеров</div>
                        <?php } else { ?>
                            <div class="quick-buy__btn-wrap<?php if ($product['product_price_type'] == 'variants') { ?> quick-buy__btn-wrap--sizes<?php } ?>">
                                <a href="#" class="quick-buy__btn">Добавить в корзину</a>
                            </div>
                            <?php if ($product['product_price_type'] == 'variants') { ?>
                                <div class="quick-buy__sizes">
                                    <div class="quick-buy__sizes-title">Выберите размер:</div>
                                    <div class="quick-buy__sizes-list">
                                        <?php foreach ($product['sizes'] as $size) { ?>
                                            <a href="#" class="quick-buy__size<?php if ($size['variant_instock'] == 'out_of_stock') { ?> quick-buy__size--disabled<?php } ?>" data-vid="<?=$size['variant_id']?>"><?=CHtml::encode($size['value_title'])?></a>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                </form>
            <?php } ?>
        </div>
        <div class="product-card__details">
            <div class="product-card__title"><a href="<?=$product_url?>"><?=CHtml::encode($product['product_title'])?></a></div>
            <?php if (!empty($product['brand_name'])) { ?>
                <div class="product-card__brand"><?=CHtml::encode($product['brand_name'])?></div>
            <?php } ?>
            <div class="product-card__price">
                <?php if ($discount_price['discount']) { ?>
                    <?=str_replace('€', '<span>€</span>', Currency::format($discount_price['price']))?>
                    <s style="color: #0098ff;color: var(--blue-color);"><?=str_replace('€', '<span>€</span>', Currency::format($product_price))?></s>
                <?php } else { ?>
                    <?=str_replace('€', '<span>€</span>', Currency::format($product_price))?>
                <?php } ?>
            </div>
        </div>
        <?php /* <div class="pc-bottom">
		<div class="pc-price">
			<?php if (!empty($discount_price['discount'])) { ?>
			<span class="pcp-standart"><i class="pcp-tip"><?=Lang::t('global.tip.standartPrice')?></i> <span><?=$product_price_prefix?><?=number_format($product_price, 2, '.', '')?>€</span><?=$product_price_suffix?></span>
			<span class="pcp-actual"><i class="pcp-tip"><?=Lang::t('global.tip.yourPrice')?></i> <span><?=$product_price_prefix?><?=number_format($discount_price['price'], 2, '.', '')?>€</span><?=$product_price_suffix?></span>
			<?php } else { ?>
			<span class="pcp-actual pcp-100"><span><?=number_format($product_price, 2, '.', '')?>€</span><?=$product_price_suffix?></span>
			<?php } ?>
		</div>
		<?php if ($product['product_price_type'] == 'variants') { ?>
		<div class="pc-add">
			<a href="<?=$product_url?>" class="btn"><?=Lang::t('product.btn.selectOption')?></a>
		</div>
		<?php } else { ?>
		<form action="<?=$this->createUrl('cart')?>" method="post">
			<?php if ($product['product_price_type'] == 'package' || $is_per_meter_pack) { ?>
			<div class="pc-calc">
				<div class="pcc-group">
					<input type="text" value="<?=$product['product_pack_size']?>" autocomplete="off">
					<a href="#" class="pcc-btn pcc-btn-up"></a>
					<a href="#" class="pcc-btn pcc-btn-down"></a>
				</div>
				<span>m<?php if ($product['product_price_type'] == 'package') { ?><sup>2</sup><?php } ?> =</span>
				<div class="pcc-group">
					<input type="text" name="cart[qty]" value="1" autocomplete="off">
					<a href="#" class="pcc-btn pcc-btn-up"></a>
					<a href="#" class="pcc-btn pcc-btn-down"></a>
				</div>
				<span><?=Lang::t('checkout.tip.pcs')?></span>
			</div>
			<?php } ?>
			<div class="pc-add">
				<input type="hidden" name="action" value="add">
				<?php if ($product['product_price_type'] == 'piece' || $product['product_price_type'] == 'item' || ($product['product_price_type'] == 'per_meter' && !$is_per_meter_pack)) { ?>
				<input type="hidden" name="cart[qty]" value="1">
				<?php } ?>
				<input type="hidden" name="cart[product_id]" value="<?=$product['product_id']?>">
				<button class="btn"><i class="icon-inline icon-responsive icon-cart"></i><?=Lang::t('product.btn.addToCart')?></button>
			</div>
		</form>
		<?php } ?>
	</div> */ ?>
    </div>
<?php } ?>

<script>
  window.addEventListener("load", function() {
    $('input[type=radio]').click(function() {
      const saleTag = document.getElementById(this.getAttribute('data-sale-tag'));

      if(this.getAttribute('data-is-sale') === '1') {
        saleTag.textContent = this.getAttribute('data-discount');
        saleTag.style.display = 'block';
      } else {
        saleTag.style.display = 'none';
      }
    });
  });
</script>
