<div class="course__thank-you-title">Поздравляем! <br>Ты зарегистрирован.</div>
<div class="course__thank-you-tip">Поделись этим событием со своими друзьями, чтобы они тоже узнали больше про кофе :)</div>
<?php if (!empty(Yii::app()->params->settings['facebook'])) { ?>
<div class="course__thank-you-facebook">
    <a href="<?=CHtml::encode(Yii::app()->params->settings['facebook'])?>" target="_blank"><i class="icon-inline icon-facebook"></i>Мы в Facebook</a>
</div>
<?php } ?>