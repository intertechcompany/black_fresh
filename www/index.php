<?php
define('DS', DIRECTORY_SEPARATOR);

if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] == 'black.loc') {
	error_reporting(E_ALL);
	
	defined('YII_DEBUG') or define('YII_DEBUG',true);
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',5);

	$yii = realpath(__DIR__ . DS . '..' . DS . 'framework' . DS . 'yiilite.php');
	require_once $yii;
	
	Yii::setPathOfAlias('root', __DIR__);

	$config = require_once __DIR__ . DS . '..' . DS . 'app' . DS . 'config' . DS . 'config-local.php';


} elseif (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] == 'fresh.black') {
	$yii = realpath(__DIR__ . DS . '..' . DS . 'framework' . DS . 'yiilite.php');
	require_once $yii;
	
	Yii::setPathOfAlias('root', __DIR__);

	$config = require_once __DIR__ . DS . '..' . DS . 'app' . DS . 'config' . DS . 'config.php';
} else {
	$yii = realpath(__DIR__ . DS . '..' . DS . 'framework' . DS . 'yiilite.php');
	require_once $yii;
	
	Yii::setPathOfAlias('root', __DIR__);

	$config = require_once __DIR__ . DS . '..' . DS . 'app' . DS . 'config' . DS . 'config-new.php';
}

// Yii::$enableIncludePath = false; // ��������� PHP include path
Yii::createWebApplication($config)->run();

$yii = realpath(__DIR__ . DS . '..' . DS . 'vendor' . DS . 'autoload.php');
	require_once $yii;