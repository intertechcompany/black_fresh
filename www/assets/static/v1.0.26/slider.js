// .firefox
var isFF = true;
var addRule = (function (style) {
    var sheet = document.head.appendChild(style).sheet;
    return function (selector, css) {
        if ( isFF ) {
            if ( sheet.cssRules.length > 0 ) {
                sheet.deleteRule( 0 );
            }

            try {
                sheet.insertRule(selector + "{" + css + "}", 0);
            } catch ( ex ) {
                isFF = false;
            }
        }
    };
})(document.createElement("style"));

addRule( '.firefox input::-moz-range-track', 'background: linear-gradient(to right, green, #005555 75%, blue)' );

$( '.firefox input' ).on( 'input', function( ) {
    addRule( '.firefox input::-moz-range-track', 'background: linear-gradient(to right, green, #005555 ' + this.value + '%, blue)' );
} );

// .chrome
$( '.chrome input' ).on( 'input', function( ) {
    $( this ).css( 'background', 'linear-gradient(to right, green, #005555 ' + this.value + '%, blue)' );
} );

// .all
if ( 'webkitRequestAnimationFrame' in window ) {
    $( '.all input' ).addClass( 'webkit-track' );
}

addRule( '.all input::-moz-range-track', 'background: linear-gradient(to right, green, #005555 75%, blue)' );

$( '.all input' ).on( 'input', function( ) {
    addRule( '.all input::-moz-range-track', 'background: linear-gradient(to right, green, #005555 ' + this.value + '%, blue)' );

    if ( $( this ).hasClass( 'webkit-track' ) ) {
        $( this ).css( 'background', 'linear-gradient(to right, green, #005555 ' + this.value + '%, blue)' );
    }
} );

$( '.all input' ).trigger( 'input' );


// .merged
if ( 'webkitRequestAnimationFrame' in window ) {
    $( '.merged input' ).addClass( 'webkit-track' );
}

$( '.merged input' ).on( 'input', function( ) {
    addRule( '.merged input::-moz-range-track', 'background: linear-gradient(to right, green, #005555 ' + this.value + '%, blue)' );

    if ( $( this ).hasClass( 'webkit-track' ) ) {
        $( this ).css( 'background', 'linear-gradient(to right, green, #005555 ' + this.value + '%, blue)' );
    }
} );

$( '.merged input' ).trigger( 'input' );